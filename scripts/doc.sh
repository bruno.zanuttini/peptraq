#!/bin/sh

cd $(dirname "$0")/.. &&
    file_list=$(mktemp)
    find aasuite/src -name "*.java" >> "$file_list" &&
    find peptraq/src -name "*.java" >> "$file_list" &&
    mkdir -p doc/ &&
    javadoc -source 1.8 -Xdoclint:all -private -quiet -cp "lib/*" -d doc/ @"$file_list"
