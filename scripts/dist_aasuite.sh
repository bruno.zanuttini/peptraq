#!/bin/sh

current_dir=$(pwd)
cd $(dirname "$0")/.. &&
    sh scripts/compile_aasuite.sh &&
    mkdir -p dist/ &&
    cd build/ &&
    jar cf ../dist/aasuite.jar . &&
    cd "${current_dir}"
    echo "Library written to '"$(realpath $(dirname "$0")/../dist/"aasuite.jar")"'"
