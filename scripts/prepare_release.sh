#!/bin/sh

[ $# = 1 ] || { echo "Usage:" $0 "version" ; exit 1 ; }
version="$1"

current_dir=$(pwd)

# Checking version
cd $(dirname "$0")/../peptraq &&
    code_version=$(rgrep "public static final String VERSION" | cut -d '=' -f 2 | tr -d " \";") &&
    [ "${code_version}" = "${version}" ] || { echo "Version in code does not match argument:" $code_version "vs" $version ; exit 2 ; }

# Preparing jar
output="peptraq-${version}.jar"
cd ${current_dir} &&
    sh $(dirname "$0")/dist.sh  > /dev/null &&
    mv peptraq.jar "${output}" &&
    echo "Executable jar written to ${output}" &&
    echo "Run..." &&
    echo "git status" &&
    echo "git tag -a -m \"Version ${version} of PepTraq Desktop\" peptraq${version}" &&
    echo "... to finalize"

