#!/bin/sh

cd $(dirname "$0")/.. &&
    file_list=$(mktemp)
    find aasuite/src -name "*.java" >> "$file_list"
    mkdir -p "build" &&
    javac -bootclasspath lib/rt.jar -source 1.8 -target 1.8 -cp "lib/*" -d "build/" @"$file_list" &&
    cp -r aasuite/src/main/resources/* build
