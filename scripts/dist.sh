#!/bin/sh

run_time_deps="biojava-aa-prop.jar biojava-core.jar jaxb-api.jar jspp.jar slf4j-api.jar slf4j-nop.jar"

current_dir=$(pwd)
cd $(dirname "$0")/.. &&
    sh scripts/compile.sh &&
    mkdir -p dist/ &&
    cd dist/ &&
    cp -r ../build/* . &&
    for f in $run_time_deps ; do
	jar xf ../lib/$f
    done &&
    jar cfe "${current_dir}"/peptraq.jar fr.unicaen.aasuite.main.GUI . &&
    echo "Executable jar written to peptraq.jar"
