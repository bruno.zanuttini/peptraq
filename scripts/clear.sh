#!/bin/sh

peptraq_dir=$(dirname "$0")/..

rm -rf "$peptraq_dir"/build
rm -rf "$peptraq_dir"/dist
rm -rf "$peptraq_dir"/doc
