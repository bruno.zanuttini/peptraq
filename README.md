# PepTraq

PepTraq is a desktop application enabling biologists to explore genomes, transcriptomes, and protein banks easily, by applying filters and transformations, saving searches at any time in fasta format, etc. This repository hosts the source code.

# Authors

PepTraq is developed at the Université de Caen Normandie, France, in laboratories GREYC (Normandie Université; UNICAEN, CNRS UMR 6072, ENSICAEN) and BOREA (MNHN, CNRS 8067, SU,
 IRD 207, UCN, UA). It was also supported by a grant by Région Normandie (Projet Émergent n°16E00790/16P03508, 2017-2019).

The authors are:

  * Bruno Zanuttini (GREYC, Université de Caen Normandie),
  * Céline Zatylny-Gaudin (BOREA, Université de Caen Normandie),
  * Joël Henry (BOREA, Université de Caen Normandie),
  * Christophe Couronne (GREYC, CNRS),
  * Abdelkader Ouali (GREYC, Université de Caen Normandie).

LICENSE

PepTraq is distributed open source under the CeCILL-B license. It embeds other libraries (see lib/README.txt).

REPOSITORY

This repository is organized as follows:

  * aasuite is standalone, and provides classes which are useful independently of the desktop application (in particular, they are used in the web version of PepTraq)
  * peptraq is the desktop application, which uses aasuite
  * scripts gives scripts for compiling, packaging, and documenting either aasuite standlone, or the whole of PepTraq desktop

Prior to compiling, the dependencies specified in lib/README.txt must be added to the lib/ folder.
