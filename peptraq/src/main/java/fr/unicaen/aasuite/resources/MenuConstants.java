package fr.unicaen.aasuite.resources;

import java.awt.event.KeyEvent;

import fr.unicaen.aasuite.gui.AcceleratorHandler;

/**
 * An interface grouping together the labels, mnemonics, and accelerators for
 * menus and menu items for PepTraq Desktop.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings, mnemonics, etc.
 */
public interface MenuConstants {

	/** The label for the file menu. */
	static final String FILE_MENU = "File";

	/** The mnemonic for the file menu. */
	static final int FILE_MENU_MNEMONIC = KeyEvent.VK_F;

	/** The label for the session menu. */
	static final String SELECTION_MENU = "Selection";

	/** The mnemonic for the session menu. */
	static final int SELECTION_MENU_MNEMONIC = KeyEvent.VK_S;

	/** The label for the macro menu. */
	static final String OPERATION_MENU = "Operations";

	/** The mnemonic for the macro menu. */
	static final int OPERATION_MENU_MNEMONIC = KeyEvent.VK_O;

	/** The label for the display menu. */
	static final String DISPLAY_MENU = "View";

	/** The mnemonic for the display menu. */
	static final int DISPLAY_MENU_MNEMONIC = KeyEvent.VK_V;

	/** The label for the constraint menu. */
	final static String CONSTRAINT_MENU = "Filter";

	/** The mnemonic for the constraint menu. */
	static final int CONSTRAINT_MENU_MNEMONIC = KeyEvent.VK_I;

	/** The label for the transform menu. */
	final static String TRANSFORM_MENU = "Transform";

	/** The mnemonic for the transform menu. */
	static final int TRANSFORM_MENU_MNEMONIC = KeyEvent.VK_T;

	/** The label for the conversion menu. */
	final static String CONVERSION_MENU = "Convert";

	/** The mnemonic for the conversion menu. */
	static final int CONVERSION_MENU_MNEMONIC = KeyEvent.VK_C;

	/** The label for the help menu. */
	final static String HELP_MENU = "Help";

	/** The mnemonic for the help menu. */
	static final int HELP_MENU_MNEMONIC = KeyEvent.VK_H;

	/** The label for the loading DNA menu item. */
	static final String LOAD_DNA = "Load DNA sequences";

	/** The accelerators for the loading DNA menu item. */
	static final AcceleratorHandler.Keys LOAD_DNA_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_D, 'd');

	/** The label for the loading proteins menu item. */
	static final String LOAD_PROTEINS = "Load protein sequences";

	/** The accelerators for the loading proteins menu item. */
	static final AcceleratorHandler.Keys LOAD_PROTEINS_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_P, 'p');

	/** The label for the saving all sequences menu item. */
	static final String SAVE = "Save all sequences";

	/** The accelerators for the saving all sequences menu item. */
	static final AcceleratorHandler.Keys SAVE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_S, 's');

	/** The label for the saving selected sequences menu item. */
	static final String SAVE_SELECTED = "Save selected sequences";

	/** The accelerators for the saving selected sequences menu item. */
	static final AcceleratorHandler.Keys SAVE_SELECTED_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_E, 'e');

	/** The label for the closing session menu item. */
	static final String CLOSE = "Close";

	/** The accelerators for the closing session menu item. */
	static final AcceleratorHandler.Keys CLOSE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_W, 'w');

	/** The label for the designing macro menu item. */
	static final String DESIGN_MACRO = "Design new macro";

	/** The accelerators for the designing macro menu item. */
	static final AcceleratorHandler.Keys DESIGN_MACRO_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_M, 'm');

	/** The label for the applying macro menu item. */
	static final String APPLY_MACRO = "Apply macro";

	/** The label for the exiting menu item. */
	static final String EXIT = "Exit";

	/** The accelerators for the exiting menu item. */
	static final AcceleratorHandler.Keys EXIT_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_X, 'x');

	/** The label for the selecting all sequences menu item. */
	static final String SELECT_ALL = "Select all sequences";

	/** The accelerators for the selecting all sequences menu item. */
	static final AcceleratorHandler.Keys SELECT_ALL_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_A, 'a');

	/** The label for the unselecting all sequences menu item. */
	static final String UNSELECT_ALL = "Unselect all sequences";

	/** The accelerators for the unselecting all sequences menu item. */
	static final AcceleratorHandler.Keys UNSELECT_ALL_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_U, 'u');

	/** The label for the discarding unselected sequences menu item. */
	static final String DISCARD_UNSELECTED = "Discard unselected sequences";

	/** The label for the undoing last operation menu item. */
	static final String BACK = "Undo last operation";

	/** The accelerators for the undoing last operation menu item. */
	static final AcceleratorHandler.Keys BACK_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_Z, 'z');

	/** The label for the display history menu item. */
	static final String DISPLAY_HISTORY = "Toggle history display";

	/** The label for the number of sequences per page menu item. */
	static final String SEQUENCES_PER_PAGE = "Pagination";

	/** The accelerators for the number of sequences per page menu item. */
	static final AcceleratorHandler.Keys SEQUENCES_PER_PAGE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_G, 'g');

	/** The label for the setting font size menu item. */
	static final String SET_FONT_SIZE = "Font size";

	/** The accelerators for the setting font size menu item. */
	static final AcceleratorHandler.Keys SET_FONT_SIZE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_F, 'f');

	/** The label for the larger font menu item. */
	static final String LARGER_FONT = "Larger font";

	/** The accelerators for the larger font menu item. */
	static final AcceleratorHandler.Keys LARGER_FONT_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_PLUS, '+');

	/** The label for the smaller font menu item. */
	static final String SMALLER_FONT = "Smaller font";

	/** The accelerators for the smaller font menu item. */
	static final AcceleratorHandler.Keys SMALLER_FONT_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_MINUS, '-');

	/** The label for the setting highlighting style menu item. */
	static final String SET_HIGHLIGHTING_STYLE = "Highlighting style";

	/** The accelerators for the setting highlighting style menu item. */
	static final AcceleratorHandler.Keys SET_HIGHLIGHTING_STYLE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_H, 'h');

	/** The label for the about menu item. */
	static final String ABOUT = "About";

	/** The accelerators for the about menu item. */
	static final AcceleratorHandler.Keys ABOUT_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_B, 'b');

	/** The label for the CeCILL license menu item. */
	static final String CECILL_LICENSE = "CeCILL-B License";

	/** The accelerators for the CeCILL license menu item. */
	static final AcceleratorHandler.Keys CECILL_LICENSE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_C, 'c');

	/** The label for the LGPL license menu item. */
	static final String LGPL_LICENSE = "LGPLv2.1 License";

	/** The accelerators for the LGPL license menu item. */
	static final AcceleratorHandler.Keys LGPL_LICENSE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_L, 'l');

	/** The label for the MIT license menu item. */
	static final String MIT_LICENSE = "MIT License";

	/** The accelerators for the MIT license menu item. */
	static final AcceleratorHandler.Keys MIT_LICENSE_KEYS = new AcceleratorHandler.Keys(KeyEvent.VK_M, 'm');

}
