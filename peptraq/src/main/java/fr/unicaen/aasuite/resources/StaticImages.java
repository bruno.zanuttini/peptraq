package fr.unicaen.aasuite.resources;

/**
 * A class for handling the static image resources specific to PepTraq Desktop.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class StaticImages {

	/** The resource path for the logo of the GREYC. */
	public static final String GREYC_LOGO_PATH = "/resources/logo-MAD-horizontal.png";

	/** The resource path for the logo of BOREA. */
	public static final String BOREA_LOGO_PATH = "/resources/Logo_BOREA_final_blanc.png";

	/** The resource path for the logo of Unicaen. */
	public static final String UNICAEN_LOGO_PATH = "/resources/LOGO-UNICAEN_V-2.1-N.png";

}
