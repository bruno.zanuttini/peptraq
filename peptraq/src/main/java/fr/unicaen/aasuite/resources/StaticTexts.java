package fr.unicaen.aasuite.resources;

/**
 * A class for handling the static text resources specific to PepTraq Desktop.
 * The resources are intended to be set and accessed statically; if they are
 * accessed while not set, exceptions are thrown (in particular, there is no
 * default value).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class StaticTexts {

	/**
	 * The text for the "about" frame. This text may use 1$ to refer to the version
	 * of PepTraq (String).
	 */
	private static String aboutText;

	/** The text of the Cecill-B license. */
	private static String cecillLicenseText;

	/** The text of the LGPLv2.1 license. */
	private static String lgplLicenseText;

	/** The text of the MIT license. */
	private static String mitLicenseText;

	/**
	 * Sets the "about" text.
	 * 
	 * @param text The text
	 * @throws IllegalStateException if the "about" text is already set
	 */
	public static void setAboutText(String text) throws IllegalStateException {
		if (aboutText != null) {
			throw new IllegalStateException("The \"about\" text has already been set");
		}
		;
		aboutText = text;
	}

	/**
	 * Sets the text of the CeCILL license.
	 * 
	 * @param text The text
	 * @throws IllegalStateException if the text of the license is already set
	 */
	public static void setCecillLicenseText(String text) throws IllegalStateException {
		if (cecillLicenseText != null) {
			throw new IllegalStateException("The text of the CeCILL license has already been set");
		}
		;
		cecillLicenseText = text;
	}

	/**
	 * Sets the text of the LGPL license.
	 * 
	 * @param text The text
	 * @throws IllegalStateException if the text of the license is already set
	 */
	public static void setLGPLLicenseText(String text) throws IllegalStateException {
		if (lgplLicenseText != null) {
			throw new IllegalStateException("The text of the LGPL license has already been set");
		}
		;
		lgplLicenseText = text;
	}

	/**
	 * Sets the text of the MIT license.
	 * 
	 * @param text The text
	 * @throws IllegalStateException if the text of the license is already set
	 */
	public static void setMITLicenseText(String text) throws IllegalStateException {
		if (mitLicenseText != null) {
			throw new IllegalStateException("The text of the MIT license has already been set");
		}
		;
		mitLicenseText = text;
	}

	/**
	 * Returns the "about" text.
	 * 
	 * @param version The version of PepTraq
	 * @return The "about" text
	 * @throws IllegalStateException if the "about" text is not set
	 */
	public static String getAboutText(String version) throws IllegalStateException {
		if (aboutText == null) {
			throw new IllegalStateException("No \"about\" text has been set");
		}
		return String.format(aboutText, version);
	}

	/**
	 * Returns the text of the CeCILL license.
	 * 
	 * @return The text of the CeCILL license
	 * @throws IllegalStateException if the text of the license is not set
	 */
	public static String getCecillLicenseText() throws IllegalStateException {
		if (cecillLicenseText == null) {
			throw new IllegalStateException("The text of the CeCILL license has not been set");
		}
		return cecillLicenseText;
	}

	/**
	 * Returns the text of the LGPL license.
	 * 
	 * @return The text of the LGPL license
	 * @throws IllegalStateException if the text of the license is not set
	 */
	public static String getLGPLLicenseText() throws IllegalStateException {
		if (lgplLicenseText == null) {
			throw new IllegalStateException("The text of the LGPL license has not been set");
		}
		return lgplLicenseText;
	}

	/**
	 * Returns the text of the MIT license.
	 * 
	 * @return The text of the MIT license
	 * @throws IllegalStateException if the text of the license is not set
	 */
	public static String getMITLicenseText() throws IllegalStateException {
		if (mitLicenseText == null) {
			throw new IllegalStateException("The text of the MIT license has not been set");
		}
		return mitLicenseText;
	}

}
