package fr.unicaen.aasuite.main;

import java.io.File;

import javax.swing.SwingUtilities;

import fr.greyc.mad.observable.Observer;
import fr.greyc.mad.observable.BasicObserved;
import fr.unicaen.aasuite.gui.MainFrame;
import fr.unicaen.aasuite.sessions.Session;
import fr.unicaen.aasuite.sessions.SessionChange;

/**
 * Main class of GUI View for PepTraq Desktop. A view essentially manages a main
 * frame and observes an instance of {@link Session}.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class GUIView implements Observer<SessionChange> {

	/** The default number of sequences displayed on each page. */
	public static final Integer DEFAULT_NB_SEQUENCES_PER_PAGE = 50;

	/** The main frame composing this view. */
	private final MainFrame frame;

	/**
	 * Builds a new view.
	 * 
	 * @param peptraqVersion The current version of PepTraq
	 * @param session        The session to be observed by this view.
	 * @param dataDirectory  The directory in which to start browsing files
	 */
	public GUIView(String peptraqVersion, Session session, File dataDirectory) {
		this(peptraqVersion, dataDirectory, session);
	}

	/**
	 * Builds a new view with no default directory in which to start browsing files.
	 * 
	 * @param peptraqVersion The current version of PepTraq
	 * @param session        The session to be observed by this view
	 */
	public GUIView(String peptraqVersion, Session session) {
		this(peptraqVersion, session, null);
	}

	/**
	 * Builds a new view.
	 * 
	 * @param peptraqVersion      The current version of PepTraq
	 * @param dataDirectoryOrNull The directory in which to start browsing files
	 *                            (null for none)
	 * @param session             The session to be observed by this view.
	 */
	private GUIView(String peptraqVersion, File dataDirectoryOrNull, Session session) {
		if (dataDirectoryOrNull == null) {
			this.frame = new MainFrame(peptraqVersion, session, GUIView.DEFAULT_NB_SEQUENCES_PER_PAGE);
		} else {
			this.frame = new MainFrame(peptraqVersion, session, dataDirectoryOrNull,
					GUIView.DEFAULT_NB_SEQUENCES_PER_PAGE);
		}
		session.addSessionChangeObserver(this);
	}

	@Override
	public void onNotification(BasicObserved<SessionChange> notifier, SessionChange ignored) {
		SwingUtilities.invokeLater(() -> GUIView.this.frame.update());
	}

}
