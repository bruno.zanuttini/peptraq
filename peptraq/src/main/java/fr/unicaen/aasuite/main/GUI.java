package fr.unicaen.aasuite.main;

import java.io.File;

import fr.unicaen.aasuite.resources.StaticTexts;
import fr.unicaen.aasuite.sessions.ConcreteSession;
import fr.unicaen.aasuite.sessions.Session;

/**
 * Main class for the GUI version of PepTraq Desktop.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class GUI {

	/** The version of this application. */
	public static final String VERSION = "2.1";

	/** The option name for the data directory. */
	public static final String DATA_DIR_OPTION = "--data-directory";

	/** The error message for the data directory. */
	public static final String DATA_DIR_ERROR = "The specified name is not a directory: %s";

	/** The error message for any error which is not caught elsewhere. */
	public static final String UNEXPECTED_ERROR = "Unexpected error (PepTraq version " + VERSION
			+ "), please send a copy of this screen to bruno.zanuttini@unicaen.fr: %s";

	/** The usage string. */
	public static final String USAGE = "Expected command-line arguments: [" + DATA_DIR_OPTION
			+ " default-data-directory]";

	/**
	 * Starts the application.
	 * 
	 * @param args command-line arguments; expected: nothing or DATA_DIR_OPTION
	 *             directory, where directory is the directory in which browsing
	 *             starts for opening files
	 */
	public static void main(String[] args) {

		// Checking command-line arguments
		if (args.length != 0 && args.length != 2) {
			GUI.printUsage();
			System.exit(1);
		}

		// Initializing data directory
		File dataDirectory = null;
		if (args.length == 2) {
			if (DATA_DIR_OPTION.equals(args[0])) {
				dataDirectory = new File(args[1]);
				if (!dataDirectory.isDirectory()) {
					System.out.println(String.format(DATA_DIR_ERROR, dataDirectory));
					GUI.printUsage();
					System.exit(1);
				}
			} else {
				GUI.printUsage();
				System.exit(1);
			}
		}

		// FIXME: clean

		// Launching application
		try {
			String aboutText = Resources.getText(Resources.ABOUT_FILE, "About");
			StaticTexts.setAboutText(aboutText);
			String cecillLicenseText = Resources.getText(Resources.CECILL_B_LICENSE_FILE, "CeCILL-B License");
			StaticTexts.setCecillLicenseText(cecillLicenseText);
			String lgplLicenseText = Resources.getText(Resources.LGPL_2_1_LICENSE_FILE, "LGPLv2.1 License");
			StaticTexts.setLGPLLicenseText(lgplLicenseText);
			String mitLicenseText = Resources.getText(Resources.MIT_LICENSE_FILE, "MIT License");
			StaticTexts.setMITLicenseText(mitLicenseText);
			// TODO: restore use of properties
			// OperationResourceHandler operationResourceHandler = new
			// OperationResourceHandler(
//					ResourceBundle.getBundle("properties.Operation", new UTF8Control()));
//			operationResourceHandler.initializeOperationTypeDescriptions();
//			operationResourceHandler.initializeConstraintStrings();
//			operationResourceHandler.initializeConversionStrings();
//			operationResourceHandler.initializeTransformStrings();
//			BuilderResourceHandler builderResourceHandler = new BuilderResourceHandler(
//					ResourceBundle.getBundle("properties.Builder", new UTF8Control()));
//			builderResourceHandler.initializeConstraintBuilderStrings();
//			builderResourceHandler.initializeConversionBuilderStrings();
//			builderResourceHandler.initializeTransformBuilderStrings();
			Session session = new ConcreteSession();
			new GUIView(VERSION, session, dataDirectory);
		} catch (Throwable e) {
			e.printStackTrace(System.out);
			System.out.println(String.format(UNEXPECTED_ERROR, e));
			System.exit(2);
		}

	}

	/**
	 * Prints usage to standard output.
	 */
	public static void printUsage() {
		System.out.println(USAGE);
	}

}
