/**
 * Main package for PepTraq Desktop, containing the executable files running the
 * application.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.main;
