package fr.unicaen.aasuite.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * A class grouping together the resource names for PepTraq Desktop.
 * 
 * Bruno Zanuttini, Université de Caen Normandie, France
 * 
 * FIXME: clean
 */
public class Resources {

	// FIXME: make enums instead of constant strings

	/** The name of the directory holding resources. */
	public static final String RESOURCE_DIR = "resources";

	/** The file holding the text for "about". */
	public static final String ABOUT_FILE = "about.txt";

	/** The file holding the text for "Cecill B license". */
	public static final String CECILL_B_LICENSE_FILE = "cecill_b_license.txt";

	/** The file holding the text for "LGPLv2.1 license". */
	public static final String LGPL_2_1_LICENSE_FILE = "lgpl_2.1_license.txt";

	/** The file holding the text for "MIT license". */
	public static final String MIT_LICENSE_FILE = "mit_license.txt";

	/** The file holding properties for operations. */
	public static final String OPERATION_PROPERTY_FILE = "operations.properties";

	/** The file holding properties for builders. */
	public static final String BUILDER_PROPERTY_FILE = "builders.properties";

	public static BufferedReader getReader(String resourceName) throws IOException {
		InputStream stream = Resources.class.getResource("/" + RESOURCE_DIR + "/" + resourceName)
				.openStream();
		return new BufferedReader(new InputStreamReader(stream));
	}

	public static String getText(String resourceName, String resourceDescription) {
		BufferedReader reader = null;
		try {
			reader = Resources.getReader(resourceName);
			StringBuilder text = new StringBuilder();
			String line = reader.readLine();
			while (line != null) {
				text.append(line);
				text.append(System.lineSeparator());
				line = reader.readLine();
			}
			reader.close();
			return text.toString();
		} catch (IOException e) {
			// FIXME: move to main?
			System.err.println("Could not read file: " + resourceName + "." + System.lineSeparator() + "\""
					+ resourceDescription + "\" information will be missing.");
		}
		return "";
	}

	public static Properties getProperties(String name) throws IOException {
		InputStream stream = Resources.class.getResource("/" + RESOURCE_DIR + "/" + name).openStream();
		Properties res = new Properties();
		res.load(stream);
		return res;
	}

}
