package fr.unicaen.aasuite.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import fr.greyc.mad.userinteraction.FileHandler;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.constraints.TautologicalConstraint;
import fr.unicaen.aasuite.control.ApplyMacroListener;
import fr.unicaen.aasuite.control.CloseSessionListener;
import fr.unicaen.aasuite.control.ExitListener;
import fr.unicaen.aasuite.control.LoadSequencesListener;
import fr.unicaen.aasuite.control.SaveSequencesListener;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.operations.OperationAdapters;
import fr.unicaen.aasuite.resources.MenuConstants;
import fr.unicaen.aasuite.resources.StaticTexts;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A class for handling the main menus of PepTraq Desktop, namely, menus that
 * are independent of the type of sequences currently explored. A handler is
 * attached to a specific session. It also handles all listeners for the menu
 * items.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         FIXME: accelerators do not work, currently deactivated (in
 *         {@link AcceleratorHandler#assignAccelerator(JMenuItem, char, Object)}).
 */
public class MenuHandler {

	/** The file menu. */
	private JMenu fileMenu;

	/** The menu item for loading DNA sequences. */
	private JMenuItem loadDNA;

	/** The menu item for saving proteins. */
	private JMenuItem loadProteins;

	/** The menu item for saving all sequences. */
	private JMenuItem saveAll;

	/** The menu item for saving all selected sequences. */
	private JMenuItem saveSelected;

	/** The menu item for closing the session. */
	private JMenuItem close;

	/** The session menu. */
	private JMenu selectionMenu;

	/** The menu item for selecting all sequences. */
	private JMenuItem selectAll;

	/** The menu item for unselecting all sequences. */
	private JMenuItem unselectAll;

	/** The menu item for discarding unselected sequences. */
	private JMenuItem discardUnselected;

	/** The menu item for undoing the last operation of the session. */
	private JMenuItem undo;

	/** The menu item for applying a macro. */
	private JMenuItem applyMacro;

	/** The macro menu. */
	private JMenu operationMenu;

	/** The constraint menu (null when N/A). */
	private JMenu constraintMenu;

	/** The transform menu (null when N/A). */
	private JMenu transformMenu;

	/** The conversion menu (null when N/A). */
	private JMenu conversionMenu;

	/** A handler for mnemonics and accelerators. */
	private final AcceleratorHandler acceleratorHandler;

	/** The session to which this handler is attached. */
	protected final Session session;

	/** The main frame to which the handled menu bar is attached. */
	protected final MainFrame frame;

	/** The menu bar to be handled. */
	protected final JMenuBar bar;

	/** A proposer for interaction with the user by some listeners. */
	protected final Proposer proposer;

	/** A file handler for use by some listeners. */
	protected final FileHandler fileHandler;

	/** The current version of PepTraq. */
	protected final String peptraqVersion;

	/**
	 * Builds a new instance.
	 * 
	 * @param session        The session to which this handler is attached
	 * @param frame          The main frame to which the handled menu bar is
	 *                       attached
	 * @param bar            The menu bar to be handled
	 * @param proposer       A proposer for interaction with the user by some
	 *                       listeners
	 * @param fileHandler    A file handler for use by some listeners
	 * @param peptraqVersion The current version of PepTraq
	 */
	public MenuHandler(Session session, MainFrame frame, JMenuBar bar, Proposer proposer, FileHandler fileHandler,
			String peptraqVersion) {
		this.acceleratorHandler = new AcceleratorHandler(InputEvent.CTRL_DOWN_MASK);
		this.session = session;
		this.frame = frame;
		this.bar = bar;
		this.proposer = proposer;
		this.fileHandler = fileHandler;
		this.peptraqVersion = peptraqVersion;
		this.buildFileMenu();
		this.buildSelectionMenu();
		this.buildOperationMenu();
		this.buildDisplayMenu();
		this.buildOperationMenus();
		this.buildHelpMenu();
		this.update();
	}

	/**
	 * Enables/disables all menus/menu items which should not be active when an
	 * operation is progressing. Enables only those menus which make sense with the
	 * content currently displayed.
	 * 
	 * @param enable true for enabling the menus and items, false for disabling them
	 */
	public void enable(boolean enable) {
		this.fileMenu.setEnabled(enable);
		this.selectionMenu.setEnabled(enable);
		this.operationMenu.setEnabled(enable);
		this.constraintMenu.setEnabled(enable);
		this.transformMenu.setEnabled(enable);
		this.conversionMenu.setEnabled(enable);
		if (enable) {
			this.update();
		}
	}

	/**
	 * Updates the menus and menu items so as to disable/enable controls depending
	 * on the content currently displayed.
	 */
	public void update() {
		boolean sequencesDisplayed = this.session.nbSteps() != 0;
		this.loadDNA.setEnabled(!sequencesDisplayed);
		this.loadProteins.setEnabled(!sequencesDisplayed);
		this.saveAll.setEnabled(sequencesDisplayed);
		this.saveSelected.setEnabled(sequencesDisplayed);
		this.selectionMenu.setEnabled(sequencesDisplayed);
		this.selectAll.setEnabled(sequencesDisplayed);
		this.unselectAll.setEnabled(sequencesDisplayed);
		this.applyMacro.setEnabled(sequencesDisplayed);
		this.close.setEnabled(this.session.nbSteps() > 0);
		this.undo.setEnabled(this.session.nbSteps() > 1);
		OperationMenuHandler.updateConstraintMenu(this.constraintMenu, this.session, this.proposer);
		OperationMenuHandler.updateTransformMenu(this.transformMenu, this.session, this.proposer);
		OperationMenuHandler.updateConversionMenu(this.conversionMenu, this.session, this.proposer);
	}

	/**
	 * Builds the file menu (loading/saving files, exiting...).
	 */
	private void buildFileMenu() {

		this.fileMenu = new JMenu(MenuConstants.FILE_MENU);
		this.acceleratorHandler.assign(this.fileMenu, MenuConstants.FILE_MENU_MNEMONIC);
		this.bar.add(this.fileMenu);

		// Loading DNA
		this.loadDNA = new JMenuItem(MenuConstants.LOAD_DNA);
		LoadSequencesListener dnaLoader = LoadSequencesListener.forDNA(this.session, this.fileHandler, this.proposer);
		this.loadDNA.addActionListener(dnaLoader);
		this.acceleratorHandler.assign(this.loadDNA, MenuConstants.LOAD_DNA_KEYS, this.fileMenu);
		this.fileMenu.add(this.loadDNA);

		// Loading proteins
		this.loadProteins = new JMenuItem(MenuConstants.LOAD_PROTEINS);
		LoadSequencesListener proteinLoader = LoadSequencesListener.forProteins(this.session, this.fileHandler,
				this.proposer);
		this.loadProteins.addActionListener(proteinLoader);
		this.acceleratorHandler.assign(this.loadProteins, MenuConstants.LOAD_PROTEINS_KEYS, this.fileMenu);
		this.fileMenu.add(this.loadProteins);

		// Saving sequences
		this.saveAll = new JMenuItem(MenuConstants.SAVE);
		SaveSequencesListener saver = new SaveSequencesListener(this.session, false, this.fileHandler, this.proposer);
		this.saveAll.addActionListener(saver);
		this.acceleratorHandler.assign(this.saveAll, MenuConstants.SAVE_KEYS, this.fileMenu);
		this.fileMenu.add(this.saveAll);

		// Saving selected sequences
		this.saveSelected = new JMenuItem(MenuConstants.SAVE_SELECTED);
		SaveSequencesListener selectedSaver = new SaveSequencesListener(this.session, true, this.fileHandler,
				this.proposer);
		this.saveSelected.addActionListener(selectedSaver);
		this.acceleratorHandler.assign(this.saveSelected, MenuConstants.SAVE_SELECTED_KEYS, this.fileMenu);
		this.fileMenu.add(this.saveSelected);

		// Closing the session
		this.close = new JMenuItem(MenuConstants.CLOSE);
		CloseSessionListener closeSessionListener = new CloseSessionListener(this.session, this.proposer);
		this.close.addActionListener(e -> closeSessionListener.actionPerformed());
		this.acceleratorHandler.assign(this.close, MenuConstants.CLOSE_KEYS, this.fileMenu);
		this.fileMenu.add(this.close);

		// Exiting
		JMenuItem exit = new JMenuItem(MenuConstants.EXIT);
		ExitListener exitListener = new ExitListener(this.proposer);
		exit.addActionListener(e -> exitListener.actionPerformed());
		this.acceleratorHandler.assign(exit, MenuConstants.EXIT_KEYS, this.fileMenu);
		this.fileMenu.add(exit);

	}

	/**
	 * Builds the selection menu.
	 */
	private void buildSelectionMenu() {

		this.selectionMenu = new JMenu(MenuConstants.SELECTION_MENU);
		this.acceleratorHandler.assign(this.selectionMenu, MenuConstants.SELECTION_MENU_MNEMONIC);
		this.bar.add(this.selectionMenu);

		// Selecting all sequences
		this.selectAll = new JMenuItem(MenuConstants.SELECT_ALL);
		this.selectAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuHandler.this.session.getModel().selectAllSequences(true);
			}
		});
		this.acceleratorHandler.assign(this.selectAll, MenuConstants.SELECT_ALL_KEYS, this.selectionMenu);
		this.selectionMenu.add(this.selectAll);

		// Unselecting all sequences
		this.unselectAll = new JMenuItem(MenuConstants.UNSELECT_ALL);
		this.unselectAll.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MenuHandler.this.session.getModel().selectAllSequences(false);
			}
		});
		this.acceleratorHandler.assign(this.unselectAll, MenuConstants.UNSELECT_ALL_KEYS, this.selectionMenu);
		this.selectionMenu.add(this.unselectAll);

		// Discarding unselected
		this.discardUnselected = new JMenuItem(MenuConstants.DISCARD_UNSELECTED);
		this.discardUnselected.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (MenuHandler.this.session.getModel().isDNA()) {
					MenuHandler.this.session.addDNAOperation(OperationAdapters.forConstraint(
							new TautologicalConstraint<>(ConstraintType.SELECTED, MenuConstants.DISCARD_UNSELECTED)));
				} else {
					MenuHandler.this.session.addProteinOperation(OperationAdapters.forConstraint(
							new TautologicalConstraint<>(ConstraintType.SELECTED, MenuConstants.DISCARD_UNSELECTED)));
				}
			}
		});
		this.selectionMenu.add(this.discardUnselected);

	}

	/**
	 * Builds the operation menu (designing, loading a macro, navigating).
	 */
	private void buildOperationMenu() {

		this.operationMenu = new JMenu(MenuConstants.OPERATION_MENU);
		this.acceleratorHandler.assign(this.operationMenu, MenuConstants.OPERATION_MENU_MNEMONIC);
		this.bar.add(this.operationMenu);

		// Going back by one step in the session
		this.undo = new JMenuItem(MenuConstants.BACK);
		this.undo.addActionListener(e -> this.session.cancelCurrentStep());
		this.acceleratorHandler.assign(this.undo, MenuConstants.BACK_KEYS, this.operationMenu);
		this.operationMenu.add(this.undo);

		// Designing new macros
		JMenuItem designMacro = new JMenuItem(MenuConstants.DESIGN_MACRO);
		designMacro.addActionListener(e -> new MacroDesignerHandler(this.session, this.proposer, this.fileHandler,
				this.peptraqVersion, this.frame.getBounds()).setVisible(true));
		this.acceleratorHandler.assign(designMacro, MenuConstants.DESIGN_MACRO_KEYS, this.operationMenu);
		this.operationMenu.add(designMacro);

		// Loading and applying a macro
		this.applyMacro = new JMenuItem(MenuConstants.APPLY_MACRO);
		this.applyMacro.addActionListener(
				new ApplyMacroListener(this.session, this.fileHandler, this.proposer, this.peptraqVersion));
		this.operationMenu.add(this.applyMacro);

	}

	/**
	 * Builds the display menu (display settings).
	 */
	private void buildDisplayMenu() {

		JMenu displayMenu = new JMenu(MenuConstants.DISPLAY_MENU);
		this.acceleratorHandler.assign(displayMenu, MenuConstants.DISPLAY_MENU_MNEMONIC);
		this.bar.add(displayMenu);

		// Toggling display of history pane
		JMenuItem displayHistory = new JMenuItem(MenuConstants.DISPLAY_HISTORY);
		displayHistory.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				MenuHandler.this.frame.toggleHistoryDisplay();
			}
		});
		displayMenu.add(displayHistory);

		// Setting the number of sequences per page
		JMenuItem nbSequences = new JMenuItem(MenuConstants.SEQUENCES_PER_PAGE);
		SetNbSequencesPerPageListener setNbSequencesPerPageListener = new SetNbSequencesPerPageListener(this.proposer,
				this.frame);
		nbSequences.addActionListener(e -> setNbSequencesPerPageListener.actionPerformed());
		this.acceleratorHandler.assign(nbSequences, MenuConstants.SEQUENCES_PER_PAGE_KEYS, displayMenu);
		displayMenu.add(nbSequences);

		// Setting the font size
		JMenuItem fontSize = new JMenuItem(MenuConstants.SET_FONT_SIZE);
		SetFontSizeListener setFontSizeListener = new SetFontSizeListener(this.proposer);
		fontSize.addActionListener(e -> setFontSizeListener.actionPerformed());
		this.acceleratorHandler.assign(fontSize, MenuConstants.SET_FONT_SIZE_KEYS, displayMenu);
		displayMenu.add(fontSize);

		// Increasing the font size
		JMenuItem increaseFontSize = new JMenuItem(MenuConstants.LARGER_FONT);
		increaseFontSize.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final int fontSize = SequencePaneHandler.getFontSize();
				SequencePaneHandler.setFontSize(fontSize + 1);
			}
		});
		this.acceleratorHandler.assign(increaseFontSize, MenuConstants.LARGER_FONT_KEYS, displayMenu);
		displayMenu.add(increaseFontSize);

		// Decreasing the font size
		JMenuItem decreaseFontSize = new JMenuItem(MenuConstants.SMALLER_FONT);
		decreaseFontSize.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				final int fontSize = SequencePaneHandler.getFontSize();
				final int newSize = Math.max(1, fontSize - 1);
				SequencePaneHandler.setFontSize(newSize);
			}
		});
		this.acceleratorHandler.assign(decreaseFontSize, MenuConstants.SMALLER_FONT_KEYS, displayMenu);
		displayMenu.add(decreaseFontSize);

		// Setting the highlighting style
		JMenuItem highlighting = new JMenuItem(MenuConstants.SET_HIGHLIGHTING_STYLE);
		SetHighlightStyleListener setHighlightStyleListener = new SetHighlightStyleListener(this.proposer);
		highlighting.addActionListener(e -> setHighlightStyleListener.actionPerformed());
		this.acceleratorHandler.assign(highlighting, MenuConstants.SET_HIGHLIGHTING_STYLE_KEYS, displayMenu);
		displayMenu.add(highlighting);

	}

	/**
	 * Builds the operation menus.
	 */
	private void buildOperationMenus() {
		this.constraintMenu = new JMenu(MenuConstants.CONSTRAINT_MENU);
		this.acceleratorHandler.assign(this.constraintMenu, MenuConstants.CONSTRAINT_MENU_MNEMONIC);
		this.bar.add(this.constraintMenu);
		this.transformMenu = new JMenu(MenuConstants.TRANSFORM_MENU);
		this.acceleratorHandler.assign(this.transformMenu, MenuConstants.TRANSFORM_MENU_MNEMONIC);
		this.bar.add(this.transformMenu);
		this.conversionMenu = new JMenu(MenuConstants.CONVERSION_MENU);
		this.acceleratorHandler.assign(this.conversionMenu, MenuConstants.CONVERSION_MENU_MNEMONIC);
		this.bar.add(this.conversionMenu);
	}

	/**
	 * Builds the help menu.
	 */
	private void buildHelpMenu() {
		JMenu helpMenu = new JMenu(MenuConstants.HELP_MENU);
		this.acceleratorHandler.assign(helpMenu, MenuConstants.HELP_MENU_MNEMONIC);
		this.bar.add(helpMenu);

		// About
		JMenuItem about = new JMenuItem(MenuConstants.ABOUT);
		about.addActionListener(
				e -> new StaticTextHandler(MenuConstants.ABOUT, StaticTexts.getAboutText(this.peptraqVersion), this.frame.getBounds())
						.setVisible(true));
		this.acceleratorHandler.assign(about, MenuConstants.ABOUT_KEYS, helpMenu);
		helpMenu.add(about);

		// Licenses
		JMenuItem cecill = new JMenuItem(MenuConstants.CECILL_LICENSE);
		cecill.addActionListener(
				e -> new StaticTextHandler(MenuConstants.CECILL_LICENSE, StaticTexts.getCecillLicenseText(), this.frame.getBounds())
						.setVisible(true));
		this.acceleratorHandler.assign(cecill, MenuConstants.CECILL_LICENSE_KEYS, helpMenu);
		helpMenu.add(cecill);
		JMenuItem lgpl = new JMenuItem(MenuConstants.LGPL_LICENSE);
		lgpl.addActionListener(
				e -> new StaticTextHandler(MenuConstants.LGPL_LICENSE, StaticTexts.getLGPLLicenseText(), this.frame.getBounds())
						.setVisible(true));
		this.acceleratorHandler.assign(lgpl, MenuConstants.LGPL_LICENSE_KEYS, helpMenu);
		helpMenu.add(lgpl);
		JMenuItem mit = new JMenuItem(MenuConstants.MIT_LICENSE);
		mit.addActionListener(
				e -> new StaticTextHandler(MenuConstants.MIT_LICENSE, StaticTexts.getMITLicenseText(), this.frame.getBounds())
						.setVisible(true));
		this.acceleratorHandler.assign(mit, MenuConstants.MIT_LICENSE_KEYS, helpMenu);
		helpMenu.add(mit);
	}

}
