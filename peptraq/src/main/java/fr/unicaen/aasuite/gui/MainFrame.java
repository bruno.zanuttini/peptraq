package fr.unicaen.aasuite.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;

import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.MonitorableExecutor;
import fr.greyc.mad.observable.Observer;
import fr.greyc.mad.observable.Progress;
import fr.greyc.mad.swingutils.Monitor;
import fr.greyc.mad.swingutils.TimeProgressBar;
import fr.greyc.mad.userinteraction.FileHandler;
import fr.unicaen.aasuite.resources.StaticImages;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.Session;

/**
 * The main frame for the GUI of PepTraq Desktop. Such a frame displays the
 * current state of a session (current sequences) and related controls. The
 * current state may consist of no sequence, of DNA sequences, or of protein
 * sequences. The frame changes to the next state of the session only when
 * required to do so via a call to {@link #update()}, but reacts (via
 * observation) to all other events (selections, display settings, etc.).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings, pixel amounts, etc.
 */
public class MainFrame implements Observer<Progress> {

	/** The default frame width, in pixels. */
	public static int DEFAULT_FRAME_WIDTH = 800;

	/** The default frame height, in pixels. */
	public static int DEFAULT_FRAME_HEIGHT = 600;

	/** The title for the frame when the current session has a name. */
	public static final String NAMED_SESSION_TITLE = "PepTraq - %s";

	/** The title for the frame when the current session has no name. */
	public static final String UNNAMED_SESSION_TITLE = "PepTraq";

	/** The label for the button for canceling an operation execution. */
	public static final String CANCEL_LABEL = "Cancel";

	/** The title for the part of the frame displaying the history. */
	public static final String HISTORY_TITLE = "Operation history";

	/** The width of the history pane (in pixels). */
	public static final int HISTORY_PANE_WIDTH = 200;

	/** The title for the part of the frame displaying the sequences. */
	public static final String SEQUENCES_TITLE = "Sequences";

	/** The title for the message informing of an execution error. */
	public static final String ERROR_TITLE = "Error";

	/** The message informing of an execution error. */
	public static final String ERROR_MESSAGE = "An error occurred: the operation could not be completed"
			+ System.lineSeparator() + "Error message: %s";

	/**
	 * The time between two updates of an estimation of remaining time for lengthy
	 * operations, in milliseconds.
	 */
	public static final int PROGRESS_UPDATE_INTERVAL = 1000;

	/** The color for clickable text. */
	public static Color CLICKABLE_COLOR = Color.BLUE;

	/** Author information. */
	public static String authorInformation = "© 2022 Bruno Zanuttini, Joël Henry, Céline Zatylny";

	/** Institute information. */
	public static String instituteInformation = "Université de Caen Normandie, France";

	/** The height of logos (in pixels). */
	public static int logoHeight = 40;

	/**
	 * The width of the blank space between logos and author information (in
	 * pixels).
	 */
	public static int separatorWidth = 40;

	/** A JFrame to which graphical rendering is delegated. */
	private final JFrame frame;

	/** A proposer associated to this frame. */
	private final GUIProposer proposer;

	/**
	 * A handler for the generic menus of the frame, the ones independent from the
	 * current type of sequences.
	 */
	private final MenuHandler menuHandler;

	/** The panel displaying the history. */
	private final JPanel historyPane;

	/** A handler for the history pane of the frame. */
	private final HistoryPaneHandler historyPaneHandler;

	/** A handler for the panel displaying the sequences. */
	private final SequenceListPaneHandler sequenceListPaneHandler;

	/**
	 * Monitors of currently ongoing operations, keyed by the observed operations.
	 */
	private final Map<Object, Monitor> monitors;

	/**
	 * Dialogs for monitoring of currently ongoing operations, keyed by the observed
	 * operations.
	 */
	private final Map<Object, JDialog> progressDialogs;

	/** The current version of PepTraq. */
	protected final String peptraqVersion;

	/** The observed session. */
	protected final Session session;

	/**
	 * Builds a new instance with no default directory in which to start browsing
	 * files, and no pagination of sequences (all are displayed), and makes the
	 * frame visible with the current state of the session.
	 * 
	 * @param peptraqVersion The current version of PepTraq
	 * @param session        The session to the displaying of which this frame is
	 *                       dedicated.
	 */
	public MainFrame(String peptraqVersion, Session session) {
		this(peptraqVersion, null, session, null);
	}

	/**
	 * Builds a new instance with no pagination, and makes the frame visible with
	 * the current state of the session.
	 * 
	 * @param peptraqVersion The current version of PepTraq
	 * @param session        The session to the displaying of which this frame is
	 *                       dedicated.
	 * @param dataDirectory  The directory in which to start browsing files.
	 */
	public MainFrame(String peptraqVersion, Session session, File dataDirectory) {
		this(peptraqVersion, dataDirectory, session, null);
	}

	/**
	 * Builds a new instance with no default directory in which to start browsing
	 * files, and makes the frame visible with the current state of the session.
	 * 
	 * @param peptraqVersion     The current version of PepTraq
	 * @param session            The session to the displaying of which this frame
	 *                           is dedicated.
	 * @param nbSequencesPerPage The number of sequences to display per page
	 * @throws IllegalArgumentException if the number of sequences per page is not
	 *                                  (strictly) positive
	 */
	public MainFrame(String peptraqVersion, Session session, int nbSequencesPerPage) throws IllegalArgumentException {
		this(peptraqVersion, null, session, nbSequencesPerPage);
	}

	/**
	 * Builds a new instance and makes the frame visible with the current state of
	 * the session.
	 * 
	 * @param peptraqVersion     The current version of PepTraq
	 * @param session            The session to the displaying of which this frame
	 *                           is dedicated.
	 * @param dataDirectory      The directory in which to start browsing files.
	 * @param nbSequencesPerPage The number of sequences to display per page
	 * @throws IllegalArgumentException if the number of sequences per page is not
	 *                                  (strictly) positive
	 */
	public MainFrame(String peptraqVersion, Session session, File dataDirectory, int nbSequencesPerPage)
			throws IllegalArgumentException {
		this(peptraqVersion, dataDirectory, session, nbSequencesPerPage);
	}

	/**
	 * Returns the current bounds of the frame.
	 * 
	 * @return The current bounds of the frame
	 */
	public Rectangle getBounds() {
		return this.frame.getBounds();
	}

	/**
	 * Toggles the display of the history pane, that is, hides it if it was visible,
	 * and shows it if it was not.
	 */
	public void toggleHistoryDisplay() {
		this.historyPane.setVisible(!this.historyPane.isVisible());
	}

	/**
	 * Decides whether the sequences on this frame are currently paginated. This
	 * depends on the current settings of the frame, even if there is no sequence
	 * displayed.
	 * 
	 * @return true if the sequences on this frame are currently paginated, false
	 *         otherwise (that is, all are displayed)
	 */
	public boolean isPaginated() {
		return this.sequenceListPaneHandler.isPaginated();
	}

	/**
	 * Returns the (maximal) number of sequences displayed at a time. This depends
	 * on the current settings of the frame, even if there is no sequence displayed.
	 * 
	 * @return The maximal number of sequences displayed at a time
	 * @throws IllegalStateException if this frame is not paginated
	 */
	public int getNbSequencesPerPage() throws IllegalStateException {
		return this.sequenceListPaneHandler.getNbSequencesPerPage();
	}

	/**
	 * Sets the maximal number of sequences to be displayed at a time.
	 * 
	 * @param nbSequencesPerPage The maximal number of sequences to display at a
	 *                           time
	 * @throws IllegalArgumentException if the given number is not (strictly)
	 *                                  positive
	 */
	public void setNbSequencesPerPage(int nbSequencesPerPage) throws IllegalArgumentException {
		this.sequenceListPaneHandler.paginate(nbSequencesPerPage);
	}

	/**
	 * Unsets the pagination, that is, now all sequences in the session are
	 * displayed at a time.
	 */
	public void removePagination() {
		this.sequenceListPaneHandler.removePagination();
	}

	/**
	 * Updates the frame so as to reflect the current state of the session.
	 */
	public void update() {
		SwingUtilities.invokeLater(() -> {
			this.frame.setTitle(this.session.hasName() ? String.format(NAMED_SESSION_TITLE, this.session.getName())
					: UNNAMED_SESSION_TITLE);
			this.menuHandler.update();
			if (this.session.nbSteps() == 0) {
				Set<AnnotatedSequence<?>> noSequence = Collections.emptySet();
				this.sequenceListPaneHandler.update(noSequence, this.session);
			} else if (this.session.getModel().isDNA()) {
				this.sequenceListPaneHandler.update(this.session.getModel().dna(), this.session);
			} else {
				assert this.session.getModel().isProteins();
				this.sequenceListPaneHandler.update(this.session.getModel().proteins(), this.session);
			}
			this.historyPaneHandler.update(this.session.getStepDescriptions());
		});
	}

	@Override
	public void onNotification(BasicObserved<Progress> observed, Progress progress) {
		SwingUtilities.invokeLater(() -> {
			MonitorableExecutor executor = progress.getExecutor();
			Monitor monitor = null;
			switch (progress.getType()) {
			case STARTED:
				this.menuHandler.enable(false);
				this.historyPaneHandler.enableHistoryOperations(false);
				monitor = new Monitor((Long) progress.getValue(), PROGRESS_UPDATE_INTERVAL);
				this.monitors.put(executor, monitor);
				JDialog dialog = this.makeProgressDialog(executor);
				this.progressDialogs.put(executor, dialog);
				dialog.setVisible(true);
				break;
			case PROGRESSED:
				monitor = this.monitors.get(executor);
				monitor.setNbSteps(progress.getValue());
				break;
			case FINISHED:
			case CANCELLED:
				this.monitors.remove(executor);
				this.progressDialogs.get(executor).dispose();
				this.progressDialogs.remove(executor);
				this.menuHandler.enable(true);
				this.historyPaneHandler.enableHistoryOperations(true);
				break;
			case ERROR:
				this.monitors.remove(executor);
				this.progressDialogs.get(executor).dispose();
				this.progressDialogs.remove(executor);
				this.proposer.inform(ERROR_TITLE, String.format(ERROR_MESSAGE, progress.getErrorMessage()));
				this.menuHandler.enable(true);
				this.historyPaneHandler.enableHistoryOperations(true);
				break;
			}
		});
	}

	/**
	 * Returns a component which looks like clickable text.
	 * 
	 * @param str A string
	 * @return A component representing the given string and whose appearance
	 *         suggests that it is clickable
	 */
	public static JButton makeClickableText(String str) {
		JButton res = new JButton(str);
		res.setBorderPainted(false);
		res.setContentAreaFilled(false);
		res.setForeground(CLICKABLE_COLOR);
		res.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		return res;
	}

	/**
	 * Helper method: builds a dialog displaying progress about a given executor and
	 * a button for Canceling it. The dialog is not set visible by this method.
	 * 
	 * @param executor The executor to display progress about; note that it is
	 *                 assumed that there is a monitor registered for this executor
	 *                 in {@link #monitors}.
	 * @return A dialog displaying progress about the given executor and a button
	 *         for canceling it
	 */
	private JDialog makeProgressDialog(final MonitorableExecutor executor) {
		Monitor monitor = this.monitors.get(executor);
		assert monitor != null;
		JDialog res = new JDialog(this.frame, executor.getDescription(), false);
		res.setLayout(new BorderLayout());
		JComponent component = new TimeProgressBar(monitor).getAsComponent();
		res.add(component, BorderLayout.NORTH);
		JButton button = new JButton(CANCEL_LABEL);
		button.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				executor.cancel();
			}

		});
		res.add(button, BorderLayout.SOUTH);
		res.pack();
		int x = (this.frame.getWidth() - res.getWidth()) / 2;
		int y = (this.frame.getHeight() - res.getHeight()) / 2;
		res.setLocation(x, y);
		return res;
	}

	/**
	 * Builds a new instance and makes the frame visible with the current state of
	 * the session.
	 * 
	 * @param peptraqVersion           The current version of PepTraq
	 * @param dataDirectoryOrNull      The directory in which to start browsing
	 *                                 files, or null for none
	 * @param session                  The session to the displaying of which this
	 *                                 frame is dedicated
	 * @param nbSequencesPerPageOrNull The number of sequences to display per page,
	 *                                 or null for no pagination
	 * @throws IllegalArgumentException if the number of sequences per page is not
	 *                                  (strictly) positive
	 */
	private MainFrame(String peptraqVersion, File dataDirectoryOrNull, Session session,
			Integer nbSequencesPerPageOrNull) {

		this.peptraqVersion = peptraqVersion;

		// The frame itself
		this.frame = new JFrame(UNNAMED_SESSION_TITLE);
		this.frame.setSize(DEFAULT_FRAME_WIDTH, DEFAULT_FRAME_HEIGHT);
		if (java.awt.Toolkit.getDefaultToolkit().isFrameStateSupported(JFrame.MAXIMIZED_BOTH)) {
			this.frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
		}
		this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frame.getContentPane().setLayout(new BorderLayout());

		// Proposer and file handler
		this.proposer = new GUIProposer(this.frame);
		FileHandler fileHandler = new FileChooser(this.frame, dataDirectoryOrNull);

		// Menus
		JMenuBar bar = new JMenuBar();
		this.frame.setJMenuBar(bar);
		this.menuHandler = new MenuHandler(session, this, bar, this.proposer, fileHandler, this.peptraqVersion);

		// History pane
		this.historyPane = new JPanel();
		this.historyPane.setPreferredSize(new Dimension(HISTORY_PANE_WIDTH, 0));
		Border historyBorder = BorderFactory.createTitledBorder(HISTORY_TITLE);
		this.historyPane.setBorder(historyBorder);
		this.frame.getContentPane().add(this.historyPane, BorderLayout.WEST);
		this.historyPaneHandler = new HistoryPaneHandler(session, this.historyPane);

		// Sequence Pane
		JPanel sequencePane = new JPanel();
		Border sequenceBorder = BorderFactory.createTitledBorder(SEQUENCES_TITLE);
		sequencePane.setBorder(sequenceBorder);
		this.frame.getContentPane().add(sequencePane, BorderLayout.CENTER);
		if (nbSequencesPerPageOrNull == null) {
			this.sequenceListPaneHandler = new SequenceListPaneHandler(session, sequencePane, this.proposer);
		} else {
			this.sequenceListPaneHandler = new SequenceListPaneHandler(session, sequencePane, this.proposer,
					nbSequencesPerPageOrNull);
		}

		// Authors and logos
		this.frame.getContentPane().add(this.makeAuthorPanel(), BorderLayout.SOUTH);

		// Progress monitoring
		this.monitors = new HashMap<>();
		this.progressDialogs = new HashMap<>();

		// Session
		this.session = session;
		session.addOperationObserver(this);

		// Making the frame up-to-date and visible
		this.frame.setVisible(true);

	}

	/**
	 * Helper method: returns a panel displaying author and copyright information.
	 * 
	 * @return A panel displaying author and copyright information
	 */
	private Component makeAuthorPanel() {
		JPanel res = new JPanel();
		// A subpanel used for centering content
		JPanel panel = new JPanel();
		res.add(panel);
		ImageIcon greyc = new ImageIcon(this.getClass().getResource(StaticImages.GREYC_LOGO_PATH));
		panel.add(new JLabel(scale(greyc, logoHeight)));
		panel.add(makeSeparator(separatorWidth));
		JPanel authors = new JPanel();
		authors.setLayout(new BoxLayout(authors, BoxLayout.Y_AXIS));
		JPanel author = new JPanel();
		author.add(new JLabel(authorInformation));
		authors.add(author);
		JPanel institute = new JPanel();
		institute.add(new JLabel(instituteInformation));
		authors.add(institute);
		panel.add(authors);
		panel.add(makeSeparator(separatorWidth));
		ImageIcon borea = new ImageIcon(this.getClass().getResource(StaticImages.BOREA_LOGO_PATH));
		panel.add(new JLabel(scale(borea, logoHeight)));
		return res;
	}

	/**
	 * Helper method: returns an image icon obtained from scaling a given image icon
	 * to a given height (preserving the ratio).
	 * 
	 * @param icon   An image icon
	 * @param height A height (in pixels)
	 * @return An image icon obtained from scaling the given icon to the given
	 *         height
	 */
	private static ImageIcon scale(ImageIcon icon, int height) {
		int initialWidth = icon.getIconWidth();
		int initialHeight = icon.getIconHeight();
		int width = initialWidth * height / initialHeight;
		return new ImageIcon(icon.getImage().getScaledInstance(width, height, Image.SCALE_SMOOTH));
	}

	/**
	 * Helper method: creates and returns a component of a given width (and no
	 * height), which can be used as a separator.
	 * 
	 * @param width The desired width
	 * @return A component of the given width
	 */
	private static Component makeSeparator(int width) {
		JPanel res = new JPanel();
		res.setPreferredSize(new Dimension(width, 0));
		return res;
	}

}
