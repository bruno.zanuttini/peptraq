package fr.unicaen.aasuite.gui;

import java.util.Collections;
import java.util.List;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.control.ConstructionListener;

/**
 * A listener for setting the font size for displaying sequences.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings
 */
public class SetFontSizeListener extends ConstructionListener<Integer> {

	/** The title for the input pane. */
	public static final String TITLE = "Set font size";

	/** The title for the input field. */
	public static final String FIELD_TITLE = "New font size";

	/** The error message for bad input. */
	public static final String ERROR_MESSAGE = "Please enter a positive integer";

	/** The help string for the builder. */
	public static String builderHelp = "Set the font size for displaying sequences";
	
	/** The help string for the input field. */
	public static String inputFieldHelp = "Enter the new font size (pt)";
	
	/**
	 * Builds a new instance.
	 * 
	 * @param proposer A proposer for interacting with the user
	 */
	public SetFontSizeListener(Proposer proposer) {
		super(proposer, TITLE);
	}

	@Override
	protected void handle(Integer size) {
		SequencePaneHandler.setFontSize(size);
	}

	@Override
	protected AbstractBuilder<InputField<String, ?>, Integer> make() {
		InputField<String, Integer> inputField = NumberFieldsFromString.makeIntegerField(false, FIELD_TITLE,
				String.valueOf(SequencePaneHandler.getFontSize()), inputFieldHelp);
		final InputField<String, Integer> field = new SimpleRestrictionInputDecorator<String, Integer>(inputField,
				ERROR_MESSAGE) {

			@Override
			protected boolean isValid(Integer value) {
				return value > 0;
			}

		};
		return new AbstractBuilder<InputField<String, ?>, Integer>(Collections.singletonList(inputField), builderHelp) {

			@Override
			protected Integer safeGetObject() {
				return field.getValue();
			}

			@Override
			public boolean isGloballyValid() {
				return true;
			}

			@Override
			public List<String> getGlobalErrors() {
				return Collections.emptyList();
			}
		};
	}

}
