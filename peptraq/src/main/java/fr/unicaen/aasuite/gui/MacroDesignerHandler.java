package fr.unicaen.aasuite.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Rectangle;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import fr.greyc.mad.swingutils.FullWidthScrollablePanel;
import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinteraction.FileHandler;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.control.ConstructionListener;
import fr.unicaen.aasuite.control.SaveMacroListener;
import fr.unicaen.aasuite.operations.Macro;
import fr.unicaen.aasuite.operations.OperationAdapters;
import fr.unicaen.aasuite.sessions.Session;
import fr.unicaen.aasuite.userinput.ConstraintBuilders;
import fr.unicaen.aasuite.userinput.ConversionBuilders;
import fr.unicaen.aasuite.userinput.TransformBuilders;

/**
 * A handler for a frame allowing the user to design a new macro (and apply and
 * save it).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings.
 */
public class MacroDesignerHandler extends AuxiliaryFrameHandler {

	/** The title for the frame. */
	public static final String TITLE = "Design macro";

	/** The label for the "apply" button. */
	public static final String APPLY_LABEL = "Apply";

	/** The label for the "save" button. */
	public static final String SAVE_LABEL = "Save";

	/** The title for the pane displaying the current macro. */
	public static final String CURRENT_MACRO_TITLE = "Current macro";

	/** The description of an empty macro. */
	public static final String EMPTY_MACRO_DESCRIPTION = "No operation";

	/** The title for the input allowing to configure an operation. */
	public static final String INPUT_PANE_TITLE = "Configure operation";

	/** The label for the "remove last operation" button. */
	public static final String REMOVE_LAST_LABEL = "Remove last operation";

	/** The title for the pane displaying the operations available for addition. */
	public static final String AVAILABLE_OPERATIONS_TITLE = "Extend macro";

	/** The prompt for adding a constraint on nucleotide sequences. */
	public static final String ADD_NUCLEOTIDE_CONSTRAINT = "Filters on DNA sequences";

	/** The prompt for adding a transform on nucleotide sequences. */
	public static final String ADD_NUCLEOTIDE_TRANSFORM = "Transformations on DNA sequences";

	/** The prompt for adding a conversion on nucleotide sequences. */
	public static final String ADD_NUCLEOTIDE_CONVERSION = "Conversions on DNA sequences";

	/** The prompt for adding a constraint on amino acid sequences. */
	public static final String ADD_AMINO_ACID_CONSTRAINT = "Filters on protein sequences";

	/** The prompt for adding a constraint on amino acid sequences. */
	public static final String ADD_AMINO_ACID_TRANSFORM = "Transformations on protein sequences";

	/** The block increment for the vertical scrollbar. */
	public static int SCROLLBAR_BLOCK_INCREMENT = 100;

	/** The unit increment for the vertical scrollbar. */
	public static int SCROLLBAR_UNIT_INCREMENT = 10;

	/**
	 * The multiplier to apply to the height of the parent frame to obtain the
	 * height of this one.
	 */
	public static float heightMultiplier = 0.75f;

	/**
	 * The multiplier to apply to the width of the parent frame to obtain the width
	 * of this one.
	 */
	public static float widthMultiplier = 0.5f;

	/** The pane holding the current macro and available operations to add. */
	private JPanel mainPane;

	/** The current version of the macro. */
	private final Macro macro;

	/** The "save" button. */
	private final JButton saveButton;

	/** The "apply" button. */
	private final JButton applyButton;

	/** The current session. */
	protected final Session session;

	/** A proposer for interacting with the user. */
	protected final Proposer proposer;

	/** A handler for choosing the output file where to save the macro. */
	protected final FileHandler fileHandler;

	/** The current version of PepTraq. */
	protected final String peptraqVersion;
	
	/**
	 * Builds a new instance and shows the frame.
	 * 
	 * @param session        The current session
	 * @param proposer       A proposer for interacting with the user
	 * @param fileHandler    A handler for choosing the output file where to save
	 *                       the macro
	 * @param peptraqVersion The current version of PepTraq
	 * @param parentBounds   The bounds of the parent frame
	 */
	public MacroDesignerHandler(Session session, Proposer proposer, FileHandler fileHandler, String peptraqVersion,
			Rectangle parentBounds) {
		super(TITLE, AuxiliaryFrameHandler.centeredBounds(parentBounds, widthMultiplier, heightMultiplier));
		this.macro = new Macro();
		this.saveButton = new JButton(SAVE_LABEL);
		this.applyButton = new JButton(APPLY_LABEL);
		this.session = session;
		this.proposer = proposer;
		this.fileHandler = fileHandler;
		this.peptraqVersion = peptraqVersion;
	}

	@Override
	protected Component makeMainPane() {
		this.mainPane = new JPanel();
		this.mainPane.setLayout(new BorderLayout());
		this.update();
		return this.mainPane;
	}

	@Override
	protected List<JButton> makeButtons() {
		this.saveButton.setEnabled(false);
		this.applyButton.setEnabled(false);
		SaveMacroListener saveMacroListener = new SaveMacroListener(this.macro, this.fileHandler, this.peptraqVersion);
		this.saveButton.addActionListener(e -> saveMacroListener.actionPerformed());
		this.applyButton.addActionListener(e -> this.session.addMacro(this.macro));
		return Arrays.asList(this.saveButton, this.applyButton);
	}

	/**
	 * Helper method: updates the main pane.
	 */
	private void update() {
		SwingUtilities.invokeLater(() -> {
			this.mainPane.removeAll();
			JComponent macroPane = this.createMacroPane();
			this.mainPane.add(macroPane);
			JComponent operationPane = this.createOperationPane();
			this.mainPane.add(operationPane, BorderLayout.SOUTH);
			boolean applicable = (this.session.getModel().isDNA() && this.macro.appliesToDNA())
					|| (this.session.getModel().isProteins() && this.macro.appliesToProteins());
			this.saveButton.setEnabled(!this.macro.isEmpty());
			this.applyButton.setEnabled(!this.macro.isEmpty() && applicable);
			this.mainPane.revalidate();
		});
	}

	/**
	 * Helper method: creates and returns a component displaying the current macro.
	 * 
	 * @return A component
	 */
	private JComponent createMacroPane() {
		JPanel macroPane = new JPanel();
		macroPane.setBorder(BorderFactory.createTitledBorder(CURRENT_MACRO_TITLE));
		macroPane.setLayout(new GridLayout(0, 1));
		if (macro.isEmpty()) {
			macroPane.add(new JLabel(EMPTY_MACRO_DESCRIPTION));
		} else {
			for (Object operation : macro.getOperations()) {
				macroPane.add(new JLabel(operation.toString()));
			}
		}
		if (!macro.getOperations().isEmpty()) {
			JButton removeLast = this.makeRemoveLastButton();
			macroPane.add(removeLast);
		}
		final JPanel pane = new FullWidthScrollablePanel(MacroDesignerHandler.SCROLLBAR_BLOCK_INCREMENT,
				MacroDesignerHandler.SCROLLBAR_BLOCK_INCREMENT);
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		pane.add(macroPane);
		final JScrollPane scrollPane = new JScrollPane(pane);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		return scrollPane;
	}

	/**
	 * Helper method: creates and returns a component displaying available
	 * operations (for addition to the current macro).
	 * 
	 * @return A component
	 */
	private JComponent createOperationPane() {
		JPanel operationPane = new JPanel();
		operationPane.setBorder(BorderFactory.createTitledBorder(AVAILABLE_OPERATIONS_TITLE));
		operationPane.setLayout(new GridLayout(0, 2));
		if (this.macro.isEmpty() || this.macro.producesDNA()) {
			this.addComponents(operationPane, ADD_NUCLEOTIDE_CONSTRAINT, ConstraintBuilders.DNA_CONSTRAINT_TYPES,
					t -> ConstraintBuilders.getDNABuilder(t, true),
					o -> this.macro.addDNAOperation(OperationAdapters.forConstraint(o)));
			this.addComponents(operationPane, ADD_NUCLEOTIDE_TRANSFORM, TransformBuilders.NUCLEOTIDE_TRANSFORM_TYPES,
					TransformBuilders::getDNABuilder,
					o -> this.macro.addDNAOperation(OperationAdapters.forTransform(o)));
			this.addComponents(operationPane, ADD_NUCLEOTIDE_CONVERSION, ConversionBuilders.NUCLEOTIDE_CONVERSION_TYPES,
					ConversionBuilders::getDNABuilder, this.macro::addDNAToProteinConversion);
		}
		if (this.macro.isEmpty() || this.macro.producesProteins()) {
			this.addComponents(operationPane, ADD_AMINO_ACID_CONSTRAINT, ConstraintBuilders.PROTEIN_CONSTRAINT_TYPES,
					t -> ConstraintBuilders.getProteinBuilder(t, true),
					o -> this.macro.addProteinOperation(OperationAdapters.forConstraint(o)));
			this.addComponents(operationPane, ADD_AMINO_ACID_TRANSFORM, TransformBuilders.AMINO_ACID_TRANSFORM_TYPES,
					TransformBuilders::getProteinBuilder,
					o -> this.macro.addProteinOperation(OperationAdapters.forTransform(o)));
		}
		return operationPane;
	}

	/**
	 * Helper method: adds a selection list and an "add" button for operations to a
	 * pane.
	 * 
	 * @param pane           The pane where to add the components
	 * @param prompt         A string indicating the type of operations
	 * @param types          The available types of operations
	 * @param builderFactory A factory for operation builders from types
	 * @param handler        A handler for operations
	 * @param                <T> The type for operation types
	 * @param                <O> The type for operations
	 */
	private <T, O> void addComponents(JPanel pane, String prompt, Iterable<T> types,
			Function<T, AbstractBuilder<InputField<String, ?>, O>> builderFactory, Consumer<O> handler) {
		JComboBox<BuilderWithType<O>> list = new JComboBox<>();
		list.setRenderer((a, builderWithType, c, d, e) -> new JLabel(builderWithType.operationDescription));
		for (final T type : types) {
			list.addItem(new BuilderWithType<>(type.toString(), builderFactory.apply(type)));
		}
		pane.add(new JLabel(prompt));
		pane.add(list);
		ConstructionListener<O> constructionListener = new ConstructionListener<O>(MacroDesignerHandler.this.proposer,
				INPUT_PANE_TITLE) {

			@Override
			protected void handle(O operation) {
				handler.accept(operation);
				MacroDesignerHandler.this.update();
			}

			@Override
			protected AbstractBuilder<InputField<String, ?>, O> make() {
				return list.getItemAt(list.getSelectedIndex()).builder;
			}
		};
		list.addActionListener(e -> constructionListener.actionPerformed());
	}

	/**
	 * Helper method: creates and returns a button for removing the last operation
	 * in the macro.
	 * 
	 * @return A JButton
	 */
	private JButton makeRemoveLastButton() {
		JButton res = MainFrame.makeClickableText(REMOVE_LAST_LABEL);
		res.addActionListener(e -> {
			this.macro.removeLast();
			MacroDesignerHandler.this.update();
		});
		return res;
	}

	/**
	 * A helper class wrapping together a builder and the description of the
	 * associated operation type.
	 * 
	 * @param <O> The type of operations built
	 */
	private static class BuilderWithType<O> {

		/** The description of the type of operations built. */
		private final String operationDescription;

		/** A builder. */
		private final AbstractBuilder<InputField<String, ?>, O> builder;

		/**
		 * Builds a new instance.
		 * 
		 * @param operationDescription The description of the type of operations built
		 * @param builder              A builder
		 */
		public BuilderWithType(String operationDescription, AbstractBuilder<InputField<String, ?>, O> builder) {
			this.operationDescription = operationDescription;
			this.builder = builder;
		}

	}

}
