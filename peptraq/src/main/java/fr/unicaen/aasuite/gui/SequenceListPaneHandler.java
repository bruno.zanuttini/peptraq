package fr.unicaen.aasuite.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

import fr.greyc.mad.datastructures.Page;
import fr.greyc.mad.observable.Observer;
import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.swingutils.FullWidthScrollablePanel;
import fr.unicaen.aasuite.control.EditAnnotationListener;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.HistoryProvider;
import fr.unicaen.aasuite.sessions.SequenceChange;
import fr.unicaen.aasuite.sessions.SequenceChange.SequenceChangeType;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A handler for a pane displaying a list of sequences. Such a handler is able
 * to display sequences using pagination and controls on a graphical pane. A
 * handler state consists of sequences to display (in general, not all of them
 * are effectively displayed, due to pagination), a number of sequences to
 * display on each page (or "all"), and a current page.
 * 
 * The instance is said "to have pagination" when its current state contains a
 * number of sequences to display on each page (as opposed to the case of
 * "all").
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for strings, colors, etc.
 */
public class SequenceListPaneHandler implements Observer<SequenceChange> {

	/** The background color for the sequence pane. */
	public static Color BACKGROUND_COLOR = Color.WHITE;

	/** The block increment for the vertical scrollbar. */
	public static int SCROLLBAR_BLOCK_INCREMENT = 100;

	/** The unit increment for the vertical scrollbar. */
	public static int SCROLLBAR_UNIT_INCREMENT = 10;

	/** The label for a pane with no sequence. */
	public static final String NO_SEQUENCE_LABEL = "No sequence";

	/** The label for the button to the first page. */
	public static final String FIRST_PAGE_BUTTON_LABEL = "first";

	/** The label for the button to the previous page. */
	public static final String PREVIOUS_PAGE_BUTTON_LABEL = "prev";

	/** The label for the button to the next page. */
	public static final String NEXT_PAGE_BUTTON_LABEL = "next";

	/** The label for the button to the last page. */
	public static final String LAST_PAGE_BUTTON_LABEL = "last";

	/** The label for the page description when the number of pages is known. */
	public static final String KNOWN_NB_PAGES_PAGE_DESCRIPTION = "Page %d of %d, sequences %d - %d";

	/** The label for the page description when the number of pages is unknown. */
	public static final String DEFAULT_PAGE_DESCRIPTION = "Page %d, sequences %d - %d";

	/** The label for the button allowing to select a sequence. */
	public static final String SELECT = "select";

	/** The label for the button allowing to unselect a sequence. */
	public static final String UNSELECT = "unselect";

	/** The label for the button allowing to open a sequence in a new window. */
	public static final String OPEN = "open in new window";

	/** The label for the button allowing to edit the annotation of a sequence. */
	public static final String EDIT_ANNOTATION = "edit annotation";

	/**
	 * The label for the button allowing to restore the initial annotation of a
	 * sequence.
	 */
	public static final String RESTORE_ANNOTATION = "restore initial annotation";

	/** The label for the total number of sequences (if available). */
	public static final String NB_SEQUENCES = "%s sequences (%s selected)";

	/** The sequences currently handled. */
	private Iterable<? extends AnnotatedSequence<?>> sequences;

	/** A provider of history for sequences. */
	private HistoryProvider historyProvider;

	/** The number of sequences to display per page (null for all). */
	private Integer nbSequencesPerPageOrNull;

	/** The current page displayed (0 if no sequence is handled). */
	private int page;

	/** The sequence pane handlers for the sequences currently displayed. */
	private Map<AnnotatedSequence<?>, SequencePaneHandler> sequencePaneHandlers;

	/** The control panes for the sequences currently displayed. */
	private Map<AnnotatedSequence<?>, JPanel> controlPanes;

	/** The JLabel for the pane title. */
	private JLabel paneTitle;

	/** The current session. */
	protected final Session session;

	/** The pane in which to display the sequences and controls. */
	protected final JPanel pane;

	/** A proposer for interaction with the user by some listeners. */
	protected final GUIProposer proposer;

	/**
	 * Builds a new instance with no sequence and no pagination (all sequences to be
	 * displayed on one page).
	 * 
	 * @param session  The current session
	 * @param pane     The pane in which to display the sequences and controls
	 * @param proposer A proposer for interaction with the user by some listeners
	 */
	public SequenceListPaneHandler(Session session, JPanel pane, GUIProposer proposer) {
		this(session, null, pane, proposer);
	}

	/**
	 * Builds a new instance with no sequence and a given number of sequences to
	 * display per page.
	 * 
	 * @param session            The current session
	 * @param pane               The pane in which to display the sequences and
	 *                           controls
	 * @param proposer           A proposer for interaction with the user by some
	 *                           listeners
	 * @param nbSequencesPerPage The number of sequences to display on one page
	 * @throws IllegalArgumentException if the number of sequences per page is not
	 *                                  (strictly) positive
	 */
	public SequenceListPaneHandler(Session session, JPanel pane, GUIProposer proposer, int nbSequencesPerPage)
			throws IllegalArgumentException {
		this(session, nbSequencesPerPage, pane, proposer);
	}

	/**
	 * Decides whether this instance has pagination.
	 * 
	 * @return true if this instance has pagination, false otherwise
	 */
	public boolean isPaginated() {
		return this.nbSequencesPerPageOrNull != null;
	}

	/**
	 * Returns the number of sequences to be displayed on one page.
	 * 
	 * @return The number of sequences to be displayed on one page
	 * @throws IllegalStateException if this instance does not have pagination
	 */
	public int getNbSequencesPerPage() throws IllegalStateException {
		if (this.nbSequencesPerPageOrNull == null) {
			throw new IllegalStateException("This instance is not paginated");
		}
		return this.nbSequencesPerPageOrNull;
	}

	/**
	 * Updates the sequences and goes to the first page.
	 * 
	 * @param sequences       The sequences to handle
	 * @param historyProvider A provider of history for the sequences
	 */
	public void update(Iterable<? extends AnnotatedSequence<?>> sequences, HistoryProvider historyProvider) {
		this.sequences = sequences;
		this.historyProvider = historyProvider;
		this.toPage(0);
	}

	/**
	 * Updates the number of sequences per page and goes to the first page.
	 * 
	 * @param nbSequencesPerPage The number of sequences to display on one page
	 * @throws IllegalArgumentException if the number of sequences per page is not
	 *                                  (strictly) positive
	 */
	public void paginate(int nbSequencesPerPage) throws IllegalArgumentException {
		if (nbSequencesPerPageOrNull != null && nbSequencesPerPageOrNull <= 0) {
			throw new IllegalArgumentException(
					"Only a (strictly) positive number of sequences can be displayed per page, found "
							+ nbSequencesPerPageOrNull);
		}
		this.nbSequencesPerPageOrNull = nbSequencesPerPage;
		this.toPage(0);
	}

	/**
	 * Changes the number of sequences to display on one page to "all sequences".
	 */
	public void removePagination() {
		this.nbSequencesPerPageOrNull = null;
		this.toPage(0);
	}

	/**
	 * Changes the current page.
	 * 
	 * @param page A page number
	 * @throws IndexOutOfBoundsException if the page number is not valid
	 */
	public void toPage(int page) throws IndexOutOfBoundsException {
		this.page = page;
		this.update();
	}

	@Override
	public void onNotification(BasicObserved<SequenceChange> notifier, SequenceChange argument) {
		SwingUtilities.invokeLater(() -> {
			// Update individual sequence panes
			if (argument.getType().equals(SequenceChangeType.ALL_SELECTED)
					|| argument.getType().equals(SequenceChangeType.ALL_DESELECTED)) {
				for (Map.Entry<?, SequencePaneHandler> entry : this.sequencePaneHandlers.entrySet()) {
					entry.getValue().update();
				}
				for (Map.Entry<AnnotatedSequence<?>, JPanel> entry : this.controlPanes.entrySet()) {
					this.setControlPane(entry.getValue(), entry.getKey());
				}
			} else {
				if (this.sequencePaneHandlers.containsKey(argument.getSequence())) {
					this.sequencePaneHandlers.get(argument.getSequence()).update();
				}
				if (this.controlPanes.containsKey(argument.getSequence())) {
					this.setControlPane(controlPanes.get(argument.getSequence()), argument.getSequence());
				}
			}
			// Update pane title if number of selected sequences changes
			if (!argument.getType().equals(SequenceChangeType.RENAMED)) {
				String title = this.getPaneTitle();
				if (title != null) {
					if (this.paneTitle == null) {
						this.paneTitle = new JLabel();
					}
					this.paneTitle.setText(this.getPaneTitle());
				}
			}
		});
	}

	/**
	 * Updates the display and controls so as to reflect the current state of this
	 * handler. The update indeed occurs even if the state has not changed.
	 * 
	 * @throws IndexOutOfBoundsException if the current page number is not valid
	 */
	protected void update() throws IndexOutOfBoundsException {
		SwingUtilities.invokeLater(() -> {
			this.pane.removeAll();
			this.sequencePaneHandlers = new HashMap<>();
			this.controlPanes = new HashMap<>();
			String title = this.getPaneTitle();
			if (title != null) {
				this.paneTitle = new JLabel();
				this.pane.add(this.paneTitle, BorderLayout.NORTH);
				this.paneTitle.setText(this.getPaneTitle());
			}
			if (!this.sequences.iterator().hasNext()) {

				// Case of no sequence
				final JLabel label = new JLabel(NO_SEQUENCE_LABEL);
				label.setVerticalAlignment(SwingConstants.CENTER);
				label.setHorizontalAlignment(SwingConstants.CENTER);
				this.pane.add(label, BorderLayout.CENTER);
				this.pane.revalidate();

			} else {

				// Scrollable panel
				final JPanel subPanel = new FullWidthScrollablePanel(SequenceListPaneHandler.SCROLLBAR_BLOCK_INCREMENT,
						SequenceListPaneHandler.SCROLLBAR_UNIT_INCREMENT);
				subPanel.setLayout(new BoxLayout(subPanel, BoxLayout.Y_AXIS));
				final JScrollPane scrollPane = new JScrollPane(subPanel);
				this.pane.add(scrollPane, BorderLayout.CENTER);

				// Sequences
				if (this.nbSequencesPerPageOrNull == null) {
					// Case of no pagination
					int sequenceIndex = 0;
					for (AnnotatedSequence<?> sequence : this.sequences) {
						this.addSequence(sequence, sequenceIndex++, subPanel);
					}
				} else {
					// Case of pagination
					final Page<? extends AnnotatedSequence<?>> currentPage = new Page<>(this.sequences, this.page,
							this.nbSequencesPerPageOrNull);

					// Sequences
					int sequenceIndex = currentPage.getFirstElementIndex();
					for (AnnotatedSequence<?> sequence : currentPage.getPageElements()) {
						this.addSequence(sequence, sequenceIndex++, subPanel);
					}

					// Navigation controls
					JPanel navigation = this.getNavigationPane(currentPage);
					this.pane.add(navigation, BorderLayout.SOUTH);
				}

			}
		});
	}

	/**
	 * Helper method: returns a listener whose action is to update this handler to a
	 * given page. Note that there is no control of whether the page number is
	 * valid.
	 * 
	 * @param page A page
	 * @return A listener whose action is to update this handler to a given page
	 */
	private ActionListener pageListener(final int page) {
		return new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				SequenceListPaneHandler.this.toPage(page);
			}
		};
	}

	/**
	 * Returns a navigation panel for the page currently displayed.
	 * 
	 * @param currentPage The page currently displayed
	 * @return A navigation panel for the page currently displayed
	 */
	protected JPanel getNavigationPane(Page<?> currentPage) {

		JPanel res = new JPanel();

		JButton first = new JButton(FIRST_PAGE_BUTTON_LABEL);
		if (this.page != 0) {
			first.addActionListener(this.pageListener(0));
		} else {
			first.setEnabled(false);
		}
		res.add(first);

		JButton previous = new JButton(PREVIOUS_PAGE_BUTTON_LABEL);
		if (currentPage.hasPreviousPage()) {
			previous.addActionListener(this.pageListener(currentPage.getPreviousPageNumber()));
		} else {
			previous.setEnabled(false);
		}
		res.add(previous);

		int firstSequence = currentPage.getFirstElementIndex() + 1;
		int lastSequence = currentPage.getLastElementIndex();
		String label = null;
		if (this.sequences instanceof Collection) {
			label = String.format(KNOWN_NB_PAGES_PAGE_DESCRIPTION, (page + 1),
					Page.getNbPages((Collection<?>) this.sequences, this.nbSequencesPerPageOrNull), firstSequence,
					lastSequence);
		} else {
			label = String.format(DEFAULT_PAGE_DESCRIPTION, (page + 1), firstSequence, lastSequence);
		}
		JLabel current = new JLabel(label);
		res.add(current);

		JButton next = new JButton(NEXT_PAGE_BUTTON_LABEL);
		if (currentPage.hasNextPage()) {
			next.addActionListener(this.pageListener(currentPage.getNextPageNumber()));
		} else {
			next.setEnabled(false);
		}
		res.add(next);

		if (this.sequences instanceof Collection) {
			int nbPages = Page.getNbPages((Collection<?>) this.sequences, this.nbSequencesPerPageOrNull);
			JButton last = new JButton(LAST_PAGE_BUTTON_LABEL);
			if (this.page != nbPages - 1) {
				last.addActionListener(this.pageListener(nbPages - 1));
			} else {
				last.setEnabled(false);
			}
			res.add(last);
		}

		return res;
	}

	/**
	 * Helper method: prepares a text pane, controls, and separator for a given
	 * sequence, and adds them to the container displaying the sequences.
	 * 
	 * @param sequence      A sequence
	 * @param sequenceIndex The index of the sequence in the underlying session
	 *                      (0-indexed)
	 * @param container     The container displaying the sequences
	 */
	private void addSequence(AnnotatedSequence<?> sequence, int sequenceIndex, Container container) {
		final JTextPane textPane = new JTextPane();
		textPane.setBackground(BACKGROUND_COLOR);
		container.add(textPane);
		this.sequencePaneHandlers.put(sequence, new SequencePaneHandler(textPane, this.session, sequence,
				this.historyProvider.getLastOperationInformation(sequence), true, false, false));
		JPanel controlPane = new JPanel();
		this.setControlPane(controlPane, sequence);
		this.controlPanes.put(sequence, controlPane);
		container.add(controlPane);
		container.add(new JSeparator());
	}

	/**
	 * Heper method: sets the whole content of a control panel for a given sequence.
	 * 
	 * @param pane     The pane to set
	 * @param sequence A sequence
	 */
	private void setControlPane(JPanel pane, final AnnotatedSequence<?> sequence) {
		pane.removeAll();

		// Control for selecting/unselecting this sequence
		boolean wasSelected = SequenceListPaneHandler.this.session.getModel().isSelected(sequence);
		JButton button = MainFrame.makeClickableText(wasSelected ? UNSELECT : SELECT);
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				SequenceListPaneHandler.this.session.getModel().select(sequence, !wasSelected);
			}
		});

		pane.add(button);

		// Control for opening this sequence in a new window
		JButton open = MainFrame.makeClickableText(OPEN);
		open.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				new DetailsHandler(SequenceListPaneHandler.this.session, sequence,
						SequenceListPaneHandler.this.historyProvider.getHistory(sequence),
						SequenceListPaneHandler.this.pane.getBounds()).setVisible(true);
			}
		});
		pane.add(open);

		// Control for editing the annotation of this sequence
		JButton edit = MainFrame.makeClickableText(EDIT_ANNOTATION);
		EditAnnotationListener editAnnotationListener = new EditAnnotationListener(this.session, sequence,
				this.proposer);
		edit.addActionListener(e -> editAnnotationListener.actionPerformed());
		pane.add(edit);

		// Control for setting the annotation of this sequence to the original one (if
		// applicable)
		JButton restoreAnnotation = MainFrame.makeClickableText(RESTORE_ANNOTATION);
		if (!sequence.getAnnotation().equals(sequence.getOriginalHeader())) {
			restoreAnnotation.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent arg0) {
					session.rename(sequence, sequence.getOriginalHeader());
				}
			});
		} else {
			restoreAnnotation.setEnabled(false);
		}
		pane.add(restoreAnnotation);

		pane.setBackground(BACKGROUND_COLOR);
	}

	/**
	 * Helper method: returns the title to be used for the pane, depending on the
	 * current status, or null if there is no relevant title.
	 * 
	 * @return The title to be used for the pane
	 */
	private String getPaneTitle() {
		if (this.sequences instanceof Collection && !((Collection<?>) this.sequences).isEmpty()) {
			int nb = ((Collection<?>) sequences).size();
			int nbSelected = this.session.getModel().nbSelected();
			return String.format(NB_SEQUENCES, nb, nbSelected);
		} else {
			return null;
		}
	}

	/**
	 * Builds a new instance with no sequence.
	 * 
	 * @param session                  The current session
	 * @param nbSequencesPerPageOrNull The number of sequences to display on one
	 *                                 page, or null for displaying all on one page
	 * @param pane                     The pane in which to display the sequences
	 *                                 and controls
	 * @param proposer                 A proposer for interaction with the user by
	 *                                 some listeners
	 */
	private SequenceListPaneHandler(Session session, Integer nbSequencesPerPageOrNull, JPanel pane,
			GUIProposer proposer) throws IllegalArgumentException {
		if (nbSequencesPerPageOrNull != null && nbSequencesPerPageOrNull <= 0) {
			throw new IllegalArgumentException(
					"Only a (strictly) positive number of sequences can be displayed per page, found "
							+ nbSequencesPerPageOrNull);
		}
		this.sequences = Collections.emptyList();
		this.nbSequencesPerPageOrNull = nbSequencesPerPageOrNull;
		this.session = session;
		this.pane = pane;
		this.pane.setLayout(new BorderLayout());
		this.proposer = proposer;
		this.session.addSequenceChangeObserver(this);
		this.toPage(0);
	}

}
