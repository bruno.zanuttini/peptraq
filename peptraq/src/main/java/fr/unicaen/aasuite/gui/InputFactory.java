package fr.unicaen.aasuite.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import fr.greyc.mad.userinput.InputField;

/**
 * A factory for building graphical input components from (string) input fields.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class InputFactory {

	/**
	 * Builds and returns a graphical input component from an input field.
	 * 
	 * @param field An input field
	 * @return A graphical input component for the given input field
	 */
	public static Input buildFor(InputField<String, ?> field) {
		if (field.hasBoundValues()) {
			return new SelectInput(field);
		} else {
			return new TextInput(field);
		}
	}

	/**
	 * A class for input components for free (one-line) text. Null is never returned
	 * by {@link #getValue()} (a blank string is returned if no text has been
	 * input).
	 */
	public static class TextInput implements Input {

		/** A JTextField to which work is delegated. */
		private JTextField textField;

		/**
		 * Builds a new instance for a given input field.
		 * 
		 * @param field An input field
		 */
		public TextInput(final InputField<String, ?> field) {
			this.textField = new JTextField();
			this.textField.setText(field.hasInput() ? field.getInput() : "");
			this.textField.getDocument().addDocumentListener(new DocumentListener() {

				private void update(DocumentEvent event) {
					field.setInput(TextInput.this.textField.getText());
				}

				@Override
				public void removeUpdate(DocumentEvent event) {
					this.update(event);
				}

				@Override
				public void insertUpdate(DocumentEvent event) {
					this.update(event);
				}

				@Override
				public void changedUpdate(DocumentEvent event) {
					this.update(event);
				}
			});
		}

		@Override
		public String getValue() {
			return this.textField.getText();
		}

		@Override
		public JComponent asJComponent() {
			return this.textField;
		}

	};

	/**
	 * A class for input components for choosing among a bounded number of strings.
	 * When no item is selected, either null or {@link #NONE} may be returned by
	 * {@link #getValue()}.
	 */
	public static class SelectInput implements Input {

		/** The value for no input when the field is optional. */
		public final static String NONE = "";

		/** A JComboBox to which work is delegated. */
		private JComboBox<String> selectField;

		/** The underlying input field. */
		protected final InputField<String, ?> field;

		/**
		 * Builds a new instance for a given input field with bounded values.
		 * 
		 * @param field An input field
		 * @throws IllegalArgumentException if the field does not have bounded values
		 * @param <V> The type for values
		 */
		public <V> SelectInput(final InputField<String, V> field) {
			int nbValues;
			try {
				nbValues = field.getValidValues().size();
			} catch (UnsupportedOperationException exception) {
				throw new IllegalArgumentException("Field does not have bounded values: " + field, exception);
			}
			if (field.isOptional()) {
				nbValues++;
			}
			String[] allowedValues = new String[nbValues];
			int i = 0;
			if (field.isOptional()) {
				allowedValues[0] = NONE;
			}
			for (V value : field.getValidValues()) {
				allowedValues[i++] = field.getInputFor(value);
			}
			this.selectField = new JComboBox<>(allowedValues);
			if (field.hasInput()) {
				this.selectField.setSelectedItem(field.getInput());
			} else {
				this.selectField.setSelectedIndex(0);
				if (!field.isOptional()) {
					field.setInput(allowedValues[0]);
				}
			}
			this.selectField.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent event) {
					field.setInput(
							SelectInput.this.selectField.getItemAt(SelectInput.this.selectField.getSelectedIndex()));
				}
			});
			this.field = field;
		}

		@Override
		public String getValue() {
			String res = this.selectField.getItemAt(this.selectField.getSelectedIndex());
			return NONE.equals(res) ? null : res;
		}

		@Override
		public JComponent asJComponent() {
			return this.selectField;
		}

	}

}
