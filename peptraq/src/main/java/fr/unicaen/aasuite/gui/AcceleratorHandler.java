package fr.unicaen.aasuite.gui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.AbstractButton;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

/**
 * A handler for mnemonics and accelerators of menus, menu items, and buttons in
 * a common application. The handler registers mnemonics and accelerators while
 * ensuring that there are no ambiguous bindings.
 * 
 * As a general rule, mnemonics are represented by VK_xxx objects of the
 * KeyEvent class, and are context-dependent: the mnemonic for a menu item does
 * not conflict with that for another item, provided that they appear in
 * different menus. On the other hand, mnemonics for the (top-level) menus
 * themselves are in conflict with each other.
 * 
 * Accelerators are represented by a character, to be pressed with a modifier,
 * the same for all accelerators. In contrast with mnemonics, these are not
 * context-dependent, and the accelerators of two different objects must always
 * be different.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: move to generic package
 */
public class AcceleratorHandler {

	/** A class for grouping together a mnemonic and an accelerator. */
	public static class Keys {

		/** A mnemonic, as a VK_xxx object of the KeyEvent class. */
		public final int mnemonic;

		/** An accelerator, intended to be used in conjunction with a modifier. */
		public final char accelerator;

		/**
		 * Builds a new instance.
		 * 
		 * @param mnemonic    A mnemonic, as a VK_xxx object of the KeyEvent class
		 * @param accelerator An accelerator, intended to be used in conjunction with a
		 *                    modifier
		 */
		public Keys(int mnemonic, char accelerator) {
			this.mnemonic = mnemonic;
			this.accelerator = accelerator;
		}
	}

	/**
	 * A cache of mnemonics already assigned, by context (null for the top-level
	 * context).
	 */
	private final Map<Object, Map<Integer, Object>> mnemonics;

	/** A cache of accelerators already assigned. */
	private final Map<Character, Object> accelerators;

	/** A modifier for all accelerators. */
	protected final int modifier;

	/**
	 * Builds a new instance.
	 * 
	 * @param modifier A modifier for all accelerators (for instance,
	 *                 InputEvent.CTRL_DOWN_MASK)
	 */
	public AcceleratorHandler(int modifier) {
		this.mnemonics = new HashMap<>();
		this.accelerators = new HashMap<>();
		this.modifier = modifier;
	}

	/**
	 * Assigns a mnemonic to a menu, menu item, or abstract button at the top-level.
	 * 
	 * @param component A component
	 * @param mnemonic  A mnemonic
	 * @throws IllegalArgumentException if the mnemonic is already assigned to
	 *                                  another object at the top-level
	 */
	public void assign(AbstractButton component, int mnemonic) throws IllegalArgumentException {
		this.assignMnemonic(component, mnemonic, null);
	}

	/**
	 * Assigns a mnemonic and an accelerator to a menu item in a certain context
	 * (typically, the enclosing menu).
	 * 
	 * FIXME: currently partly deactivated (method does not assign the accelerator) because
	 * accelerators do not work.
	 * 
	 * @param item    A menu item
	 * @param keys    A wrapper of a mnemonic and an accelerator
	 * @param context A context for the mnemonic binding
	 * @throws IllegalArgumentException if the mnemonic is already assigned to
	 *                                  another object in the same context, or the
	 *                                  accelerator is already assigned to another
	 *                                  object
	 */
	public void assign(JMenuItem item, Keys keys, Object context) throws IllegalArgumentException {
		this.assignMnemonic(item, keys.mnemonic, context);
		// this.assignAccelerator(item, keys.accelerator, context);
	}

	/**
	 * Assigns a mnemonic to a menu, menu item, or abstract button in a certain
	 * context.
	 * 
	 * @param component A component
	 * @param mnemonic  A mnemonic
	 * @param context   A context for the binding
	 * @throws IllegalArgumentException if the mnemonic is already assigned to
	 *                                  another object in the same context
	 */
	public void assignMnemonic(AbstractButton component, int mnemonic, Object context) throws IllegalArgumentException {
		if (!this.mnemonics.containsKey(context)) {
			this.mnemonics.put(context, new HashMap<>());
		}
		Map<Integer, Object> forContext = this.mnemonics.get(context);
		if (forContext.containsKey(mnemonic) && !forContext.get(mnemonic).equals(component)) {
			throw new IllegalArgumentException("Mnemonic " + mnemonic + " is already assigned to " + component
					+ (context == null ? "" : " in context " + context));
		} else if (!forContext.containsKey(mnemonic)) {
			forContext.put(mnemonic, component);
			component.setMnemonic(mnemonic);
		}
	}

	/**
	 * Assigns an accelerator to a menu item in a certain context.
	 * 
	 * @param item        A menu item
	 * @param accelerator An accelerator
	 * @param context     A context for the binding
	 * @throws IllegalArgumentException if the accelerator is already assigned to
	 *                                  another object
	 */
	public void assignAccelerator(JMenuItem item, char accelerator, Object context) throws IllegalArgumentException {
		if (this.accelerators.containsKey(accelerator) && !this.accelerators.get(accelerator).equals(item)) {
			throw new IllegalArgumentException("Accelerator " + accelerator + " is already assigned to " + item);
		} else if (!this.accelerators.containsKey(accelerator)) {
			this.accelerators.put(accelerator, item);
			item.setAccelerator(KeyStroke.getKeyStroke(Character.valueOf(accelerator), this.modifier));
		}
	}

}
