package fr.unicaen.aasuite.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Rectangle;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * A handler for an auxiliary frame in an application. An auxiliary frame is one
 * which can be displayed without blocking the rest of the application, and
 * closed (via a dedicated button or the Escaoe key) without exiting the
 * application.
 * <p>
 * The class is abstract: the main content and the main buttons (except for the
 * button allowing to close the frame) are to be defined by concrete subclasses.
 * <p>
 * Note that the frame is created only once, the first time when
 * {@link #setVisible(boolean)} is called. This may be important to keep in mind
 * if the creation of the content or buttons depends on the current state of
 * some object. Subclasses may override this behavior by calling
 * {@link #makeFrame()}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings.
 */
public abstract class AuxiliaryFrameHandler {

	/** The label for the "close" button. */
	public static final String CLOSE_LABEL = "Close";

	/** The displayed frame. */
	private JFrame frame;

	/** The title for this auxiliary frame. */
	protected final String title;

	/** The bounds for this auxiliary frame. */
	protected final Rectangle bounds;

	/**
	 * Builds a new instance.
	 * 
	 * @param title  The title for this auxiliary frame
	 * @param bounds The bounds for this auxiliary frame
	 */
	public AuxiliaryFrameHandler(String title, Rectangle bounds) {
		this.frame = null;
		this.title = title;
		this.bounds = bounds;
	}

	/**
	 * Shows or hides this frame, creating it if it has not been created before.
	 * 
	 * @param visible Whether to show (true) or hide (false) the frame
	 */
	public void setVisible(boolean visible) {
		if (this.frame == null) {
			this.makeFrame();
		}
		this.frame.setVisible(visible);
	}

	/**
	 * Helper method: creates the frame.
	 */
	protected void makeFrame() {

		this.frame = new JFrame(title);
		this.frame.setBounds(this.bounds);
		this.frame.getContentPane().setLayout(new BorderLayout());

		// Main content
		this.frame.getContentPane().add(this.makeMainPane());

		// Buttons
		final JPanel controlPane = new JPanel();
		JButton close = new JButton(CLOSE_LABEL);
		close.addActionListener(e -> this.frame.dispose());
		for (JButton button : this.makeButtons()) {
			controlPane.add(button);
		}
		controlPane.add(close);
		this.frame.getContentPane().add(controlPane, BorderLayout.SOUTH);

		// Dispose on pressing "Escape"
		KeyListener escapeListener = new KeyAdapter() {

			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
					AuxiliaryFrameHandler.this.frame.dispose();
				}
			}

		};
		this.frame.setFocusable(true);
		this.frame.addKeyListener(escapeListener);

	}

	/**
	 * Creates the main pane, that is, the content of this frame except for the
	 * buttons to show at the bottom.
	 * 
	 * @return The main pane
	 */
	protected abstract Component makeMainPane();

	/**
	 * Creates the buttons to be displayed at the bottom of the frame. These will be
	 * displayed in the returned order, and before the "close" button.
	 * 
	 * @return The buttons to be displayed at the bottom of the frame
	 */
	protected abstract List<JButton> makeButtons();

	/**
	 * Helper methods: returns the bounds such that an element with these bounds is
	 * centered (both horizontally and vertically) within given bounds.
	 * 
	 * @param bounds           The reference bounds
	 * @param widthMultiplier  A multiplier {@code wm} such that the element has
	 *                         width {@code wm * bounds.width}
	 * @param heightMultiplier A multiplier {@code hm} such that the element has
	 *                         height {@code hm * bounds.height}
	 * @return The bounds for the element
	 */
	protected static Rectangle centeredBounds(Rectangle bounds, float widthMultiplier, float heightMultiplier) {
		int width = (int) (bounds.width * widthMultiplier);
		int height = (int) (bounds.height * heightMultiplier);
		int x = bounds.x + (bounds.width - width) / 2;
		int y = bounds.y + (bounds.height - height) / 2;
		return new Rectangle(x, y, width, height);
	}

}
