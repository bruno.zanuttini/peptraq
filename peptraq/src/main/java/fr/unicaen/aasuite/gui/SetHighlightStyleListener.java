package fr.unicaen.aasuite.gui;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.BooleanInputFieldFromString;
import fr.greyc.mad.userinput.BoundValueField;
import fr.greyc.mad.userinput.Field;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.InputFieldFromStringAdapter;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.control.ConstructionListener;

/**
 * A listener for setting the highlighting style for displaying witnesses of
 * sequences.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings, colors, etc.
 */
public class SetHighlightStyleListener extends ConstructionListener<Object[]> {

	/** The title for the input pane. */
	public static final String TITLE = "Highlighting style";

	/** The label for the field corresponding to highlighting in/not in boldface. */
	public static final String BOLD_LABEL = "Highlight in bold face";

	/** The label for highlighting in boldface. */
	public static final String BOLD = "bold";

	/** The label for not highlighting in boldface. */
	public static final String NOT_BOLD = "normal";

	/**
	 * The label for the field corresponding to highlighting by/by not underlining.
	 */
	public static final String UNDERLINING_LABEL = "Highlight by underlining";

	/** The label for highlighting by underlining. */
	public static final String UNDERLINED = "underline";

	/** The label for not highlighting in boldface. */
	public static final String NOT_UNDERLINED = "normal";

	/** The label for the field corresponding to the highlighting color. */
	public static final String COLOR_LABEL = "Highlighting color";

	/** The label for highlighting in black. */
	public static final String BLACK = "black";

	/** The label for highlighting in blue. */
	public static final String BLUE = "blue";

	/** The label for highlighting in red. */
	public static final String RED = "red";

	/** The label for highlighting in green. */
	public static final String GREEN = "green";

	/** The help string for the builder. */
	public static String builderHelp = "Set the highlighting mode for witnesses of constraints in sequences";

	/** The help string for the bold input field. */
	public static String boldInputFieldHelp = "Check for displaying witnesses in bold face";

	/** The help string for the underlining input field. */
	public static String underliningInputFieldHelp = "Check for underlining witnesses";

	/** The help string for the color input field. */
	public static String colorInputFieldHelp = "Choose the color with which to display witnesses";

	/** A map from labels to colors. */
	private final static Map<String, Color> colors = new HashMap<>();

	/** The map from colors to labels. */
	private final static Map<Color, String> reverseColors = new HashMap<>();

	static {
		colors.put(BLACK, Color.BLACK);
		colors.put(BLUE, Color.BLUE);
		colors.put(RED, Color.RED);
		colors.put(GREEN, Color.GREEN);
		for (Map.Entry<String, Color> entry : colors.entrySet()) {
			reverseColors.put(entry.getValue(), entry.getKey());
		}
	}

	/**
	 * Builds a new instance.
	 * 
	 * @param proposer A proposer for interacting with the user
	 */
	public SetHighlightStyleListener(Proposer proposer) {
		super(proposer, TITLE);
	}

	@Override
	protected void handle(Object[] param) {
		SequencePaneHandler.setHighlighting((Boolean) (param[0]), (Boolean) (param[1]), (Color) (param[2]));
	}

	@Override
	protected AbstractBuilder<InputField<String, ?>, ? extends Object[]> make() {

		List<InputField<String, ?>> fields = new ArrayList<>();
		String initial = null;

		// Field for highlighting in/not in bold
		initial = SequencePaneHandler.isHighlightingInBold() ? BOLD : NOT_BOLD;
		fields.add(new BooleanInputFieldFromString(false, BOLD_LABEL, BOLD, NOT_BOLD, initial, boldInputFieldHelp));

		// Field for highlighting by/by not underlining
		initial = SequencePaneHandler.isHighlightingByUnderlining() ? UNDERLINED : NOT_UNDERLINED;
		fields.add(new BooleanInputFieldFromString(false, UNDERLINING_LABEL, UNDERLINED, NOT_UNDERLINED, initial,
				underliningInputFieldHelp));

		// Field for highlighting color
		Color current = SequencePaneHandler.getHighlightingColor();
		Field<Color> field = new BoundValueField<>(false, COLOR_LABEL, new ArrayList<>(colors.values()), current,
				colorInputFieldHelp);
		InputField<String, Color> inputField = new InputFieldFromStringAdapter<Color>(field,
				reverseColors.get(current)) {

			@Override
			protected Color tryGetValue(String input) throws IllegalArgumentException {
				if (!SetHighlightStyleListener.colors.containsKey(input)) {
					throw new IllegalArgumentException("Invalid input: " + input);
				}
				return SetHighlightStyleListener.colors.get(input);
			}

			@Override
			public String getInputFor(Color value) throws IllegalArgumentException {
				for (Map.Entry<String, Color> entry : SetHighlightStyleListener.colors.entrySet()) {
					if (entry.getValue().equals(value)) {
						return entry.getKey();
					}
				}
				throw new IllegalArgumentException("Invalid value: " + value);
			}
		};
		fields.add(inputField);

		// Builder
		return new AbstractBuilder<InputField<String, ?>, Object[]>(fields, builderHelp) {

			@Override
			protected Object[] safeGetObject() {
				return new Object[] { fields.get(0).getValue(), fields.get(1).getValue(), fields.get(2).getValue() };
			}

			@Override
			public boolean isGloballyValid() {
				return true;
			}

			@Override
			public List<String> getGlobalErrors() {
				return Collections.emptyList();
			}
		};
	}

}
