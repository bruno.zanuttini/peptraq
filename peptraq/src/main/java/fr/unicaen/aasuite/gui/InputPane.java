package fr.unicaen.aasuite.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JEditorPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinteraction.Proposer;

/**
 * A class for graphical components allowing the user to build an object as
 * ruled by an abstract builder. Only string inputs are supported. If the
 * builder has no field, a specific confirmation question is displayed to the
 * user anyway.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings and for the error font color
 */
public class InputPane {

	/** The label for the button for getting help about a given field. */
	public final static String FIELD_HELP_LABEL = "?";

	/** The title for the dialog displaying help on a given field. */
	public final static String FIELD_HELP_TITLE = "Help";

	/** The question to ask when the builder has no field. */
	public final static String CONFIRM_QUESTION = "Confirm?";

	/** The font color to use for displaying errors. */
	public final static Color ERROR_FONT_COLOR = Color.RED;

	/** The horizontal gap between a label and the corresponding input (in pixels). */
	public int horizontalGap = 20;
	
	/** The vertical gap between two successive fields (in pixels). */
	public int verticalGap = 0;
	
	/** The underlying builder. */
	protected final AbstractBuilder<InputField<String, ?>, ?> builder;

	/** A proposer used to display help. */
	protected final Proposer proposer;

	/** A map from fields of the builder to graphical input components. */
	private Map<InputField<String, ?>, Input> inputs;

	/**
	 * Builds a new instance.
	 * 
	 * @param builder  The underlying builder
	 * @param proposer A proposer, used to display help
	 */
	public InputPane(AbstractBuilder<InputField<String, ?>, ?> builder, Proposer proposer) {
		this.builder = builder;
		this.proposer = proposer;
		this.inputs = new HashMap<>();
	}

	/**
	 * Returns a graphical component allowing the user to build the object.
	 * 
	 * @param showErrors Whether to display the current errors of the builder (true)
	 *                   or not (false); typically, false should be used on the
	 *                   first call, and true afterwards
	 * @return A graphical component for the underlying builder
	 */
	public JComponent getAsComponent(boolean showErrors) {
		JPanel panel = new JPanel();
		List<InputField<String, ?>> fields = this.builder.getFields();
		if (fields.isEmpty() && this.builder.getGlobalErrors().isEmpty()) {
			panel.add(new JLabel(CONFIRM_QUESTION));
		} else {

			panel.setLayout(new BorderLayout());

			// Help
			String help = this.builder.getHelpString();
			panel.add(helpToComponent(help, panel.getBackground()), BorderLayout.NORTH);

			// Fields
			JPanel fieldPanel = new JPanel();
			fieldPanel.setLayout(new GridLayout(0, 2, horizontalGap, verticalGap));
			// Dummy fields to get correct vertical space and visually separate from help
			JLabel empty=new JLabel();
			fieldPanel.add(empty);
			empty.setVisible(false);
			JTextField dummy = new JTextField();
			fieldPanel.add(dummy);
			dummy.setVisible(false);
			// Real fields
			for (InputField<String, ?> field : fields) {
				fieldPanel.add(new JLabel(field.getLabel()));
				Input input = InputFactory.buildFor(field);
				this.inputs.put(field, input);
				JPanel pane = new JPanel();
				pane.setLayout(new BorderLayout());
				pane.add(input.asJComponent(), BorderLayout.CENTER);
				JButton helpButton = MainFrame.makeClickableText(FIELD_HELP_LABEL);
				helpButton.addActionListener(e -> this.proposer.inform(FIELD_HELP_TITLE, builder.getHelpString(field)));
				pane.add(helpButton, BorderLayout.EAST);
				fieldPanel.add(pane);
				if (showErrors && !field.isValid()) {
					// Display error below the input field (not the label)
					fieldPanel.add(new JPanel());
					fieldPanel.add(InputPane.errorsToJLabel(field.getErrors(), ERROR_FONT_COLOR));
				}
			}
			panel.add(fieldPanel, BorderLayout.CENTER);

			// Global errors
			List<String> globalErrors = builder.getGlobalErrors();
			if (showErrors && !globalErrors.isEmpty()) {
				panel.add(errorsToJLabel(globalErrors, ERROR_FONT_COLOR), BorderLayout.SOUTH);
			}

		}
		panel.setMinimumSize(panel.getSize());
		return panel;
	}

	/**
	 * Builds and returns a single JLabel for a nonempty list of one-line strings.
	 * 
	 * @param strings A nonempty list of one-line strings
	 * @param color   The font color for the strings
	 * @return A JLabel displaying all given strings
	 */
	private static JLabel errorsToJLabel(List<String> strings, Color color) {
		StringBuilder res = new StringBuilder();
		String separator = "";
		for (String error : strings) {
			res.append(separator);
			res.append(error);
			separator = "\n";
		}
		JLabel label = new JLabel(res.toString());
		label.setForeground(color);
		return label;
	}

	/**
	 * Builds and returns a component displaying help.
	 * 
	 * @param help       The text of the help (html String, supposed to have a
	 *                   reasonable width without wrapping).
	 * @param background The background color for the component
	 * @return A JComponent displaying the given help
	 */
	private static JComponent helpToComponent(String help, Color background) {
		JEditorPane res =new JEditorPane("text/plain", help);
		res.setEditable(false);
		res.setBackground(background);
		return res;
	}

}
