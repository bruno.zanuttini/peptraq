package fr.unicaen.aasuite.gui;

import java.util.Collections;
import java.util.List;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.control.ConstructionListener;

/**
 * A listener for setting the number of sequences to be displayed per page.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings
 */
public class SetNbSequencesPerPageListener extends ConstructionListener<Integer> {

	/** The title for the input pane. */
	public static final String TITLE = "Set number of sequences per page";

	/** The title for the input field. */
	public static final String FIELD_TITLE = "Number of sequences per page";

	/** The error message for bad input. */
	public static final String ERROR_MESSAGE = "Please enter a positive integer, or leave blank for all sequences on one page";

	/** The help string for the builder. */
	public static String builderHelp = "Set the number of sequences displayed on each page";
	
	/** The help string for the input field. */
	public static String inputFieldHelp = "Enter the number of sequences to display on each page (leave blank if all shoud be displayed on a single page)";
	
	/** The paginated frame. */
	protected MainFrame frame;

	/**
	 * Builds a new instance.
	 * 
	 * @param proposer A proposer for interacting with the user
	 * @param frame    The paginated frame
	 */
	public SetNbSequencesPerPageListener(Proposer proposer, MainFrame frame) {
		super(proposer, TITLE);
		this.frame = frame;
	}

	@Override
	protected void handle(Integer nbSequencesOrNull) {
		if (nbSequencesOrNull == null) {
			this.frame.removePagination();
		} else {
			this.frame.setNbSequencesPerPage(nbSequencesOrNull);
		}
	}

	@Override
	protected AbstractBuilder<InputField<String, ?>, Integer> make() {
		InputField<String, Integer> inputField = NumberFieldsFromString.makeIntegerField(true, FIELD_TITLE,
				this.frame.isPaginated() ? String.valueOf(this.frame.getNbSequencesPerPage()) : "", inputFieldHelp);
		final InputField<String, Integer> field = new SimpleRestrictionInputDecorator<String, Integer>(inputField,
				ERROR_MESSAGE) {

			@Override
			protected boolean isValid(Integer value) {
				return value > 0;
			}

		};
		return new AbstractBuilder<InputField<String, ?>, Integer>(Collections.singletonList(inputField), builderHelp) {

			@Override
			protected Integer safeGetObject() {
				return field.hasValue() ? field.getValue() : null;
			}

			@Override
			public boolean isGloballyValid() {
				return true;
			}

			@Override
			public List<String> getGlobalErrors() {
				return Collections.emptyList();
			}
		};
	}

}
