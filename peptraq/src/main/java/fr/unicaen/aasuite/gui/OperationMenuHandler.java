package fr.unicaen.aasuite.gui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.control.OperationListener;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.operations.ConversionType;
import fr.unicaen.aasuite.operations.OperationAdapters;
import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.sessions.Session;
import fr.unicaen.aasuite.userinput.ConstraintBuilders;
import fr.unicaen.aasuite.userinput.ConversionBuilders;
import fr.unicaen.aasuite.userinput.TransformBuilders;

/**
 * A utility class for handling the operation menus of PepTraq Desktop and the
 * associated listeners.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class OperationMenuHandler {

	/**
	 * Updates a menu so that its items reflects the constraints available on the
	 * current sequences of a given session. Sets the menu enabled or disabled
	 * depending on whether it contains at least one item or contains none.
	 * 
	 * @param menu     The menu to update
	 * @param session  The session on which the constraints must operate
	 * @param proposer A proposer for use by the listeners of the items
	 */
	public static void updateConstraintMenu(JMenu menu, Session session, Proposer proposer) {
		List<JMenuItem> items = null;
		if (session.nbSteps() == 0) {
			items = Collections.emptyList();
		} else if (session.getModel().isDNA()) {
			items = OperationMenuHandler.getDNAConstraintItems(session, proposer);
		} else {
			assert session.getModel().isProteins();
			items = OperationMenuHandler.getProteinConstraintItems(session, proposer);
		}
		OperationMenuHandler.updateMenu(menu, items);

	}

	/**
	 * Updates a menu so that its items reflects the transforms available on the
	 * current sequences of a given session. Sets the menu enabled or disabled
	 * depending on whether it contains at least one item or contains none.
	 * 
	 * @param menu     The menu to update
	 * @param session  The session on which the transforms must operate
	 * @param proposer A proposer for use by the listeners of the items
	 */
	public static void updateTransformMenu(JMenu menu, Session session, Proposer proposer) {
		List<JMenuItem> items = null;
		if (session.nbSteps() == 0) {
			items = Collections.emptyList();
		} else if (session.getModel().isDNA()) {
			items = OperationMenuHandler.getDNATransformItems(session, proposer);
		} else {
			assert session.getModel().isProteins();
			items = OperationMenuHandler.getProteinTransformItems(session, proposer);
		}
		OperationMenuHandler.updateMenu(menu, items);
	}

	/**
	 * Updates a menu so that its items reflects the conversions available on the
	 * current sequences of a given session. Sets the menu enabled or disabled
	 * depending on whether it contains at least one item or contains none.
	 * 
	 * @param menu     The menu to update
	 * @param session  The session on which the conversions must operate
	 * @param proposer A proposer for use by the listeners of the items
	 */
	public static void updateConversionMenu(JMenu menu, Session session, Proposer proposer) {
		List<JMenuItem> items = null;
		if (session.nbSteps() == 0) {
			items = Collections.emptyList();
		} else if (session.getModel().isDNA()) {
			items = OperationMenuHandler.getDNAConversionItems(session, proposer);
		} else {
			assert session.getModel().isProteins();
			items = Collections.emptyList();
		}
		OperationMenuHandler.updateMenu(menu, items);
	}

	/**
	 * Returns a list of all relevant menu items for constraints on DNA sequences,
	 * with the associated listeners.
	 * 
	 * @param session  The session on which the constraints must operate
	 * @param proposer A proposer for use by the listeners
	 * @return A list of menu items
	 */
	public static List<JMenuItem> getDNAConstraintItems(final Session session, Proposer proposer) {
		List<JMenuItem> res = new ArrayList<>();
		for (final ConstraintType type : ConstraintBuilders.DNA_CONSTRAINT_TYPES) {
			JMenuItem item = new JMenuItem(type.description);
			OperationListener<?> listener = new OperationListener<Constraint<NucleotideCompound>>(proposer) {

				@Override
				protected void apply(Constraint<NucleotideCompound> operation) throws IllegalStateException {
					session.addDNAOperation(OperationAdapters.forConstraint(operation));
				}

				@Override
				protected AbstractBuilder<InputField<String, ?>, ? extends Constraint<NucleotideCompound>> makeBuilder() {
					return ConstraintBuilders.getDNABuilder(type, true);
				}
			};
			item.addActionListener(new SwingWorkerListener(e -> listener.actionPerformed()));
			res.add(item);
		}
		return res;
	}

	/**
	 * Returns a list of all relevant menu items for transforms on DNA sequences,
	 * with the associated listeners.
	 * 
	 * @param session  The session on which the transforms must operate
	 * @param proposer A proposer for use by the listeners
	 * @return A list of menu items
	 */
	public static List<JMenuItem> getDNATransformItems(final Session session, Proposer proposer) {
		List<JMenuItem> res = new ArrayList<>();
		for (final TransformType type : TransformBuilders.NUCLEOTIDE_TRANSFORM_TYPES) {
			JMenuItem item = new JMenuItem(type.description);
			OperationListener<?> listener = new OperationListener<Transform<NucleotideCompound>>(proposer) {

				@Override
				protected void apply(Transform<NucleotideCompound> operation) throws IllegalStateException {
					session.addDNAOperation(OperationAdapters.forTransform(operation));
				}

				@Override
				protected AbstractBuilder<InputField<String, ?>, ? extends Transform<NucleotideCompound>> makeBuilder() {
					return TransformBuilders.getDNABuilder(type);
				}
			};

			item.addActionListener(new SwingWorkerListener(e -> listener.actionPerformed()));
			res.add(item);
		}
		return res;
	}

	/**
	 * Returns a list of all relevant menu items for conversions on DNA sequences,
	 * with the associated listeners.
	 * 
	 * @param session  The session on which the conversions must operate
	 * @param proposer A proposer for use by the listeners
	 * @return A list of menu items
	 */
	public static List<JMenuItem> getDNAConversionItems(final Session session, Proposer proposer) {
		List<JMenuItem> res = new ArrayList<>();
		for (final ConversionType type : ConversionBuilders.NUCLEOTIDE_CONVERSION_TYPES) {
			JMenuItem item = new JMenuItem(type.description);
			OperationListener<?> listener = new OperationListener<Conversion<NucleotideCompound, AminoAcidCompound>>(
					proposer) {

				@Override
				protected void apply(Conversion<NucleotideCompound, AminoAcidCompound> conversion)
						throws IllegalStateException {
					session.addDNAToProteinConversion(conversion);
				}

				@Override
				protected AbstractBuilder<InputField<String, ?>, ? extends Conversion<NucleotideCompound, AminoAcidCompound>> makeBuilder() {
					return ConversionBuilders.getDNABuilder(type);
				}
			};
			item.addActionListener(new SwingWorkerListener(e -> listener.actionPerformed()));
			res.add(item);
		}
		return res;
	}

	/**
	 * Returns a list of all relevant menu items for constraints on proteins, with
	 * the associated listeners.
	 * 
	 * @param session  The session on which the constraints must operate
	 * @param proposer A proposer for use by the listeners
	 * @return A list of menu items
	 */
	public static List<JMenuItem> getProteinConstraintItems(final Session session, Proposer proposer) {
		List<JMenuItem> res = new ArrayList<>();
		for (final ConstraintType type : ConstraintBuilders.PROTEIN_CONSTRAINT_TYPES) {
			JMenuItem item = new JMenuItem(type.description);
			OperationListener<?> listener = new OperationListener<Constraint<AminoAcidCompound>>(proposer) {

				@Override
				protected void apply(Constraint<AminoAcidCompound> operation) throws IllegalStateException {
					session.addProteinOperation(OperationAdapters.forConstraint(operation));
				}

				@Override
				protected AbstractBuilder<InputField<String, ?>, ? extends Constraint<AminoAcidCompound>> makeBuilder() {
					return ConstraintBuilders.getProteinBuilder(type, true);
				}
			};
			item.addActionListener(new SwingWorkerListener(e -> listener.actionPerformed()));
			res.add(item);
		}
		return res;
	}

	/**
	 * Returns a list of all relevant menu items for transforms on DNA sequences,
	 * with the associated listeners.
	 * 
	 * @param session  The session on which the transforms must operate
	 * @param proposer A proposer for use by the listeners
	 * @return A list of menu items
	 */
	public static List<JMenuItem> getProteinTransformItems(final Session session, Proposer proposer) {
		List<JMenuItem> res = new ArrayList<>();
		for (final TransformType type : TransformBuilders.AMINO_ACID_TRANSFORM_TYPES) {
			JMenuItem item = new JMenuItem(type.description);
			OperationListener<?> listener = new OperationListener<Transform<AminoAcidCompound>>(proposer) {

				@Override
				protected void apply(Transform<AminoAcidCompound> operation) throws IllegalStateException {
					session.addProteinOperation(OperationAdapters.forTransform(operation));
				}

				@Override
				protected AbstractBuilder<InputField<String, ?>, ? extends Transform<AminoAcidCompound>> makeBuilder() {
					return TransformBuilders.getProteinBuilder(type);
				}
			};
			item.addActionListener(new SwingWorkerListener(e -> listener.actionPerformed()));
			res.add(item);
		}
		return res;
	}

	/**
	 * Updates a menu with a list of items.so that its items reflects the
	 * conversions available on the current sequences of a given session. Sets the
	 * menu enabled or disabled depending on whether it contains at least one item
	 * or contains none.
	 * 
	 * @param menu  The menu to update
	 * @param items The items with which to update the menu
	 */
	protected static void updateMenu(JMenu menu, List<JMenuItem> items) {
		menu.removeAll();
		for (JMenuItem item : items) {
			menu.add(item);
		}
		menu.setEnabled(!items.isEmpty());
	}

}
