package fr.unicaen.aasuite.gui;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.SortedSet;

import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultCaret;
import javax.swing.text.Document;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import javax.swing.text.StyledDocument;

import fr.greyc.mad.datastructures.Interval;
import fr.greyc.mad.datastructures.UnionOfIntervals;
import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Observer;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A handler for a pane displaying a sequence.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for pixels, static fonts, etc.
 */
public class SequencePaneHandler extends ComponentAdapter implements Observer<Void> {

	/** The right margin in the pane, in pixels. */
	protected static int RIGHT_MARGIN = 10;

	/** The base style for all other styles. */
	private static Style baseStyle;

	/** The style for displaying the name of (selected) sequences. */
	private static Style nameStyle;

	/** The style for displaying the context of (selected) sequences. */
	private static Style contextStyle;

	/** The style for displaying (selected) sequences, apart from witnesses. */
	private static Style plainStyle;

	/** The style for displaying witnesses in (selected) sequences. */
	private static Style highlightStyle;

	/** The style for displaying the name of unselected sequences. */
	private static Style unselectedNameStyle;

	/** The style for displaying the context of unselected sequences. */
	private static Style unselectedContextStyle;

	/** The style for displaying unselected sequences, apart from witnesses. */
	private static Style unselectedPlainStyle;

	/** The style for displaying witnesses in unselected sequences. */
	private static Style unselectedHighlightStyle;

	static {
		StyleContext context = StyleContext.getDefaultStyleContext();
		baseStyle = context.new NamedStyle();
		StyleConstants.setFontFamily(baseStyle, "Sans-serif");
		nameStyle = context.new NamedStyle(baseStyle);
		StyleConstants.setBold(nameStyle, true);
		contextStyle = context.new NamedStyle(baseStyle);
		StyleConstants.setForeground(contextStyle, Color.LIGHT_GRAY);
		plainStyle = context.new NamedStyle(baseStyle);
		highlightStyle = context.new NamedStyle(baseStyle);
		StyleConstants.setUnderline(highlightStyle, true);
		Style unselectedStyle = context.new NamedStyle(baseStyle);
		StyleConstants.setForeground(unselectedStyle, Color.LIGHT_GRAY);
		SequencePaneHandler.unselectedNameStyle = unselectedStyle;
		SequencePaneHandler.unselectedContextStyle = unselectedStyle;
		SequencePaneHandler.unselectedPlainStyle = unselectedStyle;
		SequencePaneHandler.unselectedHighlightStyle = unselectedStyle;
	}

	/** A notifier for style changes. */
	private final static BasicObserved<Void> styleChangedNotifier = new BasicObserved<>();

	/** Whether the pane is currently visible. */
	private boolean isVisible;

	/** The width of the pane when the sequence was last redisplayed. */
	private int lastWidth;

	/** The pane handled by this instance. */
	protected final JTextPane pane;

	/** The current session. */
	protected final Session session;

	/** The sequence to display. */
	protected final AnnotatedSequence<?> sequence;

	/** Information about the last operation which led to the sequence. */
	protected final OperationInformation lastOperation;

	/** Whether to show the witnesses in the last operation, if any. */
	protected final boolean showWitnesses;

	/** Whether to show the context in the last operation, if any. */
	protected final boolean showContext;

	/**
	 * Whether to ignore the selection status of the sequence (displaying it always
	 * as if it was selected).
	 */
	protected final boolean ignoreUnselected;

	/**
	 * Builds a new instance.
	 * 
	 * @param pane             The pane handled by this instance
	 * @param session          The current session
	 * @param sequence         The sequence to display
	 * @param lastOperation    Information about the last operation which led to the
	 *                         sequence
	 * @param showWitnesses    Whether to show the witnesses in the last operation,
	 *                         if any
	 * @param showContext      Whether to show the context in the last operation, if
	 *                         any
	 * @param ignoreUnselected Whether to ignore when the sequence is unselected
	 *                         (displaying it always as if it was selected)
	 */
	public SequencePaneHandler(JTextPane pane, Session session, AnnotatedSequence<?> sequence,
			OperationInformation lastOperation, boolean showWitnesses, boolean showContext, boolean ignoreUnselected) {
		this.isVisible = pane.isVisible();
		this.lastWidth = pane.getWidth();
		this.pane = pane;
		this.session = session;
		this.sequence = sequence;
		this.lastOperation = lastOperation;
		this.showWitnesses = showWitnesses;
		this.showContext = showContext;
		this.ignoreUnselected = ignoreUnselected;
		this.pane.setEditable(false);
		this.pane.addComponentListener(this);
		// Prevent scrolling to the bottom of this sequence pane
		// https://stackoverflow.com/questions/3972337/java-swing-jtextarea-in-a-jscrollpane-how-to-prevent-auto-scroll#3973103
		DefaultCaret caret = (DefaultCaret) pane.getCaret();
		caret.setUpdatePolicy(DefaultCaret.NEVER_UPDATE);
		if (this.isVisible) {
			SequencePaneHandler.styleChangedNotifier.addObserver(this);
		}
	}

	/**
	 * Displays the sequence onto the pane as it is currently. Does not revalidate
	 * the pane, however.
	 */
	public void update() {
		this.lastWidth = this.pane.getWidth();
		final Document document = this.pane.getStyledDocument();
		try {
			document.remove(0, document.getLength());
		} catch (BadLocationException e) {
			throw new RuntimeException(e);
		}
		this.displaySequenceName();
		this.displaySequence();
	}

	/**
	 * Returns the font size for displayed sequences.
	 * 
	 * @return The font size used for displaying sequences
	 */
	public static int getFontSize() {
		return StyleConstants.getFontSize(SequencePaneHandler.baseStyle);
	}

	/**
	 * Sets the font size for displayed sequences.
	 * 
	 * @param fontSize The font size used for displaying sequences
	 */
	public static void setFontSize(int fontSize) {
		StyleConstants.setFontSize(SequencePaneHandler.baseStyle, fontSize);
		SequencePaneHandler.styleChangedNotifier.notify(null);
	}

	/**
	 * Decides whether highlighting in bold face is active.
	 * 
	 * @return true if highlighting in bold face is active, false otherwise
	 */
	public static boolean isHighlightingInBold() {
		return StyleConstants.isBold(SequencePaneHandler.highlightStyle);
	}

	/**
	 * Decides whether highlighting by underlining is active.
	 * 
	 * @return true if highlighting by underlining is active, false otherwise
	 */
	public static boolean isHighlightingByUnderlining() {
		return StyleConstants.isUnderline(SequencePaneHandler.highlightStyle);
	}

	/**
	 * Returns the current highlighting color.
	 * 
	 * @return The current highlighting color
	 */
	public static Color getHighlightingColor() {
		return StyleConstants.getForeground(SequencePaneHandler.highlightStyle);
	}

	/**
	 * Sets the highlighting mode.
	 * 
	 * @param bold        true for boldface highlighting (false for normal face)
	 * @param underline   true for underlining (false for no underlining)
	 * @param colorOrNull a color (if null, color is set to the base one)
	 */
	public static void setHighlighting(boolean bold, boolean underline, Color colorOrNull) {
		StyleConstants.setBold(SequencePaneHandler.highlightStyle, bold);
		StyleConstants.setUnderline(SequencePaneHandler.highlightStyle, underline);
		Color baseColor = StyleConstants.getForeground(SequencePaneHandler.baseStyle);
		StyleConstants.setForeground(SequencePaneHandler.highlightStyle, colorOrNull == null ? baseColor : colorOrNull);
		SequencePaneHandler.styleChangedNotifier.notify(null);
	}

	@Override
	public void componentResized(ComponentEvent e) {
		this.updateIfNecessary();
	}

	@Override
	public void componentHidden(ComponentEvent e) {
		this.isVisible = false;
		SequencePaneHandler.styleChangedNotifier.removeObserver(this);
	}

	@Override
	public void componentShown(ComponentEvent e) {
		this.isVisible = true;
		SequencePaneHandler.styleChangedNotifier.addObserver(this);
		this.updateIfNecessary();
	}

	@Override
	public void onNotification(BasicObserved<Void> notifier, Void argument) {
		if (this.isVisible) {
			this.update();
		}
	}

	/**
	 * Helper method: displays the sequence again onto the pane, only if the pane is
	 * visible and its width has changed since the last update.
	 */
	private void updateIfNecessary() {
		if (this.isVisible && this.pane.getWidth() != this.lastWidth) {
			this.update();
		}
	}

	/**
	 * Helper method: displays the sequence name, followed by a line break, onto the
	 * pane.
	 */
	private void displaySequenceName() {
		Style style = this.getStyle(SequencePaneHandler.nameStyle, SequencePaneHandler.unselectedNameStyle);
		int currentAdvance = this.wrapAndInsert(this.sequence.getAnnotation(), style, 0);
		this.breakLine(currentAdvance, style);
	}

	/**
	 * Helper method: displays the sequence and last operation information
	 * (depending on attributes {@link #showContext} and {@link #showWitnesses}),
	 * followed by a line break, onto the pane.
	 */
	private void displaySequence() {
		Style contextStyle = this.getStyle(SequencePaneHandler.contextStyle,
				SequencePaneHandler.unselectedContextStyle);
		Style plainStyle = this.getStyle(SequencePaneHandler.plainStyle, SequencePaneHandler.unselectedPlainStyle);
		Style highlightStyle = this.getStyle(SequencePaneHandler.highlightStyle,
				SequencePaneHandler.unselectedHighlightStyle);
		int currentAdvance = 0;
		// Left context
		if (this.showContext && this.lastOperation.definesContext()) {
			String parentAsString = this.lastOperation.getParent().getSequenceAsString();
			String context = parentAsString.substring(0, this.lastOperation.getStartIndex());
			currentAdvance = this.wrapAndInsert(context, contextStyle, currentAdvance);
		}
		// Sequence
		if (this.showWitnesses && this.lastOperation.hasWitnesses()) {
			String sequenceAsString = this.sequence.getSequenceAsString();
			SortedSet<Interval> witnesses = this.lastOperation.getWitnesses();
			UnionOfIntervals flattenedWitnesses = new UnionOfIntervals();
			for (Interval interval : witnesses) {
				flattenedWitnesses.addInterval(interval.getBegin(), interval.getEnd() - 1);
			}
			int lastIndex = 0;
			for (int i = 0; i < sequenceAsString.length(); i++) {
				if (flattenedWitnesses.startsInterval(i)) {
					currentAdvance = this.wrapAndInsert(sequenceAsString.substring(lastIndex, i), plainStyle,
							currentAdvance);
					lastIndex = i;
				}
				if (flattenedWitnesses.endsInterval(i)) {
					currentAdvance = this.wrapAndInsert(sequenceAsString.substring(lastIndex, i + 1), highlightStyle,
							currentAdvance);
					lastIndex = i + 1;
				}
			}
			if (lastIndex != sequenceAsString.length()) {
				currentAdvance = this.wrapAndInsert(sequenceAsString.substring(lastIndex), plainStyle, currentAdvance);
			}
		} else {
			currentAdvance = this.wrapAndInsert(this.sequence.getSequenceAsString(), plainStyle, currentAdvance);
		}
		// Right context
		Style lastStyle = plainStyle;
		if (this.showContext && this.lastOperation.definesContext()) {
			String parentAsString = this.lastOperation.getParent().getSequenceAsString();
			String context = parentAsString.substring(this.lastOperation.getEndIndex());
			currentAdvance = this.wrapAndInsert(context, contextStyle, currentAdvance);
			lastStyle = contextStyle;
		}
		// New line, if needed
		this.breakLine(currentAdvance, lastStyle);
	}

	/**
	 * Helper method: displays a string at the end of the current document, wrapping
	 * as necessary (not only on white space). If the string has only one character
	 * and the width is too small to display it, the string is not wrapped and
	 * inserted anyway.
	 * 
	 * @param str     A string
	 * @param style   The style to use
	 * @param advance The current advance in the pane (with respect to left side)
	 * @return The new advance
	 */
	private int wrapAndInsert(String str, Style style, int advance) {
		final int width = (int) this.pane.getSize().getWidth();
		final StyledDocument document = this.pane.getStyledDocument();
		final FontMetrics metrics = this.pane.getFontMetrics(document.getFont(style));
		if (str.trim().isEmpty()) {
			return advance;
		} else if (advance != 0
				&& advance + metrics.stringWidth(str.substring(0, 1)) > width - SequencePaneHandler.RIGHT_MARGIN) {
			try {
				document.insertString(document.getLength(), System.lineSeparator(), style);
				return this.wrapAndInsert(str, style, 0);
			} catch (BadLocationException e) {
				throw new RuntimeException(e);
			}
		} else {
			// Invariant: best position pos such that str.substring(0, pos) fits satisfies
			// pos in [min, max]
			int min = 1;
			int max = str.length();
			while (min != max) {
				final int middle = (min + max + 1) / 2;
				if (advance + metrics.stringWidth(str.substring(0, middle)) > width
						- SequencePaneHandler.RIGHT_MARGIN) {
					max = middle - 1;
				} else {
					min = middle;
				}
			}
			// Insert
			final int pos = min;
			if (pos == str.length()) {
				try {
					document.insertString(document.getLength(), str.substring(0, pos), style);
				} catch (BadLocationException e) {
					throw new RuntimeException(e);
				}
				return advance + metrics.stringWidth(str.substring(0, pos));
			} else {
				try {
					document.insertString(document.getLength(), str.substring(0, pos), style);
					document.insertString(document.getLength(), System.lineSeparator(), style);
					return this.wrapAndInsert(str.substring(pos), style, 0);
				} catch (BadLocationException e) {
					throw new RuntimeException(e);
				}
			}
		}
	}

	/**
	 * Helper method: adds a linebreak to the end of the current document, unless
	 * the current advance is already at the left side.
	 * 
	 * @param currentAdvance The current advance
	 * @param style          The current style
	 */
	private void breakLine(int currentAdvance, Style style) {
		if (currentAdvance != 0) {
			final Document document = this.pane.getStyledDocument();
			try {
				document.insertString(document.getLength(), System.lineSeparator(), style);
			} catch (BadLocationException e) {
				throw new RuntimeException(e);
			}
		}
	}

	/**
	 * Helper method: returns the style to be used among two given ones, depending
	 * on the current state (selection status of the sequence, and whether to ignore
	 * this status).
	 * 
	 * @param selected   The style to be returned if the current state requires to
	 *                   display the sequence as selected
	 * @param unselected The style to be returned if the current state requires to
	 *                   display the sequence as unselected
	 * @return The style to be used among the given ones
	 */
	private Style getStyle(Style selected, Style unselected) {
		return this.ignoreUnselected || this.session.getModel().isSelected(this.sequence) ? selected : unselected;
	}

}
