package fr.unicaen.aasuite.gui;

import java.awt.Component;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import fr.greyc.mad.swingutils.FullWidthScrollablePanel;

/**
 * A handler for the frame displaying a static text taken from a reader.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for numbers
 */
public class StaticTextHandler extends AuxiliaryFrameHandler {

	/** The block increment for the vertical scrollbar. */
	public static int SCROLLBAR_BLOCK_INCREMENT = 100;

	/** The unit increment for the vertical scrollbar. */
	public static int SCROLLBAR_UNIT_INCREMENT = 10;

	/**
	 * The multiplier to apply to the height of the parent frame to obtain the
	 * height of this one.
	 */
	public static float heightMultiplier = 0.75f;

	/**
	 * The multiplier to apply to the width of the parent frame to obtain the width
	 * of this one.
	 */
	public static float widthMultiplier = 0.75f;

	/** The text to be displayed. */
	protected final String text;

	/**
	 * Builds a new instance.
	 * 
	 * @param title  The title for the frame
	 * @param text   The text
	 * @param parentBounds The bounds of the parent frame
	 */
	public StaticTextHandler(String title, String text, Rectangle parentBounds) {
		super(title, AuxiliaryFrameHandler.centeredBounds(parentBounds, widthMultiplier, heightMultiplier));
		this.text = text;
	}

	@Override
	protected Component makeMainPane() {

		// The pane holding the text
		final JPanel pane = new FullWidthScrollablePanel(StaticTextHandler.SCROLLBAR_BLOCK_INCREMENT,
				StaticTextHandler.SCROLLBAR_BLOCK_INCREMENT);
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		final JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		textPane.setBackground(pane.getBackground());
		pane.add(textPane);
		textPane.setText(this.text);
		textPane.setCaretPosition(0); // scroll to top

		// A scroll pane over the pane
		final JScrollPane scrollPane = new JScrollPane(pane);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		return scrollPane;

	}

	@Override
	protected List<JButton> makeButtons() {
		return Collections.emptyList();
	}

}
