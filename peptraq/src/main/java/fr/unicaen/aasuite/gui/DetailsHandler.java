package fr.unicaen.aasuite.gui;

import java.awt.Component;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;

import fr.greyc.mad.swingutils.FullWidthScrollablePanel;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A handler for the frame displaying a sequence.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings (including title of the frame)
 */
public class DetailsHandler extends AuxiliaryFrameHandler {

	/** The block increment for the vertical scrollbar. */
	public static int SCROLLBAR_BLOCK_INCREMENT = 100;

	/** The unit increment for the vertical scrollbar. */
	public static int SCROLLBAR_UNIT_INCREMENT = 10;

	/**
	 * The multiplier to apply to the height of the parent frame to obtain the
	 * height of this one.
	 */
	public static float heightMultiplier = 0.75f;

	/**
	 * The multiplier to apply to the width of the parent frame to obtain the width
	 * of this one.
	 */
	public static float widthMultiplier = 0.75f;

	/** The current session. */
	protected final Session session;

	/** The displayed sequence. */
	protected final AnnotatedSequence<?> sequence;

	/** The history of operations which led to the sequence. */
	protected final List<OperationInformation> history;

	/**
	 * Builds a new instance and shows the frame.
	 * 
	 * @param session      The current session
	 * @param sequence     The displayed sequence
	 * @param history      The history of operations which led to the sequence
	 * @param parentBounds The bounds of the parent frame
	 */
	public DetailsHandler(Session session, AnnotatedSequence<?> sequence, List<OperationInformation> history,
			Rectangle parentBounds) {
		super(sequence.getAnnotation(), AuxiliaryFrameHandler.centeredBounds(parentBounds, widthMultiplier, heightMultiplier));
		this.session = session;
		this.sequence = sequence;
		this.history = history;
	}

	@Override
	protected Component makeMainPane() {
		
		// The pane holding the sequence
		final JPanel pane = new FullWidthScrollablePanel(DetailsHandler.SCROLLBAR_BLOCK_INCREMENT,
				DetailsHandler.SCROLLBAR_BLOCK_INCREMENT);
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		final JTextPane sequencePane = new JTextPane();
		pane.add(sequencePane);
		new SequencePaneHandler(sequencePane, this.session, sequence, history.get(0), true, true, true);

		// A scroll pane over the sequence
		final JScrollPane scrollPane = new JScrollPane(pane);
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

		return scrollPane;
	}

	@Override
	protected List<JButton> makeButtons() {
		return Collections.emptyList();
	}

}
