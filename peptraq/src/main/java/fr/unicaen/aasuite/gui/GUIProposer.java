package fr.unicaen.aasuite.gui;

import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinteraction.Proposer;

/**
 * A class for {@link Proposer}'s associated to a JFrame. Such a proposer
 * essentially uses dialogs and option panes associated to the frame.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings
 */
public class GUIProposer implements Proposer {

	/** The title for an information about an error. */
	public static final String ERROR_TITLE = "Error";

	/** The frame to which this proposer is associated. */
	protected final JFrame frame;

	/**
	 * Builds a new instance.
	 * 
	 * @param frame The frame to which this proposer is associated
	 */
	public GUIProposer(JFrame frame) {
		this.frame = frame;
	}

	@Override
	public void inform(String title, String message) {
		JComponent component = this.makeComponent(message);
		this.inform(title, component);
	}

	@Override
	public boolean propose(String title, AbstractBuilder<InputField<String, ?>, ?> builder, boolean showErrors) {
		return this.propose(title, new InputPane(builder, this).getAsComponent(showErrors));
	}

	@Override
	public boolean propose(String title, String message) {
		JComponent component = this.makeComponent(message);
		return this.propose(title, component);
	}

	@Override
	public Boolean ask(String title, String question) {
		int res = JOptionPane.showConfirmDialog(this.frame, question, title, JOptionPane.YES_NO_CANCEL_OPTION);
		return res == JOptionPane.YES_OPTION ? true : res == JOptionPane.NO_OPTION ? false : null;
	}

	@Override
	public void tellError(String message) {
		int mode = JOptionPane.ERROR_MESSAGE;
		JComponent component = this.makeComponent(message);
		JOptionPane.showMessageDialog(this.frame, component, ERROR_TITLE, mode);
	}

	/**
	 * Informs the user of something given as a JComponent until she confirms
	 * reading it.
	 * 
	 * @param title     A title for the information
	 * @param component The JComponent holding the information
	 */
	public void inform(String title, JComponent component) {
		int mode = JOptionPane.INFORMATION_MESSAGE;
		JOptionPane.showMessageDialog(this.frame, component, title, mode);
	}

	/**
	 * Proposes something given as a JComponent to the user until she confirms or
	 * cancels.
	 * 
	 * @param title     A title for the proposition
	 * @param component The JComponent holding the proposition
	 * @return true if the user confirmed, false if she cancelled
	 */
	public boolean propose(String title, JComponent component) {
		int res = JOptionPane.showConfirmDialog(this.frame, component, title, JOptionPane.OK_CANCEL_OPTION);
		return res == JOptionPane.OK_OPTION;
	}

	/**
	 * Builds a component representing a multi-line message.
	 * 
	 * @param message A message
	 * @return A JComponent
	 */
	private JComponent makeComponent(String message) {
		JTextPane component = new JTextPane();
		component.setText(message);
		component.setEditable(false);
		component.setBackground(this.frame.getBackground());
		return component;
	}

}
