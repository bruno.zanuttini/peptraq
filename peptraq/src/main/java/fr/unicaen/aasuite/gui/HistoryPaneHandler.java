package fr.unicaen.aasuite.gui;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;

import fr.greyc.mad.swingutils.FullWidthScrollablePanel;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A handler for the pane displaying the history of operations in PepTraq
 * Desktop.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all pixel amounts
 */
public class HistoryPaneHandler {

	/** The block increment for the vertical scrollbar. */
	public static int SCROLLBAR_BLOCK_INCREMENT = 100;

	/** The unit increment for the vertical scrollbar. */
	public static int SCROLLBAR_UNIT_INCREMENT = 10;

	/**
	 * The vertical space to insert before and after each element of the history (in
	 * pixels).
	 */
	public static int VERTICAL_SPACE = 10;

	/** The label for the button allowing to undo an operation. */
	public static final String UNDO_LABEL = "undo last";

	/** The text to display in place of an empty history of operations. */
	public static final String EMPTY_HISTORY = "No operation";

	/** The session represented by this pane. */
	protected final Session session;

	/** The pane displaying the history. */
	protected final JPanel panel;

	/** A subpanel used for pushing history to the top of the panel. */
	private final JPanel subPanel;

	/** The button for undoing last operation. */
	private JButton undoButton;

	/**
	 * Builds a new instance.
	 * 
	 * @param session The session represented by this pane
	 * @param panel   The pane displaying the history
	 */
	public HistoryPaneHandler(Session session, JPanel panel) {
		this.session = session;
		this.panel = panel;
		this.panel.setLayout(new BorderLayout());
		this.subPanel = new FullWidthScrollablePanel(SCROLLBAR_BLOCK_INCREMENT, SCROLLBAR_UNIT_INCREMENT);
		this.subPanel.setLayout(new BoxLayout(subPanel, BoxLayout.Y_AXIS));
		final JScrollPane scrollPane = new JScrollPane(this.subPanel);
		scrollPane.setBorder(BorderFactory.createEmptyBorder());
		this.panel.add(scrollPane, BorderLayout.CENTER);
		this.updateWithEmptyHistory();
	}

	/**
	 * Updates the handler to display a (possibly empty) history of operations.
	 * 
	 * @param descriptions A history of operations
	 */
	public void update(List<String> descriptions) {
		SwingUtilities.invokeLater(() -> {
			if (descriptions.isEmpty()) {
				this.updateWithEmptyHistory();
			} else {
				this.subPanel.removeAll();
				int i = 0;
				for (String description : descriptions) {
					JTextArea area = new JTextArea(description);
					area.setLineWrap(true);
					area.setWrapStyleWord(true);
					area.setEditable(false);
					area.setBackground(this.panel.getBackground());
					this.subPanel.add(createFiller());
					this.subPanel.add(area);
					this.subPanel.add(createFiller());
					this.subPanel.add(new JSeparator());
					if (i == descriptions.size() - 1 && this.session.nbSteps() > 1) {
						this.undoButton = MainFrame.makeClickableText(UNDO_LABEL);
						this.undoButton.addActionListener(e -> this.session.cancelCurrentStep());
						JComponent undo = new JPanel();
						undo.add(this.undoButton);
						this.subPanel.add(undo);
					}
					i++;
				}
			}
		});
	}

	/**
	 * Updates the handler to display an empty history of operations.
	 */
	public void updateWithEmptyHistory() {
		SwingUtilities.invokeLater(() -> {
			this.subPanel.removeAll();
			JTextArea area = new JTextArea(EMPTY_HISTORY);
			area.setLineWrap(true);
			area.setWrapStyleWord(true);
			area.setEditable(false);
			area.setBackground(this.panel.getBackground());
			this.subPanel.add(createFiller());
			this.subPanel.add(area);
			this.subPanel.revalidate();
		});
	}

	/**
	 * Enables/disables all operations which should not be active when an operation
	 * is progressing.
	 * 
	 * @param enable true for enabling the operations, false for disabling them
	 */
	public void enableHistoryOperations(boolean enable) {
		if (this.undoButton != null) {
			this.undoButton.setEnabled(!enable ? false : this.session.nbSteps() > 0);
		}
	}

	/**
	 * Creates a filler for vertical space between elements of the history.
	 * 
	 * @return A filler
	 */
	private Component createFiller() {
		return new Box.Filler(new Dimension(0, VERTICAL_SPACE), new Dimension(0, VERTICAL_SPACE),
				new Dimension(0, VERTICAL_SPACE));
	}

}
