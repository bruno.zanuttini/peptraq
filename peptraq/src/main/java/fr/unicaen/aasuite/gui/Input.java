package fr.unicaen.aasuite.gui;

import javax.swing.JComponent;

/**
 * An interface for graphical input components. One instance is dedicated to
 * letting the user input or choose one string.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public interface Input {

	/**
	 * Returns the value input by the user. When the user has not selected any
	 * value, either null or a blank string may be returned.
	 * 
	 * @return The value input by the user, or null
	 */
	public String getValue();

	/**
	 * Returns this instance as a graphical component through which the user can
	 * input or choose a string.
	 * 
	 * @return This instance as a graphical component
	 */
	public JComponent asJComponent();

}
