package fr.unicaen.aasuite.gui;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import fr.greyc.mad.userinteraction.AbstractFileHandler;
import fr.greyc.mad.userinteraction.Proposer;

/**
 * A graphical file chooser. The chooser is tied to a JFrame (for dialogs), and
 * otherwise handles interaction with the user through an instance of
 * {@link Proposer}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings
 */
public class FileChooser extends AbstractFileHandler {

	/** The title for the prompt when a chosen output file already exists. */
	public static final String EXISTING_TITLE = "File already exists";

	/** The question to ask when a chosen output file already exists. */
	public static final String EXISTING_QUESTION = "File %s exists. Overwrite?";

	/** The parent frame for the dialogs. */
	final protected JFrame frame;

	/** The default directory to propose. */
	final protected File directory;

	/**
	 * Builds a new instance.
	 * 
	 * @param frame     The parent frame (for dialogs)
	 * @param directory The default directory to propose
	 */
	public FileChooser(JFrame frame, File directory) {
		super(new GUIProposer(frame));
		this.frame = frame;
		this.directory = directory;
	}

	@Override
	public File chooseInputFile(String description, String... extensions) {
		JFileChooser chooser = new JFileChooser(this.directory);
		chooser.setFileFilter(new FileNameExtensionFilter(description, extensions));
		int returnVal = chooser.showOpenDialog(this.frame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			try {
				Reader fileReader = new FileReader(file);
				fileReader.close();
			} catch (Exception exception) {
				return super.handleReadException(file, exception, () -> this.chooseInputFile(description, extensions));
			}
			return file;
		} else {
			return null;
		}
	}

	@Override
	public File chooseOutputFile(String description, String... extensions) {
		return this.chooseOutputFile(null, description, extensions);
	}

	@Override
	public File chooseOutputFile(File defaultFile, String description, String... extensions) {
		JFileChooser chooser = new JFileChooser(this.directory);
		chooser.setFileFilter(new FileNameExtensionFilter(description, extensions));
		if (defaultFile != null) {
			chooser.setSelectedFile(defaultFile);
		}
		int returnVal = chooser.showSaveDialog(this.frame);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File file = chooser.getSelectedFile();
			if (file.exists()) {
				int res = JOptionPane.showConfirmDialog(this.frame,
						new JLabel(String.format(EXISTING_QUESTION, file.getName())), EXISTING_TITLE,
						JOptionPane.YES_NO_CANCEL_OPTION);
				if (res == JOptionPane.CANCEL_OPTION) {
					return null;
				} else if (res == JOptionPane.NO_OPTION) {
					return this.chooseOutputFile(defaultFile, description, extensions);
				} // else go on
			}
			try {
				Writer fileWriter = new FileWriter(file);
				fileWriter.close();
			} catch (IOException exception) {
				return super.handleWriteException(file, exception,
						() -> this.chooseOutputFile(defaultFile, description, extensions));
			}
			return file;
		} else {
			return null;
		}
	}

}
