/**
 * Main classes for the GUI of PepTraq Desktop: root displayer classes,
 * listeners for operations on the GUI itself, and handling of user input
 * through the GUI.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.gui;
