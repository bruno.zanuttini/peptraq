package fr.unicaen.aasuite.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingWorker;

/**
 * An action listener which runs another action listener (called the "wrapped
 * listener") in a SwingWorker.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SwingWorkerListener implements ActionListener {

	/** The wrapped listener. */
	protected ActionListener listener;

	/**
	 * Builds a new instance.
	 * 
	 * @param listener The wrapped listener
	 */
	public SwingWorkerListener(ActionListener listener) {
		this.listener = listener;
	}

	@Override
	public void actionPerformed(final ActionEvent event) {
		try {
			new SwingWorker<Void, Void>() {
				@Override
				protected Void doInBackground() throws Exception {
					SwingWorkerListener.this.listener.actionPerformed(event);
					return null;
				}
			}.execute();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
