package fr.unicaen.aasuite.control;

import fr.greyc.mad.userinteraction.Proposer;

/**
 * A listener for exiting the appication.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 *         TODO: properties for all strings
 */
public class ExitListener {

	/** The title for the confirmation message. */
	public static final String MESSAGE_TITLE = "Confirmation";

	/** The confirmation message. */
	public static final String CONFIRMATION_MESSAGE = "Do you really want to exit?";

	/** A proposer for asking confirmation to the user. */
	protected final Proposer proposer;

	/**
	 * Builds a new instance.
	 * 
	 * @param proposer A proposer for asking confirmation to the user
	 */
	public ExitListener(Proposer proposer) {
		this.proposer = proposer;
	}

	/**
	 * Asks confirmation to the user, then exits with exit code 0.
	 */
	public void actionPerformed() {
		boolean confirmed = this.proposer.propose(MESSAGE_TITLE, CONFIRMATION_MESSAGE);
		if (confirmed) {
			System.exit(0);
		}
	}

}
