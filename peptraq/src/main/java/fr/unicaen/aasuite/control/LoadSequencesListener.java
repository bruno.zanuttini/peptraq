package fr.unicaen.aasuite.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Progress;
import fr.greyc.mad.userinteraction.FileHandler;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.fasta.SequenceInputStream;
import fr.unicaen.aasuite.gui.SwingWorkerListener;
import fr.unicaen.aasuite.sessions.Session;

/**
 * Listeners for loading sequences from a file.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 *         TODO: properties for all strings
 */
public abstract class LoadSequencesListener extends BasicObserved<Progress> implements ActionListener{

	/** The description for the expected type of input files. */
	public static final String INPUT_FILE_DESCRIPTION = "Fasta and FNA files";

	/** The expected extensions for input file names. */
	public static final String[] INPUT_FILE_EXTENSIONS = { "fasta", "fna" };

	/** The title of the error for unreadable sequences. */
	public static final String UNREADABLE_TITLE = "Unreadable sequences";

	/** The error message for unreadable sequences. */
	public static final String UNREADABLE_MESSAGE = "There were %s unreadable sequences." + System.lineSeparator()
			+ "Save them to another file?" + System.lineSeparator() + "Maybe you tried to load sequences of the wrong type (DNA vs proteins)?";

	/** The description for the expected type of error output files. */
	public static final String ERROR_FILE_DESCRIPTION = "Text files";

	/** The expected extensions for error output file names. */
	public static final String[] ERROR_FILE_EXTENSIONS = { "txt" };

	/**
	 * The default file name proposed to the user for writing unreadable sequences.
	 */
	public static final String DEFAULT_ERROR_FILENAME = "errors-%s";

	/** The string to write for an error when reading a sequence. */
	public static final String ERROR_STRING = "[Error: %s]";

	/** The session into which to load the sequences. */
	protected final Session session;

	/** A file handler for the user to choose the input file. */
	protected final FileHandler fileHandler;

	/** A proposer for interacting in case of errors. */
	protected final Proposer proposer;

	/**
	 * Builds a new instance.
	 * 
	 * @param session     The session into which to load the sequences
	 * @param fileHandler A file handler for the user to choose the input file
	 * @param proposer    A proposer for interacting in case of errors
	 */
	public LoadSequencesListener(Session session, FileHandler fileHandler, Proposer proposer) {
		this.fileHandler = fileHandler;
		this.session = session;
		this.proposer = proposer;
	}

	/**
	 * Builds and returns a loading listener for DNA sequences.
	 * 
	 * @param session     The session into which to load the sequences
	 * @param fileHandler A file handler for the user to choose the input file
	 * @param proposer    A proposer for interacting in case of errors
	 * @return A loading listener for DNA sequences
	 */
	public static LoadSequencesListener forDNA(Session session, FileHandler fileHandler, Proposer proposer) {
		return new LoadSequencesListener(session, fileHandler, proposer) {

			@Override
			protected void addToSession(SequenceInputStream stream, long streamSize, String filename,
					List<String> unreadable, List<String> errors) {
				try {
					this.session.addDNALoading(stream, streamSize, filename, unreadable, errors);
				} catch (IOException e) {
					// FIXME: ignore (exception raised when stream is closed)
				}
			}

		};
	}

	/**
	 * Builds and returns a loading listener for proteins.
	 * 
	 * @param session     The session into which to load the sequences
	 * @param fileHandler A file handler for the user to choose the input file
	 * @param proposer    A proposer for interacting in case of errors
	 * @return A loading listener for proteins
	 */
	public static LoadSequencesListener forProteins(Session session, FileHandler fileHandler, Proposer proposer) {
		return new LoadSequencesListener(session, fileHandler, proposer) {

			@Override
			protected void addToSession(SequenceInputStream stream, long streamSize, String filename,
					List<String> unreadable, List<String> errors) {
				try {
					this.session.addProteinLoading(stream, streamSize, filename, unreadable, errors);
				} catch (IOException e) {
					// FIXME: ignore (exception raised when stream is closed)
				}

			}

		};
	}

	/**
	 * Adds the loading operation to the session.
	 * 
	 * @param stream     The stream to load sequences from
	 * @param streamSize The size of the stream
	 * @param streamName The name of the stream
	 * @param unreadable A mutable list where to add unreadable sequences
	 * @param errors     A mutable list where to add errors found for unreadable
	 *                   sequences (an element must be added to this list each time
	 *                   one is added to the list of unreadables)
	 */
	protected abstract void addToSession(SequenceInputStream stream, long streamSize, String streamName,
			List<String> unreadable, List<String> errors);

	/**
	 * {@inheritDoc}
	 * 
	 * Loads sequences, interacting with the user and telling errors as necessary. Ignores the event.
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		File file = this.fileHandler.chooseInputFile(INPUT_FILE_DESCRIPTION, INPUT_FILE_EXTENSIONS);
		if (file != null) {
			try {
				FileInputStream fileStream = new FileInputStream(file);
				SequenceInputStream stream = new SequenceInputStream(new BufferedInputStream(fileStream));
				List<String> unreadable = new ArrayList<>();
				List<String> errors = new ArrayList<>();
				new SwingWorkerListener(e -> {
					this.addToSession(stream, file.length(), file.getPath(), unreadable, errors);
					assert unreadable.size() == errors.size();
					if (!unreadable.isEmpty()) {
						boolean saveErrors = this.proposer.propose(UNREADABLE_TITLE,
								String.format(UNREADABLE_MESSAGE, unreadable.size()));
						if (saveErrors) {
							this.saveStrings(unreadable, errors, file);
						}
					}
				}).actionPerformed(null);
			} catch (IOException exception) {
				this.fileHandler.handleReadException(file, exception, () -> {
					this.actionPerformed(event);
					return null;
				});
			}
		}
	}

	/**
	 * Prompts the user for a file name and saves a given set of unreadable strings
	 * and the corresponding errors to it.
	 * 
	 * @param unreadable   The unreadable strings to save
	 * @param errors       The corresponding errors (the error at index i must
	 *                     correspond to the unreadable string at index i)
	 * @param originalFile The file where bad strings were found
	 */
	private void saveStrings(List<String> unreadable, List<String> errors, File originalFile) {
		File defaultFile = new File(originalFile.getParent(),
				String.format(DEFAULT_ERROR_FILENAME, originalFile.getName()));
		File file = this.fileHandler.chooseOutputFile(defaultFile, ERROR_FILE_DESCRIPTION, ERROR_FILE_EXTENSIONS);
		if (file != null) {
			try {
				Writer fileWriter = new BufferedWriter(new FileWriter(file));
				Iterator<String> errorIt = errors.iterator();
				for (String string : unreadable) {
					String error = errorIt.next();
					if (error != null) {
						fileWriter.write(String.format(ERROR_STRING, error));
						fileWriter.write(System.lineSeparator());
					}
					fileWriter.write(string);
					fileWriter.write(System.lineSeparator());
				}
				fileWriter.close();
			} catch (IOException exception) {
				this.fileHandler.handleWriteException(file, exception, () -> {
					this.saveStrings(unreadable, errors, originalFile);
					return null;
				});
			}
		}
	}

}
