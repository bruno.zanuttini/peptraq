package fr.unicaen.aasuite.control;

import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A listener for closing the current session.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings
 */
public class CloseSessionListener {

	/** The title for the confirmation message. */
	public static final String MESSAGE_TITLE = "Confirmation";

	/** The confirmation message. */
	public static final String CONFIRMATION_MESSAGE = "The whole session will be discarded. Continue?";

	/** The current session. */
	protected final Session session;

	/** A proposer for asking confirmation to the user. */
	protected final Proposer proposer;

	/**
	 * Builds a new instance.
	 * 
	 * @param session  The current session
	 * @param proposer A proposer for asking confirmation to the user
	 */
	public CloseSessionListener(Session session, Proposer proposer) {
		this.session = session;
		this.proposer = proposer;
	}

	/**
	 * Asks for confirmation and closes the session.
	 */
	public void actionPerformed() {
		boolean confirmed = this.proposer.propose(MESSAGE_TITLE, CONFIRMATION_MESSAGE);
		if (confirmed) {
			this.session.close();
		}
	}

}
