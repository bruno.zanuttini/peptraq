package fr.unicaen.aasuite.control;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import fr.greyc.mad.userinteraction.FileHandler;
import fr.unicaen.aasuite.operations.Macro;
import fr.unicaen.aasuite.operations.MacroIO;

/**
 * A listener for saving macros.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 *         TODO: properties for all strings
 */
public class SaveMacroListener {

	/** The description for the expected type of output files. */
	public static final String OUTPUT_FILE_DESCRIPTION = "PepTraq macro files";

	/** The expected extensions for output file names. */
	public static final String[] OUTPUT_FILE_EXTENSIONS = { "ptqm" };

	/** The default output file name. */
	public static final String DEFAULT_FILE_NAME = "macro.ptqm";

	/** The macro to be saved. */
	protected final Macro macro;

	/** A handler for choosing the output file. */
	protected final FileHandler fileHandler;

	/** The current version of PepTraq. */
	protected final String peptraqVersion;

	/**
	 * Builds a new instance.
	 * 
	 * @param macro          The macro to be saved
	 * @param fileHandler    A handler for choosing the output file
	 * @param peptraqVersion The current version of PepTraq
	 */
	public SaveMacroListener(Macro macro, FileHandler fileHandler, String peptraqVersion) {
		this.macro = macro;
		this.fileHandler = fileHandler;
		this.peptraqVersion = peptraqVersion;
	}

	/**
	 * Saves the macro, interacting with the user as necessary.
	 */
	public void actionPerformed() {
		File file = this.fileHandler.chooseOutputFile(new File(DEFAULT_FILE_NAME), OUTPUT_FILE_DESCRIPTION,
				OUTPUT_FILE_EXTENSIONS);
		try {
			Writer writer = new BufferedWriter(new FileWriter(file));
			for (String line : MacroIO.dump(this.macro, this.peptraqVersion)) {
				writer.write(line);
				writer.write(System.lineSeparator());
			}
			writer.close();
		} catch (IOException exception) {
			this.fileHandler.handleWriteException(file, exception, () -> {
				this.actionPerformed();
				return null;
			});
		}
	}

}
