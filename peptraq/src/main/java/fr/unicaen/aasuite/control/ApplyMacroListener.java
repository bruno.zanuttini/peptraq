package fr.unicaen.aasuite.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import fr.greyc.mad.userinteraction.FileHandler;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.gui.SwingWorkerListener;
import fr.unicaen.aasuite.operations.Macro;
import fr.unicaen.aasuite.operations.MacroIO;
import fr.unicaen.aasuite.sessions.Session;

/**
 * Listeners for loading sequences from a file.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: properties for all strings
 */
public class ApplyMacroListener implements ActionListener {

	/** The description for the expected type of input files. */
	public static final String INPUT_FILE_DESCRIPTION = "PepTraq macro files";

	/** The expected extensions for input file names. */
	public static final String[] INPUT_FILE_EXTENSIONS = { "ptqm" };

	/** The session to which to apply the macro. */
	protected final Session session;

	/** A file handler for the user to choose the input file. */
	protected final FileHandler fileHandler;

	/** A proposer for interacting in case of errors. */
	protected final Proposer proposer;

	/** The current version of PepTraq. */
	protected final String peptraqVersion;

	/**
	 * Builds a new instance.
	 * 
	 * @param session        The session to which to apply the macro
	 * @param fileHandler    A file handler for the user to choose the input file
	 * @param proposer       A proposer for interacting in case of errors
	 * @param peptraqVersion The current version of PepTraq
	 */
	public ApplyMacroListener(Session session, FileHandler fileHandler, Proposer proposer, String peptraqVersion) {
		this.session = session;
		this.fileHandler = fileHandler;
		this.proposer = proposer;
		this.peptraqVersion = peptraqVersion;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Prompts for a macro and applies it, interacting with the user and telling
	 * errors as necessary. Ignores the event.
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		File file = this.fileHandler.chooseInputFile(INPUT_FILE_DESCRIPTION, INPUT_FILE_EXTENSIONS);
		if (file != null) {
			try {
				FileReader fileReader = new FileReader(file);
				BufferedReader reader = new BufferedReader(fileReader);
				List<String> lines = reader.lines().collect(Collectors.toList());
				reader.close();
				Macro macro = MacroIO.load(lines, this.peptraqVersion);
				if (this.session.getModel().isDNA() && !macro.appliesToDNA()) {
					this.proposer.tellError("Macro cannot be applied to DNA");
				} else if (this.session.getModel().isProteins() && !macro.appliesToProteins()) {
					this.proposer.tellError("Macro cannot be applied to proteins");
				} else {
					new SwingWorkerListener(e -> {
						this.session.addMacro(macro);
					}).actionPerformed(null);
				}
			} catch (IOException exception) {
				this.fileHandler.handleReadException(file, exception, () -> {
					this.actionPerformed(event);
					return null;
				});
			} catch (Throwable throwable) {
				this.proposer.tellError(throwable.getMessage());
			}
		}
	}

}
