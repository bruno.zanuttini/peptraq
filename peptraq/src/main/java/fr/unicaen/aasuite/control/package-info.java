/**
 * Controllers for PepTraq Desktop. The controllers in this package are
 * controllers on the model, independent of the GUI or other type of user
 * interface.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.control;