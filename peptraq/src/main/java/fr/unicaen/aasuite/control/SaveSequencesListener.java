package fr.unicaen.aasuite.control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Progress;
import fr.greyc.mad.userinteraction.FileHandler;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.gui.SwingWorkerListener;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A listener for saving sequences. The sequences to be saved are taken from the
 * current state of a session.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 *         TODO: properties for all strings
 */
public class SaveSequencesListener extends BasicObserved<Progress> implements ActionListener {

	/** The description for the expected type of output files. */
	public static final String OUTPUT_FILE_DESCRIPTION = "Fasta files";

	/** The expected extensions for output file names. */
	public static final String[] OUTPUT_FILE_EXTENSIONS = { "fasta" };

	/** The tite for the question asking whether to crop the annotations. */
	public static String cropAnnotationsTitle = "Crop annotations?";

	/** The question asking whether to crop the annotations. */
	public static String cropAnnotationsQuestion = "Crop each annotation to the first word?";

	/** The session holding the sequences to be saved. */
	protected final Session session;

	/** Whether to save all sequences (false) or only selected ones (true). */
	protected final boolean onlySelected;

	/** A handler for choosing the output file. */
	protected final FileHandler fileHandler;

	/** A proposer for asking confirmation to the user. */
	protected final Proposer proposer;

	/**
	 * Builds a new instance.
	 * 
	 * @param session      The session holding the sequences to be saved
	 * @param onlySelected Whether to save all sequences (false) or only selected
	 *                     ones (true)
	 * @param fileHandler  A handler for choosing the output file
	 * @param proposer     A proposer for asking confirmation to the user
	 */
	public SaveSequencesListener(Session session, boolean onlySelected, FileHandler fileHandler, Proposer proposer) {
		this.session = session;
		this.onlySelected = onlySelected;
		this.fileHandler = fileHandler;
		this.proposer = proposer;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Saves the sequences, interacting with the user as necessary. Ignores the
	 * event.
	 */
	@Override
	public void actionPerformed(ActionEvent event) {
		File file = this.fileHandler.chooseOutputFile(OUTPUT_FILE_DESCRIPTION, OUTPUT_FILE_EXTENSIONS);
		try {
			Writer writer = new BufferedWriter(new FileWriter(file));
			Boolean cropAnnotations = this.proposer.ask(cropAnnotationsTitle, cropAnnotationsQuestion);
			if (cropAnnotations != null) {
				new SwingWorkerListener(e -> {
					try {
						this.session.write(writer, onlySelected, cropAnnotations);
					} catch (IOException ex) {
						// FIXME: ignore (exception raised when stream is closed)
					}
				}).actionPerformed(null);
			} // else user wants to cancel
		} catch (IOException exception) {
			this.fileHandler.handleWriteException(file, exception, () -> {
				this.actionPerformed(event);
				return null;
			});
		}
	}

}
