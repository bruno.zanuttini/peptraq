package fr.unicaen.aasuite.control;

import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Progress;
import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinteraction.Proposer;

/**
 * An abstract listener for application of operations.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 *         TODO: properties for all strings
 */
public abstract class OperationListener<O> extends BasicObserved<Progress> {

	/** The title for the message prompting the user for the operation details. */
	public static final String MESSAGE_TITLE = "Operation parameters";

	/** A (helper) construction listener. */
	private final ConstructionListener<O> constructor;

	/** A proposer for interaction with the user. */
	protected final Proposer proposer;

	/**
	 * Builds a new instance.
	 * 
	 * @param proposer A proposer for interaction with the user
	 */
	public OperationListener(Proposer proposer) {
		this.constructor = new ConstructionListener<O>(proposer, MESSAGE_TITLE) {

			@Override
			protected void handle(O operation) {
				OperationListener.this.apply(operation);
			}

			@Override
			protected AbstractBuilder<InputField<String, ?>, ? extends O> make() {
				return OperationListener.this.makeBuilder();
			}
		};
		this.proposer = proposer;
	}

	/**
	 * Prompts the user for the parameters of the operation, and applies it.
	 */
	public void actionPerformed() {
		this.constructor.actionPerformed();
	}

	/**
	 * Applies the built operation.
	 * 
	 * @param operation The operation built by interaction with the user
	 */
	protected abstract void apply(O operation);

	/**
	 * Returns a builder for the operation to be applied.
	 * 
	 * @return A builder for the operation to be applied
	 */
	protected abstract AbstractBuilder<InputField<String, ?>, ? extends O> makeBuilder();

}
