package fr.unicaen.aasuite.control;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinteraction.Proposer;

/**
 * An abstract listener for actions which require to first build an object by
 * interacting with the user, then handling this object. No action is performed
 * if the user cancels the interaction for building the object.
 *
 * @param <T> The type of objects built
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 *         TODO: properties for all strings
 */
public abstract class ConstructionListener<T> {

	/** A message to be displayed when an exception occurred at construction. */
	public final static String CONSTRUCTION_EXCEPTION = "An unexpected error occurred";

	/** The proposer for interacting with the user. */
	protected final Proposer proposer;

	/** The title for the message prompting the user for the object. */
	protected final String title;

	/**
	 * Builds an instance.
	 * 
	 * @param proposer The proposer for interacting with the user
	 * @param title    The title for the message prompting the user for the object
	 */
	public ConstructionListener(Proposer proposer, String title) {
		this.proposer = proposer;
		this.title = title;
	}

	/**
	 * Prompts the user for the parameters of the object and then handles the object.
	 */
	public void actionPerformed() {
		final AbstractBuilder<InputField<String, ?>, ? extends T> builder = this.make();
		boolean input;
		boolean showErrors = false;
		do {
			input = this.proposer.propose(this.title, builder, showErrors);
			showErrors = true;
		} while (input && !builder.isValid());
		if (input) {
			try {
				this.handle(builder.getObject());
			} catch (Throwable ex) {
				this.proposer.tellError(CONSTRUCTION_EXCEPTION);
				ex.printStackTrace();
			}
		}
	}

	/**
	 * Handles the built object.
	 * 
	 * @param object An object built by interaction with the user
	 */
	protected abstract void handle(T object);

	/**
	 * Returns a builder for the object.
	 * 
	 * @return A builder for the object
	 */
	protected abstract AbstractBuilder<InputField<String, ?>, ? extends T> make();

}
