package fr.unicaen.aasuite.control;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.FreeTextField;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinteraction.Proposer;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.Session;

/**
 * A listener for editing the annotation of a sequence.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 *         TODO: properties for all strings
 */
public class EditAnnotationListener extends ConstructionListener<String> {

	/** The title for the message prompting the user. */
	public static final String MESSAGE_TITLE = "Set number of sequences per page";
	
	/** The label for the new annotation field. */
	public static final String FIELD_LABEL = "New annotation";

	/** The help string for the builder. */
	public static String builderHelp = "Edit the annotation of the sequence; the original one can always be restored later";
	
	/** The help string for the input field. */
	public static String inputFieldHelp = "Enter the new annotation for the sequence";
	
	/** The session in which to edit the annotation. */
	protected final Session session;

	/** The sequence whose annotation to edit. */
	protected final AnnotatedSequence<?> sequence;

	/**
	 * Builds a new instance.
	 * 
	 * @param session  The session in which to edit the annotation
	 * @param sequence The sequence whose annotation to edit
	 * @param proposer A proposer for interacting with the user
	 */
	public EditAnnotationListener(Session session, AnnotatedSequence<?> sequence, Proposer proposer) {
		super(proposer, MESSAGE_TITLE);
		this.session = session;
		this.sequence = sequence;
	}

	@Override
	protected void handle(String newName) {
		this.session.rename(this.sequence, newName);
	}

	@Override
	protected AbstractBuilder<InputField<String, ?>, String> make() {
		final InputField<String, String> inputField = new FreeTextField(false, FIELD_LABEL, this.sequence.getAnnotation(), inputFieldHelp);
		List<InputField<String, ?>> fields = new ArrayList<>();
		fields.add(inputField);
		return new AbstractBuilder<InputField<String, ?>, String>(Collections.singletonList(inputField), builderHelp) {

			@Override
			protected String safeGetObject() {
				return inputField.hasValue() ? inputField.getValue() : null;
			}

			@Override
			public boolean isGloballyValid() {
				return true;
			}

			@Override
			public List<String> getGlobalErrors() {
				return Collections.emptyList();
			}
			
//			@Override
//			public String help() {
//				StringBuilder res=new StringBuilder();
//				res.append("<html>");
//				res.append("<p>");
//				res.append("Changes the annotation of the sequence.");
//				res.append("<br />");
//				res.append("The original annotation can be restored at any point later.");
//				res.append("</p>");
//				res.append("</html>");				
//				return res.toString();
//			}
		};
	}

}
