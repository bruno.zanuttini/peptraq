/**
 * Generic utils for Swing.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.greyc.mad.swingutils;