package fr.greyc.mad.swingutils;

import java.awt.BorderLayout;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

import fr.greyc.mad.observable.Observer;
import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Progress;

/**
 * A progress bar able to display approximate remaining time.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 */
public class TimeProgressBar implements Observer<Progress> {

	/** The progress bar. */
	private final JProgressBar progressBar;
	
	/** A label holding time information. */
	private final JLabel label;

	/** A pane holding the progress bar and time information. */
	private final JPanel pane;
	
	/** The underlying monitor. */
	protected final Monitor monitor;

	/**
	 * Builds a new instance.
	 * @param monitor The underlying monitor
	 */
	public TimeProgressBar(Monitor monitor) {
		this.progressBar = new JProgressBar();
		this.progressBar.setMinimum(0);
		this.progressBar.setMaximum(100);
		this.progressBar.setValue(0);
		this.progressBar.setValue(0);
		this.progressBar.setStringPainted(true);
		this.label = new JLabel();
		this.label.setText("Approx. time remaining: actualizing...");
		this.pane = new JPanel();
		this.pane.setLayout(new BorderLayout());
		this.pane.add(this.label, BorderLayout.NORTH);
		this.pane.add(this.progressBar, BorderLayout.SOUTH);
		this.monitor = monitor;
		this.monitor.addObserver(this);
	}

	/**
	 * Returns this progress bar as a graphical component.
	 * @return This progress bar as a graphical component
	 */
	public JComponent getAsComponent() {
		return this.pane;
	}

	@Override
	public void onNotification(BasicObserved<Progress> notifier, Progress argument) {
		if (notifier == this.monitor) {
			int percentage = (int) (this.monitor.getNbStepsCompleted() * 100 / this.monitor.getNbSteps());
			this.progressBar.setValue(percentage);
			if (this.monitor.hasEstimate()) {
				String estimate = TimeProgressBar.format(this.monitor.getRemainingTime());
				this.label.setText("Approx. time remaining: " + estimate);
			} else {
				this.label.setText("Approx. time remaining: actualizing...");
			}
		}
	}

	/**
	 * Formats a number of milliseconds into human-readable format.
	 * @param nbMilliSeconds A number of milliseconds
	 * @return A human readable string corresponding to the given number of millisecons
	 */
	public static String format(long nbMilliSeconds) {
		long nbSeconds = nbMilliSeconds / 1000;
		long hours = nbSeconds / 3600;
		int minutes = (int) (nbSeconds % 3600) / 60;
		int seconds = (int) nbSeconds % 60;
		// Rounding number of seconds to the (immediate greater) multiple of 5
		seconds -= seconds % 5;
		if (seconds != 55) {
			seconds += 5;
		}
		// Converting to string
		String hoursAsString = hours == 0 ? "" : hours + " h ";
		String minutesAsString = minutes == 0 && hours == 0 ? ""
				: (minutes < 10 && hours != 0 ? "0" : "") + minutes + " min ";
		String secondsAsString = (seconds < 10 && (hours != 0 || minutes != 0) ? "0" : "") + seconds + " sec";
		// Ready
		return hoursAsString + minutesAsString + secondsAsString;
	}

}
