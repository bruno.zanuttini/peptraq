package fr.greyc.mad.swingutils;

import java.awt.Dimension;
import java.awt.Rectangle;

import javax.swing.JPanel;
import javax.swing.Scrollable;

/**
 * A Scrollable JPanel with width set to the width available in the enclosing
 * scroll pane (hence designed for being vertically scrollable).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class FullWidthScrollablePanel extends JPanel implements Scrollable {
	
	/** The serialization version id. */
	private static final long serialVersionUID = 1L;

	/** The block increment for the scrollbar. */
	protected final int scrollbarBlockIncrement;
	
	/** The unit increment for the scrollbar. */
	protected final int scrollbarUnitIncrement;

	/**
	 * Builds a new instance.
	 * 
	 * @param scrollbarBlockIncrement The block increment for the scrollbar
	 * @param scrollbarUnitIncrement  The unit increment for the scrollbar
	 */
	public FullWidthScrollablePanel(int scrollbarBlockIncrement, int scrollbarUnitIncrement) {
		this.scrollbarBlockIncrement = scrollbarBlockIncrement;
		this.scrollbarUnitIncrement = scrollbarUnitIncrement;
	}

	@Override
	public Dimension getPreferredScrollableViewportSize() {
		return super.getPreferredSize();
	}

	@Override
	public int getScrollableBlockIncrement(Rectangle arg0, int arg1, int arg2) {
		return this.scrollbarBlockIncrement;
	}

	@Override
	public boolean getScrollableTracksViewportHeight() {
		return false;
	}

	@Override
	public boolean getScrollableTracksViewportWidth() {
		return true;
	}

	@Override
	public int getScrollableUnitIncrement(Rectangle arg0, int arg1, int arg2) {
		return this.scrollbarUnitIncrement;
	}

}