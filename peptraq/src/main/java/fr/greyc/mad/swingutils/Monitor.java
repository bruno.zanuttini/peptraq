package fr.greyc.mad.swingutils;

import java.util.Calendar;

import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Progress;

/**
 * A class for handling progression of tasks which are divided into steps of
 * approximately equal duration. At any instant, the monitor provides an
 * estimate of the time remaining until progression of the associated task.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class Monitor extends BasicObserved<Progress> {

	/** The number of steps already done. */
	private long nbStepsCompleted;

	/** The date (milliseconds from the Epoch) at which the task started. */
	private long startedAt;

	/** The time elapsed since the task started, in milliseconds. */
	private long elapsedTime;

	/** The estimated remaining time, in milliseconds, or null if unknown. */
	private Long remainingTime;
	
	/** The last time the estimate of remaining time was updated, in milliseconds from the Epoch. */
	private long lastEstimateTime;

	/** The total number of steps to be done. */
	protected final long nbSteps;

	/**
	 * The minimum number of milliseconds between two updates of the estimated
	 * remaining time (possibly 0).
	 */
	protected final long updateInterval;

	/**
	 * Builds a new instance, assuming no step has been completed already.
	 * 
	 * @param nbSteps        the total number of steps of the task
	 * @param updateInterval the minimum number of milliseconds between two updates
	 *                       of the estimated remaining time (possibly 0)
	 */
	public Monitor(long nbSteps, long updateInterval) {
		this.nbStepsCompleted = 0;
		this.startedAt = Calendar.getInstance().getTimeInMillis();
		this.elapsedTime = 0;
		this.remainingTime = null;
		this.lastEstimateTime = this.startedAt;
		this.nbSteps = nbSteps;
		this.updateInterval = updateInterval;
	}

	/**
	 * Returns the total number of steps of the task.
	 * 
	 * @return The total number of steps of the task
	 */
	public long getNbSteps() {
		return this.nbSteps;
	}

	/**
	 * Returns the number of steps of the task completed so far.
	 * 
	 * @return The number of steps of the task completed so far
	 */
	public long getNbStepsCompleted() {
		return this.nbStepsCompleted;
	}

	/**
	 * Decides whether an estimate of the remaining time is already available.
	 * 
	 * @return true if an estimate of the remaining time is available, false
	 *         otherwise
	 */
	public boolean hasEstimate() {
		return this.remainingTime != null;
	}

	/**
	 * Returns the estimated time remaining until completion of the task, in
	 * milliseconds.
	 * 
	 * @return The estimated time remaining until completion of the task, in
	 *         milliseconds
	 * @throws IllegalStateException if no estimate is already available
	 */
	public long getRemainingTime() throws IllegalStateException {
		if (this.remainingTime == null) {
			throw new IllegalStateException("An estimate of the remaining time is not yet available");
		}
		return this.remainingTime;
	}

	/**
	 * Indicates that some more steps of the task have been completed.
	 * 
	 * @param nbSteps The number of steps completed since the last call
	 */
	public void addNbSteps(long nbSteps) {
		this.setNbSteps(this.nbStepsCompleted + nbSteps);
	}

	/**
	 * Sets the total number of steps of the task completed so far.
	 * 
	 * @param nbSteps The number of steps completed since the task started
	 */
	public void setNbSteps(long nbSteps) {
		this.nbStepsCompleted = nbSteps;
		long currentTime = Calendar.getInstance().getTimeInMillis();
		if (currentTime - this.lastEstimateTime > this.updateInterval) {
			this.elapsedTime = currentTime - this.startedAt;
			this.remainingTime = this.elapsedTime * (this.nbSteps - this.nbStepsCompleted) / this.nbStepsCompleted;
			this.lastEstimateTime = currentTime;
		}
		super.notify(null);
	}

}
