# The following dependencies are required.

# External

biojava-aa-prop.jar version 6.0.0 # available from maven central (org.biojava.biojava-aa-prop)
biojava-core.jar version 6.0.0 # available from maven central (org.biojava.biojava-core)
jspp.jar # available at http://www.predisi.de/download.html

# Java

jaxb-api.jar # used by biojava
rt.jar # bootclasspath for java 8 ; needed for compilation only
slf4j-api.jar
slf4j-nop.jar (or another implementation of slf4j-api)

