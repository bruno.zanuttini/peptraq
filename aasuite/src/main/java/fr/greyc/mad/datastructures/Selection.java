package fr.greyc.mad.datastructures;

import java.util.HashSet;
import java.util.Set;

/**
 * A class for representing a selection of elements from a set in a sparse
 * manner, through a default status (selected/deselected) plus a set of
 * exceptions, that is, of elements with the opposite status.
 * <p>
 * An important point is that the instances of this class are not aware of the
 * whole set of elements, so that it only makes sense to hold this set fixed,
 * and to explicitly change the status only of its elements, for both the
 * selected and unselected sets of elements to be unambiguously defined.
 * <p>
 * Another important point is that sparsity is not guaranteed (as would be if
 * the default status were always the majoritary one). The representation
 * instead bets that the selection will be built with that majority status as
 * the default one, and that this status will be explicitly changed using
 * {@link #selectAll(boolean)} whenever the majority changes.
 * 
 * @author zanuttini
 *
 * @param <E> The type of elements
 */
public class Selection<E> {

	/**
	 * The status for all elements but those stored as exceptions (true for
	 * selected, false for unselected).
	 */
	private boolean defaultStatus;

	/** The set of elements whose status is the opposite of the default one. */
	private final Set<E> exceptions;

	/**
	 * Builds a new selection with a given status for all elements.
	 * 
	 * @param selected Whether all elements are selected (true) or all are
	 *                 unselected (false)
	 */
	public Selection(boolean selected) {
		this.defaultStatus = selected;
		this.exceptions = new HashSet<>();
	}

	/**
	 * Selects or unselects all elements.
	 * 
	 * @param select true for selecting all elements, false for unselecting all
	 *               elements
	 */
	public void selectAll(boolean select) {
		this.defaultStatus = select;
		this.exceptions.clear();
	}

	/**
	 * Decides whether an element is selected. The behavior is not defined if the
	 * element is not one of the implicit global set of elements.
	 * 
	 * @param element An element
	 * @return true if the given element is selected according to this instance,
	 *         false if it is unselcted
	 */
	public boolean isSelected(E element) {
		// Either sequences are selected by default and this one is no exception,
		// or sequences are unselected by default and this one is an exception.
		return this.defaultStatus != this.exceptions.contains(element);
	}

	/**
	 * Selects or unselects an element.
	 * 
	 * @param element An element
	 * @param select  true for selecting the element, false for unselecting it
	 */
	public void select(E element, boolean select) {
		if (this.defaultStatus != select) {
			this.exceptions.add(element);
		} else {
			this.exceptions.remove(element);
		}
	}

	/**
	 * Returns the number of elements which are selected, given the total number of
	 * sequences in the implicit global set (and tacitly assuming that all elements
	 * with an explicit selection status belong to this set).
	 * 
	 * @param nbElements A number of elements
	 * @return The number of selected elements given that there are nbElements in
	 *         the implicit global set
	 * @throws IllegalArgumentException if more than the given number of elements
	 *                                  have an explicit status in this selection
	 */
	public int nbSelected(int nbElements) throws IllegalArgumentException {
		if (nbElements < this.exceptions.size()) {
			throw new IllegalArgumentException("More elements than the given number have been explicitly set: "
					+ this.exceptions.size() + " vs " + nbElements);
		}
		return this.defaultStatus ? nbElements - this.exceptions.size() : this.exceptions.size();
	}

}
