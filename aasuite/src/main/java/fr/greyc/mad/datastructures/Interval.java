package fr.greyc.mad.datastructures;

/**
 * A class for representing a finite, nonempty interval of integers. Such
 * intervals are comparable: [x, y[ is less than [z, t[ if and only if x &lt; z
 * holds, or x = z and y &lt; t hold.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: equals, hashCode
 */
public class Interval implements Comparable<Interval> {

	/** The lowest element in the interval. */
	protected final int begin;

	/** The greatest element in the interval + 1. */
	protected final int end;

	/**
	 * Builds a new instance [begin, end[.
	 * 
	 * @param begin The lowest element in the interval
	 * @param end   The greatest element in the interval + 1
	 * @throws IllegalArgumentException if end is less than or equal to begin (which
	 *                                  would result in a nonempty interval).
	 */
	public Interval(int begin, int end) throws IllegalArgumentException {
		if (end <= begin) {
			throw new IllegalArgumentException("Cannot build an empty interval: " + begin + ", " + end);
		}
		this.begin = begin;
		this.end = end;
	}

	/**
	 * Builds a new interval containing a single integer.
	 * 
	 * @param i an integer
	 * @throws IllegalArgumentException if the given integer is Integer.MAX_VALUE
	 */
	public Interval(int i) throws IllegalArgumentException {
		this(i, i + 1);
		if (i == Integer.MAX_VALUE) {
			throw new IllegalArgumentException("Cannot build an interval containing only Integer.MAX_VALUE");
		}
	}

	/**
	 * Returns the lowest element in this interval.
	 * 
	 * @return The lowest element in this interval
	 */
	public int getBegin() {
		return this.begin;
	}

	/**
	 * Returns the greatest element in this interval + 1.
	 * 
	 * @return The greatest element in this interval + 1
	 */
	public int getEnd() {
		return this.end;
	}

	/**
	 * Decides whether a given integer is in this interval.
	 * 
	 * @param i An integer
	 * @return true if this interval contains i, false otherwise
	 */
	public boolean contains(int i) {
		return this.begin <= i && i < this.end;
	}

	@Override
	public int compareTo(Interval other) {
		return this.begin != other.begin ? Integer.compare(this.begin, other.begin)
				: Integer.compare(this.end, other.end);
	}

	@Override
	public String toString() {
		return "[" + this.begin + ", " + this.end + "]";
	}

}
