package fr.greyc.mad.datastructures;

import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

/**
 * A class for representing a union of intervals of integers. An example is
 * [-10,0] union [3,3] union [5,9]. The representation is always kept minimal in
 * the sense that, say, ([0,1] union [3,4] union [4,9]) or ([0,1] union [3,5]
 * union [4,9]) would both be represented as ([0,1] union [3,9]).
 * 
 * @author Bruno Zanuttini, Universit&eacute; de Caen Basse-Normandie, France
 * 
 *         TODO: equals, hashCode
 */
public class UnionOfIntervals {

	// Implementation note: the intervals are stored as semi-open intervals (to the
	// right), that is, [i,j] is stored as [i,j+1[

	/**
	 * An ordered list of integers representing intervals of the form
	 * (start_1,end_1,start_2,end_2,...) where the i-th interval is [start_i,end_i[.
	 */
	protected List<Integer> intervals;

	/**
	 * Builds a new instance with an empty set of intervals.
	 */
	public UnionOfIntervals() {
		this.intervals = new ArrayList<Integer>();
	}

	/**
	 * Decides whether a given integer is the first in some interval.
	 * 
	 * @param i An integer
	 * @return true if there is an interval [i,.] in this union of intervals, false
	 *         otherwise
	 */
	public boolean startsInterval(int i) {
		boolean isLowerBound = true; // true iff the integer currently considered in this.intervals is a lower bound
		for (int bound : this.intervals) {
			if (bound == i && isLowerBound)
				return true;
			if (bound > i)
				return false;
			isLowerBound = !isLowerBound;
		}
		return false;
	}

	/**
	 * Decides whether a given integer is the last in some interval.
	 * 
	 * @param i An integer
	 * @return true if there is an interval [.,i] in this union of intervals, false
	 *         otherwise
	 */
	public boolean endsInterval(int i) {
		boolean isUpperBound = false; // true iff the position currently considered in this.intervals is an upper
										// bound
		for (int bound : this.intervals) {
			if (bound == i + 1 && isUpperBound)
				return true;
			if (bound > i + 1)
				return false;
			isUpperBound = !isUpperBound;
		}
		return false;
	}

	/**
	 * Decides whether a given integer is in some interval.
	 * 
	 * @param i An integer
	 * @return true if there is an interval [a,b] in this union of intervals which
	 *         contains i, false otherwise
	 */
	public boolean contains(int i) {
		ListIterator<Integer> iterator = this.intervals.listIterator();
		int currentLowerBound, currentUpperBound;
		while (iterator.hasNext()) {
			currentLowerBound = iterator.next();
			currentUpperBound = iterator.next();
			if (currentLowerBound <= i && i <= currentUpperBound)
				return true;
		}
		return false;
	}

	/**
	 * Restricts this union of intervals to given bounds. For instance, restricting
	 * ([-10,-8] union [-4,1] union [3,9]) to 0,10 results in the union of intervals
	 * ([0,1] union [3,9])
	 * 
	 * @param lowerBound The first integer to be kept
	 * @param upperBound The last index to be kept
	 */
	public void restrict(int lowerBound, int upperBound) {
		int currentLowerBound, currentUpperBound;
		ListIterator<Integer> iterator = this.intervals.listIterator();
		while (iterator.hasNext()) {
			currentLowerBound = iterator.next();
			currentUpperBound = iterator.next();
			if (currentUpperBound <= lowerBound) {
				iterator.remove();
				iterator.previous();
				iterator.remove();
			} else if (currentLowerBound < lowerBound) {
				iterator.previous();
				iterator.previous();
				iterator.set(lowerBound);
				iterator.next();
				iterator.next();
			} else if (currentLowerBound > upperBound) {
				iterator.remove();
				iterator.previous();
				iterator.remove();
			} else if (currentUpperBound > upperBound + 1) {
				iterator.set(upperBound + 1);
			}
		}
	}

	/**
	 * Adds a given union of intervals to this one.
	 * 
	 * @param unionOfIntervals A union of intervals
	 */
	public void addAll(UnionOfIntervals unionOfIntervals) {
		Iterator<Integer> iterator = unionOfIntervals.intervals.iterator();
		while (iterator.hasNext())
			this.addInterval(iterator.next(), iterator.next());
	}

	/**
	 * Adds the same integer to all elements in this union of intervals. For
	 * instance, adding 5 to ([-10,-8] union [-4,1] union [3,9]) results in the
	 * union of intervals ([-5,-3] union [1,6] union [8,14]).
	 * 
	 * @param value The value to add (may be negative)
	 */
	public void addOffset(int value) {
		ListIterator<Integer> iterator = this.intervals.listIterator();
		int current;
		while (iterator.hasNext()) {
			current = iterator.next();
			iterator.previous();
			iterator.set(current + value);
			iterator.next();
		}
	}

	/**
	 * Adds an integer to this union of intervals (the integer i is seen as the
	 * interval [i,i]).
	 * 
	 * @param i The integer to add
	 */
	public void add(int i) {
		this.addInterval(i, i);
	}

	/**
	 * Adds an interval of integers to this union of intervals.
	 * 
	 * @param lowerBound The first element of the interval
	 * @param upperBound The last element of the interval (the interval is
	 *                   [lowerBound,upperBound])
	 * @throws IllegalArgumentException if the interval is not well-formed, i.e.,
	 *                                  upperBound is not geq lowerBound
	 */
	public void addInterval(int lowerBound, int upperBound) throws IllegalArgumentException {
		if (upperBound < lowerBound)
			throw new IllegalArgumentException(
					"The interval [" + lowerBound + "," + upperBound + "] is not well-formed");
		ListIterator<Integer> iterator = this.intervals.listIterator();
		boolean isLowerBound = true; // true iff the position currently considered in this.intervals is a lower bound
		boolean inserted = false;
		int current;
		// Inserting lower bound
		while (iterator.hasNext()) {
			current = iterator.next();
			if (isLowerBound && lowerBound < current) {
				iterator.previous();
				iterator.add(lowerBound);
				inserted = true;
				break;
			}
			if (isLowerBound && lowerBound >= current) {
				if (lowerBound < iterator.next()) {
					isLowerBound = !isLowerBound;
					inserted = true;
					iterator.previous();
					break;
				}
				isLowerBound = !isLowerBound;
				iterator.previous();
				continue;
			}
			if (!isLowerBound && lowerBound <= current) {
				iterator.previous();
				inserted = true;
				break;
			}
			isLowerBound = !isLowerBound;
		}
		if (!inserted) {
			iterator.add(lowerBound);
			iterator.add(upperBound + 1);
			return;
		}
		// Inserting upper bound
		while (iterator.hasNext()) {
			current = iterator.next();
			if (isLowerBound && upperBound + 1 < current) {
				iterator.previous();
				iterator.add(upperBound + 1);
				return;
			}
			if (isLowerBound && upperBound + 1 == current) {
				iterator.previous();
				iterator.remove();
				return;
			}
			if (isLowerBound && upperBound >= current) {
				iterator.previous();
				iterator.remove();
			}
			if (!isLowerBound && upperBound < current) {
				return;
			}
			if (!isLowerBound && upperBound >= current) {
				iterator.previous();
				iterator.remove();
			}
			isLowerBound = !isLowerBound;
		}
		iterator.add(upperBound + 1);
	}

	@Override
	public String toString() {
		String res = "";
		boolean isLowerBound = true; // true iff the position currently considered in this.intervals is a lower bound
		for (int bound : this.intervals) {
			if (isLowerBound)
				res += "[" + bound + ",";
			else
				res += (bound - 1) + "] ";
			isLowerBound = !isLowerBound;
		}
		return res;
	}

}
