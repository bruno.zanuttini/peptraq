package fr.greyc.mad.datastructures;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

/**
 * A sequence of elements which additionally maintains a current index. The
 * index allows one to focus on a specific position of the sequence, mostly as
 * if it were (temporarily) the end of the sequence.
 * <p>
 * This structure is designed for applications such as navigation between pages
 * in a web browser. As long as the sequence does not change, the current index
 * can be moved backwards and forward. Addition and removal of elements are
 * defined as if the current index denoted the end of the sequence, so that
 * addition occurs right after the current index, and removal occurs at the
 * current index. The current index is moved forward or backwards, respectively,
 * and in both cases the tail of the sequence is removed (think of changing
 * current page invalidating the "forward" button in a web browser).
 * <p>
 * Indices start from 0, and may also be null (no position focused at, to be
 * seen for all purposes as the current index being just before the beginning of
 * the sequence).
 * <p>
 * Note that this class does NOT implement {@code java.util.List}, since
 * modifications are heavily dependent on the current index. Another way to see
 * instances of this class is as stacks, with the property that as long as the
 * sequence itself does not change, the top of the stack can be defined to be at
 * any position in the sequence.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <E> The type of elements in the sequence
 * 
 */
public class InteractiveSequence<E> {

	/** A list to which storage of elements is delegated. */
	private final List<E> list;

	/** The current index (null if none). */
	private Integer currentIndex;

	/**
	 * Builds a new instance with no element.
	 */
	public InteractiveSequence() {
		this(new ArrayList<>());
	}

	/**
	 * Builds a new instance from a given list and no current index. The given list
	 * is backed up by this sequence, so that it should not be modified anywhere
	 * else (the behavior of this sequence would be undefined after this).
	 * 
	 * @param list A list
	 */
	public InteractiveSequence(List<E> list) {
		this(list, null);
	}

	/**
	 * Builds a new instance from a given list and a given current index. The given
	 * list is backed up by this one, so that it should not be modified anywhere
	 * else (the behavior of this sequence would be undefined after this).
	 * <p>
	 * The list is suppose to allow additional operations of addition, removal,
	 * etc., otherwise such operations will not be supported either by this
	 * sequence.
	 * 
	 * @param list  A list
	 * @param index An index in the list
	 * @throws IllegalArgumentException if the index is not a valid one in the list
	 */
	public InteractiveSequence(List<E> list, int index) throws IllegalArgumentException {
		this(list, Integer.valueOf(index));
	}

	/**
	 * Returns the size of this sequence, that is, its number of elements,
	 * independently of the current index.
	 * <p>
	 * The list is suppose to allow additional operations of addition, removal,
	 * etc., otherwise such operations will not be supported either by this
	 * sequence.
	 * 
	 * @return The size of this sequence
	 */
	public int size() {
		return this.list.size();
	}

	/**
	 * Decides whether this sequence is empty.
	 * 
	 * @return true if this sequence is empty, false otherwise
	 */
	public boolean isEmpty() {
		return this.list.isEmpty();
	}

	/**
	 * Returns the element at a given index in this sequence.
	 * 
	 * @param i An index in this sequence
	 * @return The element at the given index in this sequence
	 * @throws IndexOutOfBoundsException if the index is not valid for this sequence
	 */
	public E get(int i) throws IndexOutOfBoundsException {
		return this.list.get(i);
	}

	/**
	 * Decides whether there is a current index.
	 * 
	 * @return true if there is a current index, false otherwise
	 */
	public boolean hasCurrentIndex() {
		return this.currentIndex != null;
	}

	/**
	 * Returns the current index.
	 * 
	 * @return The current index
	 * @throws IllegalStateException if there is no current index
	 */
	public int getCurrentIndex() throws IllegalStateException {
		if (this.currentIndex == null) {
			throw new IllegalStateException("There is no current index");
		}
		return this.currentIndex;
	}

	/**
	 * Returns the element at the current index.
	 * 
	 * @return The element at the current index
	 * @throws IllegalStateException if there is no current index
	 */
	public E getCurrent() throws IllegalStateException {
		if (this.currentIndex == null) {
			throw new IllegalStateException("There is no current index");
		}
		return this.list.get(this.currentIndex);
	}

	/**
	 * Moves the current index to a given position.
	 * 
	 * @param index The target position
	 * @throws IndexOutOfBoundsException if the target index is not valid
	 */
	public void to(int index) throws IndexOutOfBoundsException {
		if (index < 0 || this.list.size() <= index) {
			throw new IndexOutOfBoundsException("Invalid index for sequence size: " + index + ", " + this.list.size());
		}
		this.currentIndex = index;
	}

	/**
	 * Adds an element right after the current index, and moves the index forward by
	 * 1, so that it corresponds to the new element. If there is no current index,
	 * then the element is inserted as the first element of the sequence, and the
	 * index becomes 0. All elements after the one added are discarded from the
	 * sequence.
	 * 
	 * @param element An element
	 */
	public void extendFromCurrent(E element) {
		this.removeTail();
		this.list.add(element);
		if (this.currentIndex == null) {
			this.currentIndex = 0;
		} else {
			this.currentIndex++;
		}
	}

	/**
	 * Removes the element at the current index, and moves the index backwards by 1.
	 * If the current index was 0, there is no current index after this call. All
	 * elements after the one added are discarded from the sequence.
	 * 
	 * @throws IllegalStateException if there is no current index
	 */
	public void removeCurrent() throws IllegalStateException {
		if (this.currentIndex == null) {
			throw new IllegalStateException("No current element to remove");
		}
		this.removeTail();
		// Next instruction: intValue enforces call to remove(int) instead of
		// remove(Object)
		this.list.remove(this.currentIndex.intValue());
		this.currentIndex--;
	}

	/**
	 * Clears this sequence (and as a consequence, unsets the current index).
	 */
	public void clear() {
		this.list.clear();
		this.currentIndex = null;
		this.removeTail();
	}

	/**
	 * Builds a new instance from the elements in a list.
	 * 
	 * @param list        A list of elements
	 * @param indexOrNull The current index to define, or null for none
	 * @throws IndexOutOfBoundsException If the index is not valid for the list
	 */
	private InteractiveSequence(List<E> list, Integer indexOrNull) throws IndexOutOfBoundsException {
		if (indexOrNull != null && (indexOrNull < 0 || list.size() <= indexOrNull)) {
			throw new IndexOutOfBoundsException("Invalid index xwrt list size: " + indexOrNull + ", " + list.size());
		}
		this.list = list;
		this.currentIndex = indexOrNull;
	}

	/**
	 * Helper methods: removes all elements after the current index (all elements if
	 * there is no current index).
	 */
	private void removeTail() {
		final int lastPreservedIndex = this.currentIndex == null ? -1 : this.currentIndex;
		ListIterator<E> iterator = this.list.listIterator(this.list.size());
		// For all rounds, i is the position at which an element is removed at this
		// round
		for (int i = this.list.size() - 1; i > lastPreservedIndex; i--) {
			iterator.previous();
			iterator.remove();
		}
	}

}
