/**
 * Classes for various data structures.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.greyc.mad.datastructures;