package fr.greyc.mad.datastructures;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A class for representing "pages" of elements, that is, sublists starting and
 * ending at given indices, as in paginated user interfaces, though without any
 * notion of representation for the user.
 * <p>
 * A page is associated with a page number, and first and last element indices
 * (relative to the whole list of which this is a page). In general, it is
 * conceived as part of a whole, and accordingly, it also knows whether it has a
 * previous and/or a next page. The last page of a collection may have fewer
 * elements than the given size of a page.
 * <p>
 * A page can never be empty. In particular, an empty list of elements has no
 * page at all, whatever the page size, and the page size cannot be 0.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <E> The type of elements in the page
 */
public class Page<E> {

	/** The list of elements in this page. */
	private List<E> pageElements;

	/** The number of the previous page (null if none). */
	private Integer previousPageNumber;

	/** The number of the next page (null if none). */
	private Integer nextPageNumber;

	/**
	 * The index of the first element of this page in the whole list (0-indexed,
	 * inclusive).
	 */
	private int firstElementIndex;

	/**
	 * The index of the last element of this page in the whole list (0-indexed,
	 * exclusive).
	 */
	private int lastElementIndex;

	/**
	 * Returns the number of pages for representing a collection. In the specific
	 * case when there are no elements, the number of pages is 0.
	 * 
	 * @param elements The whole collection of elements
	 * @param pageSize The size of each page
	 * @return The number of pages for representing a collection with the given page
	 *         size
	 * @throws IllegalArgumentException if the page size is not (strictly) positive
	 */
	public static int getNbPages(Collection<?> elements, int pageSize) throws IllegalArgumentException {
		if (pageSize <= 0) {
			throw new IllegalArgumentException();
		}
		int res = elements.size() / pageSize;
		return elements.size() % pageSize == 0 ? res : res + 1;
	}

	/**
	 * Builds a new instance. For efficiency, constructor
	 * {@link #Page(List, int, int)} should be preferred since it only creates a
	 * view of the list instead of retrieving and copying the elements. This
	 * constructor should be used only when the elements can be itered over but are
	 * not already stored as a list.
	 * 
	 * @param elements   The whole list of elements
	 * @param pageNumber The number of the page to build (0-indexed)
	 * @param pageSize   The size of each page
	 * @throws IndexOutOfBoundsException if the page number is not valid (negative,
	 *                                   or geq the number of pages), hence in
	 *                                   particular if there are no elements at all
	 */
	public Page(Iterable<E> elements, int pageNumber, int pageSize) throws IndexOutOfBoundsException {
		if (pageNumber < 0) {
			throw new IndexOutOfBoundsException("" + pageNumber);
		}
		this.firstElementIndex = pageSize * pageNumber;
		this.previousPageNumber = pageNumber == 0 ? null : pageNumber - 1;
		int i = 0;
		Iterator<E> iterator = elements.iterator();
		while (iterator.hasNext() && i < pageNumber * pageSize) {
			iterator.next();
			i++;
		}
		if (!iterator.hasNext()) {
			throw new IndexOutOfBoundsException("" + pageNumber);
		} else {
			int upperBound = pageSize * (pageNumber + 1);
			this.pageElements = new ArrayList<>();
			while (iterator.hasNext() && i < upperBound) {
				this.pageElements.add(iterator.next());
				i++;
			}
			this.lastElementIndex = i;
			this.nextPageNumber = iterator.hasNext() ? pageNumber + 1 : null;
		}
	}

	/**
	 * Builds a new instance.
	 * 
	 * @param list       The whole list of elements
	 * @param pageNumber The number of the page to build (0-indexed)
	 * @param pageSize   The size of each page
	 * @throws IndexOutOfBoundsException if the page number is not valid (negative,
	 *                                   or geq the number of pages), hence in
	 *                                   particular if the list of elements is empty
	 */
	public Page(List<E> list, int pageNumber, int pageSize) throws IndexOutOfBoundsException {
		this.firstElementIndex = pageSize * pageNumber;
		if (this.firstElementIndex >= list.size()) {
			throw new IndexOutOfBoundsException(
					"page number " + pageNumber + " for list of size " + list.size() + " with pageSize " + pageSize);
		}
		this.lastElementIndex = Math.min(pageSize * (pageNumber + 1), list.size());
		this.previousPageNumber = pageNumber == 0 ? null : pageNumber - 1;
		this.nextPageNumber = pageSize * (pageNumber + 1) >= list.size() ? null : pageNumber + 1;
		this.pageElements = list.subList(this.firstElementIndex, this.lastElementIndex);
	}

	/**
	 * Returns the elements of this page as a list.
	 * 
	 * @return The elements of this page
	 */
	public List<E> getPageElements() {
		return this.pageElements;
	}

	/**
	 * Decides whether there is a previous page for this one.
	 * 
	 * @return true if this page has a previous one, false otherwise
	 */
	public boolean hasPreviousPage() {
		return this.previousPageNumber != null;
	}

	/**
	 * Returns the number of the previous page (0-indexed).
	 * 
	 * @return The number of the previous page
	 * @throws NoSuchElementException if there is no previous page
	 */
	public int getPreviousPageNumber() throws NoSuchElementException {
		if (this.previousPageNumber == null) {
			throw new NoSuchElementException("previous page");
		}
		return this.previousPageNumber;
	}

	/**
	 * Decides whether there is a next page for this one.
	 * 
	 * @return true if this page has a next one, false otherwise
	 */
	public boolean hasNextPage() {
		return this.nextPageNumber != null;
	}

	/**
	 * Returns the number of the next page (0-indexed).
	 * 
	 * @return The number of the next page
	 * @throws NoSuchElementException if there is no next page
	 */
	public int getNextPageNumber() throws NoSuchElementException {
		if (this.nextPageNumber == null) {
			throw new NoSuchElementException("next page");
		}
		return this.nextPageNumber;
	}

	/**
	 * Returns the index of the first element of this page in the whole list
	 * (0-indexed, inclusive).
	 * 
	 * @return The index of the first element of this page
	 */
	public int getFirstElementIndex() {
		return this.firstElementIndex;
	}

	/**
	 * Returns the index of the last element of this page in the whole list
	 * (0-indexed, exclusive).
	 * 
	 * @return The index of the last element of this page
	 */
	public int getLastElementIndex() {
		return this.lastElementIndex;
	}

}
