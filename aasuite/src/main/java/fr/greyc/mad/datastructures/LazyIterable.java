package fr.greyc.mad.datastructures;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * A base class for a lazy representation of a iterable collection of elements.
 * Such a representation behaves like a list, except that an element is
 * explicited only the first time when it is accessed; explicitation is defined
 * by concrete subclasses.
 * <p>
 * This representation can be seen as a Java stream, but importantly, it is not
 * consumed when the elements are accessed, so that they can be accessed over
 * and over without expliciting them again.
 * <p>
 * This representation is supposed to be very efficient when the elements of the
 * list are the result of heavy computations (explicitation). Note however that
 * there might be counterintuitive behaviors, like an empty list being very long
 * to iterate over because a huge number of computations need be done which
 * produce no element in the end.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <E> The type of elements
 */
public abstract class LazyIterable<E> implements Iterable<E> {

	/** The list of elements already explicited (a prefix of the collection). */
	protected ArrayList<E> explicited;

	/**
	 * Builds a new instance with no element.
	 */
	public LazyIterable() {
		this.explicited = new ArrayList<>();
	}

	@Override
	public Iterator<E> iterator() {
		return new Iterator<E>() {

			private int i = 0;

			private boolean explicitUpTo(int i) {
				while (LazyIterable.this.explicited.size() <= i) {
					List<E> newExplicit = LazyIterable.this.tryExplicitMore();
					if (newExplicit.isEmpty()) {
						return false;
					} else {
						LazyIterable.this.explicited.addAll(newExplicit);
					}
				}
				return true;
			}

			@Override
			public boolean hasNext() {
				return this.explicitUpTo(i);
			}

			@Override
			public E next() {
				boolean ok = this.explicitUpTo(i);
				if (ok) {
					return LazyIterable.this.explicited.get(i++);
				} else {
					throw new NoSuchElementException();
				}
			}
		};
	}

	/**
	 * Explicits a number of elements after the ones already explicit. How many
	 * elements to explicit is left to concrete implementations, however, at least 1
	 * element should be explicited except if there are no more elements to
	 * explicit.
	 * 
	 * @return A list containing at least one new explicit element if there is one,
	 *         and en empty list otherwise
	 */
	protected abstract List<E> tryExplicitMore();

}
