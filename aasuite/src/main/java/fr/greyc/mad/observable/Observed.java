package fr.greyc.mad.observable;

import java.util.List;

/**
 * An interface for objects which can be observed, through notifications sent to
 * all observers.
 * 
 * <p>
 * As a general rule, the order of observers matters; they are notified in
 * order. Each notification is blocking (except of course if the observer runs
 * asynchronously).
 *
 * @see Observer
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <N> The type of notifications
 *
 *        TODO: change to PropertyChangeSupport?
 */
public interface Observed<N> {

	/**
	 * Returns the list of observers of this instance.
	 * @return The list of observers of this instance
	 */
	List<Observer<N>> getObservers();
	
	/**
	 * Adds an observer at the end of the current list. Nothing occurs if the
	 * observer is already registered.
	 * 
	 * @param observer An observer
	 */
	void addObserver(Observer<N> observer);

	/**
	 * Removes an observer from the current list. Nothing occurs if the observer was
	 * not registered.
	 * 
	 * @param observer An observer
	 */
	void removeObserver(Observer<N> observer);

	/**
	 * Notifies all observers.
	 * 
	 * @param notification The notification to send
	 */
	void notify(N notification);

}