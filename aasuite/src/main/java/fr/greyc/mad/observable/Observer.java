package fr.greyc.mad.observable;

/**
 * An interface for observers of notifications.
 * 
 * @see Observed
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <N> The type of notifications received
 * 
 *        TODO: change to PropertyChangeListener?
 */
public interface Observer<N> {

	/**
	 * Called when this observer is notified.
	 * 
	 * @param notifier     The notifier
	 * @param notification The notification
	 */
	public void onNotification(BasicObserved<N> notifier, N notification);

}
