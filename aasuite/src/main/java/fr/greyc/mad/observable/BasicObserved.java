package fr.greyc.mad.observable;

import java.util.ArrayList;
import java.util.List;

/**
 * A basic implementation of interface {@link Observed}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <N> The type of notifications
 *
 *        TODO: change to PropertyChangeSupport?
 */
public class BasicObserved<N> implements Observed<N> {

	/** The list of observers of this instance. */
	private final List<Observer<N>> observers;

	/**
	 * Builds a new instance with no observer.
	 */
	public BasicObserved() {
		this.observers = new ArrayList<>();
	}

	@Override
	public List<Observer<N>> getObservers() {
		return this.observers;
	}

	@Override
	public void addObserver(Observer<N> observer) {
		if (!this.observers.contains(observer)) {
			this.observers.add(observer);
		}
	}

	@Override
	public void removeObserver(Observer<N> observer) {
		this.observers.remove(observer);
	}

	@Override
	public void notify(N notification) {
		for (Observer<N> observer : this.observers) {
			observer.onNotification(this, notification);
		}
	}

}
