package fr.greyc.mad.observable;

/**
 * An abstract class for representing ongoing processes which can be monitored,
 * and possibly cancelled. As a general rule, an instance of a Monitorable
 * executor is supposed to represent one execution, and hence should not be
 * "executed" several times.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public abstract class MonitorableExecutor extends BasicObserved<Progress> {

	/** A human-readable description for this instance. */
	protected final String description;

	/**
	 * Builds a new instance.
	 * 
	 * @param description A human-readable description for the instance
	 */
	public MonitorableExecutor(String description) {
		this.description = description;
	}

	/**
	 * Returns the human-readable description for this instance.
	 * 
	 * @return The human-readable description for this instance
	 */
	public String getDescription() {
		return this.description;
	}

	/**
	 * Cancels the execution of this process.
	 */
	public abstract void cancel();

}
