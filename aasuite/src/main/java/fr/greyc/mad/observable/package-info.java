/**
 * Classes for observable processes (and classes in general). Specifically, this
 * package defines classes for monitorable processes (i.e., whose progress can
 * be monitored).
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.greyc.mad.observable;