package fr.greyc.mad.observable;

/**
 * A class holding information about the progress of a process. The intended use
 * is as a type for notifications by {@link Observed} objects.
 * <p>
 * Each instance holds a numeric value. The semantics of this value is not
 * specified, so that it may be used differently in different contexts. Typical
 * uses would be for specifying how many atomic operations were performed, or
 * how many remain to do, for instance.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class Progress {

	/** An enumeration of possible types of progress. */
	public enum ProgressType {

		/** Indicates that the process has started. */
		STARTED,

		/** Indicates that the process has progressed. */
		PROGRESSED,

		/** Indicates that the process ended normally. */
		FINISHED,

		/** Indicates that the process was cancelled. */
		CANCELLED,

		/** Indicates that there was an error during the execution of the process. */
		ERROR;

	}

	/** The executor whose progress is concerned. */
	protected final MonitorableExecutor executor;

	/** The type of this instance. */
	protected final Progress.ProgressType type;

	/** The value associated to this instance. */
	protected final long value;

	/**
	 * The error message for this instance (nonnull if and only if the type if
	 * {@code ERROR}.
	 */
	protected final String errorMessage;

	/**
	 * Builds a new instance with type {@code ERROR}.
	 * 
	 * @param executor     The executor whose progress is monitored
	 * @param value        The value associated to this instance
	 * @param errorMessage The error message for this instance
	 */
	public Progress(MonitorableExecutor executor, long value, String errorMessage) {
		super();
		this.executor = executor;
		this.type = ProgressType.ERROR;
		this.value = value;
		this.errorMessage = errorMessage;
	}

	/**
	 * Builds a new instance with any type but {@code ERROR}.
	 * 
	 * @param executor The executor whose progress is monitored
	 * @param type     The type of this instance
	 * @param value    The value associated to this instance
	 * @throws IllegalArgumentException if the type is {@code ERROR}
	 */
	public Progress(MonitorableExecutor executor, Progress.ProgressType type, long value)
			throws IllegalArgumentException {
		super();
		if (type == ProgressType.ERROR) {
			throw new IllegalArgumentException("Error notifications must come with an error message");
		}
		this.executor = executor;
		this.type = type;
		this.value = value;
		this.errorMessage = null;
	}

	/**
	 * Returns the executor whose progress is concerned.
	 * 
	 * @return The executor whose progress is concerned
	 */
	public MonitorableExecutor getExecutor() {
		return this.executor;
	}

	/**
	 * Returns the type of this instance.
	 * 
	 * @return The type of this instance
	 */
	public Progress.ProgressType getType() {
		return type;
	}

	/**
	 * Returns the value associated to this instance.
	 * 
	 * @return The value associated to this instance
	 */
	public long getValue() {
		return value;
	}

	/**
	 * Returns the error message for this instance.
	 * 
	 * @return The error message for this instance
	 * @throws IllegalStateException if the type of this instance is not
	 *                               {@code ERROR}
	 */
	public String getErrorMessage() throws IllegalStateException {
		if (!this.type.equals(ProgressType.ERROR)) {
			throw new IllegalStateException("Only error notifications have a message");
		}
		return this.errorMessage;
	}

}