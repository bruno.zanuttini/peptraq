/**
 * Facilities for handling resources.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.greyc.mad.resources;