package fr.greyc.mad.resources;

import java.util.HashSet;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.function.Consumer;

/**
 * A base class for handlers of a resource bundle, whose main purpose is to
 * provide facilities for performing operations only for those resources which
 * are defined in the bundle, to check whether all defined resources have been
 * used, etc.
 * <p>
 * In general, a key is said to be "used" if it is a key in the bundle or in one
 * of its parents, and the associated object has been retrieved, even if this
 * object did not have the expected type or its subsequent treatment ended
 * abnormally. A key is said to be "nonexisting" if it has been attempted to
 * retrieve the associated object, but the key is not one in the bundle or in
 * one of its parents.
 * <p>
 * Instances of this class are not instances of ResourceBundle. Should a method
 * be called on the wrapped bundle, the method {@link #getBundle()} can be used.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class ResourceHandler {

	/** The resource bundle. */
	protected final ResourceBundle bundle;

	/** The set of all used keys. */
	private final Set<String> used;

	/** The set of all keys which have been checked but do not exist. */
	private final Set<String> nonexisting;

	/**
	 * Builds a new instance.
	 * 
	 * @param bundle A resource bundle
	 */
	public ResourceHandler(ResourceBundle bundle) {
		this.bundle = bundle;
		this.used = new HashSet<>();
		this.nonexisting = new HashSet<>();
	}

	/**
	 * Returns the resource bundle.
	 * 
	 * @return The resource bundle
	 */
	public ResourceBundle getBundle() {
		return this.bundle;
	}

	/**
	 * Performs an operation with the string associated to a given key, throwing an
	 * exception if the key does not exist in the bundle nor in any of its parent.
	 * It the key exists, additionally marks it as used, even if it is not
	 * associated with a string, or handling it throws an exception or otherwise
	 * ends abnormally. If the key does not exist, marks it as nonexisting.
	 * 
	 * @param handler The operation to perform, called with the string associated to
	 *                the key
	 * @param key     A key in the bundle
	 * @throws MissingResourceException if the key does not exist in the bundle (nor
	 *                                  in any of its parents)
	 * @throws ClassCastException       if the key exists but the associated object
	 *                                  is not a string
	 */
	public void handleString(Consumer<String> handler, String key) throws MissingResourceException, ClassCastException {
		if (!this.bundle.containsKey(key)) {
			this.setUsed(key);
		} else {
			this.setNonexisting(key);
		}
		String str = this.bundle.getString(key);
		handler.accept(str);
	}

	/**
	 * Performs an operation with the string associated to a given key, only if the
	 * key exists in the bundle or in one of its parent (otherwise does nothing). If
	 * the key exists and is indeed associated with a string, additionally marks it
	 * as used, even if handling it throws an exception or otherwise ends
	 * abnormally. If the key does not exist, marks it as nonexisting.
	 * 
	 * @param handler The operation to perform, called with the string associated to
	 *                the key
	 * @param key     A key in the bundle
	 * @throws ClassCastException if the key exists but the associated object is not
	 *                            a string
	 */
	public void handleStringIfExists(Consumer<String> handler, String key) throws ClassCastException {
		if (this.bundle.containsKey(key)) {
			this.setUsed(key);
			handler.accept(this.bundle.getString(key));
		} else {
			this.setNonexisting(key);
		}
	}

	/**
	 * Performs an operation with the string array associated to a given key,
	 * throwing an exception if the key does not exist in the bundle nor in any of
	 * its parent. It the key exists, additionally marks it as used, even if it is
	 * not associated with a string array, or handling it throws an exception or
	 * otherwise ends abnormally. If the key does not exist, marks it as
	 * nonexisting.
	 * 
	 * @param handler The operation to perform, called with the string array
	 *                associated to the key
	 * @param key     A key in the bundle
	 * @throws MissingResourceException if the key does not exist in the bundle (nor
	 *                                  in any of its parents)
	 * @throws ClassCastException       if the key exists but the associated object
	 *                                  is not a string array
	 */
	public void handleStringArray(Consumer<String[]> handler, String key)
			throws MissingResourceException, ClassCastException {
		if (this.bundle.containsKey(key)) {
			this.setUsed(key);
		} else {
			this.setNonexisting(key);
		}
		String[] str = this.bundle.getStringArray(key);
		handler.accept(str);
	}

	/**
	 * Performs an operation with the string array associated to a given key, only
	 * if the key exists in the bundle or in one of its parent (otherwise does
	 * nothing). If the key exists and is indeed associated with a string array,
	 * additionally marks it as used, even if handling it throws an exception or
	 * otherwise ends abnormally. If the key does not exist, marks it as
	 * nonexisting.
	 * 
	 * @param handler The operation to perform, called with the string array
	 *                associated to the key
	 * @param key     A key in the bundle
	 * @throws ClassCastException if the key exists but the associated object is not
	 *                            a string array
	 */
	public void handleStringArrayIfExists(Consumer<String[]> handler, String key) throws ClassCastException {
		if (this.bundle.containsKey(key)) {
			this.setUsed(key);
			handler.accept(this.bundle.getStringArray(key));
		} else {
			this.setNonexisting(key);
		}
	}

	/**
	 * Performs an operation with the object associated to a given key, throwing an
	 * exception if the key does not exist in the bundle nor in any of its parent.
	 * It the key exists, additionally marks it as used, even if handling it throws
	 * an exception or otherwise ends abnormally. If the key does not exist, marks
	 * it as nonexisting.
	 * 
	 * @param handler The operation to perform, called with the string associated to
	 *                the key
	 * @param key     A key in the bundle
	 * @throws MissingResourceException if the key does not exist in the bundle (nor
	 *                                  in any of its parents)
	 */
	public void handle(Consumer<Object> handler, String key) throws MissingResourceException {
		if (this.bundle.containsKey(key)) {
			this.setUsed(key);
		} else {
			this.setNonexisting(key);
		}
		Object str = this.bundle.getObject(key);
		handler.accept(str);
	}

	/**
	 * Performs an operation with the object associated to a given key, only if the
	 * key exists in the bundle or in one of its parent (otherwise does nothing). If
	 * the key exists, additionally marks it as used, even if handling it throws an
	 * exception or otherwise ends abnormally. If the key does not exist, marks it
	 * as nonexisting.
	 * 
	 * @param handler The operation to perform, called with the string associated to
	 *                the key
	 * @param key     A key in the bundle
	 */
	public void handleIfExists(Consumer<Object> handler, String key) {
		if (this.bundle.containsKey(key)) {
			this.setUsed(key);
			handler.accept(this.bundle.getObject(key));
		} else {
			this.setNonexisting(key);
		}
	}

	/**
	 * Decides whether a key has been marked as used. Note that if the key does not
	 * exist in the bundle (nor in any of its parents), then by construction this
	 * call will return false.
	 * 
	 * @param key A key
	 * @return true if the key has been marked as used, false otherwise
	 */
	public boolean used(String key) {
		return this.used.contains(key);
	}

	/**
	 * Decides whether all the keys of the bundle have been marked as used.
	 * 
	 * @return true if all the keys of the bundle have been marked as used, false
	 *         otherwise
	 */
	public boolean allUsed() {
		return this.used.equals(bundle.keySet());
	}

	/**
	 * Returns the set of all keys of the bundle (or one of its parents) which have
	 * not been marked as used.
	 * 
	 * @return The set of all keys which have not been marked as used
	 */
	public Set<String> unused() {
		Set<String> res = new HashSet<>(bundle.keySet());
		res.removeAll(this.used);
		return res;
	}

	/**
	 * Returns the set of all keys which have been marked as nonexisting.
	 * 
	 * @return The set of all keys which have been marked as nonexisting
	 */
	public Set<String> nonexisting() {
		return this.nonexisting;
	}

	/**
	 * Marks a key as used.
	 * 
	 * @param key A key
	 * @throws IllegalArgumentException if the key is not one of the bundle (nor of
	 *                                  any of its parents)
	 */
	protected void setUsed(String key) throws IllegalArgumentException {
		if (!this.bundle.containsKey(key)) {
			throw new IllegalArgumentException("Key does not exist: " + key);
		}
		this.used.add(key);
	}

	/**
	 * Marks a key as nonexisting.
	 * 
	 * @param key A key
	 * @throws IllegalArgumentException if the key is one of the bundle (or of one
	 *                                  of its parents)
	 */
	protected void setNonexisting(String key) throws IllegalArgumentException {
		if (this.bundle.containsKey(key)) {
			throw new IllegalArgumentException("Key exists: " + key);
		}
		this.nonexisting.add(key);
	}

}
