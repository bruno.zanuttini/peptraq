package fr.greyc.mad.userinteraction;

import java.io.File;
import java.util.function.Supplier;

/**
 * An abstract file handler, which factors handling of errors through a
 * proposer.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public abstract class AbstractFileHandler implements FileHandler {

	/**
	 * A message for informing the user that a reading problem occurred. The string
	 * may use 1$ to refer to the absolute path of the file (as returned by
	 * {@link File#getAbsolutePath()}), 2$ to refer to the file name (as returned by
	 * {@link File#getName()}, and 3$ to refer to the exception message. The string
	 * may be multiline.
	 */
	public static String readingProblemMessage = "Problem when reading file: %1$s" + System.lineSeparator()
			+ "Error: %3$s" + System.lineSeparator() + "Please check and correct the file, or select another one.";

	/** The title for the message in case of a reading problem. */
	public static String readingProblemTitle = "Error";

	/**
	 * A message for informing the user that a reading problem occurred. The string
	 * may use 1$ to refer to the absolute path of the file (as returned by
	 * {@link File#getAbsolutePath()}), 2$ to refer to the file name (as returned by
	 * {@link File#getName()}, and 3$ to refer to the exception message. The string
	 * may be multiline.
	 */
	public static String writingProblemMessage = "Problem when writing to file: %1$s" + System.lineSeparator()
			+ "Error: %3$s" + System.lineSeparator() + "Please check permissions or select another file.";

	/** The title for the message in case of a writing problem. */
	public static String writingProblemTitle = "Error";

	/** A proposer for interacting with the user. */
	protected final Proposer proposer;

	/**
	 * Builds a new instance.
	 * 
	 * @param proposer A proposer for interacting with the user
	 */
	public AbstractFileHandler(Proposer proposer) {
		this.proposer = proposer;
	}

	@Override
	public File handleReadException(File file, Exception exception, Supplier<File> callBack) {
		String message = String.format(readingProblemMessage, file.getAbsolutePath(), file.getName(),
				exception.getMessage());
		boolean tryAgain = this.proposer.propose(readingProblemTitle, message);
		return tryAgain ? callBack.get() : null;
	}

	@Override
	public File handleWriteException(File file, Exception exception, Supplier<File> callBack) {
		String message = String.format(writingProblemMessage, file.getAbsolutePath(), file.getName(),
				exception.getMessage());
		boolean tryAgain = this.proposer.propose(writingProblemTitle, message);
		return tryAgain ? callBack.get() : null;
	}

}
