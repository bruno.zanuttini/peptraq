/**
 * Interfaces and classes for interaction with a user. This package does not
 * assume any particular mode of interaction (textual, graphicall, etc.) and
 * rather defines abstract interactions.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.greyc.mad.userinteraction;