package fr.greyc.mad.userinteraction;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;

/**
 * An interface for classes allowing interaction with the user.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public interface Proposer {

	/**
	 * Displays a message and waits until the user answers "OK".
	 * 
	 * @param title   A title for the message
	 * @param message The message to display
	 */
	void inform(String title, String message);

	/**
	 * Displays a message and waits until the user answers "OK" or "Cancel".
	 * 
	 * @param title   A title for the message
	 * @param message The message to display
	 * @return true if the user answered "OK", false if she answered "Cancel"
	 */
	boolean propose(String title, String message);

	/**
	 * Displays a question and waits until the user answers "Yes" or "No", or
	 * cancels.
	 * 
	 * @param title    A title for the question
	 * @param question The question to display (including the question mark)
	 * @return true if the user answered "Yes", false if she answered "No", null if
	 *         she cancelled
	 */
	Boolean ask(String title, String question);

	/**
	 * Displays a message warning the user of a precisely characterized error and
	 * waits until confirmation.
	 * 
	 * @param message An error message
	 */
	void tellError(String message);

	/**
	 * Displays a builder and allows the user to fill it in.
	 * 
	 * @param title      A title for the message proposing the builder
	 * @param builder    A builder
	 * @param showErrors Whether to display errors in the builder (true) or not
	 *                   (false)
	 * @return true if the user confirmed, false if she cancelled the construction;
	 *         note that the user may confirm while there are errors in the builder
	 */
	boolean propose(String title, AbstractBuilder<InputField<String, ?>, ?> builder, boolean showErrors);

}
