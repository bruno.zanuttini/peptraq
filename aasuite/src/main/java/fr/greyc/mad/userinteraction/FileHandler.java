package fr.greyc.mad.userinteraction;

import java.io.File;
import java.util.function.Supplier;

/**
 * An interface for (input and output) file choosers. Such a chooser allows the
 * user to choose a file and to test it (existence and permissions).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public interface FileHandler {

	/**
	 * Returns an input file chosen by the user after checking existence and
	 * permission to read.
	 * 
	 * @param description A description of the types of files expected to be chosen
	 * @param extensions  Expected extensions of chosen file names
	 * @return The file chosen by the user, or null if no file has been chosen
	 */
	File chooseInputFile(String description, String... extensions);

	/**
	 * Returns an output file chosen by the user after checking permission to write.
	 * 
	 * @param defaultFile A default file proposed to the user
	 * @param description A description of the types of files expected to be chosen
	 * @param extensions  Expected extensions of chosen file names
	 * @return The file chosen by the user, or null if no file has been chosen
	 */
	File chooseOutputFile(File defaultFile, String description, String... extensions);

	/**
	 * Returns an output file chosen by the user after checking permission to write.
	 * 
	 * @param description A description of the types of files expected to be chosen
	 * @param extensions  Expected extensions of chosen file names
	 * @return The file chosen by the user, or null if no file has been chosen
	 */
	File chooseOutputFile(String description, String... extensions);

	/**
	 * Handles an exception which occurred when reading a file, by informing the
	 * user and executing a callback if the user wants to try again.
	 * 
	 * @param file      The file whose reading threw an exception
	 * @param exception An exception
	 * @param callBack  A callback function
	 * @return The file chosen by the user, or null if no file has been chosen
	 */
	File handleReadException(File file, Exception exception, Supplier<File> callBack);

	/**
	 * Handles an exception which occurred when writing to a file, by informing the
	 * user and executing a callback if the user wants to try again.
	 * 
	 * @param file      The file whose writing to threw an exception
	 * @param exception An exception
	 * @param callBack  A callback function
	 * @return The file chosen by the user, or null if no file has been chosen
	 */
	File handleWriteException(File file, Exception exception, Supplier<File> callBack);

}
