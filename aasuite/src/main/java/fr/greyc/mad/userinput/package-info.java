/**
 * Classes for handling user input. This package essentially defines (abstract)
 * builders, which are combinations of fields of various types. This is
 * essentially similar to forms being combinations of input fields, but there is
 * no notion of a representation for the user, except for labels, help strings
 * and error strings.
 * <p>
 * The fields are of several types: simple fields ({@link Field}) just hold a
 * value, possibly an invalid one; input fields ({@link InputField}) hold an
 * input which describes a value (the input can be invalid); multiple fields may
 * hold zero, one, or several values.
 * <p>
 * The implementation of any (meaningful) combination can be realized using the
 * decorators defined in the packages.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, Francex
 */
package fr.greyc.mad.userinput;
