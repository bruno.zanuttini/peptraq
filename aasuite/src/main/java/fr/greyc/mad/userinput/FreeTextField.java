package fr.greyc.mad.userinput;

/**
 * A free text input field, that is, a field which has strings both as input and
 * values, with no constraints except that the value may not be blank. As a
 * consequence, blank strings are not considered to be valid values, whether the
 * field is optional or not.
 * <p>
 * Note that leading and trailing white space is preserved between inputs and
 * values (with the provisio that a blank string, including one consisting of
 * only white space, is considered to be a blank input).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class FreeTextField extends InputFieldFromStringAdapter<String> {

	/**
	 * Builds a new instance with an initial input string.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for this field
	 * @param initialInput The initial input string for this field (may be blank
	 *                     even if the field is not optional)
	 * @param helpString   The help string for this field
	 */
	public FreeTextField(boolean isOptional, String label, String initialInput, String helpString) {
		super(new FreeField<String>(isOptional, label, helpString), initialInput);
	}

	/**
	 * Builds a new instance with no initial input string.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for this field
	 * @param helpString The help string for this field
	 */
	public FreeTextField(boolean isOptional, String label, String helpString) {
		super(new FreeField<String>(isOptional, label, helpString));
	}

	@Override
	public String getInputFor(String value) throws IllegalArgumentException {
		return value;
	}

	@Override
	public String getValue(String input) throws IllegalArgumentException {
		return input;
	}

	@Override
	protected String tryGetValue(String input) {
		return input;
	}

}
