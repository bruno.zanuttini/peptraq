package fr.greyc.mad.userinput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * A class for representing generic fields with bound values, that is, with a
 * reasonably sized list of valid values (see the documentation for
 * {@link Field}). Note that instances of this class are considered to have
 * bound values however many valid values there are.
 * <p>
 * This class does NOT allow null to be a valid value. In fact, null represents
 * the absence of any value (even for nonoptional fields). Moreover, this class
 * does NOT allow a nonnull, nonvalid value to be set.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of values in the field
 */
public class BoundValueField<V> extends FieldAdapter<V> {

	/**
	 * The error for an invalid nonnull value (the error defined by
	 * {@link FieldAdapter#noValueError} is used for the absence of value in a
	 * nonoptional field). The description may use 1$ for referring to the current
	 * value, which is nonnull by definition.
	 */
	public static String invalidValueError = "Value %1$s is not allowed";

	/** The list of valid values. */
	protected List<V> validValues;

	/**
	 * Builds a new instance with a given list of valid values and a (valid) initial
	 * value.
	 * 
	 * @param isOptional   Whether this field is optional (true) or not (false)
	 * @param label        The label for this field
	 * @param validValues  The list of valid values for this field
	 * @param initialValue The initial value for this field, or null for no initial
	 *                     value
	 * @param helpString   The help string for this field
	 * @throws IllegalArgumentException If the given value is neither null nor valid
	 */
	public BoundValueField(boolean isOptional, String label, List<V> validValues, V initialValue, String helpString)
			throws IllegalArgumentException {
		// Note: not a call to super(..., initialValue) because allowedValues has to be
		// set before setting the initial value
		super(isOptional, label, helpString);
		this.validValues = validValues;
		this.setValue(initialValue);
	}

	/**
	 * Builds a new instance with a given list of valid values but no initial value.
	 * 
	 * @param isOptional  Whether this field is optional (true) or not (false)
	 * @param label       The label for this field
	 * @param validValues The list of valid values for this field
	 * @param helpString  The help string for this field
	 */
	public BoundValueField(boolean isOptional, String label, List<V> validValues, String helpString) {
		this(isOptional, label, validValues, null, helpString);
	}

	/**
	 * Builds a new instance with no valid value and no initial value.
	 * 
	 * @param isOptional Whether this field is optional (true) or not (false)
	 * @param label      The label for this field
	 * @param helpString The help string for this field
	 */
	public BoundValueField(boolean isOptional, String label, String helpString) {
		this(isOptional, label, new ArrayList<V>(), helpString);
	}

	/**
	 * Adds a value to the list of valid values of this field. The value is added to
	 * the end of the list of valid values, even if it was already in the list.
	 * <p>
	 * This method only has protected visibility because some undesirable effects
	 * may occur, like a field which is not valid, then valid whereas its current
	 * value has not changed (just because the value was added as valid). If a
	 * subclass exposes it with a greater visibility, care should be taken of such
	 * effects.
	 * 
	 * @param value A value
	 */
	protected void addAllowedValue(V value) {
		this.validValues.add(value);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method always returns false.
	 */
	@Override
	public final boolean hasBoundValues() {
		return true;
	}

	@Override
	public List<V> getValidValues() {
		return this.validValues;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws IllegalArgumentException if the value is not a valid one (but does
	 *                                  NOT throw one if the value is null, even if
	 *                                  the field is not optional)
	 */
	@Override
	public void setValue(V value) throws IllegalArgumentException {
		if (value != null && !this.validValues.contains(value)) {
			throw new IllegalArgumentException("Value " + value + " is neither null nor an allowed value");
		}
		super.setValue(value);
	}

	@Override
	public boolean isValid(V value) {
		return this.validValues.contains(value);
	}

	@Override
	public List<String> getErrors(V value) {
		if (this.validValues.contains(value)) {
			return Collections.emptyList();
		} else {
			return Collections.singletonList(String.format(invalidValueError, value));
		}
	}

}
