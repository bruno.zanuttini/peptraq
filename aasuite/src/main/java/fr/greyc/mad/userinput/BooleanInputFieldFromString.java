package fr.greyc.mad.userinput;

import java.util.Arrays;
import java.util.List;

/**
 * A bound value input field for Boolean values from strings. Note that
 * {@code null} is NOT a valid value for this field (it encodes the absence of
 * input and value, like with general input fields.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class BooleanInputFieldFromString extends InputFieldFromStringAdapter<Boolean> {

	/**
	 * Boolean values {@code true} and {@code false}, wrapped in a list. The order
	 * in this list defines the order of values in the sense of
	 * {@link BoundValueField}.
	 */
	public static List<Boolean> booleans = Arrays.asList(true, false);

	/** The string encoding {@code true}. */
	protected final String trueString;

	/** The string encoding {@code false}. */
	protected final String falseString;

	/**
	 * Builds a new instance.
	 * 
	 * @param isOptional  Whether this field is optional (true) or not (false)
	 * @param label       The label for this field
	 * @param trueString  The string encoding {@code true}
	 * @param falseString The string encoding {@code false}
	 * @param helpString  The help string for this field
	 */
	public BooleanInputFieldFromString(boolean isOptional, String label, String trueString, String falseString,
			String helpString) {
		super(new BoundValueField<>(isOptional, label, BooleanInputFieldFromString.booleans, helpString));
		this.trueString = trueString;
		this.falseString = falseString;
	}

	/**
	 * Builds a new instance with default encodings for {@code true} and
	 * {@code false}, namely {@code Boolean.TRUE.toString()} and
	 * {@code Boolean.FALSE.toString()}, respectively.
	 * 
	 * @param isOptional Whether this field is optional (true) or not (false)
	 * @param label      The label for this field
	 * @param helpString The help string for this field
	 */
	public BooleanInputFieldFromString(boolean isOptional, String label, String helpString) {
		this(isOptional, label, Boolean.TRUE.toString(), Boolean.FALSE.toString(), helpString);
	}

	/**
	 * Builds a new instance with an initial input.
	 * 
	 * @param isOptional   Whether this field is optional (true) or not (false)
	 * @param label        The label for this field
	 * @param trueString   The string encoding {@code true}
	 * @param falseString  The string encoding {@code false}
	 * @param initialInput The initial input for this field (not necessarily one
	 *                     encoding a valid value)
	 * @param helpString   The help string for this field
	 */
	public BooleanInputFieldFromString(boolean isOptional, String label, String trueString, String falseString,
			String initialInput, String helpString) {
		this(isOptional, label, trueString, falseString, helpString);
		this.setInput(initialInput);
	}

	@Override
	public String getInputFor(Boolean value) throws IllegalArgumentException {
		return value ? this.trueString : this.falseString;
	}

	@Override
	protected Boolean tryGetValue(String input) throws IllegalArgumentException {
		if (this.trueString.equals(input)) {
			return Boolean.TRUE;
		} else if (this.falseString.equals(input)) {
			return Boolean.FALSE;
		} else {
			throw new IllegalArgumentException("No Boolean value corresponds to string \"" + input + "\"");
		}
	}

}
