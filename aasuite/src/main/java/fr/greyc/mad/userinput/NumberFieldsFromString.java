package fr.greyc.mad.userinput;

import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * A factory providing input fields from strings for types of numbers.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class NumberFieldsFromString {

	/**
	 * Returns an input field from strings for integer values, with an initial
	 * input.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input for the field
	 * @param helpString   The help string for the field
	 * @return An input field from strings for integer values
	 */
	public static InputField<String, Integer> makeIntegerField(boolean isOptional, String label, String initialInput,
			String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Integer>(isOptional, label, initialInput, helpString) {
			@Override
			protected Integer tryGetValue(String input) throws IllegalArgumentException {
				return Integer.parseInt(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for integer values, with no initial
	 * input.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param helpString The help string for the field
	 * @return An input field from strings for integer values
	 */
	public static InputField<String, Integer> makeIntegerField(boolean isOptional, String label, String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Integer>(isOptional, label, helpString) {
			@Override
			protected Integer tryGetValue(String input) throws IllegalArgumentException {
				return Integer.parseInt(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for long values, with an initial input.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input for the field
	 * @param helpString   The help string for the field
	 * @return An input field from strings for long values
	 */
	public static InputField<String, Long> makeLongField(boolean isOptional, String label, String initialInput,
			String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Long>(isOptional, label, initialInput, helpString) {
			@Override
			protected Long tryGetValue(String input) throws IllegalArgumentException {
				return Long.parseLong(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for long values, with no initial input.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param helpString The help string for the field
	 * @return An input field from strings for long values
	 */
	public static InputField<String, Long> makeLongField(boolean isOptional, String label, String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Long>(isOptional, label, helpString) {
			@Override
			protected Long tryGetValue(String input) throws IllegalArgumentException {
				return Long.parseLong(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for BigInteger values, with an initial
	 * input.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input for the field
	 * @param helpString   The help string for the field
	 * @return An input field from strings for BigInteger values
	 */
	public static InputField<String, BigInteger> makeBigIntegerField(boolean isOptional, String label,
			String initialInput, String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<BigInteger>(isOptional, label, initialInput,
				helpString) {
			@Override
			protected BigInteger tryGetValue(String input) throws IllegalArgumentException {
				return new BigInteger(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for BigInteger values, with no initial
	 * input.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param helpString The help string for the field
	 * @return An input field from strings for BigInteger values
	 */
	public static InputField<String, BigInteger> makeBigIntegerField(boolean isOptional, String label,
			String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<BigInteger>(isOptional, label, helpString) {
			@Override
			protected BigInteger tryGetValue(String input) throws IllegalArgumentException {
				return new BigInteger(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for float values, with an initial input.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input for the field
	 * @param helpString   The help string for the field
	 * @return An input field from strings for float values
	 */
	public static InputField<String, Float> makeFloatField(boolean isOptional, String label, String initialInput,
			String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Float>(isOptional, label, initialInput, helpString) {
			@Override
			protected Float tryGetValue(String input) throws IllegalArgumentException {
				return Float.parseFloat(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for float values, with no initial input.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param helpString The help string for the field
	 * @return An input field from strings for float values
	 */
	public static InputField<String, Float> makeFloatField(boolean isOptional, String label, String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Float>(isOptional, label, helpString) {
			@Override
			protected Float tryGetValue(String input) throws IllegalArgumentException {
				return Float.parseFloat(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for double values, with an initial input.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input for the field
	 * @param helpString   The help string for the field
	 * @return An input field from strings for double values
	 */
	public static InputField<String, Double> makeDoubleField(boolean isOptional, String label, String initialInput,
			String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Double>(isOptional, label, initialInput, helpString) {
			@Override
			protected Double tryGetValue(String input) throws IllegalArgumentException {
				return Double.parseDouble(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for double values, with no initial input.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param helpString The help string for the field
	 * @return An input field from strings for double values
	 */
	public static InputField<String, Double> makeDoubleField(boolean isOptional, String label, String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<Double>(isOptional, label, helpString) {
			@Override
			protected Double tryGetValue(String input) throws IllegalArgumentException {
				return Double.parseDouble(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for BigDecimal values, with an initial
	 * input.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input for the field
	 * @param helpString   The help string for the field
	 * @return An input field from strings for BigDecimal values
	 */
	public static InputField<String, BigDecimal> makeBigDecimalField(boolean isOptional, String label,
			String initialInput, String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<BigDecimal>(isOptional, label, initialInput,
				helpString) {
			@Override
			protected BigDecimal tryGetValue(String input) throws IllegalArgumentException {
				return new BigDecimal(input);
			}
		};
	}

	/**
	 * Returns an input field from strings for BigDecimal values, with no initial
	 * input.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param helpString The help string for the field
	 * @return An input field from strings for BigDecimal values
	 */
	public static InputField<String, BigDecimal> makeBigDecimalField(boolean isOptional, String label,
			String helpString) {
		return new NumberFieldsFromString.NumberFieldFromString<BigDecimal>(isOptional, label, helpString) {
			@Override
			protected BigDecimal tryGetValue(String input) throws IllegalArgumentException {
				return new BigDecimal(input);
			}
		};
	}

	/**
	 * An abstract base class for input fields for values from strings.
	 * 
	 * @param <N> The type of values
	 */
	private static abstract class NumberFieldFromString<N> extends InputFieldFromStringAdapter<N> {

		/**
		 * Builds a new instance with an initial input.
		 * 
		 * @param isOptional   Whether this field is optional (true) or not (false)
		 * @param label        The label for this field
		 * @param initialInput The initial input for this field
		 * @param helpString   The help string for this field
		 */
		private NumberFieldFromString(boolean isOptional, String label, String initialInput, String helpString) {
			super(new FreeField<N>(isOptional, label, helpString), initialInput);
		}

		/**
		 * Builds a new instance with no initial input.
		 * 
		 * @param isOptional Whether this field is optional (true) or not (false)
		 * @param label      The label for this field
		 * @param helpString The help string for this field
		 */
		private NumberFieldFromString(boolean isOptional, String label, String helpString) {
			super(new FreeField<N>(isOptional, label, helpString));
		}

	}

}
