package fr.greyc.mad.userinput;

import java.util.List;

/**
 * An interface for fields whose value is defined by some input (for instance, a
 * field whose input are strings but values are integers). For such a field,
 * either the value or the input can be set, resulting in a consistent input
 * (resp. value) to be automatically set, so that the input and the value are
 * always consistent together (provided they are valid).
 * <p>
 * As a general rule, null encodes the absence of input and the absence of
 * value.
 * <p>
 * In general, there may be several inputs which encode the same value (e.g.,
 * the strings "1", "1.0", "1.00"... all encode the rational number 1). Whenever
 * one must be chosen, a method may decide which one (typically, there will be a
 * canonical one, "1" in the previous case).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <I> The type of inputs
 * @param <V> The type of values
 */
public interface InputField<I, V> extends Field<V> {

	/**
	 * Sets the input of this field. If the input is valid, the value is
	 * automatically updated to the one which it encodes. Otherwise, the method may
	 * accept the invalid input, or throw an exception.
	 * 
	 * @param input An input
	 * @throws IllegalArgumentException possibly, if the input is not valid
	 */
	void setInput(I input) throws IllegalArgumentException;

	/**
	 * Decides whether this field currently has an input (which may be invalid).
	 * 
	 * @return true if this field currently has an input, false otherwise
	 */
	boolean hasInput();

	/**
	 * Returns the current input of this field. The behavior is undefined if this
	 * field currently has no input.
	 * 
	 * @return The current input of this field
	 * @throws IllegalStateException possibly, if this field currently does not have
	 *                               any input
	 */
	I getInput() throws IllegalStateException;

	/**
	 * Returns an input encoding a given value. The behavior is undefined if the
	 * value is not valid for this field.
	 * <p>
	 * When there are several inputs encoding the given value, the method may return
	 * an arbitrary one.
	 * 
	 * @param value A value
	 * @return an input which encodes the given value
	 * @throws IllegalArgumentException possibly, if the value is not valid
	 */
	I getInputFor(V value) throws IllegalArgumentException;

	/**
	 * Returns a list of valid inputs, one per valid value of this field, provided this
	 * field has bound values (as having bound values or not is supposed to be a
	 * property of the class, this operation is optional). The order might be
	 * important, so this method should returned the inputs in the same order as
	 * the corresponding values are returned by {@link #getValidValues()}..
	 * 
	 * @return A list of valid inputs for this field, one per valid value
	 * @throws UnsupportedOperationException if this field does not have bound
	 *                                       values
	 */
	List<I> getValidInputs() throws UnsupportedOperationException;
	
}
