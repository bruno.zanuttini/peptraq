package fr.greyc.mad.userinput;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstract class for objects able to build other objects from fields.
 * 
 * A builder has an internal state, which consists of the state of the fields as
 * well as a global assessment of validity of the parameters, to be used when
 * there are interfield constraints. How this is assessed is to be defined by
 * subclasses.
 * 
 * The order of fields is relevant: they are retrieved in the same order as they
 * were added.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <F> The type for fields
 * @param <T> The type of the objects built
 */
public abstract class AbstractBuilder<F extends Field<?>, T> {

	/** The current list of fields in this builder. */
	protected List<F> fields;

	/** The help string for this builder. */
	protected final String helpString;

	/**
	 * Builds a new instance.
	 * 
	 * @param fields     The initial list of fields in this instance
	 * @param helpString The help string for this builder
	 */
	public AbstractBuilder(List<F> fields, String helpString) {
		this.fields = fields;
		this.helpString = helpString;
	}

	/**
	 * Builds a new instance with an empty initial list of fields.
	 * 
	 * @param helpString The help string for this builder
	 */
	public AbstractBuilder(String helpString) {
		this(new ArrayList<>(), helpString);
	}

	/**
	 * Adds a field to this instance.
	 * 
	 * The new field is added after the previous ones.
	 * 
	 * @param field A field
	 * @throws IllegalArgumentException if the field is already contained in this
	 *                                  instance
	 */
	public void addField(F field) throws IllegalArgumentException {
		if (this.fields.contains(field)) {
			throw new IllegalArgumentException("Field " + field + " is already contained in builder");
		}
		this.fields.add(field);
	}

	/**
	 * Returns the fields in this instance.
	 * 
	 * @return The fields in this instance
	 */
	public List<F> getFields() {
		return this.fields;
	}

	/**
	 * Decides whether the parameters currently input in the fields are valid
	 * (individually and together).
	 * 
	 * @return true if the parameters currently input are valid, false otherwise
	 */
	public boolean isValid() {
		for (F field : this.fields) {
			if (!field.isValid()) {
				return false;
			}
		}
		return this.isGloballyValid();
	}

	/**
	 * Returns the object defined by the parameters currently input in the fields,
	 * if they are valid (individually and together).
	 * 
	 * @return The object defined by the parameters currently input in the fields
	 * @throws IllegalStateException If the current parameters are not valid (either
	 *                               some of them or together)
	 */
	public T getObject() throws IllegalStateException {
		if (!this.isValid()) {
			throw new IllegalStateException("Cannot build an object: data is not valid");
		}
		return this.safeGetObject();
	}

	/**
	 * Returns the help string for this builder.
	 * 
	 * @return The help string for this builder
	 */
	public String getHelpString() {
		return this.helpString;
	}

	/**
	 * Returns the help string for a field of this builder. The implementation
	 * forwards the call to the field, but this behavior may be overridden by
	 * subclasses in case the help string for a field depends on the builder or the
	 * builder state.
	 * 
	 * @param field A field of this builder
	 * @return The help string for the given field
	 * @throws IllegalArgumentException if the field is not one of this builder's
	 */
	public String getHelpString(InputField<String, ?> field) throws IllegalArgumentException {
		if (!this.fields.contains(field)) {
			throw new IllegalArgumentException("Not a field of this builder: " + field);
		}
		return field.getHelpString();
	}

	/**
	 * Decides whether the parameters currently input are consistent together.
	 * 
	 * This method may be called even when the parameters are not individually
	 * valid. That this method returns true does not mean that the object is ready
	 * to be built. This is guaranteed only if this method returns true AND all
	 * fields are currently individually valid.
	 * 
	 * @return true if the parameters currently input are consistent together, false
	 *         otherwise
	 */
	public abstract boolean isGloballyValid();

	/**
	 * Returns the errors pertaining to the global consistency of the parameters
	 * currently input. The list is empty if these parameters are currently
	 * consistent together, and otherwise there is a single-line string for each
	 * error.
	 * 
	 * @return The errors pertaining to the global consistency of the parameters
	 *         currently input
	 */
	public abstract List<String> getGlobalErrors();

	/**
	 * Returns the object defined by the parameters currently input, assuming
	 * (without checking) that they are valid (both individually and together).
	 * 
	 * @return The object defined by the parameters currently input
	 */
	protected abstract T safeGetObject();

}
