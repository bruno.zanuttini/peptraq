package fr.greyc.mad.userinput;

/**
 * An adapter for input fields with strings as input. The adapter simplifies the
 * design of such input fields by wrapping a field, and assuming that the string
 * returned by {@code v.toString} is the canonical valid input for a value
 * {@code v}. Conversely, how a value is retrieved from an input is left to
 * concrete subclasses. Inputs are always trimmed, and in particular the blank
 * inputs are exactly the blank strings.
 * <p>
 * The label, optional character, help string, etc. are simply copied from the
 * wrapped field.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of values in the field
 */
public abstract class InputFieldFromStringAdapter<V> extends InputFieldAdapter<String, V> {

	/**
	 * Builds a new instance.
	 * 
	 * @param field The field to wrap; its value, if any, is ignored
	 */
	public InputFieldFromStringAdapter(Field<V> field) {
		super(field);
	}

	/**
	 * Builds a new instance with an initial input.
	 * 
	 * @param field        The field to wrap; its value, if any, is ignored
	 * @param initialInput The initial input for this field
	 */
	public InputFieldFromStringAdapter(Field<V> field, String initialInput) throws IllegalArgumentException {
		super(field, initialInput);
	}

	@Override
	public String getInputFor(V value) throws IllegalArgumentException {
		return value.toString();
	}

	@Override
	public boolean isBlank(String input) {
		return "".equals(input.trim());
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method returns the value associated to the trimmed version of the given input.
	 */
	@Override
	public V getValue(String input) throws IllegalArgumentException {
		try {
			return this.tryGetValue(input.trim());
		} catch (Exception e) {
			throw new IllegalArgumentException("String \"" + input + "\" does not represent a valid value", e);
		}
	}

	/**
	 * Returns the value corresponding to a given input, if possible. The method is
	 * supposed to be called only with trimmed strings, so that this method can
	 * assume that its argument is trimmed.
	 * 
	 * @param input An input string
	 * @return The value encoded by the given input string
	 * @throws IllegalArgumentException if the input encodes no value
	 */
	protected abstract V tryGetValue(String input) throws IllegalArgumentException;

}