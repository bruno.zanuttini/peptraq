package fr.greyc.mad.userinput;

import java.util.ArrayList;
import java.util.List;

/**
 * An adapter for multiple fields, based on a field for atomic values (defining
 * which ones are valid) and a minimum and/or maximum number of atomic values in
 * the list.
 * <p>
 * Whether a multiple field is optional or not is uniquely defined by the
 * minimum number of atomic values: the field is optional if and only if there
 * is no minimum, or the minimum is 0. In particular, the empty list is
 * interpreted as the multiple field having no value (whether optional or not).
 * <p>
 * Importantly, a multiple field never has bound values (even if there are a
 * small number of atomic values and, say, only singleton lists are valid).
 * <p>
 * The single field used by this multiple field should NOT be used (for
 * instance, its value set) elsewhere.
 * <p>
 * Note that the label, help string, optional character, etc. of the wrapped
 * field for atomic values, are all ignored.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of atomic values for this field
 */
public class MultipleFieldAdapter<V> implements MultipleField<V> {

	/** The error when there is no value while the field is not optional. */
	public static String noValueError = "At least one value must be provided";

	/**
	 * The error when there are not enough values. The string may use 1$ to refer to
	 * the minimum number for this instance.
	 */
	public static String lowerBoundError = "At least %1$d values must be provided";

	/**
	 * The error when there are too many values. The string may use 1$ to refer to
	 * the maximum number for this instance.
	 */
	public static String upperBoundError = "At most %1$d values must be provided";

	/** The label of this multiple field. */
	protected final String label;

	/** A field defining which atomic values are valid. */
	protected final Field<V> singleField;

	/**
	 * The minimum number of atomic values in the current value (list) for this
	 * multiple field to be valid (null for no minimum).
	 */
	protected final Integer minNbValues;

	/**
	 * The minimum number of atomic values in the current value (list) for this
	 * multiple field to be valid (null for no minimum).
	 */
	protected final Integer maxNbValues;

	/** The help string for this multiple field. */
	protected final String helpString;

	/** The current list of atomic values for this field. */
	private List<V> value;

	/**
	 * Builds a new instance with explicit minimum and maximum numbers of atomic
	 * values, and an initial list of atomic values.
	 * 
	 * @param label         The label for this multiple field
	 * @param atomicField   A field defining which atomic values are valid
	 * @param minNbValues   The minimum number of atomic values for this field to be
	 *                      valid (null or 0 for no minimum, in which case the field
	 *                      is interpreted as optional)
	 * @param maxNbValues   The minimum number of atomic values for this field to be
	 *                      valid (null for no maximum)
	 * @param initialValues The initial list of atomic values for this field (null
	 *                      or an empty list are both interpreted as an absence of
	 *                      value)
	 * @param helpString    The help string for this field
	 * @throws IllegalArgumentException if the minimum number if greated than the
	 *                                  maximum
	 */
	public MultipleFieldAdapter(String label, Field<V> atomicField, Integer minNbValues, Integer maxNbValues,
			List<V> initialValues, String helpString) throws IllegalArgumentException {
		if (minNbValues != null && maxNbValues != null && minNbValues > maxNbValues) {
			throw new IllegalArgumentException(
					"Minimum number of values should not be greater than maximum: " + minNbValues + ", " + maxNbValues);
		}
		this.label = label;
		this.singleField = atomicField;
		this.minNbValues = minNbValues;
		this.maxNbValues = maxNbValues;
		this.value = initialValues;
		this.helpString = helpString;
	}

	/**
	 * Builds a new instance without an explicit minimum nor a maximum numbers of
	 * atomic values, and an initial list of atomic values.
	 * 
	 * @param isOptional    Whether this field is optional (true) or not; if the
	 *                      field is not optional, then the minimum number of atomic
	 *                      values is set to 1
	 * @param label         The label for this multiple field
	 * @param atomicField   A field defining which atomic values are valid
	 * @param initialValues The initial list of atomic values for this field (null
	 *                      or an empty list are both interpreted as an absence of
	 *                      value)
	 * @param helpString    The help string for this field
	 */
	public MultipleFieldAdapter(boolean isOptional, String label, Field<V> atomicField, List<V> initialValues,
			String helpString) {
		this(label, atomicField, isOptional ? null : 1, null, initialValues, helpString);
	}

	/**
	 * Builds a new instance with explicit minimum and maximum numbers of atomic
	 * values, and no initial list of atomic values.
	 * 
	 * @param label       The label for this multiple field
	 * @param atomicField A field defining which atomic values are valid
	 * @param minNbValues The minimum number of atomic values for this field to be
	 *                    valid (null or 0 for no minimum, in which case the field
	 *                    is interpreted as optional)
	 * @param maxNbValues The minimum number of atomic values for this field to be
	 *                    valid (null for no maximum)
	 * @param helpString  The help string for this field
	 * @throws IllegalArgumentException if the minimum number if greated than the
	 *                                  maximum
	 */
	public MultipleFieldAdapter(String label, Field<V> atomicField, Integer minNbValues, Integer maxNbValues,
			String helpString) throws IllegalArgumentException {
		this(label, atomicField, minNbValues, maxNbValues, null, helpString);
	}

	/**
	 * Builds a new instance without an explicit minimum nor a maximum numbers of
	 * atomic values, and no initial list of atomic values.
	 * 
	 * @param isOptional  Whether this field is optional (true) or not; if the field
	 *                    is not optional, then the minimum number of atomic values
	 *                    is set to 1
	 * @param label       The label for this multiple field
	 * @param atomicField A field defining which atomic values are valid
	 * @param helpString  The help string for this field
	 */
	public MultipleFieldAdapter(boolean isOptional, String label, Field<V> atomicField, String helpString) {
		this(isOptional, label, atomicField, null, helpString);
	}

	@Override
	public boolean isOptional() {
		return this.minNbValues == null || this.minNbValues == 0;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method always returns false.
	 */
	@Override
	public final boolean hasBoundValues() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Unsupported operation.
	 */
	@Override
	public List<List<V>> getValidValues() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Multiple field adapters are never considered to have bound values");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Recall that both null and an empty list are considered to be the absence of
	 * value.
	 */
	@Override
	public void setValue(List<V> value) {
		this.value = value;
	}

	@Override
	public boolean hasValue() {
		return this.value != null;
	}

	@Override
	public List<V> getValue() {
		return this.value;
	}

	@Override
	public boolean isValid() {
		if (this.value == null) {
			return this.isOptional();
		} else if (this.minNbValues != null && this.value.size() < this.minNbValues) {
			return false;
		} else if (this.maxNbValues != null && this.value.size() > this.maxNbValues) {
			return false;
		} else {
			for (V value : this.value) {
				this.singleField.setValue(value);
				if (!this.singleField.isValid()) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public List<String> getErrors() {
		List<String> res = new ArrayList<>();
		if (this.value == null) {
			if (!this.isOptional()) {
				res.add(noValueError);
			}
		} else {
			if (this.minNbValues != null && this.value.size() < this.minNbValues) {
				res.add(String.format(lowerBoundError, this.minNbValues));
			}
			if (this.maxNbValues != null && this.value.size() > this.maxNbValues) {
				res.add(String.format(upperBoundError, this.maxNbValues));

			}
			for (V value : this.value) {
				this.singleField.setValue(value);
				if (!this.singleField.isValid()) {
					res.addAll(this.singleField.getErrors());
				}
			}
		}
		return res;
	}

	@Override
	public String getHelpString() {
		return this.helpString;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method never throws an exception (invalid atomic values, or values
	 * making the list invalid, are allowed).
	 */
	@Override
	public void addValue(V atomicValue) {
		if (this.value == null) {
			this.value = new ArrayList<>();
		}
		this.value.add(atomicValue);
	}

}
