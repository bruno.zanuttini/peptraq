package fr.greyc.mad.userinput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * An adapter for input fields, based on a field so that essentially only the
 * correspondence between individual inputs and individual values remain to be
 * defined. The label, help strings, error strings are simply copied from the
 * encapsulated field, but subclasses may override this behavior.
 * <p>
 * Like with general input fields, the null value is reserved for encoding the
 * absence of an input and the absence of a value. However, other inputs/values
 * may also encode this absence; in particular, by analogy with strings, we call
 * "blank" input any input which is not null but still encodes the absence of
 * input.
 * <p>
 * Importantly, the encapsulated field should not be used elsewhere.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <I> The type of inputs for the field
 * @param <V> The type of values in the field
 */
public abstract class InputFieldAdapter<I, V> implements InputField<I, V> {

	/**
	 * The error string explaining that there should be a value entered in the
	 * field.
	 */
	public static String noValueError = "A value must be provided";

	/**
	 * The error string explaining that an input is not valid. The string may use 1$
	 * to refer to the invalid input.
	 */
	public static String badInputError = "Input \"%1$s\" is not valid";

	// Implementation note: any setting of value or input is implemented by
	// explicitly setting the input, then setting the value corresponding to this
	// input (or null if the input has no corresponding value).

	/** The encapsulated field. */
	private Field<V> field;

	/** The current input. */
	private I input;

	/**
	 * Builds a new instance with no input (and hence no value).
	 * 
	 * @param field The field to encapsulate (observe that its value, if any, is
	 *              ignored)
	 */
	public InputFieldAdapter(Field<V> field) {
		this.field = field;
	}

	/**
	 * Builds a new instance with an initial input (and hence an initial value, if
	 * the input encodes one).
	 * 
	 * @param field        The field to encapsulate (observe that its value, if any,
	 *                     is ignored)
	 * @param initialInput The initial input for this field, not necessarily a valid
	 *                     one
	 * @throws IllegalArgumentException possibly, if the input corresponds to an
	 *                                  invalid value
	 */
	public InputFieldAdapter(Field<V> field, I initialInput) throws IllegalArgumentException {
		this(field);
		this.setInput(initialInput);
	}

	@Override
	public boolean isOptional() {
		return this.field.isOptional();
	}

	@Override
	public String getLabel() {
		return this.field.getLabel();
	}

	@Override
	public boolean hasBoundValues() {
		return this.field.hasBoundValues();
	}

	@Override
	public List<V> getValidValues() {
		return this.field.getValidValues();
	}

	@Override
	public final void setValue(V value) throws IllegalArgumentException {
		this.setInput(this.getInputFor(value));
	}

	@Override
	public boolean hasValue() {
		return this.hasInput() && this.field.hasValue();
	}

	@Override
	public V getValue() {
		return this.hasInput() ? this.field.getValue() : null;
	}

	@Override
	public boolean isValid() {
		if (this.input == null || this.isBlank(this.input)) {
			return this.isOptional();
		} else if (!this.field.hasValue()) {
			return false;
		} else {
			return this.field.isValid();
		}
	}

	@Override
	public List<String> getErrors() {
		if (this.input == null || this.isBlank(input)) {
			if (this.isOptional()) {
				return Collections.emptyList();
			} else {
				return Collections.singletonList(noValueError);
			}
		} else if (!this.field.hasValue()) {
			return Collections.singletonList(String.format(badInputError, input));
		} else {
			return this.field.getErrors();
		}
	}

	@Override
	public String getHelpString() {
		return this.field.getHelpString();
	}

	@Override
	public final void setInput(I input) throws IllegalArgumentException {
		this.input = input;
		if (this.input == null || this.isBlank(this.input)) {
			this.field.setValue(null);
		} else {
			try {
				this.field.setValue(this.getValue(input));
			} catch (IllegalArgumentException e) {
				this.field.setValue(null);
			}
		}
	}

	@Override
	public boolean hasInput() {
		return this.input != null && !this.isBlank(this.input);
	}

	@Override
	public I getInput() throws IllegalStateException {
		if (!this.hasInput()) {
			throw new IllegalStateException("Field has no input");
		} else {
			return this.input;
		}
	}

	@Override
	public List<I> getValidInputs() {
		// Implementation note: will throw UnsupportedOperationException if (and only
		// if) so does this.field.getValidValues()
		List<I> res = new ArrayList<>();
		for (V value : this.field.getValidValues()) {
			res.add(this.getInputFor(value));
		}
		return res;
	}

	/**
	 * Decides whether a nonnull input is blank, that is, encodes the absence of
	 * input. The behavior is underfined on the null string.
	 * 
	 * @param input An input
	 * @return true if the given input is blank, false otherwise
	 * @throws IllegalArgumentException possibly, if the input is null
	 */
	public abstract boolean isBlank(I input) throws IllegalArgumentException;

	/**
	 * Returns the value encoded by a given input which is not null and not blank
	 * (the behavior is undefined for such inputs).
	 * 
	 * @param input An input
	 * @return The value encoded by the given input
	 * @throws IllegalArgumentException if the input does not encode any value
	 */
	// FIXME: this suggests to program using exceptions, as done in setInput in this
	// class.
	// Replace with using an additional method isValidInput(I) or encodesValue(I).
	public abstract V getValue(I input) throws IllegalArgumentException;

}
