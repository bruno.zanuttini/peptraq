package fr.greyc.mad.userinput;

import java.util.Collections;
import java.util.List;

/**
 * A simple restriction decorator for fields, defined by an abstract method deciding the
 * validity of a value and a single error string in case the restriction is not
 * met.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of values for this field
 */
public abstract class SimpleRestrictionDecorator<V> extends RestrictionDecorator<V> {

	/** The single error string for this field. */
	protected final String errorString;

	/**
	 * Builds a new instance.
	 * @param field The decorated field
	 * @param errorString The single error string
	 */
	public SimpleRestrictionDecorator(Field<V> field, String errorString) {
		super(field);
		this.errorString = errorString;
	}

	@Override
	protected List<String> getErrors(V value) {
		if (this.isValid(value)) {
			return Collections.emptyList();
		} else {
			return Collections.singletonList(this.errorString);
		}
	}

}
