package fr.greyc.mad.userinput;

import java.util.ArrayList;
import java.util.List;

/**
 * An adapter for multiple fields with input from strings. The adapter wraps an
 * input field for atomic values (from strings) and defines the composite input
 * for a list of values to be the atomic inputs separated by a specific string.
 * <p>
 * The separator is assumed to allow for unambiguous decoding of multiple inputs
 * into lists of atomic inputs.
 * <p>
 * Like for general multiple fields, the wrapped fields are not supposed to be
 * used elsewhere, the list of values cannot be a valid input (it encodes the
 * absence of multiple input/multiple values), and the field is optional if and
 * only if there is no minimum number of values (or it is 0).
 * <p>
 * Note that the label, optional character, help string, etc. of the wrapped
 * input field for atomic values are all ignored.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of atomic values
 */
public class MultipleInputFieldFromStringAdapter<V> extends MultipleInputFieldAdapter<String, String, V> {

	/** The separator between atomic inputs. */
	protected final String separator;

	/**
	 * Builds a new instance with no input value, and no minimum nor maximum number
	 * of atomic inputs.
	 * 
	 * @param isOptional       Whether this multiple field is optional (true) or not
	 *                         (false)
	 * @param label            The label for this multiple field
	 * @param atomicInputField An input field for atomic values
	 * @param separator        The separator between atomic inputs
	 * @param helpString       The help string for this multiple field
	 */
	public MultipleInputFieldFromStringAdapter(boolean isOptional, String label, InputField<String, V> atomicInputField,
			String separator, String helpString) {
		super(new MultipleFieldAdapter<>(isOptional, label, atomicInputField, helpString), atomicInputField);
		this.separator = separator;
	}

	/**
	 * Builds a new instance with no input value, but with minimum and maximum
	 * numbers of atomic inputs.
	 * 
	 * @param label            The label for this multiple field
	 * @param atomicInputField An input field for atomic values
	 * @param minNbValues      The minimum number of values for this field to be
	 *                         valid (null for no minimum)
	 * @param maxNbValues      The maximum number of values for this field to be
	 *                         valid (null for no maximum)
	 * @param separator        The separator between atomic inputs
	 * @param helpString       The help string for this multiple field
	 */
	public MultipleInputFieldFromStringAdapter(String label, InputField<String, V> atomicInputField,
			Integer minNbValues, Integer maxNbValues, String separator, String helpString) {
		super(new MultipleFieldAdapter<>(label, atomicInputField, minNbValues, maxNbValues, helpString),
				atomicInputField);
		this.separator = separator;
	}

	/**
	 * Builds a new instance with an initial input value, but no minimum nor maximum
	 * number of atomic inputs.
	 * 
	 * @param isOptional           Whether this multiple field is optional (true) or
	 *                             not (false)
	 * @param label                The label for this multiple field
	 * @param atomicInputField     An input field for atomic values
	 * @param separator            The separator between atomic inputs
	 * @param initialMultipleInput The initial input for this multiple field
	 * @param helpString           The help string for this multiple field
	 */
	public MultipleInputFieldFromStringAdapter(boolean isOptional, String label, InputField<String, V> atomicInputField,
			String separator, String initialMultipleInput, String helpString) {
		super(new MultipleFieldAdapter<>(isOptional, label, atomicInputField, helpString), atomicInputField,
				initialMultipleInput);
		this.separator = separator;
	}

	/**
	 * Builds a new instance with an initial input value, and with minimum and
	 * maximum numbers of atomic inputs.
	 * 
	 * @param label                The label for this multiple field
	 * @param atomicInputField     An input field for atomic values
	 * @param minNbValues          The minimum number of values for this field to be
	 *                             valid (null for no minimum)
	 * @param maxNbValues          The maximum number of values for this field to be
	 *                             valid (null for no maximum)
	 * @param separator            The separator between atomic inputs
	 * @param initialMultipleInput The initial input for this multiple field
	 * @param helpString           The help string for this multiple field
	 */
	public MultipleInputFieldFromStringAdapter(String label, InputField<String, V> atomicInputField,
			Integer minNbValues, Integer maxNbValues, String separator, String initialMultipleInput,
			String helpString) {
		super(new MultipleFieldAdapter<>(label, atomicInputField, minNbValues, maxNbValues, helpString),
				atomicInputField, initialMultipleInput);
		this.separator = separator;
	}

	@Override
	public String getCompositeInput(List<String> atomicInputs) {
		return String.join(this.separator, atomicInputs);
	}

	@Override
	public List<String> getAtomicInputs(String compositeInput) {
		List<String> res = new ArrayList<>();
		for (String atomicInput : compositeInput.split(this.separator)) {
			res.add(atomicInput.trim());
		}
		return res;
	}

}
