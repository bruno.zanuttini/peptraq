package fr.greyc.mad.userinput;

import java.util.ArrayList;
import java.util.List;

/**
 * An abstract decorator for adding a restriction to the valid values for a
 * field. The restriction itself must be defined by the concrete subclasses.
 * Precisely, a value is considered to be valid if and only if its is valid for
 * the encapsulated field AND it is valid according to the {@link #isValid}
 * method declared in this class.
 * <p>
 * By default, all other properties (being optional, having bound values, label,
 * help string, etc.) are simply copied from the encapsulated field.
 * 
 * @author Bruno Zanuttini
 *
 * @param <V> The type of values for this field
 */
public abstract class RestrictionDecorator<V> implements Field<V> {

	/** The decorated field. */
	private final Field<V> field;

	/**
	 * Builds a new instance.
	 * 
	 * @param field The field to decorate.
	 */
	public RestrictionDecorator(Field<V> field) {
		this.field = field;
	}

	@Override
	public boolean isOptional() {
		return this.field.isOptional();
	}

	@Override
	public String getLabel() {
		return this.field.getLabel();
	}

	@Override
	public boolean hasBoundValues() {
		return this.field.hasBoundValues();
	}

	@Override
	public List<V> getValidValues() {
		return this.field.getValidValues();
	}

	@Override
	public void setValue(V value) throws IllegalArgumentException {
		this.field.setValue(value);
	}

	@Override
	public boolean hasValue() {
		return this.field.hasValue();
	}

	@Override
	public V getValue() {
		return this.field.getValue();
	}

	@Override
	public boolean isValid() {
		return (this.field.getValue() == null || this.isValid(this.field.getValue())) && this.field.isValid();
	}

	@Override
	public List<String> getErrors() {
		List<String> res = new ArrayList<>();
		V value = this.field.getValue();
		if (!this.field.isValid()) {
			res.addAll(this.field.getErrors());
		}
		if (value != null && !this.isValid(value)) {
			res.addAll(this.getErrors(value));
		}
		return res;
	}

	@Override
	public String getHelpString() {
		return this.field.getHelpString();
	}

	/**
	 * Decides whether a given value is valid for the restriction defined by this
	 * instance. This restriction is conjoined with those of the encapsulated field,
	 * so that it would be redundant to check {@code this.field.isValid} in this
	 * method.
	 * 
	 * @param value A value
	 * @return true if the given value if valid for the restriction defined by this
	 *         instance, false otherwise
	 */
	protected abstract boolean isValid(V value);

	/**
	 * Returns a list of strings explaining the errors for a given value. Only the
	 * errors directly caused by the restriction defined by this instance must be
	 * returned here, since errors of the encapsulated field will automatically be
	 * added.
	 * <p>
	 * This method is supposed to be called only for nonnull values; this is not
	 * supposed to be checked by a definition.
	 * 
	 * @param value A value
	 * @return The list of errors for the given value
	 */
	protected abstract List<String> getErrors(V value);

}
