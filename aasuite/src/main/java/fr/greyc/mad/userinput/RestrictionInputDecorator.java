package fr.greyc.mad.userinput;

import java.util.List;

/**
 * A decorator for adding a restriction to an input field. The restriction
 * itself must be defined by the concrete subclasses. Precisely, an input or a
 * value is considered to be valid if and only if its is valid for the
 * encapsulated field AND it is valid according to the {@link #isValid} method
 * declared in this class.
 * <p>
 * By default, all other properties (being optional, having bound values, label,
 * help string, etc.) are simply copied from the encapsulated field. Like
 * general input fields, the {@code null} input encodes the absence of input
 * (and hence is not considered to be a valid value), and the encapsulated field
 * is not supposed to be used elsewhere.
 * 
 * @author Bruno Zanuttini
 *
 * @param <I> The type of inputs for this field
 * @param <V> The type of values for this field
 */
public abstract class RestrictionInputDecorator<I, V> extends RestrictionDecorator<V> implements InputField<I, V> {

	// Implementation note: field is a pointer to the input field with the correct
	// type, but this field is also pointed to by the superclass, with type Field<V>

	/** The decorated input field. */
	private final InputField<I, V> inputField;

	/**
	 * Builds a new instance.
	 * 
	 * @param field The field to decorate
	 */
	public RestrictionInputDecorator(InputField<I, V> field) {
		super(field);
		this.inputField = field;
	}

	@Override
	public void setInput(I input) throws IllegalArgumentException {
		this.inputField.setInput(input);
	}

	@Override
	public boolean hasInput() {
		return this.inputField.hasInput();
	}

	@Override
	public I getInput() throws IllegalStateException {
		return this.inputField.getInput();
	}

	@Override
	public I getInputFor(V value) throws IllegalArgumentException {
		return this.inputField.getInputFor(value);
	}

	@Override
	public List<I> getValidInputs() throws UnsupportedOperationException {
		return this.inputField.getValidInputs();
	}

}
