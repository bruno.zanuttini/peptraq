package fr.greyc.mad.userinput;

import java.util.Collections;
import java.util.List;

/**
 * An abstract builder (from strings) with no field, and which is always globally valid. The
 * intended use is as an adaptor of {@link AbstractBuilder} when the concrete
 * subclass is always globally valid.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <O> The type of object created by this builder
 */
public abstract class TrivialBuilderFromStrings<O> extends AbstractBuilder<InputField<String, ?>, O> {

	/**
	 * Builds a new instance;
	 * 
	 * @param helpString The help string for the builder
	 */
	public TrivialBuilderFromStrings(String helpString) {
		super(helpString);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Always returns true.
	 */
	@Override
	public boolean isGloballyValid() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Always returns the empty list.
	 */
	@Override
	public List<String> getGlobalErrors() {
		return Collections.emptyList();
	}

}
