package fr.greyc.mad.userinput;

import java.util.List;

/**
 * An interface for fields whose values are lists of elements of a given type.
 * Note that the validity of the value of such a field may by definition depend
 * on the whole list, not necessarily (only) on the atomic values in it.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of atomic values in this field
 */
public interface MultipleField<V> extends Field<List<V>> {

	/**
	 * Adds a new atomic value to the value of this field. This may make the value
	 * (list) invalid, however, implementing classes may throw an exception in this
	 * case..
	 * 
	 * @param atomicValue An atomic value
	 * @throws IllegalArgumentException possibly, if it is not valid to add this
	 *                                  value
	 */
	public void addValue(V atomicValue) throws IllegalArgumentException;

}
