package fr.greyc.mad.userinput;

import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * An input field for regex patterns from strings.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class RegexpField extends InputFieldFromStringAdapter<Pattern> {

	/**
	 * Whether the pattern must be matched in a case-sensitive manner (true) or not
	 * (false).
	 */
	protected final boolean caseSensitive;

	/**
	 * Builds a new instance with an initial input.
	 * 
	 * @param isOptional    Whether this field is optional (true) or not (false)
	 * @param label         The label for this field
	 * @param caseSensitive Whether the pattern must be matched in a case-sensitive
	 *                      manner (true) or not (false)
	 * @param initialInput  The initial input for this field
	 * @param helpString    The help string for this field
	 */
	public RegexpField(boolean isOptional, String label, boolean caseSensitive, String initialInput,
			String helpString) {
		super(new FreeField<Pattern>(isOptional, label, helpString), initialInput);
		this.caseSensitive = caseSensitive;
	}

	/**
	 * Builds a new instance with no initial input.
	 * 
	 * @param isOptional    Whether this field is optional (true) or not (false)
	 * @param label         The label for this field
	 * @param caseSensitive Whether the pattern must be matched in a case-sensitive
	 *                      manner (true) or not (false)
	 * @param helpString    The help string for this field
	 */
	public RegexpField(boolean isOptional, String label, boolean caseSensitive, String helpString) {
		super(new FreeField<Pattern>(isOptional, label, helpString));
		this.caseSensitive = caseSensitive;
	}

	@Override
	protected Pattern tryGetValue(String input) throws IllegalArgumentException {
		try {
			return this.caseSensitive ? Pattern.compile(input) : Pattern.compile(input, Pattern.CASE_INSENSITIVE);
		} catch (PatternSyntaxException e) {
			throw new IllegalArgumentException("The string \"" + input + "\" does not define a valid pattern", e);
		}
	}

}
