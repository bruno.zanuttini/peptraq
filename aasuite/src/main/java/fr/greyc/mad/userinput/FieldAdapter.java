package fr.greyc.mad.userinput;

import java.util.Collections;
import java.util.List;

/**
 * An adapter for the {@link Field} interface, meant to simplify the design of
 * subclasses.
 * <p>
 * This adapter does NOT allow null as a valid value.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of values
 */
public abstract class FieldAdapter<V> implements Field<V> {

	/** The error for the absence of value in nonoptional fields. */
	public static String noValueError = "A value must be provided";

	/** Whether this field is optional (true) or not (false). */
	protected final boolean isOptional;

	/** The label of this field. */
	protected final String label;

	/** The help string of this field. */
	protected final String helpString;

	/** The current value of this field (null if none). */
	private V value;

	/**
	 * Builds a new instance with an initial value (not necessarily a valid one).
	 * 
	 * @param isOptional   Whether this field is optional (true) or not (false)
	 * @param label        The label of this field
	 * @param initialValue The initial value of this field
	 * @param helpString   The help string of this field
	 */
	public FieldAdapter(boolean isOptional, String label, V initialValue, String helpString) {
		this.label = label;
		this.isOptional = isOptional;
		this.helpString = helpString;
		this.setValue(initialValue);
	}

	/**
	 * Builds a new instance with no initial value. Even a nonoptional field may be
	 * built with no initial value (in this case, it is initially not valid).
	 * 
	 * @param isOptional Whether this field is optional (true) or not (false)
	 * @param label      The label of this field
	 * @param helpString The help string of this field
	 */
	public FieldAdapter(boolean isOptional, String label, String helpString) {
		this(isOptional, label, null, helpString);
	}

	@Override
	public boolean isOptional() {
		return this.isOptional;
	}

	@Override
	public String getLabel() {
		return this.label;
	}

	@Override
	public void setValue(V value) {
		this.value = value;
	}

	@Override
	public boolean hasValue() {
		return this.value != null;
	}

	@Override
	public V getValue() {
		return this.value;
	}

	@Override
	public boolean isValid() {
		if (this.value == null) {
			return this.isOptional();
		} else {
			return this.isValid(this.value);
		}
	}

	@Override
	public List<String> getErrors() {
		if (this.value == null) {
			if (this.isOptional()) {
				return Collections.emptyList();
			} else {
				return Collections.singletonList(noValueError);
			}
		} else {
			return this.getErrors(this.value);
		}
	}

	@Override
	public String getHelpString() {
		return this.helpString;
	}

	/**
	 * Decides whether a given nonnull value is valid for this field. The behavior
	 * is undefined for the null value.
	 * 
	 * @param value A value
	 * @return true if this value if valid for this field, false otherwise
	 */
	protected abstract boolean isValid(V value);

	/**
	 * Returns a list of errors indicating why a given value for this field is not
	 * valid. The list should be empty if the value is valid. The behavior is
	 * undefined if the value is null.
	 * 
	 * @param value A value
	 * @return a list of errors indicating why the given value is not valid (an
	 *         empty list if the value is valid)
	 */
	protected abstract List<String> getErrors(V value);

}
