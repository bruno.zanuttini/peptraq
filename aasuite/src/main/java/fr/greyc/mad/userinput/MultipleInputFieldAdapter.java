package fr.greyc.mad.userinput;

import java.util.ArrayList;
import java.util.List;

/**
 * An adapter for multiple input fields, from an input field for atomic values
 * (in addition to a field for multiple values), where how atomic inputs are
 * combined into a single multiple input is left to concrete subclasses.
 * <p>
 * Note that the absence of input in such a multiple field is encoded exactly by
 * {@code null} (there are no blank inputs of type {@code I}, but concrete
 * subclasses may override this behavior.
 * <p>
 * Like for general multiple fields, the wrapped fields are not supposed to be
 * used elsewhere, the list of values cannot be a valid input (it encodes the
 * absence of multiple input/multiple values), and the field is optional if and
 * only if there is no minimum number of values (or it is 0).
 * <p>
 * The label, optional character, help string, etc. are simply copied from the
 * wrapped multiple field, while those properties of the wrapped input field for
 * atomic values are all ignored.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <I> The type of input for this multiple field
 * @param <J> The type of input for atomic values
 * @param <V> The type of atomic values
 */
public abstract class MultipleInputFieldAdapter<I, J, V> extends InputFieldAdapter<I, List<V>>
		implements MultipleField<V> {

	/** The input field used for atomic values. */
	private InputField<J, V> multipleField;

	/**
	 * Builds a new instance with no initial input.
	 * 
	 * @param field            The field for multiple values
	 * @param atomicInputField The input field for atomic values
	 */
	public MultipleInputFieldAdapter(Field<List<V>> field, InputField<J, V> atomicInputField) {
		super(field);
		this.multipleField = atomicInputField;
	}

	/**
	 * Builds a new instance with an initial input.
	 * 
	 * @param multipleField        The field for multiple values
	 * @param atomicInputField     The input field for atomic values
	 * @param initialMultipleInput The initial input to this multiple field
	 * @throws IllegalArgumentException possibly, if the input corresponds to an
	 *                                  invalid value
	 */
	public MultipleInputFieldAdapter(Field<List<V>> multipleField, InputField<J, V> atomicInputField,
			I initialMultipleInput) throws IllegalArgumentException {
		super(multipleField, initialMultipleInput);
		this.multipleField = atomicInputField;
	}

	@Override
	public I getInputFor(List<V> value) throws IllegalArgumentException {
		List<J> atomicInputs = new ArrayList<>();
		for (V v : value) {
			atomicInputs.add(this.multipleField.getInputFor(v));
		}
		return this.getCompositeInput(atomicInputs);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method always returns false, since it is supposed to be called only on
	 * nonnull values. This behavior may be overriden by subclasses.
	 */
	@Override
	public boolean isBlank(I input) {
		return false;
	}

	@Override
	public List<V> getValue(I input) throws IllegalArgumentException {
		List<J> atomicInputs = this.getAtomicInputs(input);
		List<V> res = new ArrayList<>();
		for (J atomicInput : atomicInputs) {
			this.multipleField.setInput(atomicInput);
			if (this.multipleField.hasValue()) {
				res.add(this.multipleField.getValue());
			} else {
				throw new IllegalArgumentException("Input " + atomicInput + " corresponds to no value");
			}
		}
		return res;
	}

	@Override
	public void addValue(V value) throws IllegalStateException {
		if (this.hasInput() && !this.hasValue()) {
			throw new IllegalStateException(
					"Cannot add value: current input correspond to no value: " + this.getInput());
		} else {
			List<V> values = this.hasInput() ? this.getValue() : new ArrayList<V>();
			values.add(value);
			this.setValue(values);
		}
	}

	/**
	 * Returns a single input for a list of atomic inputs. If there are several ways
	 * to combine atomic inputs, this method may choose an arbitrary one.
	 * 
	 * @param atomicInputs A list of atomic inputs
	 * @return A single input for the given list of atomic inputs
	 * @throws IllegalArgumentException possibly, if the given list of atomic inputs
	 *                                  is not valid
	 */
	public abstract I getCompositeInput(List<J> atomicInputs) throws IllegalArgumentException;

	/**
	 * Returns the list of atomic inputs for an input to this multiple field.
	 * 
	 * @param compositeInput An input to this multiple field
	 * @return The list of atomic inputs for the given composite input
	 * @throws IllegalArgumentException possibly, if the given list of single inputs
	 *                                  is not valid
	 */
	public abstract List<J> getAtomicInputs(I compositeInput);

}
