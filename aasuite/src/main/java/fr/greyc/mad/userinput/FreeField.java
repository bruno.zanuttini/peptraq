package fr.greyc.mad.userinput;

import java.util.Collections;
import java.util.List;

/**
 * A class for fields in which all values (of the correct type) are valid. As an
 * exception, null encodes the absence of value, so it is valid only for
 * optional fields.
 * <p>
 * Importantly, an instance of this class never has bound values; it is supposed
 * to be used with types V whose numbers of instances is sufficiently large. Use
 * {@link BoundValueField} if this is not the case.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of values in this field
 */
public class FreeField<V> extends FieldAdapter<V> {

	/**
	 * Builds a new instance with an initial value. Note that the null value is
	 * allowed even if the field is not optional.
	 * 
	 * @param isOptional   Whether this field is optional (true) or not (false)
	 * @param label        The label of this field
	 * @param initialValue The initial value of this field
	 * @param helpString   The help string for this field
	 */
	public FreeField(boolean isOptional, String label, V initialValue, String helpString) {
		super(isOptional, label, initialValue, helpString);
	}

	/**
	 * Builds a new instance with no initial value. Note that this constructor can
	 * be used even if the field is not optional.
	 * 
	 * @param isOptional Whether this field is optional (true) or not (false)
	 * @param label      The label of this field
	 * @param helpString The help string for this field
	 */
	public FreeField(boolean isOptional, String label, String helpString) {
		super(isOptional, label, helpString);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method always returns false.
	 */
	@Override
	public final boolean hasBoundValues() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This operation is NOT supported.
	 */
	@Override
	public List<V> getValidValues() throws UnsupportedOperationException {
		throw new UnsupportedOperationException("Free fields have no bound values");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * This method always returns true.
	 */
	@Override
	protected final boolean isValid(V value) {
		return true;
	}

	@Override
	public List<String> getErrors(V value) {
		return Collections.emptyList();
	}

}
