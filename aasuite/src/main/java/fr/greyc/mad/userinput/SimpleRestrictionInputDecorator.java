package fr.greyc.mad.userinput;

import java.util.Collections;
import java.util.List;

/**
 * A simple restriction decorator for input fields, defined by an abstract
 * method deciding the validity of a value and a single error string in case the
 * restriction is not met.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <I> The type of inputs for this field
 * @param <V> The type of values for this field
 */
public abstract class SimpleRestrictionInputDecorator<I, V> extends RestrictionInputDecorator<I, V> {

	/** The single error string for this field. */
	protected final String errorString;

	/**
	 * Builds a new instance.
	 * 
	 * @param field       The decorated field
	 * @param errorString The single error string
	 */
	public SimpleRestrictionInputDecorator(InputField<I, V> field, String errorString) {
		super(field);
		this.errorString = errorString;
	}

	@Override
	protected abstract boolean isValid(V value);

	@Override
	protected List<String> getErrors(V value) {
		if (this.isValid(value)) {
			return Collections.emptyList();
		} else {
			return Collections.singletonList(this.errorString);
		}
	}

}
