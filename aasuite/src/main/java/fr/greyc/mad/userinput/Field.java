package fr.greyc.mad.userinput;

import java.util.List;

/**
 * An interface for fields, as used in input forms. A field is an abstract
 * notion, which at any point may have a value or not, and be valid or not. As
 * elements imagined for interaction with the user, fields have labels, help
 * strings, and errors in case they are invalid.
 * <p>
 * Note that null values are not disallowed in general: null may be a value just
 * as another one. Implementing classes may however override this.
 * <p>
 * A class of fields may be tagged as having bound values. This is a fuzzy
 * notion, meant to indicate whether there is a "reasonably sized" list of
 * possible values. For instance, there is a bounded number of integers, but it
 * does not make sense to let a user select an integer in the list of all
 * integers, so a field with all integers as possible values should not be
 * tagged as having bound values. It is expected that the fact to have bound
 * values, or not, is a property of the class and not of individual instances,
 * though this cannot be imposed by the interface.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <V> The type of values in the field
 */
public interface Field<V> {

	/**
	 * Decides whether this field is optional. An optional field is valid when it
	 * has no value, while a nonoptional field is not valid in this case.
	 * 
	 * @return true if this field is optional, false otherwise
	 */
	boolean isOptional();

	/**
	 * Returns the label of this field, namely, a string intended to be displayed to
	 * the user as a prompt for the field value.
	 * 
	 * @return The label of this field
	 */
	String getLabel();

	/**
	 * Decides whether this field has bound values. This property is supposed to be
	 * common to all instances of the same class.
	 * 
	 * @return true if this field has bound values, false otherwise
	 */
	boolean hasBoundValues();

	/**
	 * Returns a list of all values which are valid for this field, provided this
	 * field has bound values (as having bound values or not is supposed to be a
	 * property of the class, this operation is optional). The order might be
	 * important, so this method should returned the values in the same order as
	 * they were added to the field, if there is such an order.
	 * 
	 * @return A list of all values which are valid for this field
	 * @throws UnsupportedOperationException if this field does not have bound
	 *                                       values
	 */
	List<V> getValidValues() throws UnsupportedOperationException;

	/**
	 * Sets the value of this field. The value is not necessarily a valid value, but
	 * subclasses may throw an IllegalArgumentException for some invalid values.
	 * 
	 * @param value A value
	 * @throws IllegalArgumentException possibly, if the value is not valid
	 */
	void setValue(V value) throws IllegalArgumentException;

	/**
	 * Decides whether this field currently has a value.
	 * 
	 * @return true if this field currently has a value, false otherwise
	 */
	boolean hasValue();

	/**
	 * Returns the current value of this field; note that this is not necessarily a
	 * valid value. Also not that the value may be null, indicating that the field
	 * has a null value, which is different from it having no value (use
	 * {@link #hasValue()} to decide what is the case). In general however, the
	 * behavior is undefined if the field currently has no value.
	 * 
	 * @return The current value of this field
	 * @throws IllegalStateException possibly, if this field currently has no value
	 */
	V getValue() throws IllegalStateException;

	/**
	 * Decides whether this field is currently valid (that is, has a valid value, or
	 * has no value and is optional).
	 * 
	 * @return true if this field is currently valid, false otherwise
	 */
	boolean isValid();

	/**
	 * Returns a list of strings explaining the errors in the current value of this
	 * field (or in it having no value while it is not optional). If this field is
	 * valid, the list should be empty.
	 * 
	 * @return A list of strings explaining the errors in the current value of this
	 *         field
	 */
	List<String> getErrors();

	/**
	 * Returns the help string of this field. The string is assumed to be a short
	 * one, typically one sentence which can be displayed when hovering the field,
	 * for instance.
	 * 
	 * @return The help string of this field.
	 */
	String getHelpString();

}
