/**
 * A package defining classes for executing operations on sequences in a lazy
 * manner: as long as a sequence is not accessed, it is not even computed. Hence
 * most operations should be performed faster, as long at not all sequences are
 * accessed in the end. For instance, it should be essentially instantaneous to
 * display the first page of (a reasonable number of) results of any operation
 * on any number of sequences, except in exceptional circumstances when the
 * operation produces an output only for the very last sequences.
 * <p>
 * However, a caveat of such lazy execution is that it cannot be known in
 * advance how many sequences there are: for instance, assuming that 10
 * sequences have been initially loaded, that a filter has been applied which is
 * satisfied by the first 9, and that these 9 sequences have been explicity
 * accessed, it cannot be known whether the 10th satisfies the filter and hence,
 * whether there are currently 9 or 10 sequences. Another caveat is that the
 * total running time of an operation may be difficult to estimate in advance,
 * and may be surprisingly long: for instance, applying a filter then going to
 * the last sequence may require to execute all awaiting operations on all
 * sequences from the start, if none has ever been accessed.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: This package is currently unused due to the caveats mentioned
 *         above; it should be cleaned and benchmarked against concrete sessions first
 */
package fr.unicaen.aasuite.lazy;