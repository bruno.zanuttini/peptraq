package fr.unicaen.aasuite.lazy;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.datastructures.LazyIterable;
import fr.unicaen.aasuite.operations.Operation;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;

class LazyOperation<C extends Compound> extends LazyIterable<AnnotatedSequence<C>> {

	protected Iterator<AnnotatedSequence<C>> previous;

	protected Operation<C> operation;

	protected Map<Sequence<?>, OperationInformation> information;

	public LazyOperation(Iterable<AnnotatedSequence<C>> list, Operation<C> operation,
			Map<Sequence<?>, OperationInformation> information) {
		this.previous = list.iterator();
		this.operation = operation;
		this.information = information;
	}

	@Override
	protected List<AnnotatedSequence<C>> tryExplicitMore() {
		if (this.previous.hasNext()) {
			return this.operation.getSequences(this.previous.next(), information);
		}
		return Collections.emptyList();
	}

}