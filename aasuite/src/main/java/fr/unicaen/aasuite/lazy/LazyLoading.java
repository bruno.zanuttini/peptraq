package fr.unicaen.aasuite.lazy;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.LazyIterable;
import fr.unicaen.aasuite.fasta.SequenceInputStream;
import fr.unicaen.aasuite.fasta.SequenceReader;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceAdapter;

class LazyLoading<C extends Compound> extends LazyIterable<AnnotatedSequence<C>> {

	protected SequenceInputStream stream;

	protected SequenceReader<? extends AbstractSequence<C>> reader;

	public LazyLoading(SequenceReader<? extends AbstractSequence<C>> reader, SequenceInputStream stream) {
		this.stream = stream;
		this.reader = reader;
	}

	@Override
	protected List<AnnotatedSequence<C>> tryExplicitMore() {
		if (this.stream.isOver()) {
			return Collections.emptyList();
		} else {
			List<AnnotatedSequence<C>> res = null;
			try {
				res = Collections.singletonList(new AnnotatedSequenceAdapter<>(this.reader.getSequence(this.stream)));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
			if (!this.stream.isOver()) {
				stream.nextSequence();
			}
			return res;
		}
	}

}