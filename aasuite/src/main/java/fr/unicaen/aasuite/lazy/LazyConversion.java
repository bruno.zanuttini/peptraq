package fr.unicaen.aasuite.lazy;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.datastructures.LazyIterable;
import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;

class LazyConversion<C extends Compound, D extends Compound> extends LazyIterable<AnnotatedSequence<D>> {

	protected Iterator<AnnotatedSequence<C>> previous;

	protected Conversion<C, D> conversion;

	protected Map<Sequence<?>, OperationInformation> information;

	public LazyConversion(Iterable<AnnotatedSequence<C>> list, Conversion<C, D> conversion,
			Map<Sequence<?>, OperationInformation> information) {
		this.previous = list.iterator();
		this.conversion = conversion;
		this.information = information;
	}

	@Override
	protected List<AnnotatedSequence<D>> tryExplicitMore() {
		if (this.previous.hasNext()) {
			return this.conversion.getSequences(this.previous.next(), information);
		} else {
			return Collections.emptyList();
		}
	}

}
