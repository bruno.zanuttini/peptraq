package fr.unicaen.aasuite.transforms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceView;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A transform which cleaves sequences along sites. By convention, no sequence
 * of size 0 is ever produced by this transform.
 * <p>
 * This class is abstract, leaving to subclasses the decision of what
 * subsequences provoke a cleavage.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public abstract class AbstractCleavage<C extends Compound> implements Transform<C> {

	/**
	 * Builds a new instance.
	 */
	public AbstractCleavage() {
	}

	@Override
	public List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence,
			Map<Sequence<?>, OperationInformation> information) {

		List<AnnotatedSequence<C>> res = new ArrayList<>();
		int rank = 1; // rank of the next subsequence among those produced

		int begin = 0; // first position in the product currently being read
		int i = 0;
		while (i < sequence.getLength()) {
			int cleavage = this.isCleavage(sequence, i);
			if (cleavage == -1) {
				// no cleavage at this place
				i++;
			} else if (i > begin) {
				// cleavage
				String annotation = this.getAnnotation(sequence, rank++);
				AnnotatedSequence<C> oneRes = new AnnotatedSequenceView<C>(sequence, begin + 1, i, annotation);
				res.add(oneRes);
				information.put(oneRes, new OperationInformation(sequence, begin, i));
				begin = cleavage + 1;
				i = cleavage + 1;
			} else {
				// ignore (two contiguous occurrences of cleavage sites)
				begin = cleavage + 1;
				i = cleavage + 1;
			}
		}

		// Product at the end of sequence
		if (begin < sequence.getLength()) {
			String annotation = String.format(this.getAnnotation(sequence, rank++));
			AnnotatedSequence<C> oneRes = new AnnotatedSequenceView<C>(sequence, begin + 1, sequence.getLength(),
					annotation);
			res.add(oneRes);
			information.put(oneRes, new OperationInformation(sequence, begin, sequence.getLength()));
		}

		return res;
	}

	/**
	 * Decides whether there is a cleavage at a given position in a given sequence.
	 * 
	 * @param sequence the sequence to analyze
	 * @param index    a position (0-indexed)
	 * @return -1 if there is no cleavage at the given position, otherwise returns
	 *         the index (inclusive) at which the cleavage ends
	 */
	protected abstract int isCleavage(AnnotatedSequence<C> sequence, int index);

	/**
	 * Returns the annotation for a product of this transform.
	 * 
	 * @param sequence The sequence to which this transform is applied
	 * @param rank     The rank of the product among the ones produced by this
	 *                 transform (1 for the first one)
	 * @return The annotation for the given product
	 */
	protected abstract String getAnnotation(AnnotatedSequence<C> sequence, int rank);

}
