package fr.unicaen.aasuite.transforms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;

import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A transform which cleaves sequences along n-basic sites, that is, sites
 * consisting of n basic amino acids but are not including in a larger basic
 * site (n is called the "length of cleavage sites"). For instance, there is a
 * tribasic cleavage in AAARKRBBB (over RKR, resulting in AAA and BBB), but no
 * dibasic cleavage.
 * <p>
 * By convention, no sequence of size 0 is ever produced by this transform.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class BasicCleavage extends AbstractCleavage<AminoAcidCompound> {

	/** The set of all basic amino acids (R and K). */
	public static final Set<AminoAcidCompound> BASIC_AMINO_ACIDS = new HashSet<>();

	static {
		BASIC_AMINO_ACIDS.add(AminoAcidCompoundSet.getAminoAcidCompoundSet().getCompoundForString("R"));
		BASIC_AMINO_ACIDS.add(AminoAcidCompoundSet.getAminoAcidCompoundSet().getCompoundForString("K"));
	}

	/**
	 * The description for instances of this transform. The description may use 1$
	 * for the length of cleavage sites.
	 */
	public static String description = "Cleavage along %1$d-basic sites";

	/**
	 * The annotation for a subsequence issued from a sequence. The annotation may
	 * use 1$ for the annotation of the initial sequence, 2$ for the length of
	 * cleavage sites, and 3$ for the rank of the subsequence among the products of
	 * this sequence.
	 */
	public static String annotation = "%1$s [cleavage along %2$d-basic sites, #%3$d]";

	/** The length of cleavage sites. */
	protected final int n;

	/**
	 * Builds a new instance.
	 * 
	 * @param n The length of cleavage sites
	 */
	public BasicCleavage(int n) throws IllegalArgumentException {
		super();
		this.n = n;
	}

	/**
	 * Returns the length of cleavage sites.
	 * 
	 * @return The length of cleavage sites
	 */
	public int getLength() {
		return this.n;
	}

	/**
	 * Returns the set of all n-basic cleavage sites, that is, those sites
	 * consisting of exactly n basic amino acids.
	 * 
	 * @param n The length of cleavage sites
	 * @return the set of all n-basic cleavage sites
	 */
	public static Set<List<AminoAcidCompound>> getAllBasicSites(int n) {
		return combinatorialLists(BASIC_AMINO_ACIDS, n);
	}

	@Override
	public TransformType getType() {
		return TransformType.BASIC_CLEAVAGE;
	}

	@Override
	public String toString() {
		return String.format(description, this.n);
	}

	@Override
	protected int isCleavage(AnnotatedSequence<AminoAcidCompound> sequence, int index) {
		if (index > 0 && BASIC_AMINO_ACIDS.contains(sequence.getCompoundAt(index))) {
			return -1;
		}
		if (index + n > sequence.getLength()) {
			return -1;
		}
		if (index + n < sequence.getLength() && BASIC_AMINO_ACIDS.contains(sequence.getCompoundAt(index + n + 1))) {
			return -1;
		}
		for (int i = index; i < index + n; i++) {
			if (!BASIC_AMINO_ACIDS.contains(sequence.getCompoundAt(i + 1))) {
				return -1;
			}
		}
		return index + n - 1;
	}

	@Override
	protected String getAnnotation(AnnotatedSequence<AminoAcidCompound> sequence, int rank) {
		return String.format(annotation, sequence.getAnnotation(), this.n, rank);
	}

	/**
	 * Helper method. Returns all lists of a given length with elements taken from a
	 * given collection.
	 * 
	 * @param elements A collection of elements
	 * @param length   A length
	 * @return A set containing all lists of the given length with elements taken
	 *         from the given collection
	 * @param <E> The type of elements
	 */
	private static <E> Set<List<E>> combinatorialLists(Collection<E> elements, int length) {
		if (elements.isEmpty()) {
			return Collections.emptySet();
		} else if (length == 0) {
			return Collections.singleton(Collections.emptyList());
		} else {
			Set<List<E>> rec = combinatorialLists(elements, length - 1);
			Set<List<E>> res = new HashSet<>();
			for (List<E> oneRec : rec) {
				for (E element : elements) {
					List<E> oneRes = new ArrayList<>(oneRec);
					oneRes.add(element);
				}
			}
			return res;
		}
	}

}
