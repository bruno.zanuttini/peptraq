package fr.unicaen.aasuite.transforms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A transform which cleaves sequences along given sites.
 * <p>
 * By convention, no sequence of size 0 is ever produced by this transform.
 * <p>
 * To avoid ambiguity, there cannot be a cleavage site which is a substring of
 * another one.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class Cleavage<C extends Compound> extends AbstractCleavage<C> {

	/**
	 * The description for instances of this transform. The description may use 1$
	 * for the description of the list of cleavage sites.
	 */
	public static String description = "Cleavage along %1$s";

	/** A separator for a string representing several cleavage sites. */
	public static String siteSeparator = ", ";

	/**
	 * The annotation for a subsequence issued from a sequence. The annotation may
	 * use 1$ for the annotation of the initial sequence, 2$ for the description of
	 * the list of cleavage sites, and 3$ for the rank of the subsequence among the
	 * products of the initial sequence.
	 */
	public static String annotation = "%1$s [cleavage along %2$s], #%3$d]";

	/** The cleavage sites. */
	protected final Collection<List<C>> cleavageSites;

	/** A representation of the cleavage sites as a single string. */
	private final String cleavageSitesAsString;

	/**
	 * Builds a new instance.
	 * 
	 * @param cleavageSites The cleavage sites
	 * @throws IllegalArgumentException if a site is a (proper) substring of another
	 *                                  one
	 */
	public Cleavage(Collection<List<C>> cleavageSites) throws IllegalArgumentException {
		super();
		for (List<C> site : cleavageSites) {
			for (List<C> other : cleavageSites) {
				if (!site.equals(other) && isSubstring(site, other)) {
					throw new IllegalArgumentException("Site " + site + " is a substring of site " + other);
				}
			}
		}
		this.cleavageSites = cleavageSites;
		List<String> str = new ArrayList<>();
		for (List<C> site : cleavageSites) {
			StringBuffer siteStr = new StringBuffer();
			for (C compound : site) {
				siteStr.append(compound.getShortName());
			}
			str.add(siteStr.toString());
		}
		this.cleavageSitesAsString = String.join(siteSeparator, str);
	}

	/**
	 * Returns the cleavage sites for this transform.
	 * @return The cleavage sites for this transform
	 */
	public Collection<List<C>> getCleavageSites() {
		return this.cleavageSites;
	}

	@Override
	public TransformType getType() {
		return TransformType.CLEAVAGE;
	}

	@Override
	public String toString() {
		return String.format(description, this.cleavageSitesAsString);
	}

	@Override
	protected int isCleavage(AnnotatedSequence<C> sequence, int index) {
		for (List<C> site : this.cleavageSites) {
			int size = site.size();
			if (index + size <= sequence.getLength()) {
				boolean equal = true;
				int i = 0;
				for (C compound : site) {
					if (!compound.equals(sequence.getCompoundAt(index + i + 1))) {
						equal = false;
						break;
					} else {
						i++;
					}
				}
				if (equal) {
					return index + size - 1;
				}
			}
		}
		return -1;
	}

	@Override
	protected String getAnnotation(AnnotatedSequence<C> sequence, int rank) {
		return String.format(annotation, sequence.getAnnotation(), this.cleavageSitesAsString, rank);
	}

	/**
	 * Helper method: decides if a list is a sublist of another one.
	 * 
	 * @param site  A site
	 * @param other A site
	 * @return true if {@code site} is a substring of {@code other} or they are equal, false otherwise
	 * @param <E> The type of elements in the list
	 */
	private static <E> boolean isSubstring(List<E> site, List<E> other) {
		for (int i = 0; i < other.size() - site.size(); i++) {
			if (site.equals(other.subList(i, i + site.size()))) {
				return true;
			}
		}
		return false;
	}

}
