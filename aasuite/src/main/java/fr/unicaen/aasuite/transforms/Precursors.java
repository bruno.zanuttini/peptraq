package fr.unicaen.aasuite.transforms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceView;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A transform which computes (nonempty) precursors in a given sequence of amino
 * acids, and retains them only when their size is within specified bounds.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class Precursors implements Transform<AminoAcidCompound> {

	/** The description for an instance with no bound on size. */
	public static String noBoundDescription = "Precursors";

	/**
	 * The description for an instance with only a lower bound on size. The
	 * description may use 1$ for the lower bound.
	 */
	public static String lowerBoundDescription = "Precursors of size at least %1$d";

	/**
	 * The description for an instance with only an upper bound on size. The
	 * description may use 1$ for the upper bound.
	 */
	public static String upperBoundDescription = "Precursors of size at most %1$d";

	/**
	 * The description for a general instance. The description may use 1$ for the
	 * lower bound and 2$ for the upper bound.
	 */
	public static String description = "Precursors of size at least %1$d and at most %2$d";

	/**
	 * The annotation for a precursor issued from a sequence. The description may
	 * use 1$ for the annotation of the initial sequence and 2$ for the rank of the
	 * precursor among those of the sequence.
	 */
	public static String annotation = "%1$s [precursor #%2$d]";

	/** The initial amino acid in a precursor. */
	public static final AminoAcidCompound INIT = AminoAcidCompoundSet.getAminoAcidCompoundSet()
			.getCompoundForString("M");

	/** The stop amino acid for a precursor (not included in it). */
	public static final AminoAcidCompound STOP = AminoAcidCompoundSet.getAminoAcidCompoundSet()
			.getCompoundForString("*");

	/**
	 * The minimal size for a precursor to be retained by this transform (null if
	 * none).
	 */
	protected final Integer minSize;

	/**
	 * The maximal size for a precursor to be retained by this transform (null if
	 * none).
	 */
	protected final Integer maxSize;

	/**
	 * Builds a new instance.
	 * 
	 * @param minSize The minimal size for a precursor to be retained (null if none)
	 * @param maxSize The maximal size for a precursor to be retained (null if none)
	 * @throws IllegalArgumentException if the minimal size is greater than the
	 *                                  maximum one
	 */
	public Precursors(Integer minSize, Integer maxSize) throws IllegalArgumentException {
		if (minSize != null && maxSize != null && minSize > maxSize) {
			throw new IllegalArgumentException(
					"Minimum size of precursors cannot be greater than maximum: " + minSize + ", " + maxSize);
		}
		this.minSize = minSize;
		this.maxSize = maxSize;
	}

	/**
	 * Decides whether a minimum size for precursors is set.
	 * @return true if a minimum size for precursors is set, false otherwise
	 */
	public boolean hasMinSize() {
		return this.minSize != null;
	}

	/**
	 * Returns the minimum size for a precursor to be retained.
	 * @return The minimum size for a precursor to be retained
	 * @throws IllegalStateException if no minimum size is set
	 */
	public int getMinSize() throws IllegalStateException {
		if (this.minSize == null) {
			throw new IllegalStateException("Constraint has no minimal size");
		}
		return this.minSize;
	}

	/**
	 * Decides whether a maximum size for precursors is set.
	 * @return true if a maximum size for precursors is set, false otherwise
	 */
	public boolean hasMaxSize() {
		return this.maxSize != null;
	}

	/**
	 * Returns the maximum size for a precursor to be retained.
	 * @return The maximum size for a precursor to be retained
	 * @throws IllegalStateException if no maximum size is set
	 */
	public int getMaxSize() throws IllegalStateException {
		if (this.maxSize == null) {
			throw new IllegalStateException("Constraint has no maximal size");
		}
		return this.maxSize;
	}

	@Override
	public TransformType getType() {
		return TransformType.PRECURSORS;
	}

	@Override
	public List<AnnotatedSequence<AminoAcidCompound>> getSequences(AnnotatedSequence<AminoAcidCompound> sequence,
			Map<Sequence<?>, OperationInformation> information) {

		// TODO: simplify the code

		List<AnnotatedSequence<AminoAcidCompound>> precursors = new ArrayList<>();
		int rank = 1; // rank of the next precursor found
		int i = 0; // current index

		// Precursors with init codon in the sequence
		boolean inSubsequence = true;
		int lastInitIndex = 0;
		for (i = 0; i < sequence.getLength(); i++) {
			AminoAcidCompound aa = sequence.getCompoundAt(i + 1);
			if (aa.equals(INIT) && (!inSubsequence || lastInitIndex == 0)) {
				inSubsequence = true;
				lastInitIndex = i;
			} else if (aa.equals(STOP) && inSubsequence) {
				if (i > lastInitIndex && (this.minSize == null || i - lastInitIndex >= this.minSize)
						&& (this.maxSize == null || i - lastInitIndex <= this.maxSize)) {
					String newAnnotation = String.format(annotation, sequence.getAnnotation(), rank++);
					AnnotatedSequence<AminoAcidCompound> precursor = new AnnotatedSequenceView<>(sequence,
							lastInitIndex + 1, i, newAnnotation);
					precursors.add(precursor);
					information.put(precursor, new OperationInformation(sequence, lastInitIndex, i));
				}
				inSubsequence = false;
			}
		}

		// Precursor overlapping the end of sequence
		if (inSubsequence) {
			if ((this.minSize == null || i - lastInitIndex >= this.minSize)
					&& (this.maxSize == null || i - lastInitIndex <= this.maxSize)) {
				String newAnnotation = String.format(annotation, sequence.getAnnotation(), rank++);
				AnnotatedSequence<AminoAcidCompound> precursor = new AnnotatedSequenceView<>(sequence,
						lastInitIndex + 1, i, newAnnotation);
				precursors.add(precursor);
				information.put(precursor, new OperationInformation(sequence, lastInitIndex, i));
			}
		}

		return precursors;

	}

	@Override
	public String toString() {
		if (this.minSize == null && this.maxSize == null) {
			return String.format(noBoundDescription);
		} else if (this.minSize == null) {
			return String.format(upperBoundDescription, this.maxSize);
		} else if (this.maxSize == null) {
			return String.format(lowerBoundDescription, this.minSize);
		} else {
			return String.format(description, this.minSize, this.maxSize);
		}
	}

}
