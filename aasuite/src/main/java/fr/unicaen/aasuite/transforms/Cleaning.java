package fr.unicaen.aasuite.transforms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceView;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A transform which cleans sequences, i.e., keeps only those subsequences which
 * do not contain given letters. This amounts to splitting the sequence apart
 * each occurrence of a letter to clean out.
 * <p>
 * The letters are to be specified as "compounds", typically "." or "_", which
 * are part of the compound sets of BioJava.
 * <p>
 * The transform comes in two versions: keeping maximally long clean
 * subsequences only, or keeping all those with size above a given threshold
 * (possibly none, as a specific case).
 * <p>
 * By convention, no empty sequence is ever produced by this transform.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences
 */
public class Cleaning<C extends Compound> implements Transform<C> {

	/**
	 * The description for instances with maximally long clean subsequences kept.
	 * The description may use 1$ for the list of excluded compounds.
	 */
	public static String maximalSubsequencesDescription = "Maximal subsequences without %1$s";

	/**
	 * The description for instances with all clean subsequences kept. The
	 * description may use 1$ for the list of excluded compounds.
	 */
	public static String allSubsequencesDescription = "Subsequences without %1$s";

	/**
	 * The description for instances with subsequences of a minimum length kept. The
	 * description may use 1$ for the list of excluded compounds and 2$ for the
	 * minimum length.
	 */
	public static String minimumSizeDescription = "Subsequences without %1$s and of length at least %2$d";

	/**
	 * The annotation for a clean subsequence issued from a sequence. The
	 * description may use 1$ for the annotation of the initial sequence, 2$ for the
	 * list of excluded compounds, and 3$ for the rank of the subsequence among all
	 * products of this sequence.
	 */
	public static String annotation = "%1$s [%2$s-cleaned subseq. #%3$d]";

	/** The compounds to clean out. */
	protected final Set<C> toBeCleanedOut;

	/**
	 * The minimum size for a subsequence to be kept (ignored if
	 * {@link #keepOnlyMaximal} is true); null for no minimum.
	 */
	protected final Integer minSize;

	/** Whether to keep only the maximal subsequence (true) or not (false). */
	protected final boolean keepOnlyMaximal;

	/**
	 * Builds a new instance, which keeps only the maximally long clean subsequences
	 * of a given sequence.
	 * 
	 * @param toBeCleanedOut The compounds to clean out
	 */
	public Cleaning(Set<C> toBeCleanedOut) {
		this.toBeCleanedOut = toBeCleanedOut;
		this.minSize = null;
		this.keepOnlyMaximal = true;
	}

	/**
	 * Builds a new instance, which keeps all subsequences of length at least a
	 * given threshold.
	 * 
	 * @param toBeCleanedOut The compounds to clean out
	 * @param minSize        The minimum size for a subsequence to be kept (null for
	 *                       keeping all clean subsequences).
	 */
	public Cleaning(Set<C> toBeCleanedOut, Integer minSize) {
		this.toBeCleanedOut = toBeCleanedOut;
		this.minSize = minSize;
		this.keepOnlyMaximal = false;
	}

	/**
	 * Returns the set of compounds cleaned out by this transform.
	 * 
	 * @return The set of compounds cleaned out by this transform
	 */
	public Set<C> getCompounds() {
		return this.toBeCleanedOut;
	}

	/**
	 * Returns true if this transform keeps only the maximally long clean
	 * subsequences of a given sequence, false otherwise.
	 * 
	 * @return true if this transform keeps only the maximally long clean
	 *         subsequences of a given sequence, false otherwise
	 */
	public boolean keepOnlyMaximal() {
		return this.keepOnlyMaximal;
	}

	/**
	 * Returns true if this transform has a minimum size set, false otherwise. Note
	 * that false is necessarily returned if this transform keeps only maximal
	 * subsequences (but null may still be returned otherwise).
	 * 
	 * @return true if this transform has a minimum size set, false otherwise
	 */
	public boolean hasMinSize() {
		return this.minSize != null;
	}

	/**
	 * Returns the minimal size for keeping a subsequence.
	 * 
	 * @return The minimal size for keeping a subsequence
	 */
	public int getMinSize() throws IllegalStateException {
		if (this.minSize == null) {
			throw new IllegalStateException("Transform has no minimal size, only maximal subsequences are retained");
		}
		return this.minSize;
	}

	@Override
	public TransformType getType() {
		return TransformType.CLEANING;
	}

	@Override
	public List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence,
			Map<Sequence<?>, OperationInformation> information) {
		Map<Sequence<?>, OperationInformation> tmpInformation = new HashMap<>();
		List<AnnotatedSequence<C>> res = this.getAllSequences(sequence, tmpInformation);
		if (res.isEmpty()) {
			return res;
		} else if (this.keepOnlyMaximal) {
			int maxLength = res.stream().map(AnnotatedSequence::getLength).max((i1, i2) -> i1 - i2).get();
			Cleaning.retainIf(res, s -> s.getLength() == maxLength, tmpInformation, information);
			return res;
		} else {
			Predicate<AnnotatedSequence<C>> isLargeEnough = s -> this.minSize == null || s.getLength() >= this.minSize;
			Cleaning.retainIf(res, isLargeEnough, tmpInformation, information);
			return res;
		}
	}

	@Override
	public String toString() {
		if (this.keepOnlyMaximal) {
			return String.format(maximalSubsequencesDescription, this.toBeCleanedOut);
		} else if (this.minSize == null) {
			return String.format(allSubsequencesDescription, this.toBeCleanedOut);
		} else {
			return String.format(minimumSizeDescription, this.toBeCleanedOut, this.minSize);
		}
	}

	/**
	 * Helper method: returns all nonempty clean subsequences of a given sequence.
	 * 
	 * @param sequence    The sequence to analyze
	 * @param information A map which is filled with information about all produced
	 *                    sequences
	 * @return The maximal clean subsequences in the sequence; empty sequences are
	 *         not returned
	 * @throws IllegalArgumentException if the sequence is empty
	 */
	public List<AnnotatedSequence<C>> getAllSequences(AnnotatedSequence<C> sequence,
			Map<Sequence<?>, OperationInformation> information) throws IllegalArgumentException {
		List<AnnotatedSequence<C>> res = new ArrayList<>();
		int rank = 1;
		int lastStart = 0;
		for (int i = 0; i < sequence.getLength(); i++) {
			C current = sequence.getCompoundAt(i + 1);
			if (this.toBeCleanedOut.contains(current)) {
				if (i > lastStart) { // ignore empty subsequences
					String newAnnotation = String.format(annotation, sequence.getAnnotation(), this.toBeCleanedOut,
							rank++);
					AnnotatedSequence<C> clean = new AnnotatedSequenceView<>(sequence, lastStart + 1, i, newAnnotation);
					res.add(clean);
					information.put(clean, new OperationInformation(sequence, lastStart, i));
				}
				lastStart = i + 1;
			}
		}
		// Handling the last clean subsequence
		if (lastStart < sequence.getLength()) {
			String newAnnotation = String.format(annotation, sequence.getAnnotation(), this.toBeCleanedOut, rank++);
			AnnotatedSequence<C> clean = new AnnotatedSequenceView<>(sequence, lastStart + 1, sequence.getLength(),
					newAnnotation);
			res.add(clean);
			information.put(clean, new OperationInformation(sequence, lastStart, sequence.getLength()));
		}
		return res;
	}

	/**
	 * Helper method: retains from a list only those elements which satisfy a given
	 * predicate, and at the same time, for all these retained elements, copies the
	 * information stored in a map, to another map.
	 * 
	 * @param list      A list of elements
	 * @param predicate A predicate over the elements
	 * @param source    A map holding information about all elements of the initial
	 *                  list
	 * @param dest      A map where information about all retained elements is
	 *                  copied
	 * @param           <E> The type for the elements in the list
	 * @param           <I> The type for the information stored for each sequence
	 */
	private static <E, I> void retainIf(List<E> list, Predicate<E> predicate, Map<? super E, I> source,
			Map<? super E, I> dest) {
		list.removeIf(predicate.negate());
		for (E key : list) {
			dest.put(key, source.get(key));
		}
	}

}
