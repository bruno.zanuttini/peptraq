package fr.unicaen.aasuite.transforms;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.features.SignalPeptide;
import fr.unicaen.aasuite.features.SignalPeptide.OrganismType;
import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceView;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A transform which removes the most probable signal peptide in the given
 * sequence (and optionally keeps the whole sequence if no signal peptide has
 * been detected at all at the given confidence level).
 * <p>
 * The prediction relies on the JSPP package; see {@link SignalPeptide}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SignalPeptideRemoval implements Transform<AminoAcidCompound> {

	/**
	 * The description for an instance of this transform. The description may use 1$
	 * for the confidence level required and 2$ for the description of the type of organisms.
	 */
	public static String description = "Removal of most probable signal peptide (with score at least %1$s) (%2$s)";

	/**
	 * The annotation for the sequence obtained after removal. The annotation may
	 * use 1$ for the annotation of the initial sequence, 2$ for the score of the
	 * prediction, and 3$ for the description of the type of organisms.
	 */
	public static String annotation = "%1$s [most probable signal peptide removed, score=%2$s] (%3$s)";

	/** The type of organisms to which this constraint applies. */
	protected final SignalPeptide.OrganismType organismType;

	/**
	 * The minimum confidence score for detection of a signal peptide.
	 */
	protected final float minScore;

	/**
	 * Whether to keep sequences which have no signal peptide with the given
	 * confidence (true) or not (false).
	 */
	protected final boolean keepIfNone;

	/**
	 * Builds a new instance.
	 * 
	 * @param organismType The type of organisms
	 * @param minScore     the minimum confidence score for detection of a signal
	 *                     peptide
	 * @param keepIfNone   whether to keep sequences which have no signal peptide
	 *                     with the given confidence (true) or not (false)
	 */
	public SignalPeptideRemoval(SignalPeptide.OrganismType organismType, float minScore, boolean keepIfNone) {
		this.organismType = organismType;
		this.minScore = minScore;
		this.keepIfNone = keepIfNone;
	}

	/**
	 * Returns the type of organisms to which this constraint can be applied.
	 * 
	 * @return The type of organisms to which this constraint can be applied
	 */
	public OrganismType getOrganismType() {
		return this.organismType;
	}

	/**
	 * Returns the minimum confidence level for the constraint to be satisfied.
	 * 
	 * @return The minimum confidence level for the constraint to be satisfied
	 */
	public float getMinScore() {
		return this.minScore;
	}

	/**
	 * Returns true if this transform retains (as is) a sequence for which no signal
	 * peptide is predicted with confidence above the minimum score, false
	 * otherwise.
	 * 
	 * @return true if this transform retains (as is) a sequence for which no signal
	 *         peptide is predicted with confidence above the minimum score, false
	 *         otherwise
	 */
	public boolean keepIfNone() {
		return this.keepIfNone;
	}

	@Override
	public TransformType getType() {
		return TransformType.SIGNAL_PEPTIDE_REMOVAL;
	}

	@Override
	public List<AnnotatedSequence<AminoAcidCompound>> getSequences(AnnotatedSequence<AminoAcidCompound> sequence,
			Map<Sequence<?>, OperationInformation> information) {
		SignalPeptide.Info signalPeptideInformation = SignalPeptide.getInformation(sequence, this.organismType);
		if (signalPeptideInformation.position >= 0 && signalPeptideInformation.score >= this.minScore) {
			int start = signalPeptideInformation.position;
			int end = sequence.getLength();
			String newAnnotation = String.format(annotation, sequence.getAnnotation(), signalPeptideInformation.score, this.organismType.getDescription());
			AnnotatedSequence<AminoAcidCompound> res = new AnnotatedSequenceView<>(sequence, start, end, newAnnotation);
			information.put(res, new OperationInformation(sequence, start, end));
			return Collections.singletonList(res);
		} else if (this.keepIfNone) {
			// Keeps whole sequence if no signal peptide has been predicted
			AnnotatedSequence<AminoAcidCompound> res = new AnnotatedSequenceView<>(sequence, 1, sequence.getLength());
			information.put(res, new OperationInformation(sequence, 0, sequence.getLength()));
			return Collections.singletonList(res);
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	public String toString() {
		return String.format(description, this.minScore, this.organismType.getDescription());
	}

}
