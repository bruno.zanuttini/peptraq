package fr.unicaen.aasuite.transforms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A transform which produces the sequences produced by any of a given list of
 * transforms. Transforms are applied in the order in which they are given at
 * the construction.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class MultipleTransform<C extends Compound> implements Transform<C> {

	/** The description for instances of this class. The description may use 1$ for the list of transforms composing the instance. */
	public static String description = "Sequences produced by any of: %1$s";

	/** The type of this multiple transform. */
	protected final TransformType type;

	/** The list of transforms to apply. */
	protected final List<Transform<C>> transforms;

	/**
	 * Builds a new instance.
	 * 
	 * @param type       The type of this instance
	 * @param transforms The list of transforms to apply
	 */
	public MultipleTransform(TransformType type, List<Transform<C>> transforms) {
		this.type = type;
		this.transforms = transforms;
	}

	/**
	 * Returns the list of transforms composing this multiple transform, in the order in which they are applied to a sequence.
	 * 
	 * @return The list of transforms composing this multiple transform
	 */
	public List<Transform<C>> getTransforms() {
		return this.transforms;
	}

	@Override
	public TransformType getType() {
		return this.type;
	}

	@Override
	public List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence,
			Map<Sequence<?>, OperationInformation> information) throws IllegalArgumentException {

		List<AnnotatedSequence<C>> res = new ArrayList<>();
		for (Transform<C> transformer : this.transforms) {
			res.addAll(transformer.getSequences(sequence, information));
		}
		return res;
	}

	@Override
	public String toString() {
		return String.format(description, this.transforms);
	}

}
