/**
 * Concrete conversions between sequences. 
 * 
 * All classes have static default descriptions used in {@literal toString()},
 * which can be changed (they are public, and not final) so as to handle
 * internationalization, for instance. The documentation of each specifies
 * whether these strings are formated using arguments.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.conversions;