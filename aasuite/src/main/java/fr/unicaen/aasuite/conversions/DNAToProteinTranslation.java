package fr.unicaen.aasuite.conversions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
// Standard Java classes
import java.util.Map;
import java.util.Set;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
// BioJava classes
import org.biojava.nbio.core.sequence.template.Sequence;
import org.biojava.nbio.core.sequence.transcription.Frame;
import org.biojava.nbio.core.sequence.transcription.TranscriptionEngine;

import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.operations.ConversionType;
// AASuite classes
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceAdapter;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A translation from DNA to protein sequences. The translation can be operated
 * along one or several of all three forward and reverse frames.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class DNAToProteinTranslation implements Conversion<NucleotideCompound, AminoAcidCompound> {

	/** The human-readable name for all frames. */
	public static String allFramesDescription = "all frames";

	/** The human-readable name for all forward frames. */
	public static String allForwardFramesDescription = "all forward frames";

	/** The human-readable name for all reversed frames. */
	public static String allReversedFramesDescription = "all reversed frames";

	/**
	 * The description for an arbitrary list of frames. The description may use 1$
	 * for the description of the list.
	 */
	public static String frameListDescription = "frame(s) %1$s";

	/**
	 * The description of an instance of this conversion. The description may use 1$
	 * for the description of the frames involved.
	 */
	public static String description = "Translation from DNA along %1$s";

	/**
	 * The annotation for a produced sequence. The annotation may use 1$ for the
	 * annotation of the initial sequence, and 2$ for the description of the frame
	 * used. It must be different for different couples (1$, 2$).
	 */
	public static String annotation = "%1$s [translated along frame %2$s]";

	/** The transcription engine to which work is delegated. */
	private final TranscriptionEngine engine;

	/** A human-readable description of the frames. */
	private final String frameDescription;

	/** The frames along which to translate. */
	protected final Frame[] frames;

	/**
	 * Builds a new instance.
	 * 
	 * @param frames The frames along which to translate
	 * @throws IllegalArgumentException if there is no frame, or not all frames in
	 *                                  the given array are different from each
	 *                                  other
	 */
	public DNAToProteinTranslation(Frame[] frames) throws IllegalArgumentException {

		// Checking argument
		if (frames.length == 0) {
			throw new IllegalArgumentException("At least one frame  must be given");
		}
		Set<Frame> asSet = new HashSet<>();
		asSet.addAll(Arrays.asList(frames));
		if (asSet.size() != frames.length) {
			throw new IllegalArgumentException("Duplicate frame in given array: " + frames);
		}

		// Initializing engine
		this.engine = TranscriptionEngine.getDefault();

		// Computing description of set of frames
		if (frames.length == Frame.getAllFrames().length) {
			this.frameDescription = allFramesDescription;
		} else {
			if (frames.length == 3 && asSet.contains(Frame.ONE) && asSet.contains(Frame.TWO)
					&& asSet.contains(Frame.THREE)) {
				this.frameDescription = allForwardFramesDescription;
			} else if (frames.length == 3 && asSet.contains(Frame.REVERSED_ONE) && asSet.contains(Frame.REVERSED_TWO)
					&& asSet.contains(Frame.REVERSED_THREE)) {
				this.frameDescription = allReversedFramesDescription;
			} else {
				StringBuffer description = new StringBuffer();
				String sep = "";
				for (int i = 0; i < frames.length; i++) {
					description.append(sep);
					description.append(frames[i]);
					sep = ", ";
				}
				this.frameDescription = String.format(frameListDescription, description.toString());
			}
		}

		// Saving frames
		this.frames = frames;

	}

	/**
	 * Returns the frames used by this conversion.
	 * 
	 * @return The frames used by this conversion
	 */
	public Frame[] getFrames() {
		return this.frames;
	}

	@Override
	public ConversionType getType() {
		return ConversionType.DNA_TO_PROTEIN_TRANSLATION;
	}

	@Override
	public List<AnnotatedSequence<AminoAcidCompound>> getSequences(AnnotatedSequence<NucleotideCompound> sequence,
			Map<Sequence<?>, OperationInformation> information) {
		List<AnnotatedSequence<AminoAcidCompound>> res = new ArrayList<>(this.frames.length);
		for (Map.Entry<Frame, Sequence<AminoAcidCompound>> oneSequence : this.engine
				.multipleFrameTranslation(sequence, this.frames).entrySet()) {
			String newAnnotation = String.format(annotation, sequence.getAnnotation(), oneSequence.getKey());
			AnnotatedSequence<AminoAcidCompound> oneRes = new AnnotatedSequenceAdapter<>(oneSequence.getValue(),
					newAnnotation);
			res.add(oneRes);
			information.put(oneRes, new OperationInformation(sequence));
		}
		return res;
	}

	@Override
	public String toString() {
		return String.format(description, this.frameDescription);
	}

}
