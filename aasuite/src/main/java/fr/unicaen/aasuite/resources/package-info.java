/**
 * Facilities for handling the resources associated to other classes.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.resources;