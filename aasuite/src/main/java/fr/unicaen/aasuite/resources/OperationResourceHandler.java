package fr.unicaen.aasuite.resources;

import java.util.ResourceBundle;

import fr.greyc.mad.resources.ResourceHandler;
import fr.unicaen.aasuite.constraints.AnnotationConstraint;
import fr.unicaen.aasuite.constraints.FrequencyConstraint;
import fr.unicaen.aasuite.constraints.OccurrenceConstraint;
import fr.unicaen.aasuite.constraints.PositionedSubsequenceConstraint;
import fr.unicaen.aasuite.constraints.RepeatedPatternConstraint;
import fr.unicaen.aasuite.constraints.RepeatedPatternWithGapConstraint;
import fr.unicaen.aasuite.constraints.SignalPeptideConstraint;
import fr.unicaen.aasuite.constraints.SubsequenceConstraint;
import fr.unicaen.aasuite.conversions.DNAToProteinTranslation;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.operations.ConversionType;
import fr.unicaen.aasuite.operations.NegatedConstraint;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.operations.ValueConstraint;
import fr.unicaen.aasuite.transforms.BasicCleavage;
import fr.unicaen.aasuite.transforms.Cleaning;
import fr.unicaen.aasuite.transforms.Cleavage;
import fr.unicaen.aasuite.transforms.MultipleTransform;
import fr.unicaen.aasuite.transforms.Precursors;
import fr.unicaen.aasuite.transforms.SignalPeptideRemoval;

/**
 * A resource handler for resources associated to operations (constraints,
 * transforms, conversions), precisely the description of the types, the
 * descriptions of the constraints and, for transforms and conversions, the
 * annotation of the produced sequences.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class OperationResourceHandler extends ResourceHandler {

	/**
	 * Builds a new instance.
	 * 
	 * @param bundle The bundle holding the resources
	 */
	public OperationResourceHandler(ResourceBundle bundle) {
		super(bundle);
	}

	/**
	 * Initializes the description field of all operation types (viz, the members of
	 * enums {@link ConstraintType}, {@link TransformType}, {@link ConversionType})
	 * with the resources in this bundle.
	 */
	public void initializeOperationTypeDescriptions() {

		// Constraints
		super.handleStringIfExists(s -> ConstraintType.ANNOTATION.description = s, "annotation.name");
		super.handleStringIfExists(s -> ConstraintType.ELECTRICAL_LOAD.description = s, "electrical.load.name");
		super.handleStringIfExists(s -> ConstraintType.FREQUENCY.description = s, "frequency.name");
		super.handleStringIfExists(s -> ConstraintType.HYDROPHOBICITY.description = s, "hydrophobicity.name");
		super.handleStringIfExists(s -> ConstraintType.MONOISOTOPIC_WEIGHT.description = s, "monoisotopic.weight.name");
		super.handleStringIfExists(s -> ConstraintType.NEGATION.description = s, "negation.name");
		super.handleStringIfExists(s -> ConstraintType.NONEMPTINESS.description = s, "nonemptiness.name");
		super.handleStringIfExists(s -> ConstraintType.OCCURRENCE.description = s, "occurrence.name");
		super.handleStringIfExists(s -> ConstraintType.POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL.description = s,
				"positioned.subsequence.from.nterminus.name");
		super.handleStringIfExists(s -> ConstraintType.POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL.description = s,
				"positioned.subsequence.from.cterminus.name");
		super.handleStringIfExists(s -> ConstraintType.REGEXP.description = s, "repeated.pattern.name");
		super.handleStringIfExists(s -> ConstraintType.PATTERN_WITH_GAP.description = s,
				"repeated.pattern.with.gap.name");
		super.handleStringIfExists(s -> ConstraintType.SELECTED.description = s, "selected.name");
		super.handleStringIfExists(s -> ConstraintType.SIGNAL_PEPTIDE.description = s, "signal.peptide.name");
		super.handleStringIfExists(s -> ConstraintType.SIZE.description = s, "size.name");
		super.handleStringIfExists(s -> ConstraintType.SUBSEQUENCE.description = s, "subsequence.name");

		// Conversions
		super.handleStringIfExists(s -> ConversionType.DNA_TO_PROTEIN_TRANSLATION.description = s,
				"dna.to.protein.translation.name");

		// Transforms
		super.handleStringIfExists(s -> TransformType.BASIC_CLEAVAGE.description = s, "basic.cleavage.name");
		super.handleStringIfExists(s -> TransformType.CLEANING.description = s, "cleaning.name");
		super.handleStringIfExists(s -> TransformType.CLEAVAGE.description = s, "cleavage.name");
		super.handleStringIfExists(s -> TransformType.MULTIPLE_BASIC_CLEAVAGE.description = s,
				"multiple.basic.cleavage.name");
		super.handleStringIfExists(s -> TransformType.PRECURSORS.description = s, "precursors.name");
		super.handleStringIfExists(s -> TransformType.SIGNAL_PEPTIDE_REMOVAL.description = s,
				"signal.peptide.removal.name");

	}

	/**
	 * Initializes the description fields of all constraint classes, as used by
	 * their {@literal toString()} method, with the resources in this bundle.
	 */
	public void initializeConstraintStrings() {

		// Annotation constraints
		super.handleStringIfExists(s -> AnnotationConstraint.description = s, "annotation.description");

		// Electrical load constraints
		// .=this.operationProperties.getString("electrical.load.name");

		// Frequency constraints
		super.handleStringIfExists(s -> FrequencyConstraint.tautologicalDescription = s,
				"frequency.tautological.description");
		super.handleStringIfExists(s -> FrequencyConstraint.lowerBoundDescription = s,
				"frequency.lower.bound.description");
		super.handleStringIfExists(s -> FrequencyConstraint.upperBoundDescription = s,
				"frequency.upper.bound.description");
		super.handleStringIfExists(s -> FrequencyConstraint.description = s, "frequency.description");

		// Hydrophobicity constraints
		// .=this.operationProperties.getString("hydrophobicity.name");

		// Monoisotopic weight constraints
		// .=this.operationProperties.getString("monoisotopic.weight.name");

		// Negated constraints
		super.handleStringIfExists(s -> NegatedConstraint.description = s, "negation.description");

		// Nonemptiness constraints
		// .=this.operationProperties.getString("nonempty.name");

		// Occurrence constraints
		super.handleStringIfExists(s -> OccurrenceConstraint.tautologicalDescription = s,
				"occurrence.tautological.description");
		super.handleStringIfExists(s -> OccurrenceConstraint.lowerBoundDescription = s,
				"occurrence.lower.bound.description");
		super.handleStringIfExists(s -> OccurrenceConstraint.upperBoundDescription = s,
				"occurrence.upper.bound.description");
		super.handleStringIfExists(s -> OccurrenceConstraint.description = s, "occurrence.description");

		// Positioned subsequence constraints
		super.handleStringIfExists(s -> PositionedSubsequenceConstraint.unpositionedDescription = s,
				"positioned.subsequence.unpositioned.description");
		super.handleStringIfExists(s -> PositionedSubsequenceConstraint.nterAfterDescription = s,
				"positioned.subsequence.nter.after.description");
		super.handleStringIfExists(s -> PositionedSubsequenceConstraint.nterBeforeDescription = s,
				"positioned.subsequence.nter.before.description");
		super.handleStringIfExists(s -> PositionedSubsequenceConstraint.nterDescription = s,
				"positioned.subsequence.nter.description");
		super.handleStringIfExists(s -> PositionedSubsequenceConstraint.cterAfterDescription = s,
				"positioned.subsequence.cter.after.description");
		super.handleStringIfExists(s -> PositionedSubsequenceConstraint.cterBeforeDescription = s,
				"positioned.subsequence.cter.before.description");
		super.handleStringIfExists(s -> PositionedSubsequenceConstraint.cterDescription = s,
				"positioned.subsequence.cter.description");

		// Repeated pattern constraints
		super.handleStringIfExists(s -> RepeatedPatternConstraint.noBoundDescription = s,
				"repeated.pattern.no.bound.description");
		super.handleStringIfExists(s -> RepeatedPatternConstraint.lowerBoundDescription = s,
				"repeated.pattern.lower.bound.description");
		super.handleStringIfExists(s -> RepeatedPatternConstraint.upperBoundDescription = s,
				"repeated.pattern.upper.bound.description");
		super.handleStringIfExists(s -> RepeatedPatternConstraint.description = s, "repeated.pattern.description");

		// Repeated pattern with gap constraints
		super.handleStringIfExists(s -> RepeatedPatternWithGapConstraint.noOccurrenceDescription = s,
				"repeated.pattern.with.gap.no.occurrence.description");
		super.handleStringIfExists(s -> RepeatedPatternWithGapConstraint.minOccurrencesDescription = s,
				"repeated.pattern.with.gap.min.occurrences.description");
		super.handleStringIfExists(s -> RepeatedPatternWithGapConstraint.maxOccurrencesDescription = s,
				"repeated.pattern.with.gap.max.occurrences.description");
		super.handleStringIfExists(s -> RepeatedPatternWithGapConstraint.description = s,
				"repeated.pattern.with.gap.description");

		// Discard unselected constraints
		// .=this.operationProperties.getString("selected.name");

		// Signal peptide constraints
		super.handleStringIfExists(s -> SignalPeptideConstraint.description = s, "signal.peptide.description");

		// Size constraints
		// .=this.operationProperties.getString("size.name=Size");

		// Subsequence constraints
		super.handleStringIfExists(s -> SubsequenceConstraint.orderedDescription = s,
				"subsequence.ordered.description");
		super.handleStringIfExists(s -> SubsequenceConstraint.unorderedDescription = s,
				"subsequence.unordered.description");
		super.handleStringIfExists(s -> SubsequenceConstraint.wildcardDescription = s,
				"subsequence.wildcard.description");

		// Value constraints
		super.handleStringIfExists(s -> ValueConstraint.noBoundDescription = s, "value.tautological.description");
		super.handleStringIfExists(s -> ValueConstraint.lowerBoundDescription = s, "value.lower.bound.description");
		super.handleStringIfExists(s -> ValueConstraint.upperBoundDescription = s, "value.upper.bound.description");
		super.handleStringIfExists(s -> ValueConstraint.description = s, "value.description");

	}

	/**
	 * Initializes the description fields of all conversion classes, as used by
	 * their {@literal toString()} method, with the resources in this bundle.
	 */
	public void initializeConversionStrings() {
		super.handleStringIfExists(s -> DNAToProteinTranslation.allFramesDescription = s,
				"dna.to.protein.translation.all.frames.description");
		super.handleStringIfExists(s -> DNAToProteinTranslation.allForwardFramesDescription = s,
				"dna.to.protein.translation.all.forward.frames.description");
		super.handleStringIfExists(s -> DNAToProteinTranslation.allReversedFramesDescription = s,
				"dna.to.protein.translation.all.reversed.frames.description");
		super.handleStringIfExists(s -> DNAToProteinTranslation.frameListDescription = s,
				"dna.to.protein.translation.frame.list.description");
		super.handleStringIfExists(s -> DNAToProteinTranslation.description = s,
				"dna.to.protein.translation.description");
		super.handleStringIfExists(s -> DNAToProteinTranslation.annotation = s,
				"dna.to.protein.translation.annotation");
	}

	/**
	 * Initializes the description fields of all transform classes, as used by their
	 * {@literal toString()} method, with the resources in this bundle.
	 */
	public void initializeTransformStrings() {

		// Basic cleavage
		super.handleStringIfExists(s -> BasicCleavage.description = s, "basic.cleavage.description");
		super.handleStringIfExists(s -> BasicCleavage.annotation = s, "basic.cleavage.annotation");

		// Cleaning
		super.handleStringIfExists(s -> Cleaning.maximalSubsequencesDescription = s,
				"cleaning.maximal.subsequences.description");
		super.handleStringIfExists(s -> Cleaning.allSubsequencesDescription = s,
				"cleaning.all.subsequences.description");
		super.handleStringIfExists(s -> Cleaning.minimumSizeDescription = s, "cleaning.minimum.size.description");
		super.handleStringIfExists(s -> Cleaning.annotation = s, "cleaning.annotation");

		// Cleavage
		super.handleStringIfExists(s -> Cleavage.description = s, "cleavage.description");
		super.handleStringIfExists(s -> Cleavage.siteSeparator = s, "cleavage.siteSeparator");
		super.handleStringIfExists(s -> Cleavage.annotation = s, "cleavage.annotation");

		// Multiple transforms
		super.handleStringIfExists(s -> MultipleTransform.description = s, "multiple.transform.description");

		// Precursors
		super.handleStringIfExists(s -> Precursors.noBoundDescription = s, "precursors.no.bound.description");
		super.handleStringIfExists(s -> Precursors.lowerBoundDescription = s, "precursors.lower.bound.description");
		super.handleStringIfExists(s -> Precursors.upperBoundDescription = s, "precursors.upper.bound.description");
		super.handleStringIfExists(s -> Precursors.description = s, "precursors.description");
		super.handleStringIfExists(s -> Precursors.annotation = s, "precursors.annotation");

		// Signal peptide removal
		super.handleStringIfExists(s -> SignalPeptideRemoval.description = s, "signal.peptide.removal.description");
		super.handleStringIfExists(s -> SignalPeptideRemoval.annotation = s, "signal.peptide.removal.annotation");

	}

}
