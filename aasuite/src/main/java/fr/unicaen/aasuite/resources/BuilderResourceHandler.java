package fr.unicaen.aasuite.resources;

import java.util.ResourceBundle;

import fr.greyc.mad.resources.ResourceHandler;
import fr.unicaen.aasuite.userinput.AnnotationConstraintBuilder;

/**
 * A resource handler for resources associated to builders, precisely the labels
 * and help strings.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class BuilderResourceHandler extends ResourceHandler {

	/**
	 * Builds a new instance.
	 * 
	 * @param bundle The bundle holding the resources
	 */
	public BuilderResourceHandler(ResourceBundle bundle) {
		super(bundle);
	}

	/**
	 * Initializes the fields for all constraint builders, including the labels and
	 * help strings of the fields.
	 */
	public void initializeConstraintBuilderStrings() {

		// Annotation constraint builders
		super.handleStringIfExists(s -> AnnotationConstraintBuilder.patternFieldLabel = s, "annotation.pattern.label");
		super.handleStringIfExists(s -> AnnotationConstraintBuilder.patternFieldHelp = s, "annotation.pattern.help");
		super.handleStringIfExists(s -> AnnotationConstraintBuilder.help = s, "annotation.help");

	}

	/**
	 * Initializes the fields for all conversion builders, including the labels and
	 * help strings of the fields.
	 */
	public void initializeConversionBuilderStrings() {
	}

	/**
	 * Initializes the fields for all transform builders, including the labels and
	 * help strings of the fields.
	 */
	public void initializeTransformBuilderStrings() {
	}

}
