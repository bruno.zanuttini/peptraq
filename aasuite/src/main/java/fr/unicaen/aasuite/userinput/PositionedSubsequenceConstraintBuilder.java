package fr.unicaen.aasuite.userinput;

import java.util.List;
import java.util.function.Function;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.unicaen.aasuite.constraints.PositionedSubsequenceConstraint;
import fr.unicaen.aasuite.constraints.SubsequenceConstraint;

/**
 * A builder for positioned subsequence constraints. The labels, help strings,
 * etc. are copied from {@link SubsequenceConstraintBuilder}. See
 * {@link PositionedSubsequenceConstraint} for the semantics of positions.
 * <p>
 * The constructors require a reader for sequences. The static methods
 * {@link SequenceField#readDna(String)} and
 * {@link SequenceField#readProtein(String)} can be used for this.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences
 */
public class PositionedSubsequenceConstraintBuilder<C extends Compound>
		extends AbstractBuilder<InputField<String, ?>, PositionedSubsequenceConstraint<C>> {

	/**
	 * The label for the minimum position field for instances of this class with
	 * positions interpreted wrt N-terminus.
	 */
	public static String nTerminalMinPositionFieldLabel = "Minimum position from N-terminus";

	/**
	 * The help string for the minimum position field for instances of this class
	 * with positions interpreted wrt N-terminus.
	 */
	public static String nTerminalMinPositionFieldHelp = "Enter the minimum number of compounds between N-terminus and the subsequence for it to be matched";

	/**
	 * The label for the minimum position field for instances of this class with
	 * positions interpreted wrt C-terminus.
	 */
	public static String cTerminalMinPositionFieldLabel = "Minimum position from C-terminus";

	/**
	 * The help string for the minimum position field for instances of this class
	 * with positions interpreted wrt C-terminus.
	 */
	public static String cTerminalMinPositionFieldHelp = "Enter the minimum number of compounds between C-terminus and the subsequence for it to be matched";

	/**
	 * The label for the maximum position field for instances of this class with
	 * positions interpreted wrt N-terminus.
	 */
	public static String nTerminalMaxPositionFieldLabel = "Maximum position from N-terminus";

	/**
	 * The help string for the maximum position field for instances of this class
	 * with positions interpreted wrt N-terminus.
	 */
	public static String nTerminalMaxPositionFieldHelp = "Enter the maximum number of compounds between N-terminus and the subsequence for it to be matched";

	/**
	 * The label for the maximum position field for instances of this class with
	 * positions interpreted wrt C-terminus.
	 */
	public static String cTerminalMaxPositionFieldLabel = "Maximum position from C-terminus";

	/**
	 * The help string for the maximum position field for instances of this class
	 * with positions interpreted wrt C-terminus.
	 */
	public static String cTerminalMaxPositionFieldHelp = "Enter the maximum number of compounds between C-terminus and the subsequence for it to be matched";

	/** The error string for when a negative position is input. */
	public static String negativePositionErrorString = "Positions cannot be negative";

	/**
	 * The help string for instances of this class with positions interpreted wrt
	 * N-terminus.
	 */
	public static String nTerminalHelp = "Retain only sequences in which a given subsequence occurs at a position within given bounds with respect to N-terminus";

	/**
	 * The help string for instances of this class with positions interpreted wrt
	 * C-terminus.
	 */
	public static String cTerminalHelp = "Retain only sequences in which a given subsequence occurs at a position within given bounds with respect to C-terminus";

	/**
	 * The error string for when the input minimum position is greater than the
	 * maximum one.
	 */
	public static String minGreaterThanMaxError = "Maximum position must be at least as large a number as minimum position";

	/**
	 * Whether to interpret the position with respect to N-terminus (true) or
	 * C-terminus (false).
	 */
	protected final boolean nTerminal;

	/** A subsequence constraint builder to which most work is delegated. */
	private final SubsequenceConstraintBuilder<C> subsequenceConstraintBuilder;

	/**
	 * An input field for the minimum position of the subsequence wrt the given
	 * terminus.
	 */
	private final InputField<String, Integer> minPositionField;

	/**
	 * An input field for the maximum position of the subsequence wrt the given
	 * terminus.
	 */
	private final InputField<String, Integer> maxPositionField;

	/**
	 * Builds a new instance.
	 * 
	 * @param compoundSet The compound set to use
	 * @param reader      A reader for sequences
	 * @param nTerminal   Whether this builder is for constraints positioned from
	 *                    N-terminus (true) or from C-terminus (false)
	 */
	public PositionedSubsequenceConstraintBuilder(CompoundSet<C> compoundSet,
			Function<String, ? extends Sequence<C>> reader, boolean nTerminal) {
		super(nTerminal ? nTerminalHelp : cTerminalHelp);
		this.nTerminal = nTerminal;
		this.subsequenceConstraintBuilder = new SubsequenceConstraintBuilder<>(compoundSet, reader);
		for (InputField<String, ?> field : this.subsequenceConstraintBuilder.getFields()) {
			super.addField(field);
		}
		this.minPositionField = makePositionField(nTerminal, false);
		super.addField(this.minPositionField);
		this.maxPositionField = makePositionField(nTerminal, true);
		super.addField(this.maxPositionField);
	}

	/**
	 * Builds a new instance, initialized with the parameters of a given constraint.
	 * 
	 * @param constraint  A positioned subsequence constraint
	 * @param compoundSet The compound set to use
	 * @param reader      A reader for sequences
	 */
	public PositionedSubsequenceConstraintBuilder(PositionedSubsequenceConstraint<C> constraint,
			CompoundSet<C> compoundSet, Function<String, ? extends Sequence<C>> reader) {
		super(constraint.fromNTerminus() ? nTerminalHelp : cTerminalHelp);
		this.nTerminal = constraint.fromNTerminus();
		this.subsequenceConstraintBuilder = new SubsequenceConstraintBuilder<>(constraint.getSubsequenceConstraint(),
				compoundSet, reader);
		for (InputField<String, ?> field : this.subsequenceConstraintBuilder.getFields()) {
			super.addField(field);
		}
		this.minPositionField = makePositionField(nTerminal, false);
		super.addField(this.minPositionField);
		this.maxPositionField = makePositionField(nTerminal, true);
		super.addField(this.maxPositionField);
		if (constraint.hasMinPosition()) {
			this.minPositionField.setValue(constraint.getMinPosition());
		}
		if (constraint.hasMaxPosition()) {
			this.maxPositionField.setValue(constraint.getMaxPosition());
		}
	}

	@Override
	public boolean isGloballyValid() {
		if (!this.subsequenceConstraintBuilder.isGloballyValid()) {
			return false;
		}
		Integer minPosition = this.minPositionField.getValue();
		Integer maxPosition = this.maxPositionField.getValue();
		if (minPosition != null && maxPosition != null && maxPosition < minPosition) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public List<String> getGlobalErrors() {
		List<String> res = this.subsequenceConstraintBuilder.getGlobalErrors();
		Integer minPosition = this.minPositionField.getValue();
		Integer maxPosition = this.maxPositionField.getValue();
		if (minPosition != null && maxPosition != null && maxPosition < minPosition) {
			res.add(minGreaterThanMaxError);
		}
		return res;
	}

	@Override
	protected PositionedSubsequenceConstraint<C> safeGetObject() {
		SubsequenceConstraint<C> withoutPosition = this.subsequenceConstraintBuilder.safeGetObject();
		Integer minPosition = this.minPositionField.getValue();
		Integer maxPosition = this.maxPositionField.getValue();
		return new PositionedSubsequenceConstraint<C>(withoutPosition, this.nTerminal, minPosition, maxPosition);
	}

	/**
	 * Helper method: builds an input field for the (min or max) position.
	 * 
	 * @param nTerminal Whether the reference is N-terminus (true) or C-terminus
	 *                  (false)
	 * @param max       Whether the field is for the minimum position (false) or for
	 *                  the maximum position (true)
	 * @return An input field for the given position
	 */
	private static InputField<String, Integer> makePositionField(boolean nTerminal, boolean max) {
		String label = null;
		String helpString = null;
		if (max) {
			label = nTerminal ? nTerminalMaxPositionFieldLabel : cTerminalMaxPositionFieldLabel;
			helpString = nTerminal ? nTerminalMaxPositionFieldHelp : cTerminalMaxPositionFieldHelp;
		} else {
			label = nTerminal ? nTerminalMinPositionFieldLabel : cTerminalMinPositionFieldLabel;
			helpString = nTerminal ? nTerminalMinPositionFieldHelp : cTerminalMinPositionFieldHelp;
		}
		InputField<String, Integer> field = NumberFieldsFromString.makeIntegerField(true, label, helpString);
		return new SimpleRestrictionInputDecorator<String, Integer>(field, negativePositionErrorString) {
			@Override
			protected boolean isValid(Integer value) {
				return value >= 0;
			}
		};
	}

}
