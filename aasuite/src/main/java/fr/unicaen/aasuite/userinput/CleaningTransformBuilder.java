package fr.unicaen.aasuite.userinput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.unicaen.aasuite.transforms.Cleaning;

/**
 * A builder for cleaning transforms.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds
 * 
 *        TODO: Allow keep maximal and/or minimum size (all combinations)
 * 
 *        TODO: Initialize field with all non-compounds
 */
public class CleaningTransformBuilder<C extends Compound> extends AbstractBuilder<InputField<String, ?>, Cleaning<C>> {

	/** The separator to use when entering several multiplicities. */
	public static String separator = ",";

	/**
	 * The label for the compound field. This string may use "1$" for referring to
	 * the separator (String) for several compounds, and "2$" for the list of
	 * allowed compounds.
	 */
	public static String compoundFieldLabel = "Letters to clean out";

	/**
	 * The help string for the compound field. This string may use "1$" for
	 * referring to the separator (String) for several compounds, and "2$" for the
	 * list of allowed compounds.
	 */
	public static String compoundFieldHelp = "Enter the letters which you want to clean out (among %2$s, separated by '%1$s')";

	/** The label for the minimum size field. */
	public static String minSizeFieldLabel = "Minimum size of clean subsequences (optional)";

	/** The help string for the minimum size field. */
	public static String minSizeFieldHelp = "Enter the minimum size for a clean subsequence to be kept; leave blank for keeping only maximally long subsequences";

	/** The error string to use when the given min size is negative. */
	public static String minSizeFieldError = "Minimum size must be nonnegative";

	/** The help string for this class. */
	public static String help = "Clean sequences by removing some letters and keeping only the subsequences in between them";

	/** The field for entering the compounds to clean out. */
	private final InputField<String, List<C>> compoundField;

	/**
	 * The field for entering the minimum size (to be left blank for keeping
	 * maximall subsequences).
	 */
	private final InputField<String, Integer> minSizeField;

	/**
	 * Builds a new instance.
	 * 
	 * @param compoundSet The compound set to use
	 */
	public CleaningTransformBuilder(CompoundSet<C> compoundSet) {
		super(help);
		String label = String.format(compoundFieldLabel, separator, compoundSet.getAllCompounds());
		String help = String.format(compoundFieldHelp, separator, compoundSet.getAllCompounds());
		this.compoundField = new MultipleCompoundInputFieldFromString<C>(false, label, compoundSet, separator, help);
		InputField<String, Integer> sizeField = NumberFieldsFromString.makeIntegerField(true, minSizeFieldLabel,
				minSizeFieldHelp);
		this.minSizeField = new SimpleRestrictionInputDecorator<String, Integer>(sizeField, minSizeFieldError) {

			@Override
			protected boolean isValid(Integer value) {
				return value >= 0;
			}

		};
		super.addField(this.compoundField);
		super.addField(this.minSizeField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given transform.
	 * 
	 * @param transform   A Cleaning transform
	 * @param compoundSet The set of compounds to use
	 */
	public CleaningTransformBuilder(Cleaning<C> transform, CompoundSet<C> compoundSet) {
		this(compoundSet);
		this.compoundField.setValue(new ArrayList<>(transform.getCompounds()));
		if (!transform.keepOnlyMaximal()) {
			this.minSizeField.setValue(transform.getMinSize());
		}
	}

	@Override
	public boolean isGloballyValid() {
		return true;
	}

	@Override
	public List<String> getGlobalErrors() {
		return Collections.emptyList();

	}

	@Override
	protected Cleaning<C> safeGetObject() {
		Set<C> compounds = new HashSet<>(this.compoundField.getValue());
		return this.minSizeField.hasValue() ? new Cleaning<>(compounds, this.minSizeField.getValue())
				: new Cleaning<>(compounds);
	}

}
