package fr.unicaen.aasuite.userinput;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;

import fr.greyc.mad.userinput.InputFieldFromStringAdapter;

/**
 * An input field for one compound, from strings. The input string for one
 * compound is as defined by {@code CompoundSet.getCompoundForString(String)}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds
 */
public class CompoundInputFieldFromString<C extends Compound> extends InputFieldFromStringAdapter<C> {

	/** The compound set defining the valid compounds for this field. */
	private CompoundSet<C> compoundSet;

	/**
	 * Builds a new instance with no initial value.
	 * 
	 * @param isOptional  Whether this field is optional (true) or not (false)
	 * @param label       The label for this field
	 * @param compoundSet The compound set defining the valid compounds for this
	 *                    field
	 * @param helpString  The help string for this field
	 */
	public CompoundInputFieldFromString(boolean isOptional, String label, CompoundSet<C> compoundSet, String helpString) {
		super(new CompoundField<>(isOptional, label, compoundSet, helpString));
		this.compoundSet = compoundSet;
	}

	/**
	 * Builds a new instance with an initial input string.
	 * 
	 * @param isOptional   Whether this field is optional (true) or not (false)
	 * @param label        The label for this field
	 * @param compoundSet  The compound set defining the valid compounds for this
	 *                     field
	 * @param initialInput The initial input string for this field
	 * @param helpString   The help string for this field
	 */
	public CompoundInputFieldFromString(boolean isOptional, String label, CompoundSet<C> compoundSet, String initialInput,
			String helpString) {
		super(new CompoundField<>(isOptional, label, compoundSet, helpString), initialInput);
		this.compoundSet = compoundSet;
	}

	@Override
	public String getInputFor(C value) throws IllegalArgumentException {
		return this.compoundSet.getStringForCompound(value);
	}

	@Override
	protected C tryGetValue(String input) throws IllegalArgumentException {
		C res = this.compoundSet.getCompoundForString(input);
		if (res == null) {
			throw new IllegalArgumentException("No compound corresponds to string \"" + input + "\"");
		} else {
			return res;
		}
	}

}