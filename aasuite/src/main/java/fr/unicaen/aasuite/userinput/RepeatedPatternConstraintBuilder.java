package fr.unicaen.aasuite.userinput;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.RegexpField;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.unicaen.aasuite.constraints.RepeatedPatternConstraint;

/**
 * A builder for repeated pattern constraints.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences
 */
public class RepeatedPatternConstraintBuilder<C extends Compound>
		extends AbstractBuilder<InputField<String, ?>, RepeatedPatternConstraint<C>> {

	/** The label for the pattern field. */
	public static String patternFieldLabel = "Pattern";

	/** The help string for the pattern field. */
	public static String patternFieldHelp = "Enter the pattern to match; a regular expression can be used, for instance AC{3,5}.G for A followed by 3 to 5 C's followed by any compound followed by G";

	/** The label for the minimum number of occurrences field. */
	public static String minNbOccurrencesFieldLabel = "Minimum number of occurrences";

	/** The help string for the minimum number of occurrences field. */
	public static String minNbOccurrencesFieldHelp = "Enter the minimum number of occurrences of the pattern for a sequence to be retained (leave blank for no minimum)";

	/** The label for the maximum number of occurrences field. */
	public static String maxNbOccurrencesFieldLabel = "Maximum number of occurrences";

	/** The help string for the maximum number of occurrences field. */
	public static String maxNbOccurrencesFieldHelp = "Enter the maximum number of occurrences of the pattern for a sequence to be retained (leave blank for no maximum)";

	/** The error string for when a negative number of occurrences is input. */
	public static String negativeNbOccurrencesErrorString = "Value must be nonnegative";

	/** The help string for this class. */
	public static String help = "Retain only sequences in which a given pattern occurs a number of time within given bounds";

	/**
	 * The error string for when neither a minimum nor a maximum number of
	 * occurrences has been input.
	 */
	public static String noBoundError = "At least a minimum or a maximum number of occurrences must be specified";

	/**
	 * The error string for when the minimum number of occurrences input is greater
	 * than the maximum.
	 */
	public static String minGreaterThanMaxError = "Maximum number of occurrences must be at least as large a number as minimum number";

	/** The input field for the pattern. */
	private final InputField<String, Pattern> patternField;

	/** The input field for the minimum number of occurrences. */
	private final InputField<String, Integer> minNbOccurrencesField;

	/** The input field for the maximum number of occurrences. */
	private final InputField<String, Integer> maxNbOccurrencesField;

	/**
	 * Builds a new instance.
	 */
	public RepeatedPatternConstraintBuilder() {
		super(help);
		final boolean caseSensitive = false; // the whole application is case-insensitive for compounds
		this.patternField = new RegexpField(false, patternFieldLabel, caseSensitive, patternFieldHelp);
		this.minNbOccurrencesField = this.makeOccurrenceField(minNbOccurrencesFieldLabel, minNbOccurrencesFieldHelp,
				negativeNbOccurrencesErrorString);
		this.maxNbOccurrencesField = this.makeOccurrenceField(maxNbOccurrencesFieldLabel, maxNbOccurrencesFieldHelp,
				negativeNbOccurrencesErrorString);
		super.addField(this.patternField);
		super.addField(this.minNbOccurrencesField);
		super.addField(this.maxNbOccurrencesField);
	}

	/**
	 * Builds a new instance, initialized with the parameters of a given constraint.
	 * 
	 * @param constraint A repeated pattern constraint
	 */
	public RepeatedPatternConstraintBuilder(RepeatedPatternConstraint<C> constraint) {
		this();
		this.patternField.setValue(constraint.getPattern());
		if (constraint.hasMinNbOccurrences()) {
			this.minNbOccurrencesField.setValue(constraint.getMinNbOccurrences());
		}
		if (constraint.hasMaxNbOccurrences()) {
			this.maxNbOccurrencesField.setValue(constraint.getMaxNbOccurrences());
		}
	}

	@Override
	public boolean isGloballyValid() {
		Integer minNbOccurrences = this.minNbOccurrencesField.getValue();
		Integer maxNbOccurrences = this.maxNbOccurrencesField.getValue();
		if (minNbOccurrences == null && maxNbOccurrences == null) {
			return false;
		}
		if (minNbOccurrences != null && maxNbOccurrences != null && maxNbOccurrences < minNbOccurrences) {
			return false;
		}
		return true;
	}

	@Override
	public List<String> getGlobalErrors() {
		List<String> res = new ArrayList<>();
		Integer minNbOccurrences = this.minNbOccurrencesField.getValue();
		Integer maxNbOccurrences = this.maxNbOccurrencesField.getValue();
		if (minNbOccurrences == null && maxNbOccurrences == null) {
			res.add(noBoundError);
		}
		if (minNbOccurrences != null && maxNbOccurrences != null && maxNbOccurrences < minNbOccurrences) {
			res.add(minGreaterThanMaxError);
		}
		return res;
	}

	@Override
	protected RepeatedPatternConstraint<C> safeGetObject() {
		Pattern pattern = this.patternField.getValue();
		Integer minNbOccurrences = this.minNbOccurrencesField.getValue();
		Integer maxNbOccurrences = this.maxNbOccurrencesField.getValue();
		return new RepeatedPatternConstraint<C>(pattern, minNbOccurrences, maxNbOccurrences);
	}

	/**
	 * Helper method: builds an input field for the (min or max) number of
	 * occurrences.
	 * 
	 * @param label       The label for the field
	 * @param helpString  The help string for the field
	 * @param errorString The error string for when the input value is negative
	 * @return An input field for the (min or max) number of occurrences
	 */
	private InputField<String, Integer> makeOccurrenceField(String label, String helpString, String errorString) {
		InputField<String, Integer> field = NumberFieldsFromString.makeIntegerField(true, label, helpString);
		return new SimpleRestrictionInputDecorator<String, Integer>(field, errorString) {
			@Override
			protected boolean isValid(Integer value) {
				return value >= 0;
			}
		};
	}

}
