package fr.unicaen.aasuite.userinput;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;
import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.userinput.FreeField;
import fr.greyc.mad.userinput.InputFieldFromStringAdapter;

/**
 * An input field (from strings) for sequences of compounds, possibly with
 * wildcard compounds (intended to match any compound if the sequence if
 * searched for). The sequences are represented as lists of compounds, with null
 * at whatever position the wildcard was input.
 * <p>
 * Technically, the wildcard is itself a "compound" (in the relevant commpound
 * set): its short (one-letter) name acts as the wildcard in the input string.
 * Hence constructors throw an exception if there is no compound which has the
 * given wildcard as its short name. Note that in practice, the only character
 * common to nucleotide and amino acid compounds is '-'.
 * <p>
 * Constructors require a reader for sequences. For that purpose, the class
 * provides the static methods {@link #readDna(String)} and
 * {@link #readProtein(String)}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences
 */
public class SequenceField<C extends Compound> extends InputFieldFromStringAdapter<List<C>> {

	/**
	 * A dummy header added to the input string before it is passed to the reader.
	 */
	private static final String DUMMY_HEADER = ">dummy" + System.lineSeparator();

	/** The reader for sequences. */
	protected final Function<String, ? extends Sequence<C>> reader;

	/** The "compound" acting as a wildcard (null if none). */
	protected final C wildcard;

	/**
	 * Reads a DNA sequence from a string (denoting the sequence itself, without any
	 * header).
	 * 
	 * @param str A string denoting a DNA sequence
	 * @return The DNA sequence represented by the string
	 * @throws IllegalArgumentException if the string does not represent a (single)
	 *                                  DNA sequence
	 */
	public static DNASequence readDna(String str) throws IllegalArgumentException {
		Map<?, DNASequence> sequences = null;
		try {
			sequences = FastaReaderHelper
					.readFastaDNASequence(new ByteArrayInputStream((DUMMY_HEADER + str).getBytes()));
		} catch (IOException e) {
			throw new IllegalArgumentException("An error occurred while reading string", e);
		}
		if (sequences.size() != 1) {
			throw new IllegalArgumentException("String does not contain exactly one DNA sequence");
		} else {
			return sequences.values().iterator().next();
		}
	}

	/**
	 * Reads a protein sequence from a string (denoting the sequence itself, without
	 * any header).
	 * 
	 * @param str A string denoting a protein sequence
	 * @return The protein sequence represented by the string
	 * @throws IllegalArgumentException if the string does not represent a (single)
	 *                                  protein sequence
	 */
	public static ProteinSequence readProtein(String str) {
		Map<?, ProteinSequence> sequences = null;
		try {
			sequences = FastaReaderHelper
					.readFastaProteinSequence(new ByteArrayInputStream((DUMMY_HEADER + str).getBytes()));
		} catch (IOException e) {
			throw new IllegalArgumentException("An error occurred while reading stream", e);
		}
		if (sequences.size() != 1) {
			throw new IllegalArgumentException("Stream does not contain exactly one protein sequence");
		} else {
			return sequences.values().iterator().next();
		}
	}

	/**
	 * Builds a new instance with no initial input string and no wildcard.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param reader     The sequence reader to use
	 * @param helpString The help string for the field
	 */
	public SequenceField(boolean isOptional, String label, Function<String, ? extends Sequence<C>> reader,
			String helpString) {
		super(new FreeField<List<C>>(isOptional, label, helpString));
		this.reader = reader;
		this.wildcard = null;
	}

	/**
	 * Builds a new instance with no initial input string but with a wildcard.
	 * 
	 * @param isOptional  Whether the field is optional (true) or not (false)
	 * @param label       The label for the field
	 * @param reader      The sequence reader to use
	 * @param compoundSet The compound set to use
	 * @param wildcard    The wildcard to use
	 * @param helpString  The help string for the field
	 * @throws IllegalArgumentException if the wildcard is not the short name of a
	 *                                  "compound" in the compound set
	 */
	public SequenceField(boolean isOptional, String label, Function<String, ? extends Sequence<C>> reader,
			CompoundSet<C> compoundSet, char wildcard, String helpString) {
		super(new FreeField<List<C>>(isOptional, label, helpString));
		this.reader = reader;
		this.wildcard = compoundSet.getCompoundForString(String.valueOf(wildcard));
	}

	/**
	 * Builds a new instance with an initial input string and no wildcard.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input string for the field
	 * @param reader       The sequence reader to use
	 * @param helpString   The help string for the field
	 */
	public SequenceField(boolean isOptional, String label, String initialInput,
			Function<String, ? extends Sequence<C>> reader, String helpString) {
		super(new FreeField<List<C>>(isOptional, label, helpString), initialInput);
		this.reader = reader;
		this.wildcard = null;
	}

	/**
	 * Builds a new instance with an initial input string and a wildcard.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input string for the field
	 * @param reader       The sequence reader to use
	 * @param compoundSet  The compound set to use
	 * @param wildcard     The wildcard to use
	 * @param helpString   The help string for the field
	 * @throws IllegalArgumentException if the wildcard is not the short name of a
	 *                                  "compound" in the compound set
	 */
	public SequenceField(boolean isOptional, String label, String initialInput,
			Function<String, ? extends Sequence<C>> reader, CompoundSet<C> compoundSet, char wildcard,
			String helpString) {
		super(new FreeField<List<C>>(isOptional, label, helpString), initialInput);
		this.reader = reader;
		this.wildcard = compoundSet.getCompoundForString(String.valueOf(wildcard));
	}

	@Override
	public String getInputFor(List<C> value) throws IllegalArgumentException {
		StringBuffer res = new StringBuffer();
		for (C compound : value) {
			res.append(compound.getShortName());
		}
		return res.toString();
	}

	@Override
	protected List<C> tryGetValue(String input) {
		List<C> res = this.reader.apply(input).getAsList();
		res.replaceAll(compound -> compound.equals(this.wildcard) ? null : compound);
		return res;
	}

}
