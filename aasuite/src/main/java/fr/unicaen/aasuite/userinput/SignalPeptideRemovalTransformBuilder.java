package fr.unicaen.aasuite.userinput;

import fr.greyc.mad.userinput.BooleanInputFieldFromString;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.TrivialBuilderFromStrings;
import fr.unicaen.aasuite.transforms.SignalPeptideRemoval;

/**
 * A builder for signal peptide removal transforms.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SignalPeptideRemovalTransformBuilder extends TrivialBuilderFromStrings<SignalPeptideRemoval> {

	/** The default min score proposed as input for the prediction. */
	public static String defaultMinScoreInput = "0.5";

	/** The label for the organism type input field. */
	public static String organismTypeFieldLabel = "Organism type";

	/** The help string for the organism type input field. */
	public static String organismTypeFieldHelp = "Choose the type of organisms to which the sequences belong";

	/** The label for the minimum score field. */
	public static String minScoreFieldLabel = "Minimum confidence score";

	/** The help string for the minimum score field. */
	public static String minScoreFieldHelp = "Enter the minimum confidence (in [0, 1]) with which a signal peptide must be predicted for it to be removed";

	/**
	 * The input string for the "yes" option to the "keep if no signal peptide"
	 * field.
	 */
	public static String keepIfNoneInputString = "retain";

	/**
	 * The input string for the "no" option to the "keep if no signal peptide"
	 * field.
	 */
	public static String doNotKeepIfNoneInputString = "discard";

	/**
	 * The label for the "keep if no signal peptide" field. The string may use 1$ to
	 * refer to the input string for the "yes" option, and 2$ to refer to the input
	 * string for the "no" option.
	 */
	public static String keepIfNoneFieldLabel = "Retain sequences with no predicted signal peptide";

	/**
	 * The help string for the "keep if no signal peptide" field. The string may use
	 * 1$ to refer to the input string for the "yes" option, and 2$ to refer to the
	 * input string for the "no" option.
	 */
	public static String keepIfNoneFieldHelp = "Choose '%1$s' if you want the sequences in which no signal peptide has been predicted with confidence high enough to be retained (as such), and '%2$s' if you want them to be dicarded";

	/** The help string for this class. */
	public static String help = "Remove the signal peptide from each sequence, if predicted with confidence high enough";

	/** The input field for the type of organisms. */
	private final OrganismTypeInputFieldFromString typeField;

	/** The input field for the min score of the prediction. */
	private final InputField<String, Float> minScoreField;

	/**
	 * The input field for whether to keep sequences which have no signal peptide.
	 */
	private final BooleanInputFieldFromString keepIfNoneField;

	/**
	 * Builds a new instance.
	 */
	public SignalPeptideRemovalTransformBuilder() {
		super(help);
		this.typeField = new OrganismTypeInputFieldFromString(false, organismTypeFieldLabel, organismTypeFieldHelp);
		super.addField(this.typeField);
		this.minScoreField = NumberFieldsFromString.makeFloatField(false, minScoreFieldLabel, defaultMinScoreInput,
				minScoreFieldHelp);
		super.addField(this.minScoreField);
		String label = String.format(keepIfNoneFieldLabel, keepIfNoneInputString, doNotKeepIfNoneInputString);
		String help = String.format(keepIfNoneFieldHelp, keepIfNoneInputString, doNotKeepIfNoneInputString);
		final String defaultInput = doNotKeepIfNoneInputString;
		this.keepIfNoneField = new BooleanInputFieldFromString(false, label, keepIfNoneInputString,
				doNotKeepIfNoneInputString, defaultInput, help);
		super.addField(this.keepIfNoneField);
	}

	/**
	 * Builds a new instance, initialized with the parameters of a given transform.
	 * 
	 * @param transform A signall peptide removal transform
	 */
	public SignalPeptideRemovalTransformBuilder(SignalPeptideRemoval transform) {
		this();
		this.typeField.setValue(transform.getOrganismType());
		this.minScoreField.setValue(transform.getMinScore());
		this.keepIfNoneField.setValue(transform.keepIfNone());
	}

	@Override
	protected SignalPeptideRemoval safeGetObject() {
		return new SignalPeptideRemoval(this.typeField.getValue(), this.minScoreField.getValue(),
				this.keepIfNoneField.getValue());
	}

}
