package fr.unicaen.aasuite.userinput;

import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.TrivialBuilderFromStrings;
import fr.unicaen.aasuite.constraints.SignalPeptideConstraint;

/**
 * A builder for signal peptide constraints.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SignalPeptideConstraintBuilder extends TrivialBuilderFromStrings<SignalPeptideConstraint> {

	/** The default min score proposed as input for the prediction. */
	public static String defaultMinScoreInput = "0.5";

	/** The label for the organism type input field. */
	public static String organismTypeFieldLabel = "Organism type";

	/** The help string for the organism type input field. */
	public static String organismTypeFieldHelp = "Choose the type of organisms to which the sequences belong";

	/** The label for the minimum score field. */
	public static String minScoreFieldLabel = "Minimum confidence score";

	/** The help string for the minimum score field. */
	public static String minScoreFieldHelp = "Enter the minimum confidence (in [0, 1]) with which a signal peptide must be predicted for a sequence to be retained";

	/** The help string for this class. */
	public static String help = "Retain only sequences which have a signal peptide with confidence high enough";

	/** The input field for the type of organisms. */
	private final OrganismTypeInputFieldFromString typeField;

	/** The input field for the min score of the prediction. */
	private final InputField<String, Float> minScoreField;

	/**
	 * Builds a new instance.
	 */
	public SignalPeptideConstraintBuilder() {
		super(help);
		this.typeField = new OrganismTypeInputFieldFromString(false, organismTypeFieldLabel, organismTypeFieldHelp);
		super.addField(this.typeField);
		this.minScoreField = NumberFieldsFromString.makeFloatField(false, minScoreFieldLabel, defaultMinScoreInput,
				minScoreFieldHelp);
		super.addField(this.minScoreField);
	}

	/**
	 * Builds a new instance, initialized with the parameters of a given constraint.
	 * @param constraint A signal peptide constraint
	 */
	public SignalPeptideConstraintBuilder(SignalPeptideConstraint constraint) {
		this();
		this.typeField.setValue(constraint.getOrganismType());
		this.minScoreField.setValue(constraint.getMinScore());
	}

	@Override
	protected SignalPeptideConstraint safeGetObject() {
		return new SignalPeptideConstraint(this.typeField.getValue(), this.minScoreField.getValue());
	}

}
