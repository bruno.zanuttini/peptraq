package fr.unicaen.aasuite.userinput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.MultipleInputFieldFromStringAdapter;
import fr.unicaen.aasuite.transforms.Cleavage;

/**
 * A builder for cleavage transforms (along user-specified cleavage sites).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class CleavageTransformBuilder<C extends Compound> extends AbstractBuilder<InputField<String, ?>, Cleavage<C>> {

	/** The separator to use when entering several cleavage sites. */
	public static String separator = ",";

	/**
	 * The label for the cleavage sites field. This string may use "1$" for
	 * referring to the separator (String) for several sites.
	 */
	public static String cleavageSitesFieldLabel = "Sites along which to cleave";

	/**
	 * The help string for the cleavage sites field. This string may use "1$" for
	 * referring to the separator (String) for several sites.
	 */
	public static String cleavageSitesFieldHelp = "Enter the sites along which you want to cleave (separated by '%1$s')";

	/** The help string for this class. */
	public static String help = "Cleave along given sites";

	/** The input field for the cleavage sites. */
	private final InputField<String, List<List<C>>> siteField;

	/**
	 * Builds a new instance.
	 * 
	 * @param cleavageSiteField A field for entering a single cleavage site
	 */
	public CleavageTransformBuilder(InputField<String, List<C>> cleavageSiteField) {
		super(help);
		String label = String.format(cleavageSitesFieldLabel, separator);
		String help = String.format(cleavageSitesFieldHelp, separator);
		this.siteField = new MultipleInputFieldFromStringAdapter<>(label, cleavageSiteField, 1, null, separator, help);
		super.addField(this.siteField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given transform.
	 * 
	 * @param transform     A cleavage transform
	 * @param sequenceField A field for entering a single cleavage site
	 */
	public CleavageTransformBuilder(Cleavage<C> transform, InputField<String, List<C>> sequenceField) {
		this(sequenceField);
		this.siteField.setValue(new ArrayList<>(transform.getCleavageSites()));
	}

	@Override
	public boolean isGloballyValid() {
		return true;
	}

	@Override
	public List<String> getGlobalErrors() {
		return Collections.emptyList();
	}

	@Override
	protected Cleavage<C> safeGetObject() {
		return new Cleavage<C>(this.siteField.getValue());
	}

}
