/**
 * Classes for user input. Specifically, this package defines classes for input
 * fields and for operation builders.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.userinput;