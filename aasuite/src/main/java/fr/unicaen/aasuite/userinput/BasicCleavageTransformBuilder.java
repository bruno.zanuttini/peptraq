package fr.unicaen.aasuite.userinput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.MultipleInputFieldFromStringAdapter;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.transforms.BasicCleavage;
import fr.unicaen.aasuite.transforms.MultipleTransform;

/**
 * A builder for basic cleavage transforms.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class BasicCleavageTransformBuilder
		extends AbstractBuilder<InputField<String, ?>, Transform<AminoAcidCompound>> {

	/** The separator to use when entering several multiplicities. */
	public static String separator = ",";

	/**
	 * The label for the multiplicity field. This string may use "1$" for referring
	 * to the separator (String) for several multiplicities.
	 */
	public static String multiplicityFieldLabel = "Sizes of basic sites along which to cleave";

	/**
	 * The help string for the multiplicity field. This string may use "1$" for
	 * referring to the separator (String) for several multiplicities.
	 */
	public static String multiplicityFieldHelp = "Enter the sizes along which you want to cleave (separated by '%1$s'); for instance, \"1%1s3\" for cleaving along monobasic and tribasic sites";

	/** The help string for this class. */
	public static String help = "Cleave along basic sites of given sizes";

	/** The field for the multiplicities along which to cleave. */
	private final InputField<String, List<Integer>> multiplicityField;

	/**
	 * Builds a new instance.
	 */
	public BasicCleavageTransformBuilder() {
		super(help);
		InputField<String, Integer> singleMultiplicityField = NumberFieldsFromString.makeIntegerField(false, null,
				null);
		String label = String.format(multiplicityFieldLabel, separator);
		String help = String.format(multiplicityFieldHelp, separator);
		this.multiplicityField = new MultipleInputFieldFromStringAdapter<>(label, singleMultiplicityField, 1, null,
				separator, help);
		super.addField(this.multiplicityField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given transform.
	 * 
	 * @param transform A multiple transform composed of {@link BasicCleavage}
	 *                  transforms
	 * @throws ClassCastException if the transforms in the multiple transform are
	 *                            not all instances of {@link BasicCleavage}
	 */
	public BasicCleavageTransformBuilder(MultipleTransform<AminoAcidCompound> transform) throws ClassCastException {
		this();
		List<Integer> multiplicities = new ArrayList<>();
		for (Transform<AminoAcidCompound> cleaver : transform.getTransforms()) {
			multiplicities.add(((BasicCleavage) cleaver).getLength());
		}
		this.multiplicityField.setValue(multiplicities);
	}

	@Override
	public boolean isGloballyValid() {
		return true;
	}

	@Override
	public List<String> getGlobalErrors() {
		return Collections.emptyList();
	}

	@Override
	protected Transform<AminoAcidCompound> safeGetObject() {
		List<Integer> multiplicities = this.multiplicityField.getValue();
		if (multiplicities.size() == 1) {
			return new BasicCleavage(multiplicities.get(0));
		} else {
			List<Transform<AminoAcidCompound>> cleavers = new ArrayList<>();
			Set<Integer> seen = new HashSet<>();
			for (int n : this.multiplicityField.getValue()) {
				if (!seen.contains(n)) {
					cleavers.add(new BasicCleavage(n));
					seen.add(n);
				}
			}
			return new MultipleTransform<AminoAcidCompound>(TransformType.MULTIPLE_BASIC_CLEAVAGE, cleavers) {

				@Override
				public String toString() {
					StringBuilder res = new StringBuilder();
					res.append("Cleavage along ");
					String sep = "";
					for (Integer multiplicity : multiplicities) {
						res.append(sep);
						res.append(multiplicity.toString());
						sep = "/";
					}
					res.append("-basic sites");
					return res.toString();
				}
			};

		}

	}

}
