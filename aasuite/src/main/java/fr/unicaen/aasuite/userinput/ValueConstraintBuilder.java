package fr.unicaen.aasuite.userinput;

import java.util.Collections;
import java.util.List;
import java.util.function.Function;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.operations.ValueConstraint;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A builder for value constraints.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds to which the constraint applies
 * @param <T> The type of values
 */
public class ValueConstraintBuilder<C extends Compound, T extends Comparable<T>>
		extends AbstractBuilder<InputField<String, ?>, ValueConstraint<C, T>> {

	/**
	 * An interface for methods which build input fields from strings as needed by
	 * this class.
	 *
	 * @param <T> The type of values in the fields
	 */
	@FunctionalInterface
	public interface FieldMaker<T> {

		/**
		 * Builds an input field from strings with values of type {@code T}.
		 * 
		 * @param isOptional Whether the field is optional (true) or not (false)
		 * @param label      The label for the field
		 * @param helpString The help string for the field
		 * @return An input field for values of type {@code T}, from strings
		 */
		InputField<String, T> make(boolean isOptional, String label, String helpString);

	}

	/**
	 * The label for the minimum value field. The string may use 1$ to refer to the
	 * (lowercase) string describing the feature whose value is to be constrained.
	 */
	public static String minFieldLabel = "Minimum value";

	/**
	 * The help string for the minimum value field. The string may use 1$ to refer
	 * to the (lowercase) string describing the feature whose value is to be
	 * constrained.
	 */
	public static String minFieldHelp = "Enter the minimum value for a sequence to be retained; leave blank for no minimum";

	/**
	 * The label for the maximum value field. The string may use 1$ to refer to the
	 * (lowercase) string describing the feature whose value is to be constrained.
	 */
	public static String maxFieldLabel = "Maximum value";

	/**
	 * The help string for the maximum value field. The string may use 1$ to refer
	 * to the (lowercase) string describing the feature whose value is to be
	 * constrained.
	 */
	public static String maxFieldHelp = "Enter the maximum value for a sequence to be retained; leave blank for no maximum";

	/**
	 * The help string for instances of this class. The string may use 1$ to refer
	 * to the (lowercase) string describing the feature whose value is to be
	 * constrained.
	 */
	public static String help = "Retain only sequences for which the value of %1$s is in a given interval";

	/**
	 * The error string for when neither a minimum nor a maximum value has been
	 * input. The string may use 1$ to refer to the (lowercase) string describing
	 * the feature whose value is to be constrained.
	 */
	public static String noBoundError = "At least a minimum or a maximum value must be specified";

	/**
	 * The error string for when the minimum value input is greater than the
	 * maximum.
	 */
	public static String minGreaterThanMaxError = "Maximum value must be at least as large as minimum value";

	/** The type of constraints built by this builder. */
	protected final ConstraintType type;

	/** The feature whose value to constrain. */
	protected final Function<? super AnnotatedSequence<C>, T> feature;

	/** A description of the feature whose value to constrain. */
	protected final String featureDescription;

	/** The input field for the minimum value. */
	private final InputField<String, T> minField;

	/** The input field for the maximum value. */
	private final InputField<String, T> maxField;

	/**
	 * Builds a new instance.
	 * 
	 * @param type               The type of constraints to be build
	 * @param feature            The feature whose value to constrain
	 * @param featureDescription A description of the feature whose value to
	 *                           constrain (with initial cap)
	 * @param fieldMaker         A field maker for the input fields for values of
	 *                           the feature
	 */
	public ValueConstraintBuilder(ConstraintType type, Function<? super AnnotatedSequence<C>, T> feature,
			String featureDescription, FieldMaker<T> fieldMaker) {
		super(String.format(help, featureDescription.toLowerCase()));
		this.type = type;
		this.feature = feature;
		this.featureDescription = featureDescription;
		String minLabel = String.format(minFieldLabel, featureDescription.toLowerCase());
		String minHelp = String.format(minFieldHelp, featureDescription.toLowerCase());
		this.minField = fieldMaker.make(true, minLabel, minHelp);
		String maxLabel = String.format(maxFieldLabel, featureDescription.toLowerCase());
		String maxHelp = String.format(maxFieldHelp, featureDescription.toLowerCase());
		this.maxField = fieldMaker.make(true, maxLabel, maxHelp);
		super.addField(this.minField);
		super.addField(this.maxField);
	}

	/**
	 * Builds a new instance, initialized with the parameters of a given constraint.
	 * 
	 * @param constraint A value constraint
	 * @param fieldMaker A field maker for the input fields for values of the
	 *                   feature
	 */
	public ValueConstraintBuilder(ValueConstraint<C, T> constraint, FieldMaker<T> fieldMaker) {
		this(constraint.getType(), constraint.getFeature(), constraint.getFeatureDescription(), fieldMaker);
		if (constraint.hasMinValue()) {
			this.minField.setValue(constraint.getMinValue());
		}
		if (constraint.hasMaxValue()) {
			this.maxField.setValue(constraint.getMaxValue());
		}
	}

	@Override
	public boolean isGloballyValid() {
		T min = this.minField.getValue();
		T max = this.maxField.getValue();
		if (min == null && max == null) {
			return false;
		} else if (min == null || max == null) {
			return true;
		}
		if (max.compareTo(min) < 0) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public List<String> getGlobalErrors() {
		T min = this.minField.getValue();
		T max = this.maxField.getValue();
		if (min == null && max == null) {
			return Collections.singletonList(noBoundError);
		} else if (min == null || max == null) {
			return Collections.emptyList();
		} else if (max.compareTo(min) < 0) {
			return Collections.singletonList(minGreaterThanMaxError);
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	protected ValueConstraint<C, T> safeGetObject() {
		return new ValueConstraint<>(this.type, this.feature, this.minField.getValue(), this.maxField.getValue(),
				this.featureDescription);
	}

}
