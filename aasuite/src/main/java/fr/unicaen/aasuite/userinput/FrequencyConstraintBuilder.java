package fr.unicaen.aasuite.userinput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.unicaen.aasuite.constraints.FrequencyConstraint;

/**
 * A builder for frequency constraints.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds to which the constraints apply
 */
public class FrequencyConstraintBuilder<C extends Compound>
		extends AbstractBuilder<InputField<String, ?>, FrequencyConstraint<C>> {

	/** The separator to use when entering several compounds. */
	public static String separator = ",";

	/**
	 * The label for the compound input field. This label may use "1$" for referring
	 * to the separator (String) to be used for specifying several compounds.
	 */
	public static String compoundFieldLabel = "Compounds";

	/**
	 * The help string for the compound input field. This string may use "1$" for
	 * referring to the separator (String) to be used for specifying several
	 * compounds.
	 */
	public static String compoundFieldHelp = "Enter one or several compounds (separated by '%1$s'); frequencies of these compounds will be added to each other";

	/** The label for the minimum frequency field. */
	public static String minFrequencyFieldLabel = "Minimum frequency";

	/** The help string for the minimum frequency field. */
	public static String minFrequencyFieldHelp = "Enter a frequency (in %), or leave blank for no minimum";

	/** The label for the maximum frequency field. */
	public static String maxFrequencyFieldLabel = "Maximum frequency";

	/** The help string for the maximum frequency field. */
	public static String maxFrequencyFieldHelp = "Enter a frequency (in %), or leave blank for no maximum";

	/**
	 * The error displayed if the minimum frequency field holds a negative value.
	 */
	public static String minFrequencyFieldError = "Value must be nonnegative";

	/**
	 * The error displayed if the maximum frequency field holds a value greater than
	 * 100.
	 */
	public static String maxFrequencyFieldError = "Value must be at most 100 (%)";

	/** The help string for this class. */
	public static String help = "Retain only sequences in which the cumulated frequency of some compounds match given bounds";

	/**
	 * The error displayed if neither a minimum nor a maximum frequency is given.
	 */
	public static String noBoundError = "At least one bound (minimum or maximum frequency) must be specified";

	/**
	 * The error displayed if the given minimum is greater than the given maximum
	 * frequency.
	 */
	public static String minGreaterThanMaxError = "Maximum must be at least as large as minimum";

	/** A field for the compounds whose frequency to test. */
	private final MultipleCompoundInputFieldFromString<C> compoundField;

	/** A field for the minimum required frequency. */
	private final InputField<String, Float> minField;

	/** A field for the maximum required frequency. */
	private final InputField<String, Float> maxField;

	/**
	 * Builds a new instance.
	 * 
	 * @param compoundSet The set of compounds which can be chosen
	 */
	public FrequencyConstraintBuilder(CompoundSet<C> compoundSet) {
		super(help);
		String label = String.format(compoundFieldLabel, separator);
		String help = String.format(compoundFieldHelp, separator);
		this.compoundField = new MultipleCompoundInputFieldFromString<>(false, label, compoundSet, separator, help);
		this.minField = this.makeMinField();
		this.maxField = this.makeMaxField();
		super.addField(this.compoundField);
		super.addField(this.minField);
		super.addField(this.maxField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given constraint.
	 * 
	 * @param constraint  A frequency constraint
	 * @param compoundSet The set of compounds which can be chosen
	 */
	public FrequencyConstraintBuilder(FrequencyConstraint<C> constraint, CompoundSet<C> compoundSet) {
		this(compoundSet);
		this.compoundField.setValue(new ArrayList<>(constraint.getCompounds()));
		if (constraint.hasMinFrequency()) {
			this.minField.setValue(constraint.getMinFrequency());
		}
		if (constraint.hasMaxFrequency()) {
			this.maxField.setValue(constraint.getMaxFrequency());
		}
	}

	@Override
	public boolean isGloballyValid() {
		Float min = this.minField.getValue();
		Float max = this.maxField.getValue();
		if (min == null && max == null) {
			return false;
		} else if (min == null || max == null) {
			return true;
		}
		if (max < min) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public List<String> getGlobalErrors() {
		Float min = this.minField.getValue();
		Float max = this.maxField.getValue();
		if (min == null && max == null) {
			return Collections.singletonList(noBoundError);
		} else if (min == null || max == null) {
			return Collections.emptyList();
		} else if (max < min) {
			return Collections.singletonList(minGreaterThanMaxError);
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	protected FrequencyConstraint<C> safeGetObject() {
		return new FrequencyConstraint<>(this.compoundField.getValue(), this.minField.getValue(),
				this.maxField.getValue());
	}

	/**
	 * Helper method: builds the field for the minimum required frequency.
	 * 
	 * @return The field for the minimum required frequency
	 */
	private InputField<String, Float> makeMinField() {
		InputField<String, Float> field = NumberFieldsFromString.makeFloatField(true, minFrequencyFieldLabel,
				minFrequencyFieldHelp);
		return new SimpleRestrictionInputDecorator<String, Float>(field, minFrequencyFieldError) {
			@Override
			protected boolean isValid(Float value) {
				return value >= 0;
			}
		};
	}

	/**
	 * Helper method: builds the field for the maximum required frequency.
	 * 
	 * @return The field for the maximum required frequency
	 */
	private InputField<String, Float> makeMaxField() {
		InputField<String, Float> field = NumberFieldsFromString.makeFloatField(true, maxFrequencyFieldLabel,
				maxFrequencyFieldHelp);
		return new SimpleRestrictionInputDecorator<String, Float>(field, maxFrequencyFieldError) {
			@Override
			protected boolean isValid(Float value) {
				return value <= 100;
			}
		};
	}

}
