package fr.unicaen.aasuite.userinput;

import java.util.Arrays;
import java.util.List;

import org.biojava.nbio.aaproperties.PeptideProperties;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.compound.DNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.TrivialBuilderFromStrings;
import fr.unicaen.aasuite.constraints.AnnotationConstraint;
import fr.unicaen.aasuite.constraints.FrequencyConstraint;
import fr.unicaen.aasuite.constraints.OccurrenceConstraint;
import fr.unicaen.aasuite.constraints.PositionedSubsequenceConstraint;
import fr.unicaen.aasuite.constraints.RepeatedPatternConstraint;
import fr.unicaen.aasuite.constraints.RepeatedPatternWithGapConstraint;
import fr.unicaen.aasuite.constraints.SignalPeptideConstraint;
import fr.unicaen.aasuite.constraints.SubsequenceConstraint;
import fr.unicaen.aasuite.features.ElectricalLoad;
import fr.unicaen.aasuite.features.MonoisotopicWeight;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.operations.NegatedConstraint;
import fr.unicaen.aasuite.operations.NoWitnessConstraint;
import fr.unicaen.aasuite.operations.ValueConstraint;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A factory for constraint builders.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: enum instead of list for all constraint types
 */
public class ConstraintBuilders {

	/** The help string for non emptiness constraint builders. */
	public static String nonEmptinessHelp = "Discard empty sequences";

	/** The list of all constraint types for DNA sequences. */
	public static final List<ConstraintType> DNA_CONSTRAINT_TYPES = Arrays.asList(ConstraintType.FREQUENCY,
			ConstraintType.NONEMPTINESS, ConstraintType.OCCURRENCE, ConstraintType.ANNOTATION, ConstraintType.REGEXP,
			ConstraintType.PATTERN_WITH_GAP, ConstraintType.SIZE, ConstraintType.SUBSEQUENCE,
			ConstraintType.POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL,
			ConstraintType.POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL);

	/** The list of all constraint types for protein sequences. */
	public static final List<ConstraintType> PROTEIN_CONSTRAINT_TYPES = Arrays.asList(ConstraintType.AVG_HYDROPHOBICITY,
			ConstraintType.FREQUENCY, ConstraintType.NONEMPTINESS, ConstraintType.MOLECULAR_WEIGHT,
			ConstraintType.MONOISOTOPIC_WEIGHT, ConstraintType.NET_CHARGE, ConstraintType.OCCURRENCE,
			ConstraintType.ANNOTATION, ConstraintType.SIGNAL_PEPTIDE, ConstraintType.REGEXP,
			ConstraintType.PATTERN_WITH_GAP, ConstraintType.SIZE, ConstraintType.SUBSEQUENCE,
			ConstraintType.POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL,
			ConstraintType.POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL, ConstraintType.HYDROPHOBICITY);

	/**
	 * Returns a new builder for a given constraint type for DNA sequences.
	 * 
	 * @param type         A constraint type
	 * @param withNegation Whether the returned builder should be decorated with a
	 *                     {@link NegationDecorator} in case this makes sense; if
	 *                     false, the builder will not be decorated, and if true, it
	 *                     will be decorated if this makes sense for the given
	 *                     constraint type, and not decorated if this does not make
	 *                     sense
	 * @return A constraint builder for the given type
	 * @throws IllegalArgumentException If the given type is not one for DNA
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Constraint<NucleotideCompound>> getDNABuilder(
			ConstraintType type, boolean withNegation) throws IllegalArgumentException {
		AbstractBuilder<InputField<String, ?>, ? extends Constraint<NucleotideCompound>> res = null;
		boolean meaningfulNegation = true;
		switch (type) {
		case ANNOTATION:
			res = new AnnotationConstraintBuilder<NucleotideCompound>();
			break;
		case FREQUENCY:
			res = new FrequencyConstraintBuilder<>(DNACompoundSet.getDNACompoundSet());
			break;
		case NONEMPTINESS:
			res = ConstraintBuilders.getNonemptinessConstraintBuilder(nonEmptinessHelp);
			meaningfulNegation = false;
			break;
		case OCCURRENCE:
			res = new OccurrenceConstraintBuilder<>(DNACompoundSet.getDNACompoundSet());
			break;
		case PATTERN_WITH_GAP:
			res = new RepeatedPatternWithGapConstraintBuilder<>(SequenceField::readDna);
			break;
		case POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL:
			res = new PositionedSubsequenceConstraintBuilder<>(DNACompoundSet.getDNACompoundSet(),
					SequenceField::readDna, false);
			break;
		case POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL:
			res = new PositionedSubsequenceConstraintBuilder<>(DNACompoundSet.getDNACompoundSet(),
					SequenceField::readDna, true);
			break;
		case REGEXP:
			res = new RepeatedPatternConstraintBuilder<NucleotideCompound>();
			break;
		case SIZE:
			res = new ValueConstraintBuilder<>(ConstraintType.SIZE, s -> s.getLength(), "Number of compounds",
					NumberFieldsFromString::makeIntegerField);
			break;
		case SUBSEQUENCE:
			res = new SubsequenceConstraintBuilder<>(DNACompoundSet.getDNACompoundSet(), SequenceField::readDna);
			break;
		default:
			throw new IllegalArgumentException("Unknown constraint type for DNA sequences: " + type);
		}
		return withNegation && meaningfulNegation ? new NegationDecorator<>(res, false) : res;
	}

	/**
	 * Returns a builder initialized to build a given constraint for DNA sequences.
	 * 
	 * @param constraint A constraint on DNA sequences
	 * @return A constraint builder initialized to build the given constraint
	 * @throws IllegalArgumentException If the given type is not one for DNA
	 *                                  sequences
	 */
	@SuppressWarnings("unchecked")
	public static AbstractBuilder<InputField<String, ?>, ? extends Constraint<NucleotideCompound>> getDNABuilder(
			Constraint<NucleotideCompound> constraint) throws IllegalArgumentException {
		switch (constraint.getType()) {
		case ANNOTATION:
			return new AnnotationConstraintBuilder<>((AnnotationConstraint<NucleotideCompound>) constraint);
		case FREQUENCY:
			return new FrequencyConstraintBuilder<>((FrequencyConstraint<NucleotideCompound>) constraint,
					DNACompoundSet.getDNACompoundSet());
		case NEGATION:
			AbstractBuilder<InputField<String, ?>, ? extends Constraint<NucleotideCompound>> builder = getDNABuilder(
					((NegatedConstraint<NucleotideCompound>) constraint).getNegatedConstraint());
			return new NegationDecorator<>(builder, true);
		case NONEMPTINESS:
			return getNonemptinessConstraintBuilder(nonEmptinessHelp);
		case OCCURRENCE:
			return new OccurrenceConstraintBuilder<>((OccurrenceConstraint<NucleotideCompound>) constraint,
					DNACompoundSet.getDNACompoundSet());
		case PATTERN_WITH_GAP:
			return new RepeatedPatternWithGapConstraintBuilder<>(
					(RepeatedPatternWithGapConstraint<NucleotideCompound>) constraint, SequenceField::readDna);
		case POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL:
		case POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL:
			return new PositionedSubsequenceConstraintBuilder<>(
					(PositionedSubsequenceConstraint<NucleotideCompound>) constraint,
					DNACompoundSet.getDNACompoundSet(), SequenceField::readDna);
		case REGEXP:
			return new RepeatedPatternConstraintBuilder<>((RepeatedPatternConstraint<NucleotideCompound>) constraint);
		case SIZE:
			return new ValueConstraintBuilder<>((ValueConstraint<NucleotideCompound, Integer>) constraint,
					NumberFieldsFromString::makeIntegerField);
		case SUBSEQUENCE:
			return new SubsequenceConstraintBuilder<>((SubsequenceConstraint<NucleotideCompound>) constraint,
					DNACompoundSet.getDNACompoundSet(), SequenceField::readDna);
		default:
			throw new IllegalArgumentException("Unknown constraint type for DNA sequences: " + constraint.getType());
		}
	}

	/**
	 * Returns a new builder for a given constraint type for protein sequences.
	 * 
	 * @param type         A constraint type
	 * @param withNegation Whether the returned builder should be decorated with a
	 *                     {@link NegationDecorator} in case this makes sense; if
	 *                     false, the builder will not be decorated, and if true, it
	 *                     will be decorated if this makes sense for the given
	 *                     constraint type, and not decorated if this does not make
	 *                     sense
	 * @return A constraint builder for the given type
	 * @throws IllegalArgumentException If the given type is not one for protein
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Constraint<AminoAcidCompound>> getProteinBuilder(
			ConstraintType type, boolean withNegation) throws IllegalArgumentException {
		AbstractBuilder<InputField<String, ?>, ? extends Constraint<AminoAcidCompound>> res = null;
		boolean meaningfulNegation = true;
		switch (type) {
		case ANNOTATION:
			res = new AnnotationConstraintBuilder<AminoAcidCompound>();
			break;
		case AVG_HYDROPHOBICITY:
			res = new ValueConstraintBuilder<>(ConstraintType.AVG_HYDROPHOBICITY,
					s -> PeptideProperties.getAvgHydropathy(s.getSequenceAsString()),
					ConstraintType.AVG_HYDROPHOBICITY.description, NumberFieldsFromString::makeDoubleField);
			break;
		case ELECTRICAL_LOAD:
			res = new ValueConstraintBuilder<>(ConstraintType.ELECTRICAL_LOAD, ElectricalLoad::getLoad,
					ConstraintType.ELECTRICAL_LOAD.description, NumberFieldsFromString::makeIntegerField);
			break;
		case FREQUENCY:
			res = new FrequencyConstraintBuilder<>(AminoAcidCompoundSet.getAminoAcidCompoundSet());
			break;
		case HYDROPHOBICITY:
			res = new ValueConstraintBuilder<>(ConstraintType.HYDROPHOBICITY,
					s -> PeptideProperties.getAvgHydropathy(s.getSequenceAsString()) * s.getSequenceAsString().length(),
					ConstraintType.HYDROPHOBICITY.description, NumberFieldsFromString::makeDoubleField);
			break;
		case MOLECULAR_WEIGHT:
			res = new ValueConstraintBuilder<>(ConstraintType.MOLECULAR_WEIGHT,
					s -> PeptideProperties.getMolecularWeight(s.getSequenceAsString()),
					ConstraintType.MOLECULAR_WEIGHT.description, NumberFieldsFromString::makeDoubleField);
			break;
		case MONOISOTOPIC_WEIGHT:
			res = new ValueConstraintBuilder<>(ConstraintType.MONOISOTOPIC_WEIGHT, MonoisotopicWeight::getWeight,
					ConstraintType.MONOISOTOPIC_WEIGHT.description, NumberFieldsFromString::makeDoubleField);
			break;
		case NET_CHARGE:
			res = new ValueConstraintBuilder<>(ConstraintType.NET_CHARGE,
					s -> PeptideProperties.getNetCharge(s.getSequenceAsString()), ConstraintType.NET_CHARGE.description,
					NumberFieldsFromString::makeDoubleField);
			break;
		case NONEMPTINESS:
			res = ConstraintBuilders.getNonemptinessConstraintBuilder(nonEmptinessHelp);
			meaningfulNegation = false;
			break;
		case OCCURRENCE:
			res = new OccurrenceConstraintBuilder<>(AminoAcidCompoundSet.getAminoAcidCompoundSet());
			break;
		case PATTERN_WITH_GAP:
			res = new RepeatedPatternWithGapConstraintBuilder<>(SequenceField::readProtein);
			break;
		case POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL:
			res = new PositionedSubsequenceConstraintBuilder<>(AminoAcidCompoundSet.getAminoAcidCompoundSet(),
					SequenceField::readProtein, false);
			break;
		case POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL:
			res = new PositionedSubsequenceConstraintBuilder<>(AminoAcidCompoundSet.getAminoAcidCompoundSet(),
					SequenceField::readProtein, true);
			break;
		case REGEXP:
			res = new RepeatedPatternConstraintBuilder<AminoAcidCompound>();
			break;
		case SIGNAL_PEPTIDE:
			res = new SignalPeptideConstraintBuilder();
			break;
		case SIZE:
			res = new ValueConstraintBuilder<>(ConstraintType.SIZE, s -> s.getLength(), ConstraintType.SIZE.description,
					NumberFieldsFromString::makeIntegerField);
			break;
		case SUBSEQUENCE:
			res = new SubsequenceConstraintBuilder<>(AminoAcidCompoundSet.getAminoAcidCompoundSet(),
					SequenceField::readProtein);
			break;
		default:
			throw new IllegalArgumentException("Unknown constraint type for protein sequences: " + type);
		}
		return withNegation && meaningfulNegation ? new NegationDecorator<>(res, false) : res;
	}

	/**
	 * Returns a builder initialized to build a given constraint for protein
	 * sequences.
	 * 
	 * @param constraint A constraint on protein sequences
	 * @return A constraint builder initialized to build the given constraint
	 * @throws IllegalArgumentException If the given type is not one for protein
	 *                                  sequences
	 */
	@SuppressWarnings("unchecked")
	public static AbstractBuilder<InputField<String, ?>, ? extends Constraint<AminoAcidCompound>> getProteinBuilder(
			Constraint<AminoAcidCompound> constraint) throws IllegalArgumentException {
		switch (constraint.getType()) {
		case ANNOTATION:
			return new AnnotationConstraintBuilder<AminoAcidCompound>(
					(AnnotationConstraint<AminoAcidCompound>) constraint);
		case AVG_HYDROPHOBICITY:
			return new ValueConstraintBuilder<>((ValueConstraint<AminoAcidCompound, Double>) constraint,
					NumberFieldsFromString::makeDoubleField);
		case ELECTRICAL_LOAD:
			return new ValueConstraintBuilder<>((ValueConstraint<AminoAcidCompound, Integer>) constraint,
					NumberFieldsFromString::makeIntegerField);
		case FREQUENCY:
			return new FrequencyConstraintBuilder<>((FrequencyConstraint<AminoAcidCompound>) constraint,
					AminoAcidCompoundSet.getAminoAcidCompoundSet());
		case HYDROPHOBICITY:
			return new ValueConstraintBuilder<>((ValueConstraint<AminoAcidCompound, Double>) constraint,
					NumberFieldsFromString::makeDoubleField);
		case MOLECULAR_WEIGHT:
			return new ValueConstraintBuilder<>((ValueConstraint<AminoAcidCompound, Double>) constraint,
					NumberFieldsFromString::makeDoubleField);
		case MONOISOTOPIC_WEIGHT:
			return new ValueConstraintBuilder<>((ValueConstraint<AminoAcidCompound, Double>) constraint,
					NumberFieldsFromString::makeDoubleField);
		case NEGATION:
			AbstractBuilder<InputField<String, ?>, ? extends Constraint<AminoAcidCompound>> builder = getProteinBuilder(
					((NegatedConstraint<AminoAcidCompound>) constraint).getNegatedConstraint());
			return new NegationDecorator<>(builder, true);
		case NET_CHARGE:
			return new ValueConstraintBuilder<>((ValueConstraint<AminoAcidCompound, Double>) constraint,
					NumberFieldsFromString::makeDoubleField);
		case NONEMPTINESS:
			return ConstraintBuilders.getNonemptinessConstraintBuilder(nonEmptinessHelp);
		case OCCURRENCE:
			return new OccurrenceConstraintBuilder<>((OccurrenceConstraint<AminoAcidCompound>) constraint,
					AminoAcidCompoundSet.getAminoAcidCompoundSet());
		case PATTERN_WITH_GAP:
			return new RepeatedPatternWithGapConstraintBuilder<>(
					(RepeatedPatternWithGapConstraint<AminoAcidCompound>) constraint, SequenceField::readProtein);
		case POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL:
		case POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL:
			return new PositionedSubsequenceConstraintBuilder<>(
					(PositionedSubsequenceConstraint<AminoAcidCompound>) constraint,
					AminoAcidCompoundSet.getAminoAcidCompoundSet(), SequenceField::readProtein);
		case REGEXP:
			return new RepeatedPatternConstraintBuilder<AminoAcidCompound>(
					(RepeatedPatternConstraint<AminoAcidCompound>) constraint);
		case SIGNAL_PEPTIDE:
			return new SignalPeptideConstraintBuilder((SignalPeptideConstraint) constraint);
		case SIZE:
			return new ValueConstraintBuilder<>((ValueConstraint<AminoAcidCompound, Integer>) constraint,
					NumberFieldsFromString::makeIntegerField);
		case SUBSEQUENCE:
			return new SubsequenceConstraintBuilder<>((SubsequenceConstraint<AminoAcidCompound>) constraint,
					AminoAcidCompoundSet.getAminoAcidCompoundSet(), SequenceField::readProtein);
		default:
			throw new IllegalArgumentException(
					"Unknown constraint type for protein sequences: " + constraint.getType());
		}
	}

	/**
	 * Helper method: returns a constraint builder for a constraint satisfied only
	 * by nonempty sequences.
	 * 
	 * @param helpString The help string for the builder
	 * @return A constraint builder for a constraint satisfied only by nonempty
	 *         sequences
	 * 
	 * @param <C> The type of compounds
	 */
	private static <C extends Compound> AbstractBuilder<InputField<String, ?>, Constraint<C>> getNonemptinessConstraintBuilder(
			String helpString) {
		return new TrivialBuilderFromStrings<Constraint<C>>(helpString) {

			@Override
			protected Constraint<C> safeGetObject() {
				return new NoWitnessConstraint<C>() {

					@Override
					public boolean satisfies(AnnotatedSequence<C> sequence) {
						return sequence.getLength() > 0;
					}

					@Override
					public ConstraintType getType() {
						return ConstraintType.NONEMPTINESS;
					}

					@Override
					public String toString() {
						return ConstraintType.NONEMPTINESS.description;
					}

				};
			}

		};
	}

}
