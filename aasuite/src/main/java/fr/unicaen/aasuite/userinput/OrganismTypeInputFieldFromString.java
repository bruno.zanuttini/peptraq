package fr.unicaen.aasuite.userinput;

import java.util.Arrays;

import fr.greyc.mad.userinput.BoundValueField;
import fr.greyc.mad.userinput.InputFieldFromStringAdapter;
import fr.unicaen.aasuite.features.SignalPeptide;

/**
 * An input field (from strings) for types of organisms (eukaryotes,
 * gram-negative prokaryotes, gram-positive prokaryotes). The input string for a
 * given type if defined to be the value returned by
 * {@link SignalPeptide.OrganismType#getDescription()}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class OrganismTypeInputFieldFromString extends InputFieldFromStringAdapter<SignalPeptide.OrganismType> {

	/**
	 * Builds a new instance with no initial input string.
	 * 
	 * @param isOptional Whether the field is optional (true) or not (false)
	 * @param label      The label for the field
	 * @param helpString The help string for the field
	 */
	public OrganismTypeInputFieldFromString(boolean isOptional, String label, String helpString) {
		super(new BoundValueField<>(isOptional, label, Arrays.asList(SignalPeptide.OrganismType.values()), helpString));
	}

	/**
	 * Builds a new instance with an initial input string.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param initialInput The initial input string for the field
	 * @param helpString   The help string for the field
	 */
	public OrganismTypeInputFieldFromString(boolean isOptional, String label, String initialInput, String helpString) {
		super(new BoundValueField<>(isOptional, label, Arrays.asList(SignalPeptide.OrganismType.values()), helpString),
				initialInput);
	}

	@Override
	public String getInputFor(SignalPeptide.OrganismType value) throws IllegalArgumentException {
		return value.getDescription();
	}

	@Override
	protected SignalPeptide.OrganismType tryGetValue(String input) throws IllegalArgumentException {
		for (SignalPeptide.OrganismType type : SignalPeptide.OrganismType.values()) {
			if (type.getDescription().equals(input)) {
				return type;
			}
		}
		throw new IllegalArgumentException("No organism type corresponds to string \"" + input + "\"");
	}

}
