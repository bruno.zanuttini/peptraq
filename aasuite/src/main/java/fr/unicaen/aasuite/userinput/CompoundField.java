package fr.unicaen.aasuite.userinput;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;

import fr.greyc.mad.userinput.BoundValueField;

/**
 * A field for compounds.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds
 */
public class CompoundField<C extends Compound> extends BoundValueField<C> {

	/**
	 * Builds a new instance.
	 * @param isOptional Whether this field is optional l(true) or not (false)
	 * @param label The label for this field
	 * @param compoundSet The compound set defining the valid compounds for this field
	 * @param helpString The help string for this field
	 */
	public CompoundField(boolean isOptional, String label, CompoundSet<C> compoundSet, String helpString) {
		super(isOptional, label, compoundSet.getAllCompounds(), helpString);
	}

}
