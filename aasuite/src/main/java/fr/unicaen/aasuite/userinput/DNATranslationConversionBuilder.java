package fr.unicaen.aasuite.userinput;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.biojava.nbio.core.sequence.transcription.Frame;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.BoundValueField;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.InputFieldFromStringAdapter;
import fr.unicaen.aasuite.conversions.DNAToProteinTranslation;

/**
 * A builder for DNA to protein translations.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 */
public class DNATranslationConversionBuilder extends AbstractBuilder<InputField<String, ?>, DNAToProteinTranslation> {

	// Implementation note: the following constants are essential, since
	// Frame.getXXFrames seems to build a new Array on each call (hence the array
	// is not .equals to the one previously returned by the same method)

	/** A constant array containing all frames. */
	private static final Frame[] ALL_FRAMES = Frame.getAllFrames();

	/** A constant array containing all forward frames. */
	private static final Frame[] FORWARD_FRAMES = Frame.getForwardFrames();

	/** A constant array containing all reverse frames. */
	private static final Frame[] REVERSE_FRAMES = Frame.getReverseFrames();

	/** A constant list of all options for the frames. */
	private static final List<Frame[]> OPTIONS = Arrays.asList(DNATranslationConversionBuilder.ALL_FRAMES,
			DNATranslationConversionBuilder.FORWARD_FRAMES, DNATranslationConversionBuilder.REVERSE_FRAMES);

	/** The input string for selecting all frames. */
	public static String allInputString = "all frames";

	/** The input string for selecting all forward frames. */
	public static String forwardInputString = "forward only";

	/** The input string for selecting all reverse frames. */
	public static String reverseInputString = "reverse only";

	/** The label for the frame field. */
	public static String frameFieldLabel = "Frames";

	/** The help string for the frame field. */
	public static String frameFieldHelp = "Select the frames along which to translate";

	/** The help string for this class. */
	public static String help = "Translate DNA to protein sequences along given frames";

	/** The field for the frames to use. */
	private final InputField<String, Frame[]> frameField;

	/**
	 * Builds a new instance.
	 */
	public DNATranslationConversionBuilder() {
		super(help);
		BoundValueField<Frame[]> field = new BoundValueField<>(false, frameFieldLabel, OPTIONS, frameFieldHelp);
		this.frameField = new InputFieldFromStringAdapter<Frame[]>(field) {

			@Override
			public String getInputFor(Frame[] value) throws IllegalArgumentException {
				if (value.equals(DNATranslationConversionBuilder.ALL_FRAMES)) {
					return allInputString;
				} else if (value.equals(DNATranslationConversionBuilder.FORWARD_FRAMES)) {
					return forwardInputString;
				} else if (value.equals(DNATranslationConversionBuilder.REVERSE_FRAMES)) {
					return reverseInputString;
				} else {
					throw new IllegalArgumentException("Value \"" + value + "\" is not allowed");
				}
			}

			@Override
			protected Frame[] tryGetValue(String input) throws IllegalArgumentException {
				if (input.equals(allInputString)) {
					return DNATranslationConversionBuilder.ALL_FRAMES;
				} else if (input.equals(forwardInputString)) {
					return DNATranslationConversionBuilder.FORWARD_FRAMES;
				} else if (input.equals(reverseInputString)) {
					return DNATranslationConversionBuilder.REVERSE_FRAMES;
				} else {
					return null;
				}
			}

		};
		super.addField(this.frameField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given translator.
	 * 
	 * @param conversion A DNA to protein translator
	 */
	public DNATranslationConversionBuilder(DNAToProteinTranslation conversion) {
		this();
		this.frameField.setValue(conversion.getFrames());
	}

	@Override
	public boolean isGloballyValid() {
		return true;
	}

	@Override
	public List<String> getGlobalErrors() {
		return Collections.emptyList();

	}

	@Override
	protected DNAToProteinTranslation safeGetObject() {
		return new DNAToProteinTranslation(this.frameField.getValue());
	}

}
