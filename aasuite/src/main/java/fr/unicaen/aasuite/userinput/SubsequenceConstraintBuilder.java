package fr.unicaen.aasuite.userinput;

import java.util.List;
import java.util.function.Function;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.userinput.BooleanInputFieldFromString;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.TrivialBuilderFromStrings;
import fr.unicaen.aasuite.constraints.SubsequenceConstraint;

/**
 * A builder for subsequence constraints.
 * <p>
 * The constructors require a reader for sequences. The static methods
 * {@link SequenceField#readDna(String)} and
 * {@link SequenceField#readProtein(String)} can be used for this.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences
 */
public class SubsequenceConstraintBuilder<C extends Compound> extends TrivialBuilderFromStrings<SubsequenceConstraint<C>> {

	/** The character to use as a wildcard. */
	public static char wildcard = '-';

	/**
	 * The label for the subsequence field. The string may use 1$ to refer to the
	 * character (char) which can be used as a wildcard.
	 */
	public static String subsequenceFieldLabel = "Subsequence";

	/**
	 * The help string for the subsequence field. The string may use 1$ to refer to
	 * the character (char) which can be used as a wildcard.
	 */
	public static String subsequenceFieldHelp = "Enter the subsequence to search for; '%1$s' can be used to match any compound";

	/** The input string for including permutations of the subsequence. */
	public static String includePermutationsInputString = "yes";

	/** The input string for not including permutations of the subsequence. */
	public static String doNotIncludePermutationsInputString = "no";

	/**
	 * The label for the "include permutations" field. The string may use 1$ and 2$
	 * to refer to the input strings for "true" and "false", respectively.
	 */
	public static String includePermutationsFieldLabel = "Match permutations of the subsequence";

	/**
	 * The help string for the "include permutations" field. The string may use 1$
	 * and 2$ to refer to the input strings for "true" and "false", respectively.
	 */
	public static String includePermutationsFieldHelp = "Enter '%1$s' for matching the subsequence in any order, and '%2$s' for matching it only in the given order";

	/** The help string for this class. */
	public static String help = "Retain only sequences in which a given subsequence occurs";

	/** The input field for the subsequence to search for. */
	private final SequenceField<C> subsequenceField;

	/** The input field for whether to match permutations of the subsequence. */
	private final InputField<String, Boolean> includePermutationsField;

	/**
	 * Builds a new instance.
	 * 
	 * @param compoundSet The compound set to use
	 * @param reader      A reader for sequences
	 */
	public SubsequenceConstraintBuilder(CompoundSet<C> compoundSet, Function<String, ? extends Sequence<C>> reader) {
		super(help);
		String subsequenceLabel = String.format(subsequenceFieldLabel, wildcard);
		String subsequenceHelp = String.format(subsequenceFieldHelp, wildcard);
		this.subsequenceField = new SequenceField<>(false, subsequenceLabel, reader, compoundSet, wildcard,
				subsequenceHelp);
		String includePermutationsLabel = String.format(includePermutationsFieldLabel, includePermutationsInputString,
				doNotIncludePermutationsInputString);
		String includePermutationsHelp = String.format(includePermutationsFieldHelp, includePermutationsInputString,
				doNotIncludePermutationsInputString);
		this.includePermutationsField = new BooleanInputFieldFromString(false, includePermutationsLabel,
				includePermutationsInputString, doNotIncludePermutationsInputString,
				doNotIncludePermutationsInputString, includePermutationsHelp);
		super.addField(this.subsequenceField);
		super.addField(this.includePermutationsField);
	}

	/**
	 * Builds a new instance, initialized with the parameters of a given constraint.
	 * 
	 * @param constraint  A subsequence constraint
	 * @param compoundSet The compound set to use
	 * @param reader      A reader for sequences
	 */
	public SubsequenceConstraintBuilder(SubsequenceConstraint<C> constraint, CompoundSet<C> compoundSet,
			Function<String, ? extends Sequence<C>> reader) {
		this(compoundSet, reader);
		this.subsequenceField.setValue(constraint.getSubsequence());
		this.includePermutationsField.setValue(constraint.includePermutations());
	}

	@Override
	protected SubsequenceConstraint<C> safeGetObject() {
		List<C> subsequence = this.subsequenceField.getValue();
		boolean ordered = !this.includePermutationsField.getValue();
		return new SubsequenceConstraint<C>(subsequence, ordered);
	}

}
