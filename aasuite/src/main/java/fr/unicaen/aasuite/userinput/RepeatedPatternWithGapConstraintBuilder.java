package fr.unicaen.aasuite.userinput;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.unicaen.aasuite.constraints.RepeatedPatternWithGapConstraint;

/**
 * A builder for constraints that a given pattern with gap is repeated.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences
 */
public class RepeatedPatternWithGapConstraintBuilder<C extends Compound>
		extends AbstractBuilder<InputField<String, ?>, RepeatedPatternWithGapConstraint<C>> {

	/** The label for the prefix field. */
	public static String prefixFieldLabel = "Prefix";

	/** The help string for the prefix field. */
	public static String prefixFieldHelp = "Enter the prefix of the pattern (sequence of compounds)";

	/** The label for the minimum gap field. */
	public static String minGapFieldLabel = "Minimum gap";

	/** The help string for the minimum gap field. */
	public static String minGapFieldHelp = "Enter the minimum number of compounds between the prefix and the suffix for an occurrence to be matched";

	/** The label for the maximum gap field. */
	public static String maxGapFieldLabel = "Maximum gap";

	/** The help string for the maximum gap field. */
	public static String maxGapFieldHelp = "Enter the maximum number of compounds between the prefix and the suffix for an occurrence to be matched";

	/** The label for the suffix field. */
	public static String suffixFieldLabel = "Suffix";

	/** The help string for the suffix field. */
	public static String suffixFieldHelp = "Enter the prefix of the pattern (sequence of compounds)";

	/** The label for the minimum number of repetitions field. */
	public static String minNbOccurrenceFieldLabel = "Minimum number of occurrences";

	/** The help string for the minimum number of repetitions field. */
	public static String minNbOccurrenceFieldHelp = "Enter the minimum number of occurrences of the pattern in a sequence for it to be retained (enter 1 if you just want it to occur; leave blank for no minimum; otherwise enter a positive number)";

	/** The label for the maximum number of repetitions field. */
	public static String maxNbOccurrenceFieldLabel = "Maximum number of occurrences";

	/** The help string for the maximum number of repetitions field. */
	public static String maxNbOccurrenceFieldHelp = "Enter the maximum number of occurrences of the pattern in a sequence for it to be retained (leave blank for no maximum; otherwise enter a positive number)";

	/**
	 * The error string for indicating that an integer has been input which is
	 * negative.
	 */
	public static String nonnegativeIntegerFieldError = "Value cannot be negative";

	/** The help string for this class. */
	public static String help = "Retain only sequences in which a given pattern, with a varying gap between prefix and suffix, has a number of occurrences within given bounds";

	/**
	 * The error string for when the minimum gap input is greater than the maximum.
	 */
	public static String minGapGreaterThanMaxError = "Maximum gap must be at least as large a number as minimum gap";

	/**
	 * The error string for when no minimum nor maximum number of occurrences is
	 * input.
	 */
	public static String noBoundError = "At least a minimum or a maximum number of occurrences must be specified";

	/**
	 * The error string for when the minimum number of occurrences input is greater
	 * than the maximum.
	 */
	public static String minNbOccurrencesGreaterThanMaxError = "Maximum number of occurrences must be at least as large a number as minimum number";

	/** The input field for the prefix of the pattern. */
	private final InputField<String, List<C>> prefixField;

	/** The input field for the minimum gap of the pattern. */
	private final InputField<String, Integer> minGapField;

	/** The input field for the maximum gap of the pattern. */
	private final InputField<String, Integer> maxGapField;

	/** The input field for the suffix of the pattern. */
	private final InputField<String, List<C>> suffixField;

	/** The input field for the minimum number of repetitions. */
	private final InputField<String, Integer> minNbOccurrencesField;

	/** The input field for the maximum number of repetitions. */
	private final InputField<String, Integer> maxNbOccurrencesField;

	/**
	 * Builds a new instance.
	 * 
	 * @param reader A reader for sequences
	 */
	public RepeatedPatternWithGapConstraintBuilder(Function<String, ? extends Sequence<C>> reader) {
		super(help);
		this.prefixField = new SequenceField<>(false, prefixFieldLabel, reader, prefixFieldHelp);
		this.minGapField = makeNonnegativeIntegerField(minGapFieldLabel, minGapFieldHelp, nonnegativeIntegerFieldError);
		this.maxGapField = makeNonnegativeIntegerField(maxGapFieldLabel, maxGapFieldHelp, nonnegativeIntegerFieldError);
		this.suffixField = new SequenceField<>(false, suffixFieldLabel, reader, suffixFieldHelp);
		this.minNbOccurrencesField = makeNonnegativeIntegerField(minNbOccurrenceFieldLabel, minNbOccurrenceFieldHelp,
				nonnegativeIntegerFieldError);
		this.maxNbOccurrencesField = makeNonnegativeIntegerField(maxNbOccurrenceFieldLabel, maxNbOccurrenceFieldHelp,
				nonnegativeIntegerFieldError);
		super.addField(this.prefixField);
		super.addField(this.minGapField);
		super.addField(this.maxGapField);
		super.addField(this.suffixField);
		super.addField(this.minNbOccurrencesField);
		super.addField(this.maxNbOccurrencesField);
	}

	/**
	 * Builds a new instance, initialized with the parameters of a given constraint.
	 * 
	 * @param constraint A constraint
	 * @param reader     A reader for sequences
	 */
	public RepeatedPatternWithGapConstraintBuilder(RepeatedPatternWithGapConstraint<C> constraint,
			Function<String, ? extends Sequence<C>> reader) {
		this(reader);
		this.prefixField.setValue(constraint.getPrefix());
		if (constraint.hasMinGap()) {
			this.minGapField.setValue(constraint.getMinGap());
		}
		if (constraint.hasMaxGap()) {
			this.maxGapField.setValue(constraint.getMaxGap());
		}
		this.suffixField.setValue(constraint.getSuffix());
		if (constraint.hasMinNbOccurrences()) {
			this.minNbOccurrencesField.setValue(constraint.getMinNbOccurrences());
		}
		if (constraint.hasMaxNbOccurrences()) {
			this.maxNbOccurrencesField.setValue(constraint.getMaxNbOccurrences());
		}
	}

	@Override
	public boolean isGloballyValid() {
		Integer minGap = this.minGapField.getValue();
		Integer maxGap = this.maxGapField.getValue();
		if (minGap != null && maxGap != null && maxGap < minGap) {
			return false;
		}
		Integer minNbOccurrences = this.minNbOccurrencesField.getValue();
		Integer maxNbOccurrences = this.maxNbOccurrencesField.getValue();
		if (minNbOccurrences == null && maxNbOccurrences == null) {
			return false;
		}
		if (minNbOccurrences != null && maxNbOccurrences != null && maxNbOccurrences < minNbOccurrences) {
			return false;
		}
		return true;
	}

	@Override
	public List<String> getGlobalErrors() {
		List<String> res = new ArrayList<>();
		Integer minGap = this.minGapField.getValue();
		Integer maxGap = this.maxGapField.getValue();
		if (minGap != null && maxGap != null && maxGap < minGap) {
			res.add(minGapGreaterThanMaxError);
		}
		Integer minNbOccurrences = this.minNbOccurrencesField.getValue();
		Integer maxNbOccurrences = this.maxNbOccurrencesField.getValue();
		if (minNbOccurrences == null && maxNbOccurrences == null) {
			res.add(noBoundError);
		}
		if (minNbOccurrences != null && maxNbOccurrences != null && maxNbOccurrences < minNbOccurrences) {
			res.add(minNbOccurrencesGreaterThanMaxError);
		}
		return res;
	}

	@Override
	protected RepeatedPatternWithGapConstraint<C> safeGetObject() {
		List<C> prefix = this.prefixField.getValue();
		Integer minGap = this.minGapField.getValue();
		Integer maxGap = this.maxGapField.getValue();
		List<C> suffix = this.suffixField.getValue();
		Integer minNbOccurrences = this.minNbOccurrencesField.getValue();
		Integer maxNbOccurrences = this.maxNbOccurrencesField.getValue();
		return new RepeatedPatternWithGapConstraint<>(prefix, minGap, maxGap, suffix, minNbOccurrences,
				maxNbOccurrences);
	}

	/**
	 * Helper method: builds one of the input fields for the (minimum or maximum)
	 * gap or number of occurrences.
	 * 
	 * @param label       The label for the field
	 * @param helpString  The help string for the field
	 * @param errorString The error string when a negative value is input
	 * @return An input field for the minimum or maximum gap or number of
	 *         occurrences
	 */
	private static InputField<String, Integer> makeNonnegativeIntegerField(String label, String helpString,
			String errorString) {
		InputField<String, Integer> field = NumberFieldsFromString.makeIntegerField(true, label, helpString);
		return new SimpleRestrictionInputDecorator<String, Integer>(field, errorString) {
			@Override
			protected boolean isValid(Integer value) {
				return value >= 0;
			}
		};
	}

}
