package fr.unicaen.aasuite.userinput;

import java.util.Arrays;
import java.util.List;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.compound.DNACompoundSet;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.unicaen.aasuite.operations.Transform;
import fr.unicaen.aasuite.operations.TransformType;
import fr.unicaen.aasuite.transforms.Cleaning;
import fr.unicaen.aasuite.transforms.Cleavage;
import fr.unicaen.aasuite.transforms.MultipleTransform;
import fr.unicaen.aasuite.transforms.Precursors;
import fr.unicaen.aasuite.transforms.SignalPeptideRemoval;

/**
 * A factory for transform builders.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: enum instead of list for all transform types
 */
public class TransformBuilders {

	/** The list of all transform types for DNA sequences. */
	public static final List<TransformType> NUCLEOTIDE_TRANSFORM_TYPES = Arrays.asList(TransformType.CLEANING,
			TransformType.CLEAVAGE);

	/** The list of all transform types for protein sequences. */
	public static final List<TransformType> AMINO_ACID_TRANSFORM_TYPES = Arrays.asList(TransformType.CLEANING,
			TransformType.MULTIPLE_BASIC_CLEAVAGE, TransformType.CLEAVAGE, TransformType.PRECURSORS,
			TransformType.SIGNAL_PEPTIDE_REMOVAL);

	/**
	 * Returns a new builder for a given transform type for DNA sequences.
	 * 
	 * @param type A transform type
	 * @return A conversion builder for the given type
	 * @throws IllegalArgumentException If the given type is not one for DNA
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Transform<NucleotideCompound>> getDNABuilder(
			TransformType type) throws IllegalArgumentException {
		switch (type) {
		case CLEANING:
			return new CleaningTransformBuilder<NucleotideCompound>(DNACompoundSet.getDNACompoundSet());
		case CLEAVAGE:
			return new CleavageTransformBuilder<>(
					new SequenceField<NucleotideCompound>(false, null, SequenceField::readDna, null));
		default:
			throw new IllegalArgumentException("Unknown transform type for DNA sequences: " + type);
		}
	}

	/**
	 * Returns a builder initialized to build a given transform for DNA sequences.
	 * 
	 * @param transform A transform on DNA sequences
	 * @return A transform builder initialized to build the given transform
	 * @throws IllegalArgumentException If the given type is not one for DNA
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Transform<NucleotideCompound>> getDNABuilder(
			Transform<NucleotideCompound> transform) throws IllegalArgumentException {
		switch (transform.getType()) {
		case CLEANING:
			return new CleaningTransformBuilder<>((Cleaning<NucleotideCompound>) transform,
					DNACompoundSet.getDNACompoundSet());
		case CLEAVAGE:
			return new CleavageTransformBuilder<>((Cleavage<NucleotideCompound>) transform,
					new SequenceField<NucleotideCompound>(false, null, SequenceField::readDna, null));
		default:
			throw new IllegalArgumentException("Unknown transform type for DNA sequences: " + transform.getType());
		}
	}

	/**
	 * Returns a new builder for a given transform type for protein sequences.
	 * 
	 * @param type A transform type
	 * @return A transform builder for the given type
	 * @throws IllegalArgumentException If the given type is not one for protein
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Transform<AminoAcidCompound>> getProteinBuilder(
			TransformType type) throws IllegalArgumentException {
		switch (type) {
		case CLEANING:
			return new CleaningTransformBuilder<AminoAcidCompound>(AminoAcidCompoundSet.getAminoAcidCompoundSet());
		case CLEAVAGE:
			return new CleavageTransformBuilder<>(
					new SequenceField<AminoAcidCompound>(false, null, SequenceField::readProtein, null));
		case MULTIPLE_BASIC_CLEAVAGE:
			return new BasicCleavageTransformBuilder();
		case PRECURSORS:
			return new PrecursorsTransformBuilder();
		case SIGNAL_PEPTIDE_REMOVAL:
			return new SignalPeptideRemovalTransformBuilder();
		default:
			throw new IllegalArgumentException("Unknown transform type for protein sequences: " + type);
		}
	}

	/**
	 * Returns a builder initialized to build a given transform for protein
	 * sequences.
	 * 
	 * @param transform A transform on protein sequences
	 * @return A transform builder initialized to build the given transform
	 * @throws IllegalArgumentException If the given type is not one for protein
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Transform<AminoAcidCompound>> getProteinBuilder(
			Transform<AminoAcidCompound> transform) throws IllegalArgumentException {
		switch (transform.getType()) {
		case CLEANING:
			return new CleaningTransformBuilder<AminoAcidCompound>((Cleaning<AminoAcidCompound>) transform,
					AminoAcidCompoundSet.getAminoAcidCompoundSet());
		case CLEAVAGE:
			return new CleavageTransformBuilder<>((Cleavage<AminoAcidCompound>) transform,
					new SequenceField<AminoAcidCompound>(false, null, SequenceField::readProtein, null));
		case MULTIPLE_BASIC_CLEAVAGE:
			return new BasicCleavageTransformBuilder((MultipleTransform<AminoAcidCompound>) transform);
		case PRECURSORS:
			return new PrecursorsTransformBuilder((Precursors) transform);
		case SIGNAL_PEPTIDE_REMOVAL:
			return new SignalPeptideRemovalTransformBuilder((SignalPeptideRemoval) transform);
		default:
			throw new IllegalArgumentException("Unknown transform type for protein sequences: " + transform.getType());
		}
	}

}
