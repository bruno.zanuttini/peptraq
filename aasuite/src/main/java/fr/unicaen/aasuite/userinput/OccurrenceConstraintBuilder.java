package fr.unicaen.aasuite.userinput;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.unicaen.aasuite.constraints.OccurrenceConstraint;

/**
 * A builder for occurrence constraints.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds to which the constraints apply
 */
public class OccurrenceConstraintBuilder<C extends Compound>
		extends AbstractBuilder<InputField<String, ?>, OccurrenceConstraint<C>> {

	/** The separator to use when entering several compounds. */
	public static String separator = ",";

	/**
	 * The label for the compound field. This label may use "1$" for referring to
	 * the separator (String) to be used for specifying several compounds.
	 */
	public static String compoundFieldLabel = "Compounds";

	/**
	 * The help string for the compound field. This string may use "1$" for
	 * referring to the separator (String) to be used for specifying several
	 * compounds.
	 */
	public static String compoundFieldHelp = "Enter one or several compounds (separated by '%1$s'); the numbers of occurrences of these compounds will be added to each other";

	/** The label for the minimum (number of occurrences) field. */
	public static String minNbOccurrenceFieldLabel = "Minimum number of occurrences in the sequence";

	/** The help string for the minimum field. */
	public static String minNbOccurrenceFieldHelp = "Enter a number of occurrences, or leave blank for no minimum";

	/** The label for the maximum (number of occurrences) field. */
	public static String maxNbOccurrenceFieldLabel = "Maximum number of occurrences in the sequence";

	/** The help string for the maximum field. */
	public static String maxNbOccurrenceFieldHelp = "Enter a number of occurrences, or leave blank for no minimum";

	/**
	 * The error displayed if one of the number of occurrences field holds a
	 * negative value.
	 */
	public static String nbOccurrenceFieldError = "Value must be nonnegative";

	/** The help string for this class. */
	public static String help = "Retain only sequences in which the cumulated number of occurrences of some compounds match given bounds";

	/**
	 * The error displayed if neither a minimum nor a maximum number of occurrences
	 * is given.
	 */
	public static String noBoundError = "At least one bound (minimum or maximum number of occurrences) must be specified";

	/**
	 * The error displayed if the given minimum is greater than the given maximum
	 * number of occurrences.
	 */
	public static String minGreaterThanMaxError = "Maximum must be at least as large as minimum";

	/** A field for the compounds whose number of occurrences to test. */
	private final MultipleCompoundInputFieldFromString<C> compoundField;

	/** A field for the minimum required occurrences. */
	private final InputField<String, Integer> minField;

	/** A field for the maximum required number of occurrences. */
	private final InputField<String, Integer> maxField;

	/**
	 * Builds a new instance.
	 * 
	 * @param compoundSet The set of compounds which can be chosen
	 */
	public OccurrenceConstraintBuilder(CompoundSet<C> compoundSet) {
		super(help);
		String label = String.format(compoundFieldLabel, separator);
		String help = String.format(compoundFieldHelp, separator);
		this.compoundField = new MultipleCompoundInputFieldFromString<>(false, label, compoundSet, separator, help);
		this.minField = this.makeNbOccurrenceField(true);
		this.maxField = this.makeNbOccurrenceField(false);
		super.addField(compoundField);
		super.addField(minField);
		super.addField(maxField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given constraint.
	 * 
	 * @param constraint  An occurrence constraint
	 * @param compoundSet The set of compounds which can be chosen
	 */
	public OccurrenceConstraintBuilder(OccurrenceConstraint<C> constraint, CompoundSet<C> compoundSet) {
		this(compoundSet);
		this.compoundField.setValue(new ArrayList<>(constraint.getCompounds()));
		if (constraint.hasMinNumber()) {
			this.minField.setValue(constraint.getMinNumber());
		}
		if (constraint.hasMaxNumber()) {
			this.maxField.setValue(constraint.getMaxNumber());
		}
	}

	@Override
	public boolean isGloballyValid() {
		Integer min = this.minField.getValue();
		Integer max = this.maxField.getValue();
		if (min == null && max == null) {
			return false;
		} else if (min == null || max == null) {
			return true;
		}
		if (max < min) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public List<String> getGlobalErrors() {
		Integer min = this.minField.getValue();
		Integer max = this.maxField.getValue();
		if (min == null && max == null) {
			return Collections.singletonList(noBoundError);
		} else if (min == null || max == null) {
			return Collections.emptyList();
		} else if (max < min) {
			return Collections.singletonList(minGreaterThanMaxError);
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	protected OccurrenceConstraint<C> safeGetObject() {
		return new OccurrenceConstraint<>(this.compoundField.getValue(), this.minField.getValue(),
				this.maxField.getValue());
	}

	/**
	 * Helper method: builds the field for the (minimum or maximum) number of
	 * occurrences.
	 * 
	 * @param min true for building the field for the minimum number, false for the
	 *            maximum number
	 * @return A field
	 */
	private InputField<String, Integer> makeNbOccurrenceField(boolean min) {
		String label = min ? minNbOccurrenceFieldLabel : maxNbOccurrenceFieldLabel;
		String help = min ? minNbOccurrenceFieldHelp : maxNbOccurrenceFieldHelp;
		InputField<String, Integer> field = NumberFieldsFromString.makeIntegerField(true, label, help);
		return new SimpleRestrictionInputDecorator<String, Integer>(field, nbOccurrenceFieldError) {
			@Override
			protected boolean isValid(Integer value) {
				return value >= 0;
			}
		};
	}

}
