package fr.unicaen.aasuite.userinput;

import java.util.Collections;
import java.util.List;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.NumberFieldsFromString;
import fr.greyc.mad.userinput.SimpleRestrictionInputDecorator;
import fr.unicaen.aasuite.transforms.Precursors;

/**
 * A builder for precursors transforms.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 */
public class PrecursorsTransformBuilder extends AbstractBuilder<InputField<String, ?>, Precursors> {

	/** The label for the minimum size field. */
	public static String minSizeFieldLabel = "Minimum size";

	/** The help string for the minimum size field. */
	public static String minSizeFieldHelp = "Enter the minimum size for retaining a precursor (leave blank for no minimum)";

	/** The label for the maximum size field. */
	public static String maxSizeFieldLabel = "Maximum size";

	/** The help string for the maximum size field. */
	public static String maxSizeFieldHelp = "Enter the maximum size for retaining a precursor (leave blank for no maximum)";;

	/**
	 * The error string for indicating that a size has been input which is negative.
	 */
	public static String sizeFieldError = "Size cannot be negative";

	/** The help string for this class. */
	public static String help = "Compute precursors in each sequence";

	/**
	 * The error displayed if the given minimum is greater than the given maximum
	 * size.
	 */
	public static String minGreaterThanMaxError = "Maximum must be at least as large as minimum";

	/** The field for the minimum size of precursors to retain. */
	private final InputField<String, Integer> minSizeField;

	/** The field for the maximum size of precursors to retain. */
	private final InputField<String, Integer> maxSizeField;

	/**
	 * Builds a new instance.
	 */
	public PrecursorsTransformBuilder() {
		super(help);
		this.minSizeField = makeSizeField(minSizeFieldLabel, minSizeFieldHelp, sizeFieldError);
		this.maxSizeField = makeSizeField(maxSizeFieldLabel, maxSizeFieldHelp, sizeFieldError);
		super.addField(this.minSizeField);
		super.addField(this.maxSizeField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given transform.
	 * 
	 * @param transform A precursors transform
	 */
	public PrecursorsTransformBuilder(Precursors transform) {
		this();
		if (transform.hasMinSize()) {
			this.minSizeField.setValue(transform.getMinSize());
		}
		if (transform.hasMaxSize()) {
			this.maxSizeField.setValue(transform.getMaxSize());
		}
	}

	@Override
	public boolean isGloballyValid() {
		Integer min = this.minSizeField.getValue();
		Integer max = this.maxSizeField.getValue();
		if (min == null || max == null) {
			return true;
		} else if (max < min) {
			return false;
		} else {
			return true;
		}
	}

	@Override
	public List<String> getGlobalErrors() {
		Integer min = this.minSizeField.getValue();
		Integer max = this.maxSizeField.getValue();
		if (min == null || max == null) {
			return Collections.emptyList();
		} else if (max < min) {
			return Collections.singletonList(minGreaterThanMaxError);
		} else {
			return Collections.emptyList();
		}
	}

	@Override
	protected Precursors safeGetObject() {
		return new Precursors(this.minSizeField.getValue(), this.maxSizeField.getValue());
	}

	/**
	 * Helper method: builds a input field for (min or max) size.
	 * 
	 * @param label       The label for the field
	 * @param helpString  The help string for the field
	 * @param errorString The error string for negative input
	 * @return An input field for size
	 */
	private static InputField<String, Integer> makeSizeField(String label, String helpString, String errorString) {
		InputField<String, Integer> field = NumberFieldsFromString.makeIntegerField(true, label, helpString);
		return new SimpleRestrictionInputDecorator<String, Integer>(field, errorString) {
			@Override
			protected boolean isValid(Integer value) {
				return value >= 0;
			}
		};
	}

}
