package fr.unicaen.aasuite.userinput;

import java.util.Collections;
import java.util.List;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.FreeTextField;
import fr.greyc.mad.userinput.InputField;
import fr.unicaen.aasuite.constraints.AnnotationConstraint;

/**
 * A builder for annotation constraints.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences
 */
// FIXME: replace C with Compound
public class AnnotationConstraintBuilder<C extends Compound>
		extends AbstractBuilder<InputField<String, ?>, AnnotationConstraint<C>> {

	/**
	 * The label for the pattern field. This label may use "1$" for referring to the
	 * wildcard (char) for specifying an arbitrary character.
	 */
	public static String patternFieldLabel = "Pattern or keyword";

	/**
	 * The help string for the pattern field. This label may use "1$" for referring
	 * to the wildcard (char) for specifying an arbitrary character.
	 */
	public static String patternFieldHelp = "Enter any pattern; '%1$s' can be used for matching any character";

	/** The help string for this class. */
	public static String help = "Retain only sequences which match the given pattern";

	/** The field for the pattern to search for. */
	private final FreeTextField patternField;

	/**
	 * Builds a new instance.
	 */
	public AnnotationConstraintBuilder() {
		super(help);
		String label = String.format(patternFieldLabel, '.');
		String help = String.format(patternFieldHelp, '.');
		this.patternField = new FreeTextField(false, label, help);
		super.addField(this.patternField);
	}

	/**
	 * Builds a new instance, with parameters initialized as in a given constraint.
	 * 
	 * @param constraint An annotation constraint
	 */
	public AnnotationConstraintBuilder(AnnotationConstraint<C> constraint) {
		this();
		this.patternField.setValue(constraint.getRegexp());
	}

	@Override
	public boolean isGloballyValid() {
		return true;
	}

	@Override
	public List<String> getGlobalErrors() {
		return Collections.emptyList();
	}

	@Override
	protected AnnotationConstraint<C> safeGetObject() {
		return new AnnotationConstraint<C>(this.patternField.getValue());
	}

}
