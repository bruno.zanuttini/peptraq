package fr.unicaen.aasuite.userinput;

import java.util.Arrays;
import java.util.List;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.unicaen.aasuite.conversions.DNAToProteinTranslation;
import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.operations.ConversionType;

/**
 * A factory for conversion builders.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: enum instead of list for all conversion types
 */
public class ConversionBuilders {

	/** The list of all conversion types for DNA sequences. */
	public static final List<ConversionType> NUCLEOTIDE_CONVERSION_TYPES = Arrays
			.asList(ConversionType.DNA_TO_PROTEIN_TRANSLATION);

	/**
	 * Returns a new builder for a given conversion type for DNA sequences.
	 * 
	 * @param type A conversion type
	 * @return A conversion builder for the given type
	 * @throws IllegalArgumentException If the given type is not one for DNA
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Conversion<NucleotideCompound, AminoAcidCompound>> getDNABuilder(
			ConversionType type) throws IllegalArgumentException {
		switch (type) {
		case DNA_TO_PROTEIN_TRANSLATION:
			return new DNATranslationConversionBuilder();
		default:
			throw new IllegalArgumentException("Unknown conversion type for DNA sequences: " + type);
		}
	}

	/**
	 * Returns a builder initialized to build a given conversion for DNA sequences.
	 * 
	 * @param conversion A conversion on DNA sequences
	 * @return A conversion builder initialized to build the given conversion
	 * @throws IllegalArgumentException If the given type is not one for DNA
	 *                                  sequences
	 */
	public static AbstractBuilder<InputField<String, ?>, ? extends Conversion<NucleotideCompound, AminoAcidCompound>> getDNABuilder(
			Conversion<NucleotideCompound, AminoAcidCompound> conversion) throws IllegalArgumentException {
		switch (conversion.getType()) {
		case DNA_TO_PROTEIN_TRANSLATION:
			return new DNATranslationConversionBuilder((DNAToProteinTranslation) conversion);
		default:
			throw new IllegalArgumentException("Unknown conversion type for DNA sequences: " + conversion.getType());
		}
	}

}
