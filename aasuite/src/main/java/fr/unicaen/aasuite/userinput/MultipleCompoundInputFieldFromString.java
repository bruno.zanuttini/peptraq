package fr.unicaen.aasuite.userinput;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;

import fr.greyc.mad.userinput.InputField;
import fr.greyc.mad.userinput.MultipleInputFieldFromStringAdapter;

/**
 * A multiple input field for compounds (from string inputs), that is, a field
 * which allows to choose a (nonempty) list of compounds by inputting a string.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds
 */
public class MultipleCompoundInputFieldFromString<C extends Compound> extends MultipleInputFieldFromStringAdapter<C> {

	/**
	 * Builds a new instance with no initial input.
	 * 
	 * @param isOptional  Whether the field is optional (true) or not (false)
	 * @param label       The label for the field
	 * @param compoundSet The compound set defining the valid compounds
	 * @param separator   The separator to use between atomic inputs
	 * @param helpString  The help string for the field
	 */
	public MultipleCompoundInputFieldFromString(boolean isOptional, String label, CompoundSet<C> compoundSet,
			String separator, String helpString) {
		super(isOptional, label, getAtomicInputField(compoundSet), separator, helpString);
	}

	/**
	 * Builds a new instance with no initial input.
	 * 
	 * @param label       The label for the field
	 * @param minNbValues The minimum number of compounds for an input to be valid
	 *                    (inclusive)
	 * @param maxNbValues The maximum number of compounds for an input to be valid
	 *                    (inclusive)
	 * @param compoundSet The compound set defining the valid compounds
	 * @param separator   The separator to use between atomic inputs
	 * @param helpString  The help string for the field
	 */
	public MultipleCompoundInputFieldFromString(String label, Integer minNbValues, Integer maxNbValues,
			CompoundSet<C> compoundSet, String separator, String helpString) {
		super(label, getAtomicInputField(compoundSet), minNbValues, maxNbValues, separator, helpString);
	}

	/**
	 * Builds a new instance with an initial input string.
	 * 
	 * @param isOptional   Whether the field is optional (true) or not (false)
	 * @param label        The label for the field
	 * @param compoundSet  The compound set defining the valid compounds
	 * @param separator    The separator to use between atomic inputs
	 * @param initialInput The initial input string for the field
	 * @param helpString   The help string for the field
	 */
	public MultipleCompoundInputFieldFromString(boolean isOptional, String label, CompoundSet<C> compoundSet,
			String separator, String initialInput, String helpString) {
		super(isOptional, label, getAtomicInputField(compoundSet), separator, initialInput, helpString);
	}

	/**
	 * Builds a new instance with an initial input string.
	 * 
	 * @param label        The label for the field
	 * @param minNbValues  The minimum number of compounds for an input to be valid
	 *                     (inclusive)
	 * @param maxNbValues  The maximum number of compounds for an input to be valid
	 *                     (inclusive)
	 * @param compoundSet  The compound set defining the valid compounds
	 * @param separator    The separator to use between atomic inputs
	 * @param initialInput The initial input string for the field
	 * @param helpString   The help string for the field
	 */
	public MultipleCompoundInputFieldFromString(String label, Integer minNbValues, Integer maxNbValues,
			CompoundSet<C> compoundSet, String separator, String initialInput, String helpString) {
		super(label, getAtomicInputField(compoundSet), minNbValues, maxNbValues, separator, helpString);
	}

	/**
	 * Helper method: builds an input field for a single compound.
	 * 
	 * @param compoundSet The compound set defining the valid compounds
	 * @return An input field for a given compound
	 * 
	 * @param <C> The type of compounds
	 */
	private static <C extends Compound> InputField<String, C> getAtomicInputField(CompoundSet<C> compoundSet) {
		return new CompoundInputFieldFromString<>(false, null, compoundSet, null);
	}

}
