package fr.unicaen.aasuite.userinput;

import java.util.List;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.BooleanInputFieldFromString;
import fr.greyc.mad.userinput.InputField;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.NegatedConstraint;

/**
 * A decorator for constraints builders, which adds the possibility to input the
 * negation of the constraint instead of the constraint itself. Hence a builder
 * of this class allows to specify the parameters of the given constraint and in
 * addition to decide whether to build the constraint itself or its negation.
 * <p>
 * As a special case, if the decorated builder is itself an instance of this
 * class, then this instance will have two fields for defining whether the
 * negation of the constraint should be built (and whether this should be will
 * depend on whether exactly one of them is true - an odd number in general).
 * <p>
 * Importantly, changes in the decorated builder occurring after the creation of
 * the decorator will not be reflected in the decorator.
 * <p>
 * The help string and labels, errors strings of the fields are simply copied
 * from the decorated builder.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds
 */
public class NegationDecorator<C extends Compound> extends AbstractBuilder<InputField<String, ?>, Constraint<C>> {

	/**
	 * The label for the "reverse constraint" field. The string may use 1$ and 2$ to
	 * refer to the input strings for "true" and "false", respectively.
	 */
	public String reverseFieldLabel = "Reverse constraint";

	/**
	 * The help string for the "reverse constraint" field. The string may use 1$ and
	 * 2$ to refer to the input strings for "true" and "false", respectively.
	 */
	public String reverseFieldHelp = "Choose '%1$s' for reversing the constraint (retain only the sequences which do not satisfy it), and '%2$s' for using the normal constraint";

	/** The input string for the "reverse" option. */
	public String reverseFieldTrueString = "reverse";

	/** The input string for the "do not reverse" option. */
	public String reverseFieldFalseString = "normal";

	/** The decorated builder. */
	protected final AbstractBuilder<InputField<String, ?>, ? extends Constraint<C>> builder;

	/** The field for whether the negation of the constraint should be built. */
	private final BooleanInputFieldFromString reverseInputField;

	/**
	 * Builds a new instance.
	 * 
	 * @param builder      The constraint builder to decorate
	 * @param initialInput Whether to set the "reverse" field to {@code true} or
	 *                     {@code false} initially
	 */
	public NegationDecorator(AbstractBuilder<InputField<String, ?>, ? extends Constraint<C>> builder,
			boolean initialInput) {
		super(builder.getHelpString());
		String label = String.format(reverseFieldLabel, reverseFieldTrueString, reverseFieldFalseString);
		String input = initialInput ? reverseFieldTrueString : reverseFieldFalseString;
		String help = String.format(reverseFieldHelp, reverseFieldTrueString, reverseFieldFalseString);
		this.reverseInputField = new BooleanInputFieldFromString(false, label, reverseFieldTrueString,
				reverseFieldFalseString, input, help);
		this.builder = builder;
		for (InputField<String, ?> field : builder.getFields()) {
			super.addField(field);
		}
		this.addField(this.reverseInputField);
	}

	/**
	 * Returns the constraint builder decorated by this instance.
	 * 
	 * @return The constraint builder decorated by this instance
	 */
	public AbstractBuilder<InputField<String, ?>, ? extends Constraint<C>> getDecoratedBuilder() {
		return this.builder;
	}

	@Override
	public boolean isGloballyValid() {
		return this.builder.isGloballyValid();
	}

	@Override
	public List<String> getGlobalErrors() {
		return this.builder.getGlobalErrors();
	}

	@Override
	protected Constraint<C> safeGetObject() {
		Constraint<C> constraint = this.builder.getObject();
		return reverseInputField.getValue() ? new NegatedConstraint<>(constraint) : constraint;
	}

}
