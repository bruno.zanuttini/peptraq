package fr.unicaen.aasuite.features;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompoundSet;
import org.biojava.nbio.core.sequence.template.Sequence;

/**
 * A feature representing the electrical load of a protein sequence.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 *         TODO: switch to BioJava implementation
 */
public class ElectricalLoad {

	/** A human-readable description of this feature. */
	public static final String DESCRIPTION = "Electrical load";

	/** The amino acid compounds with positive electrical load. */
	public static final Set<AminoAcidCompound> POSITIVE = new HashSet<>();

	/** The amino acid compounds with negative electrical load. */
	public static final Set<AminoAcidCompound> NEGATIVE = new HashSet<>();

	static {
		AminoAcidCompoundSet set = AminoAcidCompoundSet.getAminoAcidCompoundSet();
		for (String aa : Arrays.asList("R", "H", "K")) {
			POSITIVE.add(set.getCompoundForString(aa));
		}
		for (String aa : Arrays.asList("D", "E")) {
			NEGATIVE.add(set.getCompoundForString(aa));
		}
	}

	/**
	 * Returns the electrical load of a given protein sequence.
	 * 
	 * @param sequence A protein sequence
	 * @return The electrical load of the given sequence
	 */
	public static int getLoad(Sequence<AminoAcidCompound> sequence) {
		int res = 0;
		for (AminoAcidCompound aa : sequence) {
			if (POSITIVE.contains(aa)) {
				res++;
			} else if (NEGATIVE.contains(aa)) {
				res--;
			}
		}
		return res;
	}

}
