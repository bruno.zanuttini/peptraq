package fr.unicaen.aasuite.features;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

/**
 * A feature representing the monoisotopic weight of a protein sequence (in
 * Daltons).
 * 
 * FIXME: temporary? same as net charge?
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class MonoisotopicWeight {

	/** A human-readable description of this feature. */
	public static final String DESCRIPTION = "Monoisotopic weight";

	/** The weight of one molecule of water (in Daltons). */
	public static final double WATER_WEIGHT = 18.016d;

	/**
	 * Returns the monoisotopic weight of a given protein sequence (in Daltons).
	 * 
	 * @param sequence A protein sequence
	 * @return The monoisotopic weight of the given sequence
	 */
	public static double getWeight(Sequence<AminoAcidCompound> sequence) {
		double res = 0;
		for (AminoAcidCompound aa : sequence) {
			res += getWeight(aa);
		}
		res += WATER_WEIGHT;
		return res;
	}

	/**
	 * Returns the monoisotopic weight of a given amino acid.
	 * 
	 * @param aa An amino acid
	 * @return The monoisotopic weight of the given amino acid (in Daltons)
	 */
	public static double getWeight(AminoAcidCompound aa) {
		switch (aa.getShortName().charAt(0)) {
		case '*':
			return 0.0;
		case 'A':
			return 71.03711;
		case 'R':
			return 156.10111;
		case 'N':
			return 114.04293;
		case 'D':
			return 115.02694;
		case 'C':
			return 103.00919;
		case 'E':
			return 129.04259;
		case 'Q':
			return 128.05858;
		case 'G':
			return 57.02146;
		case 'H':
			return 137.05891;
		case 'I':
			return 113.08406;
		case 'L':
			return 113.08406;
		case 'K':
			return 128.09496;
		case 'M':
			return 131.04049;
		case 'F':
			return 147.06841;
		case 'P':
			return 97.05276;
		case 'S':
			return 87.03203;
		case 'T':
			return 101.04768;
		case 'W':
			return 186.07931;
		case 'Y':
			return 163.06333;
		case 'V':
			return 99.06841;
		}
		throw new IllegalArgumentException("Unknown monoisotopic weight for " + aa);
	}

}
