package fr.unicaen.aasuite.features;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

/**
 * A feature representing the hydrophobicity of a protein sequence.
 * 
 * TODO: switch to BioJava implementation
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class Hydrophobicity {

	/** A human-readable description of this feature. */
	public static final String DESCRIPTION = "Hydrophobicity";

	/**
	 * Returns the hydrophobicity of a given protein sequence.
	 * 
	 * @param sequence A protein sequence
	 * @return The hydrophobicity of the given sequence
	 */
	public static double getHydrophobicity(Sequence<AminoAcidCompound> sequence) {
		double res = 0;
		for (AminoAcidCompound aa : sequence) {
			res += getHydrophobicity(aa);
		}
		return res;
	}

	/**
	 * Returns the hydrophobicity degree of a given amino acid.
	 * 
	 * @param aa An amino acid
	 * @return The hydrophobicity of the given amino acid
	 */
	public static double getHydrophobicity(AminoAcidCompound aa) {
		switch (aa.getShortName().charAt(0)) {
		case '*':
			return 0.0;
		case 'I':
			return 4.5;
		case 'V':
			return 4.2;
		case 'L':
			return 3.80;
		case 'F':
			return 2.8;
		case 'C':
			return 2.5;
		case 'M':
			return 1.9;
		case 'A':
			return 1.8;
		case 'G':
			return -0.40;
		case 'T':
			return -0.70;
		case 'S':
			return -0.80;
		case 'W':
			return -0.9;
		case 'Y':
			return -1.3;
		case 'P':
			return -1.6;
		case 'H':
			return -3.2;
		case 'Q':
			return -3.5;
		case 'N':
			return -3.5;
		case 'E':
			return -3.5;
		case 'D':
			return -3.5;
		case 'K':
			return -3.9;
		case 'R':
			return -4.5;
		}
		throw new IllegalArgumentException("Unknown hydrophobicity degree for " + aa);
	}

}
