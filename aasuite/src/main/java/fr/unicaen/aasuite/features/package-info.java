/**
 * Classes for features of sequences. A feature is just a function from
 * sequences to some range.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 * FIXME: clean this package: which features make sense, and are they correctly computed?
 */
package fr.unicaen.aasuite.features;