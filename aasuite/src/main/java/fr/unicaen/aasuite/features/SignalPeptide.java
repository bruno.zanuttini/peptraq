package fr.unicaen.aasuite.features;

import java.io.ObjectInputStream;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

import jspp.SearchMatrix;
import jspp.SignalPeptidePredictor;

/**
 * A utility class for retrieving information about signal peptides in protein
 * sequences.
 * 
 * The prediction relies on the JSPP package, and on resources for the search
 * matrices used by it. For a description of JSPP, see: PrediSi: prediction of
 * signal peptides and their cleavage positions. Hiller K, Grote A, Scheer M,
 * Münch R, Jahn D.. Nucleic Acids Res. (2004) 32.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SignalPeptide {

	// FIXME: specify resources as static search matrices (nonfinal)

	/** The file name of the resource containing the search matrix for eukaryotes. */
	public static String EUKARYA_FILE = "matrices/eukarya.smx";

	/** The file name of the resource containing the search matrix for Gram-negative prokaryotes. */
	public static String GRAMN_FILE = "matrices/gramn.smx";

	/** The file name of the resource containing the search matrix for Gram-positive prokaryotes. */
	public static String GRAMP_FILE = "matrices/gramp.smx";

	/** An enumeration of possible types of organisms. */
	public enum OrganismType {

		/** The type for eukaryotes. */
		EUKARYA("Eukaryote", EUKARYA_FILE),

		/** The type for Gram-negative prokaryotes. */
		GRAMN("Gram-negative", GRAMN_FILE),

		/** The type for Gram-positive prokaryotes. */
		GRAMP("Gram-positive", GRAMP_FILE);

		/** The description of this type. */
		protected String description;

		/** The predictor for this type. */
		protected final SignalPeptidePredictor predictor;

		/**
		 * Builds a new instance.
		 * 
		 * @param description The description for this instance
		 * @param filename    The name of the file holding the search matrix for this
		 *                    instance
		 */
		private OrganismType(String description, String filename) {
			this.description = description;
			try {
				ObjectInputStream in = new ObjectInputStream(
						SignalPeptide.class.getClassLoader().getResourceAsStream(filename));
				SearchMatrix smp = (SearchMatrix) in.readObject();
				in.close();
				this.predictor = new SignalPeptidePredictor(smp);
			} catch (Exception ex) {
				throw new RuntimeException("Could not read model of signal peptide prediction for " + description, ex);
			}
		}

		/**
		 * Returns the description of this instance.
		 * 
		 * @return The description of this instance
		 */
		public String getDescription() {
			return this.description;
		}

		/**
		 * Returns the predictor for this instance.
		 * 
		 * @return The predictor for this instance
		 */
		public SignalPeptidePredictor getPredictor() {
			return this.predictor;
		}

		@Override
		public String toString() {
			return this.description;
		}

	}

	/**
	 * Returns signal peptide information about a given sequence.
	 * 
	 * @param sequence A sequence
	 * @param type     The type of organisms
	 * @return Signal peptide information about the given sequence
	 * @throws RuntimeException if the sequence is not a precursor (so that it does
	 *                          not make sense to ask the information)
	 */
	public static Info getInformation(Sequence<AminoAcidCompound> sequence, OrganismType type) throws RuntimeException {
		synchronized (type.getPredictor()) {
			try {
				int position = type.getPredictor().predictEnhancedPosition(sequence.getSequenceAsString());
				return new Info(position - 1, type.getPredictor().getScore());
			} catch (Exception e) {
				throw new RuntimeException(
						"Cannot predict signal peptide position; try to restrict to precursors first");
			}
		}
	}

	/**
	 * A class for storing signal peptide information.
	 */
	public static class Info {

		/**
		 * The cleavage position of the predicted signal peptide, if any (0-indexed).
		 */
		public final int position;

		/** The score of the prediction (in [0, 1], the higher the more likely). */
		public final double score;

		/**
		 * Builds a new instance.
		 * 
		 * @param position The position of the predicted signal peptide, if any
		 *                 (0-indexed)
		 * @param score    The score of the prediction (in [0, 1], the higher the more
		 *                 likely).
		 */
		public Info(int position, double score) {
			this.position = position;
			this.score = score;
		}

	}

}
