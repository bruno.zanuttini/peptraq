package fr.unicaen.aasuite.operations;

/**
 * An enumeration of all constraint types (common to DNA and protein sequences).
 * Note that this is different from classes of transforms, since, for instance,
 * a single transform class may be used to represent several transform types,
 * depending on some parameters.
 * <p>
 * Each constraint type has a default description, which can be set differently
 * for handling internationalization, for instance.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public enum TransformType {

	/** The type for cleaning transforms. */
	CLEANING("Clean sequences"),

	/** The type for cleavage transforms, along user-specified sites. */
	CLEAVAGE("Cleave along custom sites"),

	/** The type for (multiarity) basic cleavage transforms. */
	MULTIPLE_BASIC_CLEAVAGE("Cleave along basic sites"),

	/** The type for transforms which compute precursors. */
	PRECURSORS("Compute precursors"),

	/** The type for transforms which remove signal peptides. */
	SIGNAL_PEPTIDE_REMOVAL("Remove signal peptide"),

	/** The type for (monoarity) basic cleavage transforms. */
	BASIC_CLEAVAGE("Cleave along n-basic sites");

	/** The description of this type. */
	public String description;

	/**
	 * Builds a new instance.
	 * 
	 * @param description The description of this type
	 */
	private TransformType(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return this.description;
	}

}