package fr.unicaen.aasuite.operations;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

import fr.greyc.mad.userinput.AbstractBuilder;
import fr.greyc.mad.userinput.InputField;
import fr.unicaen.aasuite.operations.OperationAdapters.ConstraintOperation;
import fr.unicaen.aasuite.operations.OperationAdapters.TransformOperation;
import fr.unicaen.aasuite.userinput.ConstraintBuilders;
import fr.unicaen.aasuite.userinput.ConversionBuilders;
import fr.unicaen.aasuite.userinput.TransformBuilders;

/**
 * A utility class for dumping and loading macros.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class MacroIO {

	// Implementation note: the following strings are hardcoded constants for dumps
	// to be consistent over time

	/**
	 * The string indicating that the next operation to read is one on DNA
	 * sequences.
	 */
	private static final String DNA = "- DNA operation -";

	/**
	 * The string indicating that the next operation to read is one on protein
	 * sequences.
	 */
	private static final String PROTEIN = "- Protein sequence operation -";

	/** The string indicating that a field has no input. */
	private static final String NO_INPUT_STRING = "";

	/**
	 * Dumps a given macro.
	 * 
	 * @param macro   A macro
	 * @param version The version of the current version of AASuite
	 * @return A list of (one-line) strings from which the macro can be
	 *         reconstructed (using {@link #load(List, String)})
	 */
	public static List<String> dump(Macro macro, String version) {
		List<String> res = new ArrayList<>();
		res.add(version);
		for (Operation<NucleotideCompound> operation : macro.getDNAOperations()) {
			if (operation instanceof ConstraintOperation<?>) {
				Constraint<NucleotideCompound> constraint = ((ConstraintOperation<NucleotideCompound>) operation)
						.getConstraint();
				res.addAll(dump(true, constraint.getType().name(), ConstraintBuilders.getDNABuilder(constraint)));
			} else if (operation instanceof TransformOperation<?>) {
				Transform<NucleotideCompound> transform = ((TransformOperation<NucleotideCompound>) operation)
						.getTransform();
				res.addAll(dump(true, transform.getType().name(), TransformBuilders.getDNABuilder(transform)));
			} else {
				throw new RuntimeException("Operation is neither a constraint nor a transform: " + operation);
			}
		}
		if (macro.hasDNAToProteinConversion()) {
			Conversion<NucleotideCompound, AminoAcidCompound> conversion = macro.getDNAToProteinConversion();
			res.addAll(dump(true, conversion.getType().name(), ConversionBuilders.getDNABuilder(conversion)));
		}
		for (Operation<AminoAcidCompound> operation : macro.getProteinOperations()) {
			if (operation instanceof ConstraintOperation<?>) {
				Constraint<AminoAcidCompound> constraint = ((ConstraintOperation<AminoAcidCompound>) operation)
						.getConstraint();
				res.addAll(dump(false, constraint.getType().name(), ConstraintBuilders.getProteinBuilder(constraint)));
			} else if (operation instanceof TransformOperation<?>) {
				Transform<AminoAcidCompound> transform = ((TransformOperation<AminoAcidCompound>) operation)
						.getTransform();
				res.addAll(dump(false, transform.getType().name(), TransformBuilders.getProteinBuilder(transform)));
			} else {
				throw new RuntimeException("Operation is neither a constraint nor a transform: " + operation);
			}
		}
		return res;
	}

	/**
	 * Loads a macro from a list of strings.
	 * 
	 * @param lines   A list of strings as produced by {@link #dump(Macro, String)}
	 * @param version The version of the current version of AASuite
	 * @return The macro represented by the list of strings
	 * @throws IOException if a macro cannot be loaded from the given strings
	 */
	public static Macro load(List<String> lines, String version) throws IOException {
		Macro res = new Macro();
		if (lines.isEmpty()) {
			throw new IOException("Cannot load a macro from empty set of lines");
		} else {
			String fileVersion = lines.get(0);
			int i = 1;
			while (i < lines.size()) {
				try {
					i = load(lines, i, res);
				} catch (IOException e) {
					String message = e.getMessage() + " - Maybe macro was saved with too old a version of PepTraq ("
							+ fileVersion + ")";
					throw new IOException(message, e);
				}
			}
		}
		return res;
	}

	/**
	 * Helper method: dumps an operation given by a builder.
	 * 
	 * @param dna     true if the operation is on DNA sequences, false if it is on
	 *                protein sequences
	 * @param type    An unambiguous string for the type of operation
	 * @param builder A builder ready to build the desired operation
	 * @return A list of (one-line) strings from which the operation can be
	 *         reconstructed (using {@link #load(List, int, Macro)}
	 */
	private static List<String> dump(boolean dna, String type, AbstractBuilder<InputField<String, ?>, ?> builder) {
		List<String> res = new ArrayList<>();
		res.add(dna ? DNA : PROTEIN);
		res.add(type);
		for (InputField<String, ?> field : builder.getFields()) {
			res.add(field.hasInput() ? field.getInput() : NO_INPUT_STRING);
		}
		return res;
	}

	/**
	 * Helper method: loads one operation from a list of strings into a given macro.
	 * This method reads only what is necessary for reconstructing one operation,
	 * starting from a given index.
	 * 
	 * @param lines A list of strings as produced by {@link #dump(Macro, String)}
	 * @param i     The index of the first line to read (0-indexed, inclusive)
	 * @param macro The macro to which the operation read should be added
	 * @return The first index which has not been read (0-indexed, inclusive); so
	 *         this index should be the one given (via parameter {@code i}) to a
	 *         subsequent call to this method
	 * @throws IOException if an operation cannot be loaded from the given strings
	 *                     starting at the given index
	 */
	private static int load(List<String> lines, int i, Macro macro) throws IOException {

		// Note: we know by construction that there is at least one line
		assert (lines.size() > i);

		// Type (dna or proteins)
		boolean isDna;
		String type = lines.get(i);
		if (DNA.equals(type)) {
			isDna = true;
		} else if (PROTEIN.equals(type)) {
			isDna = false;
		} else {
			throw new IOException("Error on Line " + i + ", expected \"" + DNA + "\" or \"" + PROTEIN + "\"");
		}

		// Type (constraint, transform, or conversion)
		if (i + 1 >= lines.size()) {
			throw new IOException("End of input reached while an operation type was expected");
		}
		String operationType = lines.get(i + 1);
		boolean isConstraint = false;
		try {
			ConstraintType.valueOf(operationType);
			isConstraint = true;
		} catch (Exception e) {
		}
		boolean isTransform = false;
		try {
			TransformType.valueOf(operationType);
			isTransform = true;
		} catch (Exception e) {
		}
		boolean isConversion = false;
		try {
			ConversionType.valueOf(operationType);
			isConversion = true;
		} catch (Exception e) {
		}
		if (!isConstraint && !isTransform && !isConversion) {
			throw new IOException("Unknown operation type on Line " + (i + 1) + ": " + operationType);
		}

		// Builder
		int index = i + 2;
		if (isDna) {
			if (isConstraint) {
				AbstractBuilder<InputField<String, ?>, ? extends Constraint<NucleotideCompound>> builder = ConstraintBuilders
						.getDNABuilder(ConstraintType.valueOf(operationType), false);
				index = fill(builder, lines, index);
				macro.addDNAOperation(OperationAdapters.forConstraint(builder.getObject()));
			} else if (isTransform) {
				AbstractBuilder<InputField<String, ?>, ? extends Transform<NucleotideCompound>> builder = TransformBuilders
						.getDNABuilder(TransformType.valueOf(operationType));
				index = fill(builder, lines, index);
				macro.addDNAOperation(OperationAdapters.forTransform(builder.getObject()));
			} else {
				assert isConversion;
				AbstractBuilder<InputField<String, ?>, ? extends Conversion<NucleotideCompound, AminoAcidCompound>> builder = ConversionBuilders
						.getDNABuilder(ConversionType.valueOf(operationType));
				index = fill(builder, lines, index);
				macro.addDNAToProteinConversion(builder.getObject());
			}
		} else {
			if (isConstraint) {
				AbstractBuilder<InputField<String, ?>, ? extends Constraint<AminoAcidCompound>> builder = ConstraintBuilders
						.getProteinBuilder(ConstraintType.valueOf(operationType), false);
				index = fill(builder, lines, index);
				macro.addProteinOperation(OperationAdapters.forConstraint(builder.getObject()));
			} else if (isTransform) {
				AbstractBuilder<InputField<String, ?>, ? extends Transform<AminoAcidCompound>> builder = TransformBuilders
						.getProteinBuilder(TransformType.valueOf(operationType));
				index = fill(builder, lines, index);
				macro.addProteinOperation(OperationAdapters.forTransform(builder.getObject()));
			} else {
				throw new IOException("Unknown operation type on Line " + (i + 1) + ": " + operationType);
			}
		}
		return index;
	}

	/**
	 * Helper method: sets the values of the fields of a given builder from a list
	 * of strings. This method reads only what is necessary for reconstructing one
	 * operation, starting from a given index.
	 * 
	 * @param builder The builder whose fields to fill
	 * @param lines   A list of strings as produced by {@link #dump(Macro, String)}
	 * @param i       The index of the first line to read (0-indexed, inclusive)
	 * @return The first index which has not been read (0-indexed, inclusive); so
	 *         this index should be the one given (via parameter {@code i}) to a
	 *         subsequent call to this method
	 * @throws IOException if the fields of the given builder cannot be filled from
	 *                     the given strings starting at the given index
	 */
	private static int fill(AbstractBuilder<InputField<String, ?>, ?> builder, List<String> lines, int i)
			throws IOException {
		int index = i;
		for (InputField<String, ?> field : builder.getFields()) {
			if (index >= lines.size()) {
				throw new IOException("End of input reached while an operation argument was expected");
			}
			try {
				field.setInput(lines.get(index));
			} catch (Exception e) {
				throw new IOException("Bad value on Line " + index + ": " + lines.get(index));
			}
			index++;
		}
		return index;
	}

}
