/**
 * Classes and interfaces for generic or abstract operations and conversions on sequences.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.operations;
