package fr.unicaen.aasuite.operations;

/**
 * An enumeration of all constraint types (common to DNA and protein sequences).
 * Note that this is different from classes of constraints, since, for instance,
 * a single constraint class may be used to represent several constraint types,
 * depending on some parameters.
 * <p>
 * Each constraint type has a default description, which can be set differently
 * for handling internationalization, for instance.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public enum ConstraintType {

	/** The type for constraints on annotations. */
	ANNOTATION("Pattern or keyword in annotation"),
	
	/** The type for constraints on average hydrophobicity. */
	AVG_HYDROPHOBICITY("Average hydrophobicity"),
	
	/** The type for the constraints on electrical load. */
	ELECTRICAL_LOAD("Electrical load"),

	/** The type for frequency constraints. */
	FREQUENCY("Frequency of compounds"),

	/** The type for the constraints on hydrophobicity. */
	HYDROPHOBICITY("Total hydrophobicity"),

	/** The type for the constraints on molecular weight. */
	MOLECULAR_WEIGHT("Molecular weight"),

	/** The type for the constraints on monoisotopic weight. */
	MONOISOTOPIC_WEIGHT("Monoisotopic weight"),
	
	/** The type for negated constraints. */
	NEGATION("Negated constraint"),

	/** The type for the constraints on net charge. */
	NET_CHARGE("Net charge at pH 7"),

	/** The type for the constraints of being nonempty. */
	NONEMPTINESS("Keep only nonempty sequences"),

	/** The type for occurrence constraints. */
	OCCURRENCE("Occurrence of compounds"),

	/**
	 * The type for the constraints of containing a repeated pattern (with a hole).
	 */
	PATTERN_WITH_GAP("Repeated pattern with hole"),

	/**
	 * The type for the constraints of containing a given subsequence positioned
	 * from N-terminus.
	 */
	POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL("Subsequence positioned from N-terminus"),

	/**
	 * The type for the constraints of containing a given subsequence positioned
	 * from C-terminus.
	 */
	POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL("Subsequence positioned from C-terminus"),

	/** The type for the constraints of containing a repeated pattern. */
	REGEXP("Repeated pattern"),

	/** The type for the constraints of being currently selected. */
	SELECTED("Discard unselected sequences"),

	/** The type for the constraints of containing a signal peptide. */
	SIGNAL_PEPTIDE("Presence of a signal peptide"),

	/** The type for size constraints. */
	SIZE("Size"),

	/** The type for the constraints of containing a given subsequence. */
	SUBSEQUENCE("Subsequence");

	/** The description of this type. */
	public String description;

	/**
	 * Builds a new instance.
	 * 
	 * @param description The description of this type
	 */
	private ConstraintType(String description) {
		this.description = description;
	}
	
	@Override
	public String toString() {
		return this.description;
	}

}
