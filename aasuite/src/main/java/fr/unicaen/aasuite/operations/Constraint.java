package fr.unicaen.aasuite.operations;

import java.util.Collection;
import java.util.TreeSet;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * An interface for representing any constraint on a sequence. A constraint is
 * any predicate on such sequences. The satisfaction of a constraint can be
 * witnessed by some subsequences or compounds in it.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type for compounds in the handled sequences
 */
public interface Constraint<C extends Compound> {

	/**
	 * Returns the type of this constraint.
	 * 
	 * @return The type of this constraint
	 */
	ConstraintType getType();

	/**
	 * Decides whether a given sequence satisfies this constraint. The
	 * implementation defaults to {@link #satisfies(AnnotatedSequence, Collection)}
	 * (with an ignored set of witnesses).
	 * 
	 * @param sequence A sequence
	 * @return true if the sequence satisfies the constraint, false otherwise
	 */
	default boolean satisfies(AnnotatedSequence<C> sequence) {
		return this.satisfies(sequence, new TreeSet<>());
	}

	/**
	 * Decides whether a given sequence satisfies this constraint, and in the
	 * affirmative, stores witnesses of this fact.
	 * 
	 * @param sequence  A sequence
	 * @param witnesses A collection of witnesses which will be completed with
	 *                  witnesses that the sequence satisfies the constraint; the
	 *                  witnesses are stored as the (0-indexed) positions of the
	 *                  corresponding amino acids, within intervals
	 * @return true if the sequence satisfies the constraint, false otherwise
	 */
	boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses);

}
