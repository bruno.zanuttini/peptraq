package fr.unicaen.aasuite.operations;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A utility class for adapting constraints and transforms, etc. to interface
 * {@link Operation}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class OperationAdapters {

	/**
	 * Returns an operation representing a given constraint (the operation returns
	 * either a list containing exactly the given sequence or an empty list,
	 * depending on whether the given sequence satisfies the constraint or does
	 * not).
	 * 
	 * @param constraint A constraint
	 * @return An operation representing the given constraint
	 * @param <C> The type of compounds to which the constraint applies
	 */
	public static <C extends Compound> Operation<C> forConstraint(final Constraint<C> constraint) {
		return new ConstraintOperation<>(constraint);
	}

	/**
	 * Returns an operation representing a given transform.
	 * 
	 * @param transform A transform
	 * @return An operation representing the given transform
	 * @param <C> The type of compounds to which the transform applies
	 */
	public static <C extends Compound> Operation<C> forTransform(final Transform<C> transform) {
		return new TransformOperation<>(transform);
	}

	/**
	 * A class for operations wrapping a constraint.
	 *
	 * @param <C> The type of compounds to which the constraint applies
	 */
	public static class ConstraintOperation<C extends Compound> implements Operation<C> {

		/** The wrapped constraint. */
		protected final Constraint<C> constraint;

		/**
		 * Builds a new instance.
		 * 
		 * @param constraint The wrapped constraint
		 */
		public ConstraintOperation(Constraint<C> constraint) {
			this.constraint = constraint;
		}

		/**
		 * Returns the wrapped constraint.
		 * 
		 * @return The wrapped constraint
		 */
		public Constraint<C> getConstraint() {
			return this.constraint;
		}

		@Override
		public List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence,
				Map<Sequence<?>, OperationInformation> information) {
			SortedSet<Interval> witnesses = new TreeSet<>();
			if (this.constraint.satisfies(sequence, witnesses)) {
				information.put(sequence, new OperationInformation(sequence, witnesses));
				return Collections.singletonList(sequence);
			} else {
				return new ArrayList<>();
			}
		}

		@Override
		public String toString() {
			return this.constraint.toString();

		}

	}

	/**
	 * A class for operations wrapping a transform.
	 *
	 * @param <C> The type of compounds to which the transform applies
	 */
	public static class TransformOperation<C extends Compound> implements Operation<C> {

		/** The wrapped transform. */
		protected final Transform<C> transform;

		/**
		 * Builds a new instance.
		 * 
		 * @param transform The wrapped transform
		 */
		public TransformOperation(Transform<C> transform) {
			this.transform = transform;
		}

		/**
		 * Returns the wrapped transform.
		 * 
		 * @return The wrapped transform
		 */
		public Transform<C> getTransform() {
			return this.transform;
		}

		@Override
		public List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence,
				Map<Sequence<?>, OperationInformation> information) {
			return transform.getSequences(sequence, information);
		}

		@Override
		public String toString() {
			return transform.toString();
		}

	}

}
