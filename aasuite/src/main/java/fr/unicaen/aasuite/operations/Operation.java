package fr.unicaen.aasuite.operations;

import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * A generic interface for operations on sequences, which produce zero, one or
 * more sequences for any given sequence of the same type.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type for handled sequences
 */
public interface Operation<C extends Compound> {

	/**
	 * Returns a list of the sequences produced by the operation for a given
	 * sequence. The produced sequences may or may not be newly allocated sequences.
	 * 
	 * @param sequence    A sequence
	 * @param information A map in which information about this operation will be
	 *                    stored for each produced sequence
	 * @return A list of the sequences produced by the operation for the given
	 *         sequence
	 */
	List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence, Map<Sequence<?>, OperationInformation> information);

}
