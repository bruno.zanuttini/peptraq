package fr.unicaen.aasuite.operations;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

/**
 * A class for representing "macro" operations, namely, lists of operations and
 * conversions which are intended to be applied one after the other. This class
 * is dedicated to macros which produce DNA sequences from DNA sequences,
 * protein sequences from protein sequences, or protein sequences from DNA
 * sequences using exactly one conversion.
 * 
 * Macros must be built incrementally, namely, each operation added comes as the
 * new last operation. In particular, if the currently last operation produces
 * sequences of compounds of type C, only an operation on sequences of type C,
 * or a conversion from sequences of type C, can be added.
 *
 * The particular case of an empty macro is considered to be the empty list of
 * operations, which can be applied to any type of sequences.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class Macro {

	/** The list of operations on DNA sequences of this macro (possibly empty). */
	private final Deque<Operation<NucleotideCompound>> dnaOperations;

	/**
	 * The conversion from DNA to protein sequences of this macro (null if none).
	 */
	private Conversion<NucleotideCompound, AminoAcidCompound> dnaToProteinConversion;

	/**
	 * The list of operations on protein sequences of this macro (possibly empty).
	 */
	private final Deque<Operation<AminoAcidCompound>> proteinOperations;

	/**
	 * Builds a new instance.
	 */
	public Macro() {
		this.dnaOperations = new LinkedList<>();
		this.dnaToProteinConversion = null;
		this.proteinOperations = new LinkedList<>();
	}

	/**
	 * Adds an operation on DNA sequences to this macro.
	 * 
	 * @param operation An operation on DNA sequences
	 * @throws IllegalStateException if there is already at least one operation or
	 *                               conversion in this macro, and the last one
	 *                               produces something else than DNA sequences
	 */
	public void addDNAOperation(Operation<NucleotideCompound> operation) throws IllegalStateException {
		if (!this.isEmpty() && !this.producesDNA()) {
			throw new IllegalStateException(
					"Cannot add DNA operation to a nonempty macro which does not produce DNA: " + this);
		} else {
			this.dnaOperations.addLast(operation);
		}
	}

	/**
	 * Adds a conversion from DNA sequences to protein sequences to this macro.
	 * 
	 * @param conversion A conversion from DNA to protein sequences
	 * @throws IllegalStateException if there is already at least one operation or
	 *                               conversion in this macro, and the last one
	 *                               produces something else than DNA sequences
	 */
	public void addDNAToProteinConversion(Conversion<NucleotideCompound, AminoAcidCompound> conversion)
			throws IllegalStateException {
		if (!this.isEmpty() && !this.producesDNA()) {
			throw new IllegalStateException(
					"Cannot add DNA to protein conversion to a nonempty macro which does not produce DNA: " + this);
		} else {
			assert this.dnaToProteinConversion == null;
			this.dnaToProteinConversion = conversion;
		}
	}

	/**
	 * Adds an operation on protein sequences to this macro.
	 * 
	 * @param operation An operation on protein sequences
	 * @throws IllegalStateException if there is already at least one operation or
	 *                               conversion in this macro, and the last one
	 *                               produces something else than protein sequences
	 */
	public void addProteinOperation(Operation<AminoAcidCompound> operation) throws IllegalStateException {
		if (!this.isEmpty() && !this.producesProteins()) {
			throw new IllegalStateException(
					"Cannot add protein operation to a nonempty macro which does not produce proteins: " + this);
		}
		this.proteinOperations.addLast(operation);
	}

	/**
	 * Removes the last operation from this macro.
	 * 
	 * @throws IllegalStateException If this macro is empty
	 */
	public void removeLast() throws IllegalStateException {
		if (this.isEmpty()) {
			throw new IllegalStateException("Cannot remove last operation from an empty macro");
		}
		if (!this.proteinOperations.isEmpty()) {
			this.proteinOperations.removeLast();
		} else if (this.dnaToProteinConversion != null) {
			this.dnaToProteinConversion = null;
		} else {
			assert !this.dnaOperations.isEmpty();
			this.dnaOperations.removeLast();
		}
	}

	/**
	 * Decides whether this macro can be applied to DNA sequences. By convention, an
	 * empty macro does apply to DNA sequences.
	 * 
	 * @return true is this macro can be applied to DNA sequences, false otherwise
	 */
	public boolean appliesToDNA() {
		return this.isEmpty() || !this.dnaOperations.isEmpty() || this.dnaToProteinConversion != null;
	}

	/**
	 * Decides whether this macro can be applied to protein sequences. By
	 * convention, an empty macro does apply to protein sequences.
	 * 
	 * @return true is this macro can be applied to protein sequences, false
	 *         otherwise
	 */
	public boolean appliesToProteins() {
		return this.dnaOperations.isEmpty() && this.dnaToProteinConversion == null;
	}

	/**
	 * Decides whether this macro is empty (i.e., contains no operation nor
	 * conversion).
	 * 
	 * @return true if this macro is empty, false otherwise
	 */
	public boolean isEmpty() {
		return this.dnaOperations.isEmpty() && this.dnaToProteinConversion == null && this.proteinOperations.isEmpty();
	}

	/**
	 * Decides whether this macro produces DNA sequences when applied (to what it
	 * can apply). As a particular case, this method cannot be applied to the empty
	 * macro, since what it produces depends on what it is applied to.
	 * 
	 * @return true if this macro produces DNA sequences, false otherwise
	 * @throws IllegalStateException if this macro is empty
	 */
	public boolean producesDNA() throws IllegalStateException {
		if (this.isEmpty()) {
			throw new IllegalStateException("Macro is empty");
		}
		return !this.dnaOperations.isEmpty() && this.dnaToProteinConversion == null;
	}

	/**
	 * Decides whether this macro produces protein sequences when applied (to what
	 * it can apply). As a particular case, this method cannot be applied to the
	 * empty macro, since what it produces depends on what it is applied to.
	 * 
	 * @return true if this macro produces protein sequences, false otherwise
	 * @throws IllegalStateException if this macro is empty
	 */
	public boolean producesProteins() {
		if (this.isEmpty()) {
			throw new IllegalStateException("Macro is empty");
		}
		return this.dnaToProteinConversion != null || !this.proteinOperations.isEmpty();
	}

	/**
	 * Returns the list of DNA operations in this macro.
	 * 
	 * @return The list of DNA operations in this macro
	 */
	public Deque<Operation<NucleotideCompound>> getDNAOperations() {
		return this.dnaOperations;
	}

	/**
	 * Decides whether this macro includes a conversion from DNA to proteins.
	 * 
	 * @return true if this macro includes a conversion from DNA to proteins, false
	 *         otherwise
	 */
	public boolean hasDNAToProteinConversion() {
		return this.dnaToProteinConversion != null;
	}

	/**
	 * Returns the conversion from DNA to proteins included in this macro.
	 * 
	 * @return The conversion from DNA to proteins included in this macro
	 * @throws IllegalStateException if there is no conversion from DNA to proteins
	 *                               included in this macro
	 */
	public Conversion<NucleotideCompound, AminoAcidCompound> getDNAToProteinConversion() throws IllegalStateException {
		if (this.dnaToProteinConversion == null) {
			throw new IllegalStateException("Macro has no conversion from DNA to proteins");
		}
		return this.dnaToProteinConversion;
	}

	/**
	 * Returns the list of protein operations in this macro.
	 * 
	 * @return The list of protein operations in this macro
	 */
	public Deque<Operation<AminoAcidCompound>> getProteinOperations() {
		return this.proteinOperations;
	}

	/**
	 * Returns the list of all operations in this macro (including the conversion,
	 * if any).
	 * 
	 * @return The list of all operations in this macro (including the conversion,
	 *         if any)
	 */
	public List<?> getOperations() {
		List<Object> res = new ArrayList<>();
		res.addAll(this.dnaOperations);
		if (this.dnaToProteinConversion != null) {
			res.add(this.dnaToProteinConversion);
		}
		res.addAll(this.proteinOperations);
		return res;
	}

}
