package fr.unicaen.aasuite.operations;

import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * An interface for representing any conversion on sequences. A conversion is an
 * operation taking a sequence and returning various sequences. The class is
 * parameterized by two types of Compound, C and D (possibly the same), meaning
 * that the conversion is from {@literal AnnotatedSequence<C>} instances to
 * {@literal AnnotatedSequence<D>} instances.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 * @param <C> The type for compounds in the handled sequences
 * @param <D> The type for compounds in the produced sequences
 */
public interface Conversion<C extends Compound, D extends Compound> {

	/**
	 * Returns the type of this conversion.
	 * 
	 * @return The type of this conversion
	 */
	ConversionType getType();

	/**
	 * Returns the sequences obtained from the conversion of a given sequence.
	 * 
	 * @param sequence    A sequence
	 * @param information A map in which information about this operation will be
	 *                    stored for each produced sequence
	 * @return A list of the sequences obtained from the conversion of the given
	 *         sequence
	 */
	List<AnnotatedSequence<D>> getSequences(AnnotatedSequence<C> sequence,
			Map<Sequence<?>, OperationInformation> information);

}
