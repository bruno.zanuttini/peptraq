package fr.unicaen.aasuite.operations;

/**
 * An enumeration of all conversion types (common to DNA and protein sequences).
 * Note that this is different from classes of conversions, since, for instance,
 * a single conversion class may be used to represent several conversion types,
 * depending on some parameters.
 * <p>
 * * Each conversion type has a default description, which can be set
 * differently for handling internationalization, for instance.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public enum ConversionType {

	/** The type for conversions from DNA to proteins. */
	DNA_TO_PROTEIN_TRANSLATION("Translate to proteins");

	/** The description of this type. */
	public String description;

	/**
	 * Builds a new instance.
	 * 
	 * @param description The description of this type
	 */
	private ConversionType(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return this.description;
	}

}