package fr.unicaen.aasuite.operations;

import java.util.function.Function;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A class for constraints which are satisfied when some feature of the sequence
 * has a value within some range.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type for compounds in the handled sequences
 * @param <T> The type for values
 */
public class ValueConstraint<C extends Compound, T extends Comparable<T>> extends NoWitnessConstraint<C> {

	/**
	 * The description for an instance with no bound. The description may use 1$ for
	 * the description of the feature.
	 */
	public static String noBoundDescription = "%1$s: any";

	/**
	 * The description for an instance with no upper bound. The description may use
	 * 1$ for the description of the feature, and 2$ for the lower bound.
	 */
	public static String lowerBoundDescription = "%1$s: at least %2$s";

	/**
	 * The description for an instance with no lower bound. The description may use
	 * 1$ for the description of the feature, and 2$ for the upper bound.
	 */
	public static String upperBoundDescription = "%1$s: at most %2$s";

	/**
	 * The description for a general instance. The description may use 1$ for the
	 * description of the feature, 2$ for the lower bound, and 3$ for the upper
	 * bound.
	 */
	public static String description = "%1$s: at least %2$s and at most %3$s";

	/** The type of this constraint. */
	protected final ConstraintType type;

	/** The feature, as a function computing the value for a given sequence. */
	protected final Function<? super AnnotatedSequence<C>, T> feature;

	/** The minimum value for the constraint to be satisfied (null for none). */
	protected final T minValue;

	/** The maximum value for the constraint to be satisfied (null for none). */
	protected final T maxValue;

	/** A human-readable description of the feature (capitalized). */
	protected final String featureDescription;

	/**
	 * Builds a new instance. Either only a minimum value, or only a maximum, or
	 * both, or none, can be specified (null for ignoring one). Note that both
	 * values (when given) are inclusive, that is, the constraint is satisfied if
	 * the feature has a value equal to one of them. If both values are null, then
	 * the constraint is always satisfied.
	 * 
	 * @param type               The type of this constraint
	 * @param feature            The feature, as a function computing the value for
	 *                           a given sequence
	 * @param minValue           A minimum value (strictly) under which the
	 *                           constraint is not satisfied (null for none)
	 * @param maxValue           A maximum value (strictly) above which the
	 *                           constraint is not satisfied (null for none)
	 * @param featureDescription A human-readable description of the feature (with
	 *                           initial cap)
	 * @throws IllegalArgumentException If both a minimum and a maximum value are
	 *                                  given but the minimum is (strictly) greater
	 *                                  than the maximum
	 */
	public ValueConstraint(ConstraintType type, Function<? super AnnotatedSequence<C>, T> feature, T minValue,
			T maxValue, String featureDescription) throws IllegalArgumentException {
		super();
		if (minValue != null && maxValue != null && minValue.compareTo(maxValue) > 0) {
			throw new IllegalArgumentException("Minimum is greater than maximum: " + minValue + " > " + maxValue);
		}
		this.type = type;
		this.feature = feature;
		this.minValue = minValue;
		this.maxValue = maxValue;
		this.featureDescription = featureDescription;
	}

	@Override
	public ConstraintType getType() {
		return this.type;
	}

	/**
	 * Returns the feature for this constraint.
	 * 
	 * @return The feature for this constraint
	 */
	public Function<? super AnnotatedSequence<C>, T> getFeature() {
		return this.feature;
	}

	/**
	 * Returns the human-readable description of the feature for this constraint
	 * (with initial cap).
	 * 
	 * @return The human-readable description of the feature for this constraint
	 */
	public String getFeatureDescription() {
		return this.featureDescription;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence) {
		T value = this.feature.apply(sequence);
		return (this.minValue == null || this.minValue.compareTo(value) <= 0)
				&& (this.maxValue == null || value.compareTo(this.maxValue) <= 0);
	}

	@Override
	public String toString() {
		if (this.minValue == null && this.maxValue == null) {
			return String.format(noBoundDescription, this.featureDescription);
		} else if (this.maxValue == null) {
			return String.format(lowerBoundDescription, this.featureDescription, this.minValue);
		} else if (this.minValue == null) {
			return String.format(upperBoundDescription, this.featureDescription, this.maxValue);
		} else {
			return String.format(description, this.featureDescription, this.minValue, this.maxValue);
		}
	}

	public boolean hasMinValue() {
		return this.minValue != null;
	}

	public T getMinValue() throws IllegalStateException {
		if (this.minValue == null) {
			throw new IllegalStateException("Constraint has no minimal value");
		}
		return this.minValue;
	}

	public boolean hasMaxValue() {
		return this.maxValue != null;
	}

	public T getMaxValue() throws IllegalStateException {
		if (this.maxValue == null) {
			throw new IllegalStateException("Constraint has no maximal value");
		}
		return this.maxValue;
	}

}
