package fr.unicaen.aasuite.operations;

import java.util.Collection;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * An abstract class for constraints for which there is never any witness, to
 * serve as an adapter.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type for compounds in the handled sequences
 */
public abstract class NoWitnessConstraint<C extends Compound> implements Constraint<C> {

	/**
	 * Builds a new instance.
	 */
	protected NoWitnessConstraint() {
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * The method is made abstract, overriding the default implementation in
	 * {@link Constraint} and in effect enforcing its definition by concrete
	 * subclasses.
	 */
	@Override
	public abstract boolean satisfies(AnnotatedSequence<C> sequence);

	/**
	 * {@inheritDoc}
	 * <p>
	 * The implementation delegates work to {@link #satisfies(AnnotatedSequence)},
	 * leaving the set of witnesses unchanged.
	 */
	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses) {
		return this.satisfies(sequence);
	}

}
