package fr.unicaen.aasuite.operations;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.OperationInformation;

/**
 * An interface for representing any transform on sequences. A transform is an
 * operation taking a sequence and returning various sequences (or views on the
 * given sequence) of the same type, typically subsequences of it.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 * @param <C> The type for compounds in the handled sequences
 */
public interface Transform<C extends Compound> {

	/**
	 * Returns the type of this transform.
	 * 
	 * @return The type of this transform
	 */
	TransformType getType();

	/**
	 * Returns a list of the sequences obtained from the transform of a given
	 * sequence. The implementation defaults to
	 * {@link #getSequences(AnnotatedSequence, Map)} (with ignored information).
	 * 
	 * @param sequence A sequence
	 * @return A list of the sequences obtained from the transform of the given
	 *         sequence (possibly newly allocated sequences, or only views on the
	 *         given sequence, etc.)
	 */
	default List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence) {
		return this.getSequences(sequence, new HashMap<>());
	}

	/**
	 * Returns a list of the sequences obtained from the transform of a given
	 * sequence.
	 * 
	 * @param sequence    A sequence
	 * @param information A map in which information about this operation will be
	 *                    stored for each produced sequence
	 * @return A list of the sequences obtained from the transform of the given
	 *         sequence (possibly newly allocated sequences, or only views on the
	 *         given sequence, etc.)
	 */
	List<AnnotatedSequence<C>> getSequences(AnnotatedSequence<C> sequence,
			Map<Sequence<?>, OperationInformation> information);

}
