package fr.unicaen.aasuite.operations;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A class for constraints which are the negation of another one, i.e., are
 * satisfied exactly when the wrapped constraint is falsified.
 * 
 * By convention, a negation never has witnesses, whatever the negated
 * constraint.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type for compounds in the sequences concerned by this
 *        constraint
 */
public class NegatedConstraint<C extends Compound> extends NoWitnessConstraint<C> {

	/**
	 * The description of an instance. The description may use 1$ for the
	 * description of the negated constraint.
	 */
	public static String description = "Reverse of: %1$s";

	/** The negated constraint. */
	protected final Constraint<C> constraint;

	/**
	 * Builds a new instance.
	 * 
	 * @param constraint The constraint whose this constraint should be the negation
	 */
	public NegatedConstraint(Constraint<C> constraint) {
		super();
		this.constraint = constraint;
	}

	/**
	 * Returns the negated constraint.
	 * 
	 * @return The negated constraint
	 */
	public Constraint<C> getNegatedConstraint() {
		return this.constraint;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.NEGATION;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence) {
		return !this.constraint.satisfies(sequence);
	}

	@Override
	public String toString() {
		return String.format(description, this.constraint);
	}

}
