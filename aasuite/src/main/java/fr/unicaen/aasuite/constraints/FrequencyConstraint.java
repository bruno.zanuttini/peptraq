package fr.unicaen.aasuite.constraints;

import java.util.ArrayList;
import java.util.Collection;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A constraint on the percentage of occurrences of given compounds in sequences
 * of compounds. When several compounds are specified, the occurrences of all
 * are summed together and compared to the required percentages.
 * <p>
 * Percentages are mapped to floating point numbers, in the sense that, e.g., a
 * sequence of length 50 with 12 cysteines does not satisfy a minimum frequency
 * of 25 % cysteines, but a sequence of length 50 with 13 cysteines does satisfy
 * it.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class FrequencyConstraint<C extends Compound> implements Constraint<C> {

	/** The description for a tautological instance. */
	public static String tautologicalDescription = "Tautological frequency constraint";

	/**
	 * The description for an instance with only a lower bound. This description may
	 * use 1$ for the lower bound and 2$ for the collection of compounds.
	 */
	public static String lowerBoundDescription = "At least %1$s %% of %2$s";

	/**
	 * The description for an instance with only an upper bound. This description
	 * may use 1$ for the upper bound and 2$ for the collection of compounds.
	 */
	public static String upperBoundDescription = "At most %1$s %% of %2$s";

	/**
	 * The description for a general instance. This description may use 1$ for the
	 * lower bound, 2$ for the upper bound, and 3$ for the collection of compounds.
	 */
	public static String description = "At least %1$s and at most %2$s %% of %3$s";

	/** The compound concerned with the constraint. */
	protected final Collection<C> compounds;

	/**
	 * The minimal percentage of occurrences of the compounds required (null if
	 * none).
	 */
	protected final Float minFrequency;

	/**
	 * The maximal percentage of occurrences of the compounds required (null if
	 * none).
	 */
	protected final Float maxFrequency;

	/**
	 * Builds a new instance.
	 * 
	 * @param compounds    The compounds whose (aggregated) number of occurrences to
	 *                     constrain
	 * @param minFrequency The minimal (inclusive) percentage of compounds equal to
	 *                     one of the given ones (null if none)
	 * @param maxFrequency The maximal (inclusive) percentage of compounds equal to
	 *                     one of the given ones (null if none)
	 * @throws IllegalArgumentException if minFrequency is greater than maxFrequency
	 */
	public FrequencyConstraint(Collection<C> compounds, Float minFrequency, Float maxFrequency)
			throws IllegalArgumentException {
		if (minFrequency != null && maxFrequency != null && minFrequency > maxFrequency) {
			throw new IllegalArgumentException(
					"Minimum frequency cannot be greater than maximum: " + minFrequency + ", " + maxFrequency);
		}
		this.compounds = compounds;
		this.minFrequency = minFrequency;
		this.maxFrequency = maxFrequency;
	}

	/**
	 * Returns the collection of compounds whose (aggregated) percentage of occurrences is constrained.
	 * @return The collection of compounds whose (aggregated) percentage of occurrences is constrained
	 */
	public Collection<C> getCompounds() {
		return this.compounds;
	}

	/**
	 * Decides whether a minimal frequency is set.
	 * @return true if a minimal frequency is set, false otherwise
	 */
	public boolean hasMinFrequency() {
		return this.minFrequency != null;
	}

	/**
	 * Returns the minimal frequency imposed by this constraint.
	 * @return The minimal frequency imposed by this constraint
	 * @throws IllegalStateException if no minimal frequency is set
	 */
	public float getMinFrequency() throws IllegalStateException {
		if (this.minFrequency == null) {
			throw new IllegalStateException("Constraint has no minimal frequency");
		}
		return this.minFrequency;
	}

	/**
	 * Decides whether a maximal frequency is set.
	 * @return true if a maximal frequency is set, false otherwise
	 */
	public boolean hasMaxFrequency() {
		return this.maxFrequency != null;
	}

	/**
	 * Returns the maximal frequency imposed by this constraint.
	 * @return The maximal frequency imposed by this constraint
	 * @throws IllegalStateException if no maximal frequency is set
	 */
	public float getMaxFrequency() throws IllegalStateException {
		if (this.maxFrequency == null) {
			throw new IllegalStateException("Constraint has no maximal frequency");
		}
		return this.maxFrequency;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.FREQUENCY;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses) {
		Collection<Integer> positions = new ArrayList<>();
		int nbOccurrences = 0;
		for (int position = 0; position < sequence.getLength(); position++) {
			C compound = sequence.getCompoundAt(position + 1);
			if (this.compounds.contains(compound)) {
				nbOccurrences++;
				positions.add(position);
				if (this.maxFrequency != null && nbOccurrences * 100 > sequence.getLength() * this.maxFrequency) {
					return false;
				}
			}
		}
		// Note: checking upper bound again in case no match has been found and the
		// bound is negative
		if ((this.minFrequency == null || nbOccurrences * 100 >= sequence.getLength() * this.minFrequency)
				&& (this.maxFrequency == null || nbOccurrences * 100 <= sequence.getLength() * this.maxFrequency)) {
			for (int position : positions) {
				witnesses.add(new Interval(position));
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		if (this.minFrequency == null && this.maxFrequency == null) {
			return tautologicalDescription;
		} else if (this.minFrequency == null) {
			return String.format(upperBoundDescription, this.maxFrequency, this.compounds);
		} else if (this.maxFrequency == null) {
			return String.format(lowerBoundDescription, this.minFrequency, this.compounds);
		} else {
			return String.format(description, this.minFrequency, this.maxFrequency, this.compounds);
		}
	}

}
