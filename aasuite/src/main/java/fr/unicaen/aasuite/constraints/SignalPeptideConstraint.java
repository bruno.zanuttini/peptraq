package fr.unicaen.aasuite.constraints;

import java.util.Collection;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.features.SignalPeptide;
import fr.unicaen.aasuite.features.SignalPeptide.Info;
import fr.unicaen.aasuite.features.SignalPeptide.OrganismType;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A constraint which is satisfied by a sequence of amino acid compounds if and
 * only if the sequence contains a signal peptide at (at most) a given
 * confidence level.
 * <p>
 * The prediction relies on the JSPP package; see {@link SignalPeptide}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SignalPeptideConstraint implements Constraint<AminoAcidCompound> {

	/**
	 * The description of an instance of this constraint. The description may use 1$
	 * for the required score and 2$ for the description of the type of organisms.
	 */
	public static String description = "Presence of a signal peptide with confidence (normalized score) at least %1$s (%2$s)";

	/** The type of organisms to which this constraint applies. */
	protected final SignalPeptide.OrganismType organismType;

	/**
	 * The minimal confidence level in the presence of a signal peptide for the
	 * constraint to be satisfied.
	 */
	protected final float minScore;

	/**
	 * Builds a new instance.
	 * 
	 * @param type     The type of organisms
	 * @param minScore The minimal confidence level in the presence of a signal
	 *                 peptide for the constraint to be satisfied; 0 means that any
	 *                 sequence will be considered to contain a signal peptide
	 */
	public SignalPeptideConstraint(SignalPeptide.OrganismType type, float minScore) {
		this.organismType = type;
		this.minScore = minScore;
	}

	/**
	 * Returns the type of organisms to which this constraint can be applied.
	 * 
	 * @return The type of organisms to which this constraint can be applied
	 */
	public OrganismType getOrganismType() {
		return this.organismType;
	}

	/**
	 * Returns the minimum confidence level for the constraint to be satisfied.
	 * 
	 * @return The minimum confidence level for the constraint to be satisfied
	 */
	public float getMinScore() {
		return this.minScore;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.SIGNAL_PEPTIDE;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * @throws RuntimeException if the sequence is not a precursor (so that it does
	 *                          not make sense to ask the information)
	 */
	@Override
	public boolean satisfies(AnnotatedSequence<AminoAcidCompound> sequence, Collection<Interval> witnesses)
			throws RuntimeException {
		Info predictions = SignalPeptide.getInformation(sequence, this.organismType);
		if (predictions.score >= this.minScore) {
			witnesses.add(new Interval(0, predictions.position + 1));
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return String.format(description, this.minScore, this.organismType);
	}

}
