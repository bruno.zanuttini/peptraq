package fr.unicaen.aasuite.constraints;

import java.util.ArrayList;
import java.util.Collection;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A constraint on the number of occurrences of given compounds in sequences of
 * compounds. When several compounds are specified, the occurrences of all are
 * summed together and compared to the required bounds.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class OccurrenceConstraint<C extends Compound> implements Constraint<C> {

	/** The description for a tautological instance. */
	public static String tautologicalDescription = "Tautological occurrence constraint";

	/**
	 * The description for an instance with only a lower bound. This description may
	 * use 1$ for the lower bound and 2$ for the collection of compounds.
	 */
	public static String lowerBoundDescription = "At least %1$d occurrences of %2$s";

	/**
	 * The description for an instance with only an upper bound. This description
	 * may use 1$ for the upper bound and 2$ for the collection of compounds.
	 */
	public static String upperBoundDescription = "At most %1$d occurrences of %2$s";

	/**
	 * The description for a general instance. This description may use 1$ for the
	 * lower bound, 2$ for the upper bound, and 3$ for the collection of compounds.
	 */
	public static String description = "At least %1$d and at most %2$d occurrences of %3$s";

	/** The compounds concerned with the constraint. */
	protected final Collection<C> compounds;

	/**
	 * The minimal required number of occurrences of the compounds (null if none).
	 */
	protected final Integer minNumber;

	/**
	 * The maximal required number of occurrences of the compounds (null if none).
	 */
	protected final Integer maxNumber;

	/**
	 * Builds a new instance.
	 * 
	 * @param compounds The compounds whose (aggregated) number of occurrences to
	 *                  constrain
	 * @param minNumber The minimal (inclusive) number of compounds equal to one of
	 *                  the given ones (null if none)
	 * @param maxNumber The maximal (inclusive) number of compounds equal to one of
	 *                  the given ones (null if none)
	 * @throws IllegalArgumentException if minNumber is greater than maxNumber
	 */
	public OccurrenceConstraint(Collection<C> compounds, Integer minNumber, Integer maxNumber)
			throws IllegalArgumentException {
		if (minNumber != null && maxNumber != null && minNumber > maxNumber) {
			throw new IllegalArgumentException(
					"Minimum number of occurrences cannot be greater than maximum: " + minNumber + ", " + maxNumber);
		}
		this.compounds = compounds;
		this.minNumber = minNumber;
		this.maxNumber = maxNumber;
	}

	/**
	 * Returns the collection of compounds whose (aggregated) number of occurrences is constrained.
	 * @return The collection of compounds whose (aggregated) number of occurrences is constrained
	 */
	public Collection<C> getCompounds() {
		return this.compounds;
	}

	/**
	 * Decides whether a minimal number of occurrences is set.
	 * @return true if a minimal number of occurrences is set, false otherwise
	 */
	public boolean hasMinNumber() {
		return this.minNumber != null;
	}

	/**
	 * Returns the minimal number of occurrences imposed by this constraint.
	 * @return The minimal number of occurrences imposed by this constraint
	 * @throws IllegalStateException if no minimal number of occurrences is set
	 */
	public int getMinNumber() throws IllegalStateException {
		if (this.minNumber == null) {
			throw new IllegalStateException("Constraint has no minimal number");
		}
		return this.minNumber;
	}

	/**
	 * Decides whether a maximal number of occurrences is set.
	 * @return true if a maximal number of occurrences is set, false otherwise
	 */
	public boolean hasMaxNumber() {
		return this.maxNumber != null;
	}

	/**
	 * Returns the maximal number of occurrences imposed by this constraint.
	 * @return The maximal number of occurrences imposed by this constraint
	 * @throws IllegalStateException if no maximal number of occurrences is set
	 */
	public int getMaxNumber() throws IllegalStateException {
		if (this.maxNumber == null) {
			throw new IllegalStateException("Constraint has no maximal number");
		}
		return this.maxNumber;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.OCCURRENCE;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses) {
		Collection<Integer> positions = new ArrayList<>();
		int nbOccurrences = 0;
		for (int position = 0; position < sequence.getLength(); position++) {
			C compound = sequence.getCompoundAt(position + 1);
			if (this.compounds.contains(compound)) {
				nbOccurrences++;
				positions.add(position);
				if (this.maxNumber != null && nbOccurrences > this.maxNumber) {
					return false;
				}
			}
		}
		// Note: checking upper bound again in case no match has been found and the
		// bound is negative
		if ((this.minNumber == null || nbOccurrences >= this.minNumber)
				&& (this.maxNumber == null || nbOccurrences <= this.maxNumber)) {
			for (int position : positions) {
				witnesses.add(new Interval(position));
			}
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		if (this.minNumber == null && this.maxNumber == null) {
			return tautologicalDescription;
		} else if (this.minNumber == null) {
			return String.format(upperBoundDescription, this.maxNumber, this.compounds);
		} else if (this.maxNumber == null) {
			return String.format(lowerBoundDescription, this.minNumber, this.compounds);
		} else {
			return String.format(description, this.minNumber, this.maxNumber, this.compounds);
		}
	}

}
