package fr.unicaen.aasuite.constraints;

import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A constraint satisfied when a sequence contains a pattern specified by a
 * (nonempty) prefix, a number of arbitrary compounds in a given interval, and a
 * (nonempty) suffix, and this pattern is repeated a certain number of times.
 * <p>
 * The matching subsequences must not overlap with each other for the sequence
 * to satisfy the constraint. Additionally, only matches which do not include
 * other matches are considered to be witnesses of satisfaction.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class RepeatedPatternWithGapConstraint<C extends Compound> implements Constraint<C> {

	// Implementation note: this constraint is NOT implemented using the regular
	// expression (pattern){min,max} because we need to retrieve the witnesses
	// (occurrences of the pattern).

	// TODO: maybe it would be more efficient to try to match the regular expression
	// (pattern){min,max} and only when it is matched, fire a second search for the
	// witnesses.

	/**
	 * The description for an instance with a tautological number of occurrences.
	 * This description may use 1$ for the pattern.
	 */
	public static String noOccurrenceDescription = "Any number of occurrences of %1$s";

	/**
	 * The description for an instance with only a lower bound on the number of
	 * occurrences. This description may use 1$ for the lower bound and 2$ for the
	 * pattern.
	 */
	public static String minOccurrencesDescription = "At least %1$d occurrence(s) of %2$s";

	/**
	 * The description for an instance with only an upper bound on the number of
	 * occurrences. This description may use 1$ for the lower bound and 2$ for the
	 * pattern.
	 */
	public static String maxOccurrencesDescription = "At most %1$d occurrence(s) of %2$s";

	/**
	 * The description for a general instance. This description may use 1$ for the
	 * lower bound, 2$ for the upper bound, and 3$ for the pattern.
	 */
	public static String description = "At least %1$d and at most %2$d occurrence(s) of %3$s";

	/** The prefix required to occur. */
	protected final List<C> prefix;

	/** The minimal gap between the prefix and the suffix (null if none). */
	protected final Integer minGap;

	/** The maximal gap between the prefix and the suffix (null if none). */
	protected final Integer maxGap;

	/** The suffix required to occur. */
	protected final List<C> suffix;

	/** The minimal number of occurrences of the pattern (null if none). */
	protected final Integer minNbOccurrences;

	/** The maximal number of occurrences of the pattern (null if none). */
	protected final Integer maxNbOccurrences;

	/** A regexp constraint to which the work is delegated. */
	private final RepeatedPatternConstraint<C> constraint;

	/**
	 * Builds a new instance.
	 * 
	 * @param prefix           The prefix required to occur
	 * @param minGap           The minimal gap between the prefix and the suffix
	 *                         (null for none)
	 * @param maxGap           The maximal gap between the prefix and the suffix
	 *                         (null for none)
	 * @param suffix           The suffix required to occur
	 * @param minNbOccurrences The minimal number of occurrences of the pattern
	 *                         (null for none)
	 * @param maxNbOccurrences The maximal number of occurrences of the pattern
	 *                         (null for none)
	 * @throws IllegalArgumentException If the prefix or the suffix is empty, or the
	 *                                  minimum gap is greater than the maximum, or
	 *                                  the minimum number of occurrences is greater
	 *                                  than the maximum
	 */
	public RepeatedPatternWithGapConstraint(List<C> prefix, Integer minGap, Integer maxGap, List<C> suffix,
			Integer minNbOccurrences, Integer maxNbOccurrences) throws IllegalArgumentException {
		if (prefix.isEmpty()) {
			throw new IllegalArgumentException("Prefix cannot be empty");
		}
		if (minGap != null && maxGap != null && minGap > maxGap) {
			throw new IllegalArgumentException("Minimum gap cannot be greater than maximum: " + minGap + ", " + maxGap);
		}
		if (suffix.isEmpty()) {
			throw new IllegalArgumentException("Suffix cannot be empty");
		}
		if (minNbOccurrences != null && maxNbOccurrences != null && minNbOccurrences > maxNbOccurrences) {
			throw new IllegalArgumentException("Minimum number of occurrences cannot be greater than maximum: "
					+ minNbOccurrences + ", " + maxNbOccurrences);
		}
		this.prefix = prefix;
		this.minGap = minGap;
		this.maxGap = maxGap;
		this.suffix = suffix;
		this.minNbOccurrences = minNbOccurrences;
		this.maxNbOccurrences = maxNbOccurrences;
		this.constraint = this.makeRegexpConstraint();
	}

	/**
	 * Returns the prefix required to occur.
	 * 
	 * @return The prefix required to occur
	 */
	public List<C> getPrefix() {
		return this.prefix;
	}

	/**
	 * Decides whether a minimum gap is set.
	 * 
	 * @return true if a minimum gap is set, false otherwise
	 */
	public boolean hasMinGap() {
		return this.minGap != null;
	}

	/**
	 * Returns the minimum gap required in the pattern.
	 * 
	 * @return The minimum gap required in the pattern
	 * @throws IllegalStateException if no minimum gap is set
	 */
	public int getMinGap() throws IllegalStateException {
		if (this.minGap == null) {
			throw new IllegalStateException("Constraint has no minimal gap");
		}
		return this.minGap;
	}

	/**
	 * Decides whether a maximum gap is set.
	 * 
	 * @return true if a maximum gap is set, false otherwise
	 */
	public boolean hasMaxGap() {
		return this.maxGap != null;
	}

	/**
	 * Returns the maximum gap required in the pattern.
	 * 
	 * @return The maximum gap required in the pattern
	 * @throws IllegalStateException if no maximum gap is set
	 */
	public int getMaxGap() throws IllegalStateException {
		if (this.maxGap == null) {
			throw new IllegalStateException("Constraint has no maximal gap");
		}
		return this.maxGap;
	}

	/**
	 * Returns the suffix required to occur.
	 * 
	 * @return The suffix required to occur
	 */
	public List<C> getSuffix() {
		return this.suffix;
	}

	/**
	 * Decides whether a minimum number of occurrences of the pattern is set.
	 * 
	 * @return true if a minimum number of occurrences of the pattern is set, false
	 *         otherwise
	 */
	public boolean hasMinNbOccurrences() {
		return this.minNbOccurrences != null;
	}

	/**
	 * Returns the minimum number of occurrences of the pattern.
	 * 
	 * @return The minimum number of occurrences of the pattern
	 * @throws IllegalStateException if no minimum number is set
	 */
	public int getMinNbOccurrences() throws IllegalStateException {
		if (this.minNbOccurrences == null) {
			throw new IllegalStateException("Constraint has no minimal number of occurrences");
		}
		return this.minNbOccurrences;
	}

	/**
	 * Decides whether a maximum number of occurrences of the pattern is set.
	 * 
	 * @return true if a maximum number of occurrences of the pattern is set, false
	 *         otherwise
	 */
	public boolean hasMaxNbOccurrences() {
		return this.maxNbOccurrences != null;
	}

	/**
	 * Returns the maximum number of occurrences of the pattern.
	 * 
	 * @return The maximum number of occurrences of the pattern
	 * @throws IllegalStateException if no maximum number is set
	 */
	public int getMaxNbOccurrences() throws IllegalStateException {
		if (this.maxNbOccurrences == null) {
			throw new IllegalStateException("Constraint has no maximal number of occurrences");
		}
		return this.maxNbOccurrences;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.PATTERN_WITH_GAP;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses) {
		return this.constraint.satisfies(sequence, witnesses);
	}

	@Override
	public String toString() {
		String pattern = this.constraint.getPattern().toString();
		if (this.minNbOccurrences == null && this.maxNbOccurrences == null) {
			return String.format(noOccurrenceDescription, pattern);
		} else if (this.minNbOccurrences == null) {
			return String.format(maxOccurrencesDescription, this.maxNbOccurrences, pattern);
		} else if (this.maxNbOccurrences == null) {
			return String.format(minOccurrencesDescription, this.minNbOccurrences, pattern);
		} else {
			return String.format(description, this.minNbOccurrences, this.maxNbOccurrences, pattern);
		}
	}

	/**
	 * Helper method: returns a regexp constraint equivalent to this constraint;
	 * uses the attributes of this class, so these must be initialized before this
	 * method is called.
	 * 
	 * @return A regexp constraint equivalent to this constraint
	 */
	private RepeatedPatternConstraint<C> makeRegexpConstraint() {
		StringBuilder regexp = new StringBuilder();

		// Pattern
		regexp.append("(");
		regexp.append(sequenceToRegexp(this.prefix));
		boolean emptyGap = (this.minGap == null || this.minGap == 0) && (this.maxGap != null && this.maxGap == 0);
		if (!emptyGap) {
			regexp.append(".");
			if (this.minGap == null && this.maxGap == null) {
				// TOOD: handle special compound "*" vs wildcard for regexp
				regexp.append("*");
			} else {
				regexp.append("{");
				regexp.append(this.minGap == null ? 0 : this.minGap);
				regexp.append(",");
				if (this.maxGap != null) {
					regexp.append(this.maxGap);
				}
				regexp.append("}");
			}
		}
		regexp.append(sequenceToRegexp(this.suffix));
		regexp.append(")");
		return new RepeatedPatternConstraint<C>(Pattern.compile(regexp.toString(), Pattern.CASE_INSENSITIVE),
				this.minNbOccurrences, this.maxNbOccurrences);
	}

	/**
	 * Hellper method: returns a string representing a sequence of compounds.
	 * 
	 * @param sequence A sequence of compounds
	 * @return The representation of the given sequence as a string
	 */
	private static String sequenceToRegexp(List<? extends Compound> sequence) {
		StringBuffer res = new StringBuffer();
		for (Compound compound : sequence) {
			res.append(compound.getShortName());
		}
		return res.toString();
	}

}
