package fr.unicaen.aasuite.constraints;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.operations.NoWitnessConstraint;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A constraint which is satisfied by any sequence.
 * 
 * @author Bruno Zanuttini
 *
 * @param <C> The type of compounds
 */
public class TautologicalConstraint<C extends Compound> extends NoWitnessConstraint<C> {

	/** The type of this instance. */
	protected final ConstraintType type;

	/** The description for this instance. */
	protected final String description;

	/**
	 * Builds a new instance.
	 * 
	 * @param type        The type of this instance
	 * @param description The description for this instance
	 */
	public TautologicalConstraint(ConstraintType type, String description) {
		super();
		this.type = type;
		this.description = description;
	}

	@Override
	public ConstraintType getType() {
		return this.type;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence) {
		return true;
	}

	@Override
	public String toString() {
		return this.description;
	}

}