package fr.unicaen.aasuite.constraints;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// DataStructures classes
// BioJava classes
import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;
// strings.constraints classes (not imported because of name clash, but required)
// import fr.greyc.mad.strings.constraints.SubsequenceConstraint;import fr.greyc.mad.datastructures.UnionOfIntervals;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
// PepTraq classes
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A class for representing constraints on sequences of compounds which require
 * a given (nonempty) subsequence to occur.
 * <p>
 * The constraint is enforced in a case-insensitive manner. A wildcard can be
 * used, which matches any compound. Finally, the sequence can be searched for
 * as it is given (modulo the wildcard), referred to as the "ordered" version,
 * or all permutations of it can be matched, referred to as the "unordered"
 * version.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SubsequenceConstraint<C extends Compound> implements Constraint<C> {

	// Implementation note: this constraint is NOT implemented using a regular
	// expression corresponding to the subsequence because of the technicalities
	// arising from the wildcard and from the unordered version.
	
	// TODO: using regular expressions might be more efficient.

	/**
	 * The description of this constraint when it is ordered. The description may
	 * use 1$ for a description of the subsequence.
	 */
	public static String orderedDescription = "Occurrence of subsequence %1$s";

	/**
	 * The description of this constraint when it is unordered. The description may
	 * use 1$ for a description of the subsequence.
	 */
	public static String unorderedDescription = "Occurrence of subsequence %1$s or a permutation of it";

	/**
	 * The description of the wildcard, to be used in the description of the
	 * subsequence.
	 */
	public static String wildcardDescription = ".";

	/** The sequence to search for (null encodes the wildcard). */
	protected final List<C> subsequence;

	/**
	 * Whether the subsequence (true) or any permutation of it must be matched.
	 */
	protected final boolean ordered;

	/**
	 * The trace of the subsequence: number of occurrences of each compound (null
	 * for ordered search).
	 */
	private Map<C, Integer> trace;

	/**
	 * Builds a new instance.
	 * 
	 * @param subsequence The subsequence required to occur
	 * @param ordered     true if the subsequence is required to occur in order,
	 *                    false if permutations are matched
	 * @throws IllegalArgumentException if the subsequence is empty
	 */
	public SubsequenceConstraint(List<C> subsequence, boolean ordered) throws IllegalArgumentException {
		if (subsequence.isEmpty()) {
			throw new IllegalArgumentException("Subsequence is empty");
		}
		this.subsequence = subsequence;
		this.ordered = ordered;
		if (!this.ordered) {
			this.trace = this.getTrace(subsequence);
		}
	}

	/**
	 * Returns the subsequence required to occur (null represents the wildcard).
	 * 
	 * @return The subsequence required to occur
	 */
	public List<C> getSubsequence() {
		return this.subsequence;
	}

	/**
	 * Returns true if this constraint is ordered, false if it is unordered.
	 * 
	 * @return true if this constraint is ordered, false if it is unordered
	 */
	public boolean ordered() {
		return this.ordered;
	}

	/**
	 * Returns true if this permutations of the subsequence are matched, false if
	 * only the subsequence itself is matched. Alias for the negation of
	 * {@link #ordered()}.
	 * 
	 * @return true if this permutations of the subsequence are matched, false if
	 *         only the subsequence itself is matched
	 */
	public boolean includePermutations() {
		return !this.ordered;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.SUBSEQUENCE;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses) {
		int length = sequence.getLength();
		int subLength = this.subsequence.size();
		boolean res = false;
		for (int i = 1; i <= length - subLength + 1; i++) {
			if (this.ordered && this.match(sequence, i, this.subsequence)) {
				witnesses.add(new Interval(i - 1, i + subLength - 1));
				res = true;
			} else if (!this.ordered && this.matches(sequence, i, i + this.subsequence.size() - 1, this.trace)) {
				witnesses.add(new Interval(i - 1, i + subLength - 1));
				res = true;
			}
		}
		return res;
	}

	@Override
	public String toString() {
		StringBuffer substring = new StringBuffer();
		for (C compound : this.subsequence) {
			substring.append(compound == null ? wildcardDescription : compound.getShortName());
		}
		return String.format(this.ordered ? orderedDescription : unorderedDescription, substring.toString());
	}

	/**
	 * Helper method: decides whether a sequence matches a subsequence (in the given
	 * order) starting at a given index.
	 * 
	 * @param sequence    A sequence
	 * @param begin       An index in the sequence (1-indexed)
	 * @param subsequence A subsequence, possibly containing occurrences of null
	 *                    (representing the wildcard)
	 * @return true if the sequence matches the subsequence at the given index,
	 *         false otherwise
	 */
	private boolean match(Sequence<C> sequence, int begin, List<C> subsequence) {
		for (int i = 0; i < subsequence.size(); i++) {
			if (subsequence.get(i) != null && !sequence.getCompoundAt(begin + i).equals(subsequence.get(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Helper method: returns the trace of a sequence, that is, the number of
	 * occurrences of each compound (including null).
	 * 
	 * @param sequence A sequence
	 * @return The trace of the given sequence
	 */
	private Map<C, Integer> getTrace(Iterable<C> sequence) {
		Map<C, Integer> res = new HashMap<>();
		for (C compound : sequence) {
			res.put(compound, res.getOrDefault(compound, 0) + 1);
		}
		return res;
	}

	/**
	 * Helper method: decides whether a subsequence of a sequence matches exactly a
	 * given trace (where occurrences of null in the trace can be matched to any
	 * compound).
	 * 
	 * @param sequence A sequence
	 * @param begin    The index defining where in the sequence the subsequence
	 *                 starts (1-indexed, inclusive)
	 * @param end      The index defining where in the sequence the subsequence ends
	 *                 (1-indexed, inclusive)
	 * @param trace    The trace to be matched
	 * @return true if the subsequence matches the trace, false otherwise
	 */
	private boolean matches(Sequence<C> sequence, int begin, int end, Map<C, Integer> trace) {
		Map<C, Integer> sequenceTrace = new HashMap<>();
		for (int i = begin; i <= end; i++) {
			C compound = sequence.getCompoundAt(i);
			int alreadySeen = sequenceTrace.getOrDefault(compound, 0);
			int max = trace.getOrDefault(compound, 0);
			if (alreadySeen < max) {
				sequenceTrace.put(compound, alreadySeen + 1);
			} else {
				alreadySeen = sequenceTrace.getOrDefault(null, 0);
				max = trace.getOrDefault(null, 0);
				if (alreadySeen < max) {
					sequenceTrace.put(null, alreadySeen + 1);
				} else {
					return false;
				}
			}
		}
		return true;
	}

}
