package fr.unicaen.aasuite.constraints;

import java.util.ArrayList;
import java.util.Collection;

// DataStructures classes
// BioJava classes
import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
// PepTraq classes
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceView;

/**
 * A class for representing constraints on sequences of AminoAcidCompound's
 * which require a given (nonempty) subsequence to occur at a given interval of positions
 * relative to N-terminus of C-terminus. Allowed subsequences are as in
 * {@link SubsequenceConstraint}, including wildcards and the possibility of
 * unordered matches.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class PositionedSubsequenceConstraint<C extends Compound> implements Constraint<C> {

	// N-terminus is the left extremity (beginning) of the string representing the
	// sequence, and C-terminus is its right extremity (end).

	/**
	 * The description for an instance with no constraint on the position. The
	 * description may use 1$ for the description of the subsequence constraint.
	 */
	public static String unpositionedDescription = "%1$s";

	/**
	 * The description for an instance with a lower bound on the position wrt
	 * N-terminus. The description may use 1$ for the description of the subsequence constraint and 2$ for the
	 * lower bound.
	 */
	public static String nterAfterDescription = "Subsequence %1$s starting at least %2$s position(s) from N-terminus";

	/**
	 * The description for an instance with an upper bound on the position wrt
	 * N-terminus. The description may use 1$ for the description of the subsequence constraint and 2$ for the
	 * upper bound.
	 */
	public static String nterBeforeDescription = "%1$s starting at most %2$s position(s) from N-terminus";

	/**
	 * The description for an instance with a lower and an upper bound on the
	 * position wrt N-terminus. The description may use 1$ for the description of the subsequence constraint, 2$
	 * for the lower bound, and 3$ for the upper bound.
	 */
	public static String nterDescription = "%1$s starting at least %2$s and at most %3$s position(s) from N-terminus";

	/**
	 * The description for an instance with a lower bound on the position wrt
	 * C-terminus. The description may use 1$ for the description of the subsequence constraint and 2$ for the
	 * lower bound.
	 */
	public static String cterAfterDescription = "%1$s ending at least %2$s position(s) from C-terminus";

	/**
	 * The description for an instance with an upper bound on the position wrt
	 * C-terminus. The description may use 1$ for the description of the subsequence constraint and 2$ for the
	 * upper bound.
	 */
	public static String cterBeforeDescription = "%1$s ending at most %2$s position(s) from C-terminus";

	/**
	 * The description for an instance with a lower and an upper bound on the
	 * position wrt C-terminus. The description may use 1$ for the description of the subsequence constraint, 2$
	 * for the lower bound, and 3$ for the upper bound.
	 */
	public static String cterDescription = "%1$s ending at least %2$s and at most %3$s position(s) from C-terminus";

	/**
	 * A subsequence constraint for the subsequence to be matched (ignoring the
	 * position).
	 */
	private final SubsequenceConstraint<C> constraint;

	/**
	 * Whether to interpret the position from N-terminus (true) or from C-terminus
	 * (false).
	 */
	protected final boolean nTerminal;

	/**
	 * The minimum number of compounds between the given terminus and the
	 * subsequence (null for no minimum).
	 */
	protected final Integer minPosition;

	/**
	 * The maximum number of compounds between the given terminus and the
	 * subsequence (null for no maximum).
	 */
	protected final Integer maxPosition;

	/**
	 * Builds a new instance.
	 * 
	 * @param constraint  A subsequence constraint for the subsequence to be matched
	 *                    (ignoring the position)
	 * @param nTerminal   Whether to interpret the position from N-terminus (true)
	 *                    or from C-terminus (false)
	 * @param minPosition The minimum number of compounds between the given terminus
	 *                    and the subsequence (null for no minimum)
	 * @param maxPosition The maximum number of compounds between the given terminus
	 *                    and the subsequence (null for no minimum)
	 * @throws IllegalArgumentException if the minimum position is greater than the
	 *                                  maximum
	 */
	public PositionedSubsequenceConstraint(SubsequenceConstraint<C> constraint, boolean nTerminal, Integer minPosition,
			Integer maxPosition) {
		if (minPosition != null && maxPosition != null && minPosition > maxPosition) {
			throw new IllegalArgumentException(
					"Minimum position cannot be greater than maximum: " + minPosition + ", " + maxPosition);
		}
		this.constraint = constraint;
		this.nTerminal = nTerminal;
		this.minPosition = minPosition;
		this.maxPosition = maxPosition;
	}

	/**
	 * Returns the subsequence constraint for the subsequence to be matched
	 * (ignoring the position).
	 * 
	 * @return The subsequence constraint for the subsequence to be matched
	 */
	public SubsequenceConstraint<C> getSubsequenceConstraint() {
		return this.constraint;
	}

	/**
	 * Decides whether this constraint imposes a position from N-terminus.
	 * 
	 * @return true if this constraint imposes a position from N-terminus, false
	 *         otherwise
	 */
	public boolean fromNTerminus() {
		return this.nTerminal;
	}

	/**
	 * Decides whether this constraint imposes a position from C-terminus.
	 * 
	 * @return true if this constraint imposes a position from C-terminus, false
	 *         otherwise
	 */
	public boolean fromCTerminus() {
		return !this.nTerminal;
	}

	/**
	 * Decides whether a minimum position is set.
	 * 
	 * @return true if a minimum position is set, false otherwise
	 */
	public boolean hasMinPosition() {
		return this.minPosition != null;
	}

	/**
	 * Returns the minimum position for the subsequence.
	 * 
	 * @return The minimum position for the subsequence
	 * @throws IllegalStateException if no minimum position is set
	 */
	public int getMinPosition() throws IllegalStateException {
		if (this.minPosition == null) {
			throw new IllegalStateException("Constraint has no minimal position");
		}
		return this.minPosition;
	}

	/**
	 * Decides whether a maximum position is set.
	 * 
	 * @return true if a maximum position is set, false otherwise
	 */
	public boolean hasMaxPosition() {
		return this.maxPosition != null;
	}

	/**
	 * Returns the maximum position for the subsequence.
	 * 
	 * @return The maximum position for the subsequence
	 * @throws IllegalStateException if no maximum position is set
	 */
	public int getMaxPosition() throws IllegalStateException {
		if (this.maxPosition == null) {
			throw new IllegalStateException("Constraint has no maximal position");
		}
		return this.maxPosition;
	}

	@Override
	public ConstraintType getType() {
		return this.nTerminal ? ConstraintType.POSITIONED_SUBSEQUENCE_FROM_N_TERMINAL
				: ConstraintType.POSITIONED_SUBSEQUENCE_FROM_C_TERMINAL;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence) {
		// Implementation: building a view of the sequence which matches the
		// subsequence constraint if and only if the sequence matches this constraint
		int begin = this.begin(sequence.getLength());
		int end = this.end(sequence.getLength());
		return this.constraint.satisfies(new AnnotatedSequenceView<C>(sequence, begin, end));
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses) {
		int begin = this.begin(sequence.getLength());
		int end = this.end(sequence.getLength());
		Collection<Interval> temporaryWitnesses = new ArrayList<>();
		boolean res = this.constraint.satisfies(new AnnotatedSequenceView<C>(sequence, begin, end), temporaryWitnesses);
		if (!res) {
			return false;
		} else {
			for (Interval interval : temporaryWitnesses) {
				witnesses.add(new Interval(interval.getBegin() + begin - 1, interval.getEnd() + begin - 1));
			}
		}
		return true;
	}

	@Override
	public String toString() {
		if (this.minPosition == null && this.maxPosition == null) {
			return String.format(unpositionedDescription, this.constraint);
		} else if (this.nTerminal) {
			if (this.maxPosition == null) {
				return String.format(nterAfterDescription, this.constraint, this.minPosition);
			} else if (this.minPosition == null) {
				return String.format(nterBeforeDescription, this.constraint, this.maxPosition);
			} else {
				return String.format(nterDescription, this.constraint, this.minPosition, this.maxPosition);
			}
		} else {
			if (this.maxPosition == null) {
				return String.format(cterBeforeDescription, this.constraint, this.minPosition);
			} else if (this.minPosition == null) {
				return String.format(cterAfterDescription, this.constraint, this.maxPosition);
			} else {
				return String.format(cterDescription, this.constraint, this.minPosition, this.maxPosition);
			}
		}
	}

	/**
	 * Helper method: returns the minimal position to consider in the sequence
	 * (1-indexed) depending on reference (N-terminus or C-terminus) and minimum,
	 * maximum positions; the method uses the attributes of this class, so it must
	 * be called only after these are initialized.
	 * 
	 * @param subsequenceLength The length of the subsequence to match
	 * @return The minimal position to consider in the sequence (1-indexed)
	 */
	private int begin(int subsequenceLength) {
		if (this.nTerminal) {
			return this.minPosition == null ? 1 : Math.min(1 + this.minPosition, subsequenceLength);
		} else {
			return this.maxPosition == null ? 1
					: Math.max(1, subsequenceLength - this.maxPosition - this.constraint.getSubsequence().size() + 1);
		}
	}

	/**
	 * Helper method: returns the maximal position to consider in the sequence
	 * (1-indexed) depending on reference (N-terminus or C-terminus) and minimum,
	 * maximum positions; the method uses the attributes of this class, so it must
	 * be called only after these are initialized.
	 * 
	 * @param subsequenceLength The length of the subsequence to match
	 * @return The maximal position to consider in the sequence (1-indexed)
	 */
	private int end(int subsequenceLength) {
		if (this.nTerminal) {
			return this.maxPosition == null ? subsequenceLength
					: Math.min(this.maxPosition + this.constraint.getSubsequence().size(), subsequenceLength);
		} else {
			return this.minPosition == null ? subsequenceLength : Math.max(1, subsequenceLength - this.minPosition);
		}
	}

}
