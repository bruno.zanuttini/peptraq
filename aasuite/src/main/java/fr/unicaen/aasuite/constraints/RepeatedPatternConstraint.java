package fr.unicaen.aasuite.constraints;

import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.datastructures.Interval;
import fr.unicaen.aasuite.operations.Constraint;
import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A class for representing constraints on sequences of AminoAcidCompound's
 * which require a given regular expression to be matched by a certain number of
 * subsequences (within some interval).
 * <p>
 * The matching subsequences must not overlap with each other for the sequence
 * to satisfy the constraint. Additionally, only matches which do not include other matches
 * are considered to be witnesses of satisfaction.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class RepeatedPatternConstraint<C extends Compound> implements Constraint<C> {

	// Implementation note: this constraint is NOT implemented using the regular
	// expression (pattern){min, max} because we need to retrieve the witnesses
	// (occurrences of the pattern).

	// TODO: maybe it would be more efficient to try to match the regular expression
	// (pattern){min,max} and only when it is matched, fire a second search for the
	// witnesses.

	/**
	 * The description for an instance with no bound on the number of occurrences.
	 * The description may use 1$ for the pattern.
	 */
	public static String noBoundDescription = "Sequence containing %1$s";

	/**
	 * The description for an instance with only a lower bound on the number of
	 * occurrences. The description may use 1$ for the pattern and 2$ for the lower
	 * bound.
	 */
	public static String lowerBoundDescription = "Sequence containing %1$s at least %2$s time(s)";

	/**
	 * The description for an instance with only an upper bound on the number of
	 * occurrences. The description may use 1$ for the pattern and 2$ for the upper
	 * bound.
	 */
	public static String upperBoundDescription = "Sequence containing %1$s at most %2$s time(s)";

	/**
	 * The description for a general instance. The description may use 1$ for the
	 * pattern, 2$ for the lower bound, and 3$ for the upper bound.
	 */
	public static String description = "Sequence containing %1$s at least %2$s and at most %3$s time(s)";

	/** The pattern to be matched for a sequence to satisfy this constraint. */
	protected final Pattern pattern;

	/** The minimum number of repetitions required (null if none). */
	protected final Integer minNbOccurrences;

	/** The maximum number of repetitions required (null if none). */
	protected final Integer maxNbOccurrences;

	/**
	 * Builds a new instance.
	 * 
	 * @param pattern          The pattern to be matched a number of times for a
	 *                         sequence to satisfy this constraint
	 * @param minNbOccurrences The minimum number of repetitions required (null
	 *                         means no minimum)
	 * @param maxNbOccurrences The maximum number of repetitions required (null
	 *                         means no maximum)
	 * @throws IllegalArgumentException if the minimum number of occurrences is
	 *                                  greater than the maximum
	 */
	public RepeatedPatternConstraint(Pattern pattern, Integer minNbOccurrences, Integer maxNbOccurrences)
			throws IllegalArgumentException {
		if (minNbOccurrences != null && maxNbOccurrences != null && maxNbOccurrences < minNbOccurrences) {
			throw new IllegalArgumentException("Minimum number of repetitions cannot be greater than maximum: "
					+ minNbOccurrences + ", " + maxNbOccurrences);
		}
		this.pattern = pattern;
		this.minNbOccurrences = minNbOccurrences;
		this.maxNbOccurrences = maxNbOccurrences;
	}

	/**
	 * Returns the pattern to be matched.
	 * 
	 * @return The pattern to be matched
	 */
	public Pattern getPattern() {
		return this.pattern;
	}

	/**
	 * Decides whether a minimum number of occurrences of the pattern is set.
	 * 
	 * @return true if a minimum number of occurrences of the pattern is set, false
	 *         otherwise
	 */
	public boolean hasMinNbOccurrences() {
		return this.minNbOccurrences != null;
	}

	/**
	 * Returns the minimum number of occurrences of the pattern.
	 * 
	 * @return The minimum number of occurrences of the pattern
	 * @throws IllegalStateException if no minimum number is set
	 */
	public int getMinNbOccurrences() throws IllegalStateException {
		if (this.minNbOccurrences == null) {
			throw new IllegalStateException("Constraint has no minimal number of occurrences");
		}
		return this.minNbOccurrences;
	}

	/**
	 * Decides whether a maximum number of occurrences of the pattern is set.
	 * 
	 * @return true if a maximum number of occurrences of the pattern is set, false
	 *         otherwise
	 */
	public boolean hasMaxNbOccurrences() {
		return this.maxNbOccurrences != null;
	}

	/**
	 * Returns the maximum number of occurrences of the pattern.
	 * 
	 * @return The maximum number of occurrences of the pattern
	 * @throws IllegalStateException if no maximum number is set
	 */
	public int getMaxNbOccurrences() throws IllegalStateException {
		if (this.maxNbOccurrences == null) {
			throw new IllegalStateException("Constraint has no maximal number of occurrences");
		}
		return this.maxNbOccurrences;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.REGEXP;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence, Collection<Interval> witnesses) {
		Matcher matcher = this.pattern.matcher(sequence.getSequenceAsString());
		matcher.useAnchoringBounds(false);
		Collection<Interval> tmpWitnesses = new ArrayList<>();
		int nbOccurrences = 0;
		int currentStart = 0;
		boolean goOn = true;
		while (goOn) {
			boolean found = false;
			for (int currentEnd = currentStart + 1; currentEnd <= sequence.getLength(); currentEnd++) {
				matcher.region(currentStart, currentEnd);
				if (matcher.find()) {
					found = true;
					int start = matcher.start();
					int end = matcher.end();
					for (currentStart++; currentStart < currentEnd; currentStart++) {
						matcher.region(currentStart, currentEnd);
						if (matcher.find()) {
							start = matcher.start();
						} else {
							// preserves completeness since if find returns false in
							// [start, end[, it cannot return false in [start + k, end[
							// (this is true even if the subsequence is anchored with ^ and/or $,
							// since the matcher does not use anchoring bounds)
							break;
						}
					}
					tmpWitnesses.add(new Interval(start, end));
					currentStart = currentEnd + 1;
					break;
				}
			}
			if (found) {
				nbOccurrences++;
				if (this.maxNbOccurrences != null && nbOccurrences > this.maxNbOccurrences) {
					return false;
				}
			} else {
				goOn = false;
			}
		}
		// Note: checking upper bound again in case no match has been found and the
		// bound is negative
		if ((this.minNbOccurrences == null || nbOccurrences >= this.minNbOccurrences)
				&& (this.maxNbOccurrences == null || nbOccurrences <= this.maxNbOccurrences)) {
			witnesses.addAll(tmpWitnesses);
			return true;
		} else {
			return false;
		}
	}

	@Override
	public String toString() {
		if (this.minNbOccurrences == null && this.maxNbOccurrences == null) {
			return String.format(noBoundDescription, pattern.pattern());
		} else if (this.minNbOccurrences == null) {
			return String.format(upperBoundDescription, this.pattern.pattern(), this.maxNbOccurrences);
		} else if (this.maxNbOccurrences == null) {
			return String.format(lowerBoundDescription, this.pattern.pattern(), this.minNbOccurrences);
		} else {
			return String.format(description, this.pattern.pattern(), this.minNbOccurrences, this.maxNbOccurrences);
		}
	}

}
