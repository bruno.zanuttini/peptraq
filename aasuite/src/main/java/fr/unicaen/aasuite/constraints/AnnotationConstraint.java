package fr.unicaen.aasuite.constraints;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.unicaen.aasuite.operations.ConstraintType;
import fr.unicaen.aasuite.operations.NoWitnessConstraint;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A constraint on the annotation of sequences. Allowed constraints are regular
 * expressions required to occur in the annotation; matching is
 * case-insensitive.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class AnnotationConstraint<C extends Compound> extends NoWitnessConstraint<C> {

	/**
	 * The description for an instance. The description can use 1$ to refer to the
	 * regexp required to occur.
	 */
	public static String description = "Annotation containing %1$s";

	/**
	 * The regexp to be matched by some substring of the annotation for the sequence
	 * to satisfy this constraint.
	 */
	protected final String regexp;

	/**
	 * The pattern to be matched by the annotation for the sequence to satisfy this
	 * constraint.
	 */
	private final Pattern pattern;

	/**
	 * Builds a new instance.
	 * 
	 * @param regexp The pattern to be matched by some substring of the annotation
	 *               for the sequence to satisfy this constraint
	 */
	public AnnotationConstraint(String regexp) {
		super();
		this.regexp = regexp;
		Pattern pattern = Pattern.compile(".*" + regexp + ".*", Pattern.CASE_INSENSITIVE);
		this.pattern = pattern;
	}

	/**
	 * Returns the pattern to be matched by some substring of the annotation for the
	 * sequence to satisfy this constraint
	 * 
	 * @return The pattern to be matched by some substring of the annotation for the
	 *         sequence to satisfy this constraint
	 */
	public String getRegexp() {
		return this.regexp;
	}

	/**
	 * Returns the pattern to be matched for this constraint to be satisfied.
	 * 
	 * @return The pattern to be matched for this constraint to be satisfied
	 */
	public Pattern getPattern() {
		return this.pattern;
	}

	@Override
	public ConstraintType getType() {
		return ConstraintType.ANNOTATION;
	}

	@Override
	public boolean satisfies(AnnotatedSequence<C> sequence) {
		Matcher matcher = this.pattern.matcher(sequence.getAnnotation());
		return matcher.matches();
	}

	@Override
	public String toString() {
		return String.format(description, this.regexp);
	}

}
