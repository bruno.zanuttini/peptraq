package fr.unicaen.aasuite.fasta;

import java.util.HashMap;
import java.util.Map;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A class for writing sequences of compounds (and their annotations) to files.
 * <p>
 * The class can be asked to write "cropped" annotations, that is, annotations
 * cropped to their first word (as determined by white spaces) instead of the
 * annotations themselves. An instance of this class makes sure that each such
 * cropped annotation is unique across all sequences it is required to write, by
 * appending a number to the cropped annotation (strating from 1).
 * <p>
 * Note that when the class is not required to crop annotations, it does NOT
 * check that each annotations is unique across all sequences it is required to
 * write.
 * <p>
 * Finally, an instance can be parameterized by a line width: this is used for
 * wrapping the sequences themselves; annotations are always written on a single
 * line.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SequenceWriter {

	/** The character introducing fasta annotations. */
	public static final char ANNOTATION_CHARACTER = '>';

	/**
	 * A string inserted between a cropped annotation and its number (used only if
	 * annotations are cropped).
	 */
	public String numberSeparator = "_";

	/**
	 * The string to use in place of the cropped annotation if it is empty (used
	 * only if annotations are cropped).
	 */
	public String defaultAnnotation = "unannotated_sequence";

	/** The default line width for sequences. */
	public static int DEFAULT_LINE_WIDTH = 80;

	/**
	 * A cache of the number of occurrences of each cropped annotation (null if the
	 * class is not required to crop the annotations).
	 */
	// Implementation note: whether this cache is null or not is used as a memory of
	// whether the annotations should be preserved or cropped, respectively.
	private final Map<String, Integer> croppedAnnotationOccurrences;

	/** The line width of this instance. */
	protected final int lineWidth;

	/**
	 * Builds a new instance.
	 * 
	 * @param cropAnnotations Whether to crop the annotations (true) or not (false)
	 * @param lineWidth       The line width for this instance
	 */
	public SequenceWriter(boolean cropAnnotations, int lineWidth) {
		this.lineWidth = lineWidth;
		this.croppedAnnotationOccurrences = cropAnnotations ? new HashMap<>() : null;
	}

	/**
	 * Builds a new instance with the default line width.
	 * 
	 * @param cropAnnotations Whether to crop the annotations (true) or not (false)
	 */
	public SequenceWriter(boolean cropAnnotations) {
		this(cropAnnotations, DEFAULT_LINE_WIDTH);
	}

	/**
	 * Returns a multi-line string in fasta format representing a given sequence and
	 * its annotation.
	 * 
	 * @param sequence A sequence to write
	 * @return A string representing the given sequence in fasta format
	 */
	public String sequenceToLines(AnnotatedSequence<?> sequence) {
		String res = "";
		String newline = System.lineSeparator();
		String annotation = this.getFinalAnnotation(sequence.getAnnotation());
		res += ANNOTATION_CHARACTER + annotation + newline;
		String toBeWritten = sequence.getSequenceAsString();
		int end = 0;
		for (int i = 0; i < toBeWritten.length(); i += this.lineWidth) {
			end = Math.min(i + this.lineWidth, toBeWritten.length());
			res += toBeWritten.substring(i, end) + newline;
		}
		return res;
	}

	/**
	 * Helper method: returns the annotation to write for a sequence, given the
	 * parameters of this instance.
	 * 
	 * @param annotation The initial annotation of the sequence
	 * @return The annotation to write for the given sequence, given the parameters
	 *         of this instance.
	 */
	public String getFinalAnnotation(String annotation) {
		if (this.croppedAnnotationOccurrences == null) {
			return annotation;
		} else {
			String[] parts = annotation.split("\\s");
			String res = "";
			for (int i = 0; i < parts.length; i++) {
				if (!parts[i].isEmpty()) {
					res = parts[i];
					break;
				}
			}
			if (res.isEmpty()) {
				res = defaultAnnotation;
			}
			int nbOccurrences = this.croppedAnnotationOccurrences.getOrDefault(res, 0);
			int newNbOccurrences = nbOccurrences + 1;
			this.croppedAnnotationOccurrences.put(res, newNbOccurrences);
			return res + numberSeparator + newNbOccurrences;
		}
	}

}
