package fr.unicaen.aasuite.fasta;

import java.io.InputStream;
import java.io.IOException;

/**
 * An adapter for input streams which pretends to be exhausted after each
 * sequence (i.e., sequence name + sequence, in fasta format). Precisely, read()
 * returns -1 when a sequence has been read. A call to {@link #nextSequence}
 * makes reading of the next sequence ready. The method
 * {@link #oneSequenceExhausted} tells whether there are no more sequences to
 * read.
 * <p>
 * Due to its specific behavior (the stream can be "over" without being "really
 * over", the normal {@link #close()} operation does nothing; really closing the
 * stream (and hence the underlying one) can be done using
 * {@link #reallyClose()}
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SequenceInputStream extends InputStream {

	/**
	 * The character introducing fasta annotations, on reading which a new sequence
	 * is assumed to start.
	 */
	public static final char ANNOTATION_CHARACTER = '>';

	/** Whether a sequence has been exhausted. */
	private boolean oneSequenceExhausted;

	/** Whether the whole stream has been exhausted. */
	private boolean isReallyOver;

	/**
	 * A buffered byte (to be returned by next call to {@link #read}, or -1 if none.
	 */
	private int bufferedByte;

	/** The string read since the last call to nextSequence(). */
	private StringBuffer lastString;

	/** The adapted input stream. */
	protected final InputStream stream;

	/**
	 * Builds a new instance.
	 * 
	 * @param stream The input stream to adapt
	 * @throws IOException if a read error occurs in the adapted stream
	 */
	public SequenceInputStream(InputStream stream) throws IOException {
		this.oneSequenceExhausted = false;
		this.isReallyOver = false;
		this.bufferedByte = -1;
		this.lastString = new StringBuffer();
		this.stream = stream;
		int oneByte = this.stream.read();
		while (oneByte != -1 && Character.isWhitespace(oneByte)) {
			oneByte = this.stream.read();
		}
		if (oneByte == -1) {
			this.isReallyOver = true;
			this.oneSequenceExhausted = true;
		} else
			this.bufferedByte = oneByte;
	}

	/**
	 * Reads the next byte from this stream.
	 * 
	 * @return The next byte from this stream, or -1 is a sequence has been
	 *         exhausted
	 * @throws IOException if reading from the adapted stream fails
	 */
	@Override
	public int read() throws IOException {
		if (this.oneSequenceExhausted) {
			return -1;
		} else if (this.bufferedByte != -1) {
			int res = this.bufferedByte;
			this.bufferedByte = -1;
			this.lastString.append((char) res);
			return res;
		} else {
			int oneByte = this.stream.read();
			if (oneByte == -1) {
				this.isReallyOver = true;
				this.oneSequenceExhausted = true;
				return -1;
			} else if ((char) oneByte == SequenceInputStream.ANNOTATION_CHARACTER) {
				this.bufferedByte = oneByte;
				this.oneSequenceExhausted = true;
				return -1;
			} else {
				this.lastString.append((char) oneByte);
				return oneByte;
			}
		}
	}

	/**
	 * Decides whether the whole stream (i.e., all sequences) has been read.
	 * 
	 * @return true if the whole stream has been read, false otherwise
	 */
	public boolean isOver() {
		return this.isReallyOver;
	}

	/**
	 * Prepares the stream to read the next sequence (i.e., the next call to
	 * {@link #read} will not return -1).
	 * 
	 * @throws IllegalStateException if the whole stream has been read (i.e.,
	 *                               {@link #isReallyOver} returns true), or a
	 *                               sequence is being read (i.e., {@link #read}
	 *                               returns -1).
	 */
	public void nextSequence() throws IllegalStateException {
		if (!this.oneSequenceExhausted) {
			throw new IllegalStateException("A sequence is being read");
		} else if (this.isReallyOver) {
			throw new IllegalStateException("There is no next sequence");
		} else {
			this.oneSequenceExhausted = false;
			this.lastString = new StringBuffer();
		}
	}

	/**
	 * Returns the string read since the last call to {@link #nextSequence}.
	 * 
	 * @return The string read since the last call to {@link #nextSequence}
	 */
	public String getLastStringRead() {
		return this.lastString.toString();
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Does nothing: prevents closing after reading a sequence.
	 */
	@Override
	public void close() {
	}

	/**
	 * Really closes this stream, by forwarding the call to the adapted stream.
	 * 
	 * @throws IOException if an I/O exception occurs
	 */
	public void reallyClose() throws IOException {
		this.stream.close();
	}

}
