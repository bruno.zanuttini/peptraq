package fr.unicaen.aasuite.fasta;

import java.io.IOException;
import java.io.InputStream;
// Standard Java classes
import java.util.Collection;
import java.util.NoSuchElementException;

import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.io.FastaReaderHelper;

/**
 * An interface for readers able to read sequences of compounds from input
 * streams.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 * @param <S> The type of sequences read
 */
public interface SequenceReader<S> {

	/**
	 * Builds and returns a reader for DNA sequences.
	 * 
	 * @return A reader for DNA sequences
	 */
	public static SequenceReader<DNASequence> forDNA() {
		return stream -> FastaReaderHelper.readFastaDNASequence(stream).values();
	}

	/**
	 * Builds and returns a reader for protein sequences.
	 * 
	 * @return A reader for protein sequences
	 */
	public static SequenceReader<ProteinSequence> forProteins() {
		return stream -> FastaReaderHelper.readFastaProteinSequence(stream).values();
	}

	/**
	 * Returns the sequence represented by a given input stream. The stream must be
	 * in fasta format and include an annotation first. If several sequences are
	 * represented in the stream, only the first one is returned.
	 * 
	 * @param stream A stream where to read a sequence
	 * @return The first sequence represented in the stream
	 * @throws NoSuchElementException If the stream does not start with a sequence
	 * @throws IOException            If a parse error occurs
	 */
	default S getSequence(SequenceInputStream stream) throws NoSuchElementException, IOException {
		return this.getAllSequences(stream).iterator().next();
	}

	/**
	 * Returns the sequences represented by a given input stream. The stream must be
	 * in fasta format and start with an annotation.
	 * 
	 * @param stream A stream where to read sequences
	 * @return The collection of sequences represented in the stream
	 * @throws IOException If a parse error occurs
	 */
	Collection<S> getAllSequences(InputStream stream) throws IOException;

}
