/**
 * A package for reading/writing in fasta format. 
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.fasta;