/**
 * Classes and interfaces for representing (annotated) sequences.
 * 
 * TODO: wouldn't Sequence from BioJava be enough (using the "description"
 * field)?
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.sequences;
