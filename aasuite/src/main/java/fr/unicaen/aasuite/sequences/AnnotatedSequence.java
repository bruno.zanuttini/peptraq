package fr.unicaen.aasuite.sequences;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

/**
 * An interface for sequences with an annotation. Such sequences may have an
 * annotation which is different from their original header (as stored by any
 * sequence).
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 * @param <C> The type for compounds in the sequence
 */
public interface AnnotatedSequence<C extends Compound> extends Sequence<C> {

	/**
	 * Returns the annotation of this sequence.
	 * 
	 * @return the annotation of this sequence
	 */
	String getAnnotation();

	/**
	 * Returns the original header of this sequence.
	 * 
	 * @return The original header of this sequence
	 */
	String getOriginalHeader();

	/**
	 * Sets the annotation of this sequence (without changing the original header).
	 * 
	 * @param annotation A new annotation for this sequence
	 */
	void setAnnotation(String annotation);

}
