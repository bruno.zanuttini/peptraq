package fr.unicaen.aasuite.sequences;

import java.util.Iterator;
import java.util.List;

import org.biojava.nbio.core.sequence.AccessionID;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.CompoundSet;
import org.biojava.nbio.core.sequence.template.Sequence;
import org.biojava.nbio.core.sequence.template.SequenceView;

/**
 * An adapter for abstract sequences to interface {@link AnnotatedSequence}.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type for compounds in the sequence
 */
public class AnnotatedSequenceAdapter<C extends Compound> implements AnnotatedSequence<C> {

	/** The original header of this instance. */
	protected final String originalHeader;

	/** The adapted object. */
	protected final Sequence<C> sequence;

	/** The annotation of this instance. */
	private String annotation;

	/**
	 * Builds a new instance from an abstract sequence. The original header and the
	 * annotation of the instance are both defined to be the original header of the
	 * adapted sequence.
	 * 
	 * @param sequence The sequence to adapt
	 */
	public AnnotatedSequenceAdapter(AbstractSequence<C> sequence) {
		this.originalHeader = sequence.getOriginalHeader();
		this.sequence = sequence;
		this.annotation = sequence.getOriginalHeader();
	}

	/**
	 * Builds a new instance with a given annotation. The given annotation is taken
	 * to be both the original header and the annotation of the new sequence.
	 * 
	 * TODO: check the semantics of original header; shouldn't the original header
	 * of the adapted sequence be taken to be that of the sequence built?
	 * 
	 * @param sequence   The sequence to adapt
	 * @param annotation The annotation for this instance
	 */
	public AnnotatedSequenceAdapter(Sequence<C> sequence, String annotation) {
		this.originalHeader = annotation;
		this.sequence = sequence;
		this.annotation = annotation;
	}

	@Override
	public String getAnnotation() {
		return this.annotation;
	}

	@Override
	public String getOriginalHeader() {
		return this.originalHeader;
	}

	@Override
	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	@Override
	@SafeVarargs // No heap pollution possible
	public final int countCompounds(C... compounds) {
		return this.sequence.countCompounds(compounds);
	}

	@Override
	public List<C> getAsList() {
		return this.sequence.getAsList();
	}

	@Override
	public C getCompoundAt(int position) {
		return this.sequence.getCompoundAt(position);
	}

	@Override
	public CompoundSet<C> getCompoundSet() {
		return this.sequence.getCompoundSet();
	}

	@Override
	public int getIndexOf(C compound) {
		return this.sequence.getIndexOf(compound);
	}

	@Override
	public SequenceView<C> getInverse() {
		return this.sequence.getInverse();
	}

	@Override
	public int getLastIndexOf(C compound) {
		return this.sequence.getLastIndexOf(compound);
	}

	@Override
	public int getLength() {
		return this.sequence.getLength();
	}

	@Override
	public String getSequenceAsString() {
		return this.sequence.getSequenceAsString();
	}

	@Override
	public SequenceView<C> getSubSequence(Integer start, Integer end) {
		return this.sequence.getSubSequence(start, end);
	}

	@Override
	public Iterator<C> iterator() {
		return this.sequence.iterator();
	}

	@Override
	public AccessionID getAccession() {
		return this.sequence.getAccession();
	}

}
