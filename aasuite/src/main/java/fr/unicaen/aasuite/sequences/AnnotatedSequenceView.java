package fr.unicaen.aasuite.sequences;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.SequenceProxyView;

/**
 * A class for representing sequence views with an annotation.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 * 
 * @param <C> The type for compounds in the sequence
 */
public class AnnotatedSequenceView<C extends Compound> extends SequenceProxyView<C> implements AnnotatedSequence<C> {

	/** The original header for this sequence. */
	private final String originalHeader;

	/** The annotation for this sequence. */
	private String annotation;

	/**
	 * Builds a new instance.
	 * 
	 * @param sequence   The viewed sequence
	 * @param bioStart   the first viewed index (1 for first element, inclusive)
	 * @param bioEnd     the last viewed index (sequence.getLength() for last
	 *                   element, inclusive)
	 * @param annotation The annotation for this instance
	 * @throws IllegalArgumentException if the interval [bioStart,bioEnd] is not
	 *                                  included in or equal to
	 *                                  [1,sequence.getLength()] or is empty
	 */
	public AnnotatedSequenceView(AnnotatedSequence<C> sequence, int bioStart, int bioEnd, String annotation)
			throws IllegalArgumentException {
		super(sequence, bioStart, bioEnd);
		if (bioStart < 1 || bioStart > bioEnd || bioEnd > sequence.getLength()) {
			throw new IllegalArgumentException(
					"Invalid bounds: " + bioStart + "," + bioEnd + " for building a view of sequence " + sequence);
		}
		this.originalHeader = annotation;
		this.annotation = annotation;
	}

	/**
	 * Builds a new instance as a partial view of a sequence, with the annotation
	 * taken to be that of the viewed sequence.
	 * 
	 * @param sequence The viewed sequence
	 * @param bioStart the first viewed index (1 for first element, inclusive)
	 * @param bioEnd   the last viewed index (sequence.getLength() for last element,
	 *                 inclusive)
	 * @throws IllegalArgumentException if the interval [bioStart,bioEnd[ is not
	 *                                  included in or equal to
	 *                                  [1,sequence.getLength()] or is empty
	 */
	public AnnotatedSequenceView(AnnotatedSequence<C> sequence, int bioStart, int bioEnd)
			throws IllegalArgumentException {
		this(sequence, bioStart, bioEnd, sequence.getAnnotation());
	}

	@Override
	public String getAnnotation() {
		return this.annotation;
	}

	@Override
	public String getOriginalHeader() {
		return this.originalHeader;
	}

	@Override
	public void setAnnotation(String name) {
		this.annotation = name;
	}

}
