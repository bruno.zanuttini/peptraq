package fr.unicaen.aasuite.sessions;

/**
 * An enumeration of possible changes of the state of a session. Such a change
 * always corresponds to the current step of the session changing, bringing
 * precisions to what exactly changed.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public enum SessionChange {

	/**
	 * Indicates that the step changed but without the type of sequences changing.
	 */
	NEW_MODEL,

	/**
	 * Indicates that the step changed and that the new step consists of DNA
	 * sequences (and did not before).
	 */
	NOW_DNA,

	/**
	 * Indicates that the step changed and that the new step consists of protein
	 * sequences (and did not before).
	 */
	NOW_PROTEINS,

	/**
	 * Indicates that the step changed and that the new step consists of no
	 * sequence.
	 */
	NOW_EMPTY;

}