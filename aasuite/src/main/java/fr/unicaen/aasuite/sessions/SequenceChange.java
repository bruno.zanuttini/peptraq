package fr.unicaen.aasuite.sessions;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A class holding information about change for a given sequence or for all
 * sequences. The intended use is as a type for notifications.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class SequenceChange {

	/** An enumeration of possible types of sequence changes. */
	public enum SequenceChangeType {

		/** Indicates that the sequence has been selected. */
		SELECTED,

		/** Indicates that the sequence has been deselected. */
		DESELECTED,

		/** Indicates that the sequence has been renamed. */
		RENAMED,

		/** Indicates that all sequences have been selected. */
		ALL_SELECTED,

		/** Indicates that all sequences have been deselected. */
		ALL_DESELECTED;

	}

	/** The type of change for this instance. */
	protected final SequenceChange.SequenceChangeType type;

	/** The sequence which has changed (null if the change is for all sequences). */
	protected final AnnotatedSequence<?> sequence;

	/**
	 * Builds a new instance, representing a change for a single sequence.
	 * 
	 * @param type     The type of change
	 * @param sequence The sequence which has changed
	 * @throws IllegalArgumentException If the type of change is one for all
	 *                                  sequences ({@code ALL_SELECTED} or
	 *                                  {@code ALL_DESELECTED})
	 */
	public SequenceChange(SequenceChange.SequenceChangeType type, AnnotatedSequence<?> sequence)
			throws IllegalArgumentException {
		if (type.equals(SequenceChangeType.ALL_SELECTED) || type.equals(SequenceChangeType.ALL_DESELECTED)) {
			throw new IllegalArgumentException("Type " + type + " does not concern a specific sequence");
		}
		this.type = type;
		this.sequence = sequence;
	}

	/**
	 * Builds a new instance, representing a change for all sequences.
	 * 
	 * @param type The type of change
	 * @throws IllegalArgumentException If the type of change is not one for all
	 *                                  sequences ({@code ALL_SELECTED} or
	 *                                  {@code ALL_DESELECTED})
	 */
	public SequenceChange(SequenceChange.SequenceChangeType type) throws IllegalArgumentException {
		if (!type.equals(SequenceChangeType.ALL_SELECTED) && !type.equals(SequenceChangeType.ALL_DESELECTED)) {
			throw new IllegalArgumentException("Type " + type + " concerns a specific sequence");
		}
		this.type = type;
		this.sequence = null;
	}

	/**
	 * Returns the type of change for this instance.
	 * 
	 * @return The type of change for this instance
	 */
	public SequenceChange.SequenceChangeType getType() {
		return this.type;
	}

	/**
	 * Returns the sequence which has changed.
	 * 
	 * @return The sequence which has changed
	 * @throws IllegalStateException if this instance represents a change for all
	 *                               sequences ({@code ALL_SELECTED} or
	 *                               {@code ALL_DESELECTED})
	 */
	public AnnotatedSequence<?> getSequence() throws IllegalStateException {
		if (this.sequence == null) {
			throw new IllegalArgumentException("Change concerns all sequences: " + this.type);
		}
		return this.sequence;
	}

}