package fr.unicaen.aasuite.sessions;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.observable.MonitorableExecutor;
import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.operations.Operation;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * An adapter for operations and conversions on sequences, to the
 * {@link MonitorableExecutor} interface. Here an atomic operation correspond to
 * handling one sequence.
 * <p>
 * The class is abstract but provides static methods {@link #forOperation} and
 * {@link #forConversion} which build a concrete instance for given
 * operation/conversion.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences on which to operate
 * @param <D> The type of compounds in the produced sequences
 */
public abstract class OperationExecutorAdapter<C extends Compound, D extends Compound> extends AbstractExecutor<D> {

	/**
	 * The message for an exception occurring when handling a sequence. The string
	 * may use 1$ to refer to the message of the exception, and 2$ to refer to the
	 * sequence annotation (cropped).
	 */
	public static String exceptionMessage = "%1$s" + System.lineSeparator() + "Sequence: %2$s";

	/**
	 * How many characters to crop a sequence annotation to in exception messages.
	 */
	public static int sequenceSizeInExceptions = 50;

	/**
	 * Returns a monitorable executor of a given operation on a given list of
	 * sequences.
	 * 
	 * @param sequences   The list of sequences to operate on
	 * @param operation   The operation to execute
	 * @param information A map to which operation information will be added for
	 *                    each produced sequence
	 * @return A monitorable executor of the given operation on the given list of
	 *         sequences
	 * @param <C> The type of compounds in sequences handled and produced by the
	 *        operation
	 */
	public static <C extends Compound> OperationExecutorAdapter<C, C> forOperation(List<AnnotatedSequence<C>> sequences,
			final Operation<C> operation, final Map<Sequence<?>, OperationInformation> information) {
		return new OperationExecutorAdapter<C, C>(sequences, operation.toString()) {

			@Override
			protected List<AnnotatedSequence<C>> getNextSequences() throws Exception {
				this.nbSequencesHandled++;
				AnnotatedSequence<C> sequence = this.sequenceIterator.next();
				try {
					return operation.getSequences(sequence, information);
				} catch (Exception e) {
					String annotation = sequence.getAnnotation().length() > sequenceSizeInExceptions
							? sequence.getAnnotation().substring(0, sequenceSizeInExceptions) + "..."
							: sequence.getAnnotation();
					throw new Exception(String.format(exceptionMessage, e.getMessage(), annotation), e);
				}
			}

		};
	}

	/**
	 * Returns a monitorable executor of a given conversion on a given list of
	 * sequences.
	 * 
	 * @param sequences   The list of sequences to convert
	 * @param conversion  The conversion to execute
	 * @param information A map to which operation information will be added for
	 *                    each produced sequence
	 * @return A monitorable executor of the given conversion on the given list of
	 *         sequences
	 * @param <C> The type of compounds in sequences handled by the operation
	 * @param <D> The type of compounds in sequences produced by the operation
	 */
	public static <C extends Compound, D extends Compound> OperationExecutorAdapter<C, D> forConversion(
			List<AnnotatedSequence<C>> sequences, final Conversion<C, D> conversion,
			final Map<Sequence<?>, OperationInformation> information) {
		return new OperationExecutorAdapter<C, D>(sequences, conversion.toString()) {

			@Override
			protected List<AnnotatedSequence<D>> getNextSequences() {
				this.nbSequencesHandled++;
				AnnotatedSequence<C> sequence = this.sequenceIterator.next();
				try {
					return conversion.getSequences(sequence, information);
				} catch (Exception e) {
					throw new RuntimeException(
							String.format(exceptionMessage, e.getMessage(), sequence.getAnnotation()), e);
				}
			}

		};
	}

	// Implementation note: sequenceIterator and nbSequences are protected because
	// they are manipulated by methods defined in concrete subclasses

	/** An iterator on the sequences on which to operate. */
	protected final Iterator<AnnotatedSequence<C>> sequenceIterator;

	/** The total number of sequences to handled. */
	private final int nbSequences;

	/** The number of sequences handled so far. */
	protected int nbSequencesHandled;

	/**
	 * Builds a new instance.
	 * 
	 * @param sequences   The sequences on which to operate
	 * @param description A human-readable description of this instance
	 */
	protected OperationExecutorAdapter(List<AnnotatedSequence<C>> sequences, String description) {
		super(description);
		this.sequenceIterator = sequences.iterator();
		this.nbSequences = sequences.size();
		this.nbSequencesHandled = 0;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * The definition simply calls {@link AbstractExecutor#doExecute()}, as in
	 * {@link AbstractExecutor}, but removing the {@code throws Exception} clause,
	 * since no exception can be raised other than {@code IllegalStateException}.
	 */
	@Override
	public List<AnnotatedSequence<D>> execute() throws IllegalStateException {
		return super.doExecute();
	}

	@Override
	protected boolean isOver() {
		return !this.sequenceIterator.hasNext();
	}

	@Override
	protected long getNbAtomicOperationsSoFar() {
		return this.nbSequencesHandled;
	}

	@Override
	protected long getEstimatedNbAtomicOperationsRemaining() {
		return this.nbSequences - this.nbSequencesHandled;
	}

}
