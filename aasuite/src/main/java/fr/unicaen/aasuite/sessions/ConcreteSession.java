package fr.unicaen.aasuite.sessions;

import java.io.IOException;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.core.sequence.template.Compound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.observable.Observed;
import fr.unicaen.aasuite.fasta.SequenceInputStream;
import fr.unicaen.aasuite.fasta.SequenceReader;
import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.operations.Operation;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * An implementation of {@link Session} with concrete models consisting of lists
 * of sequences computed entirely at the moment when an operation is added.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class ConcreteSession extends
		AbstractSession<List<AnnotatedSequence<NucleotideCompound>>, List<AnnotatedSequence<AminoAcidCompound>>> {

	/**
	 * Builds a new instance, without a name.
	 */
	public ConcreteSession() {
		super(null);
	}

	/**
	 * Builds a new instance with a name.
	 * 
	 * @param name The name for this instance
	 */
	public ConcreteSession(String name) {
		super(name);
	}

	@Override
	public void write(Writer writer, boolean onlySelected, boolean cropAnnotations) throws IOException {
		List<? extends AnnotatedSequence<?>> sequences = onlySelected
				? (this.getModel().isDNA() ? this.selectedDNA() : this.selectedProteins())
				: (this.getModel().isDNA() ? this.getModel().dna() : this.getModel().proteins());
		WritingExecutor<?> executor = new WritingExecutor<>(sequences, writer, cropAnnotations);
		super.observe(executor);
		executor.save();
		writer.close();
	}

	// Definition of methods abstract in AbstractSession

	@Override
	protected ConcreteModel makeDnaModel(List<AnnotatedSequence<NucleotideCompound>> sequences,
			Observed<SequenceChange> sequenceChangeNotifier) {
		return ConcreteModel.fromDNA(sequences, sequenceChangeNotifier);
	}

	@Override
	protected ConcreteModel makeProteinModel(List<AnnotatedSequence<AminoAcidCompound>> sequences,
			Observed<SequenceChange> sequenceChangeNotifier) {
		return ConcreteModel.fromProteins(sequences, sequenceChangeNotifier);
	}

	@Override
	protected List<AnnotatedSequence<NucleotideCompound>> selectedDNA() throws IllegalStateException {
		return this.getModel().dna().stream().filter(this.getModel()::isSelected).collect(Collectors.toList());
	}

	@Override
	protected List<AnnotatedSequence<AminoAcidCompound>> selectedProteins() throws IllegalStateException {
		return this.getModel().proteins().stream().filter(this.getModel()::isSelected).collect(Collectors.toList());
	}

	@Override
	protected List<AnnotatedSequence<NucleotideCompound>> getDNASequences(SequenceReader<DNASequence> reader,
			SequenceInputStream stream, long streamSize, List<String> unreadable, List<String> errors)
			throws IllegalArgumentException, IOException {
		return this.getSequences(reader, stream, streamSize, unreadable, errors);
	}

	@Override
	protected List<AnnotatedSequence<AminoAcidCompound>> getProteinSequences(SequenceReader<ProteinSequence> reader,
			SequenceInputStream stream, long streamSize, List<String> unreadable, List<String> errors)
			throws IllegalArgumentException, IOException {
		return this.getSequences(reader, stream, streamSize, unreadable, errors);
	}

	@Override
	protected List<AnnotatedSequence<NucleotideCompound>> getDNASequences(Operation<NucleotideCompound> operation,
			List<AnnotatedSequence<NucleotideCompound>> sequences, Map<Sequence<?>, OperationInformation> information) {
		return this.getSequences(operation, sequences, information);
	}

	@Override
	protected List<AnnotatedSequence<AminoAcidCompound>> getProteinSequences(
			Conversion<NucleotideCompound, AminoAcidCompound> conversion,
			List<AnnotatedSequence<NucleotideCompound>> sequences, Map<Sequence<?>, OperationInformation> information) {
		OperationExecutorAdapter<NucleotideCompound, AminoAcidCompound> executor = OperationExecutorAdapter
				.forConversion(sequences, conversion, information);
		super.observe(executor);
		// Note: cannot throw IllegalStateException since the executor is created within
		// this method
		return executor.execute();
	}

	@Override
	protected List<AnnotatedSequence<AminoAcidCompound>> getProteinSequences(Operation<AminoAcidCompound> operation,
			List<AnnotatedSequence<AminoAcidCompound>> sequences, Map<Sequence<?>, OperationInformation> information) {
		return this.getSequences(operation, sequences, information);
	}

	// Helper methods

	/**
	 * Helper method: returns the sequences loaded from a given stream. This method
	 * closes the stream when loading has been completed, and notifies progress.
	 * 
	 * @param reader     A reader for sequences
	 * @param stream     The stream to load from
	 * @param streamSize The size of the stream (number of characters)
	 * @param unreadable A list of strings which will be augmented with those
	 *                   strings from which a sequence cannot be read from the
	 *                   stream
	 * @param errors     A list of strings which will be augmented with errors
	 *                   describing those strings from which a sequence cannot be
	 *                   read from the stream; errors will be added to the list in
	 *                   the same order as the corresponding sequences, so that an
	 *                   error is at the same position as the corresponding
	 *                   unreadable string
	 * @return The list of sequences read, or null if an error or cancellation
	 *         occurred
	 * @throws IllegalArgumentException if the lists of unreadable strings and of
	 *                                  errors do not have the same size
	 * @throws IOException              if an I/O error occurs (other than a
	 *                                  sequence which cannot be read, in which case
	 *                                  the operation is cancelled and the sequence
	 *                                  is stored with an error message in
	 *                                  {@code unreadable} and {@code errors})
	 * 
	 * @param <C> The type of compounds
	 */
	private <C extends Compound> List<AnnotatedSequence<C>> getSequences(
			SequenceReader<? extends AbstractSequence<C>> reader, SequenceInputStream stream, long streamSize,
			List<String> unreadable, List<String> errors) throws IllegalArgumentException, IOException {
		LoadingExecutor<C> executor = new LoadingExecutor<>(reader, stream, streamSize, unreadable, errors);
		super.observe(executor);
		// Note: cannot throw IllegalStateException since the executor is created within
		// this method
		return executor.execute();
	}

	/**
	 * Helper method: returns the sequences resulting from an operation applied to
	 * given protein sequences.
	 * 
	 * @param operation   An operation on sequences
	 * @param sequences   The sequences on which to operate
	 * @param information A map from sequences to pieces of information, completed
	 *                    by the method for each sequence produced
	 * @return The sequences resulting from applying the given operation to the
	 *         given sequences
	 * 
	 * @param <C> The type of compounds
	 */
	private <C extends Compound> List<AnnotatedSequence<C>> getSequences(Operation<C> operation,
			List<AnnotatedSequence<C>> sequences, Map<Sequence<?>, OperationInformation> information) {
		OperationExecutorAdapter<C, C> executor = OperationExecutorAdapter.forOperation(sequences, operation,
				information);
		super.observe(executor);
		// Note: cannot throw IllegalStateException since the executor is created within
		// this method
		return executor.execute();
	}

}
