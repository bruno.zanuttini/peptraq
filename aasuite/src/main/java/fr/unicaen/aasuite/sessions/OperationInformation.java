package fr.unicaen.aasuite.sessions;

import java.util.SortedSet;

import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.datastructures.Interval;

/**
 * A structure for recording information about how a sequence was obtained
 * through an operation. The information is about the parent sequence and/or the
 * witnesses of a constraint and/or the position of the sequence in its parent.
 * <p>
 * There are formally no specific semantics attached to this information,
 * however the intended use is as follows. A sequence which has been directly
 * loaded has no information attached. It a sequence has been obtained by
 * filtering a list of sequences by a constraint, then it is its own parent, and
 * witnesses of it satisfying the constraint may complete this information. If
 * it has been obtained by a transform, then the transformed sequence is its
 * parent, and the position of the produced sequence in it may complete the
 * information (the parent together with the position define what is called a
 * "context" here). Finally, if a sequence has been produced by a conversion,
 * then the only information is its parent sequence.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class OperationInformation {

	/**
	 * The unique instance with no information at all (typically, for a sequence
	 * just loaded from an input stream).
	 */
	public final static OperationInformation EMPTY_OPERATION_INFORMATION = new OperationInformation();

	/** The parent of the sequence, or null if none. */
	private final Sequence<?> parentOrNull;

	/**
	 * The set of witnesses for the constraint which produced the sequence, or null
	 * if none.
	 */
	private final SortedSet<Interval> witnessesOrNull;

	/**
	 * The indices (start and end) delimiting this sequence in its parent
	 * (0-indexed, start inclusive and end exclusive), or null if none.
	 */
	private final int[] indicesOrNull;

	/**
	 * Builds a new instance with no information at all.
	 */
	private OperationInformation() {
		this.parentOrNull = null;
		this.witnessesOrNull = null;
		this.indicesOrNull = null;
	}

	/**
	 * Builds a new instance with only a parent (no witnesses nor indices).
	 * 
	 * @param parent The parent sequence
	 */
	public OperationInformation(Sequence<?> parent) {
		this.parentOrNull = parent;
		this.witnessesOrNull = null;
		this.indicesOrNull = null;
	}

	/**
	 * Builds a new instance with a parent and a set of witnesses (referring to
	 * positions in the child sequence).
	 * 
	 * @param parent    The parent sequence
	 * @param witnesses The witnesses (0-indexed, relative to the child sequence)
	 */
	public OperationInformation(Sequence<?> parent, SortedSet<Interval> witnesses) {
		this.parentOrNull = parent;
		this.witnessesOrNull = witnesses;
		this.indicesOrNull = null;
	}

	/**
	 * Builds a new instance with a parent and start and end indices in it.
	 * 
	 * @param parent The parent sequence
	 * @param start  The index where the child sequence starts in the parent
	 *               sequence (0-indexed, inclusive)
	 * @param end    The index where the child sequence ends in the parent sequence
	 *               (0-indexed, exclusive)
	 */
	public OperationInformation(Sequence<?> parent, int start, int end) {
		this.parentOrNull = parent;
		this.witnessesOrNull = null;
		this.indicesOrNull = new int[] { start, end };
	}

	/**
	 * Decides whether there is a parent sequence registered in this instance.
	 * 
	 * @return true if there is a parent sequence registered in this instance, false
	 *         otherwise
	 */
	public boolean hasParent() {
		return this.parentOrNull != null;
	}

	/**
	 * Returns the parent sequence registered in this instance.
	 * 
	 * @return The parent sequence registered in this instance
	 * @throws IllegalStateException if there is no parent sequence registered in
	 *                               this instance
	 */
	public Sequence<?> getParent() throws IllegalStateException {
		if (this.parentOrNull == null) {
			throw new IllegalStateException("There is no information about a parent");
		}
		return this.parentOrNull;
	}

	/**
	 * Decides whether this information stores witnesses.
	 * @return true if this information stores witnesses, false otherwise
	 */
	public boolean hasWitnesses() {
		return this.witnessesOrNull != null;
	}

	/**
	 * Returns the witnesses stored in this information.
	 * @return The witnesses stored in this information
	 * @throws IllegalStateException if this information does not store witnesses
	 */
	public SortedSet<Interval> getWitnesses() throws IllegalStateException {
		if (this.witnessesOrNull == null) {
			throw new IllegalStateException("There is no information about witnesses");
		}
		return this.witnessesOrNull;
	}

	/**
	 * Decides whether this information defines a context, that is, a parent sequence
	 * and start and end positions in it.
	 * 
	 * @return true if this information defines a context, false otherwise
	 */
	public boolean definesContext() {
		return this.parentOrNull != null && this.indicesOrNull != null;
	}

	/**
	 * Returns the starting position in the parent sequence stored by this information (0-indexed, inclusive). 
	 * @return The starting position in the parent sequence stored by this information
	 * @throws IllegalStateException If this information does not define a context
	 */
	public int getStartIndex() throws IllegalStateException {
		if (!this.definesContext()) {
			throw new IllegalStateException("The information does not define a context");
		}
		return this.indicesOrNull[0];
	}

	/**
	 * Returns the ending position in the parent sequence stored by this information (0-indexed, exclusive). 
	 * @return The ending position in the parent sequence stored by this information
	 * @throws IllegalStateException If this information does not define a context
	 */
	public int getEndIndex() throws IllegalStateException {
		if (!this.definesContext()) {
			throw new IllegalStateException("The information does not define a context");
		}
		return this.indicesOrNull[1];
	}

}
