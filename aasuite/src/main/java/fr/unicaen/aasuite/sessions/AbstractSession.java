package fr.unicaen.aasuite.sessions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.biojava.nbio.core.sequence.DNASequence;
import org.biojava.nbio.core.sequence.ProteinSequence;
import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.datastructures.InteractiveSequence;
import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Observed;
import fr.greyc.mad.observable.Observer;
import fr.greyc.mad.observable.Progress;
import fr.unicaen.aasuite.fasta.SequenceInputStream;
import fr.unicaen.aasuite.fasta.SequenceReader;
import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.operations.Macro;
import fr.unicaen.aasuite.operations.Operation;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.SequenceChange.SequenceChangeType;

/**
 * An adapter for interface {@link Session}, which factors most functionalities.
 * <p>
 * The class is parameterized by the type used for representing collections of
 * sequences. Concrete classes are responsible for defining how these are
 * created, and how operations, loading, etc. are concretely performed on them.
 * They are also responsible for notifying about the progress of these
 * operations.
 * <p>
 * Note that this class is NOT thread-safe. If an operation is launched while
 * another is in progress, the behavior is undefined.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France.
 *
 * @param <D> The type for collections of DNA sequences
 * @param <P> The type for collections of protein sequences
 */
public abstract class AbstractSession<D extends Iterable<AnnotatedSequence<NucleotideCompound>>, P extends Iterable<AnnotatedSequence<AminoAcidCompound>>>
		implements Session {

	/**
	 * The description of a step consisting of loading DNA from a stream. The string
	 * may use 1$ to refer to a description of the stream.
	 */
	public static String dnaLoadingDescription = "Loading DNA from %1$s";

	/**
	 * The description of a step consisting of loading proteins from a stream. The
	 * string may use 1$ to refer to a description of the stream.
	 */
	public static String proteinLoadingDescription = "Loading proteins from %1$s";

	/** The name of this session (null if none). */
	protected String name;

	/** The steps and current position in this session. */
	private final InteractiveSequence<RichModel<D, P>> steps;

	/** A notifier for session changes. */
	private final BasicObserved<SessionChange> sessionChangeNotifier;

	/** A notifier for sequence changes. */
	private final BasicObserved<SequenceChange> sequenceChangeNotifier;

	/** A notifier for operation progress. */
	private final BasicObserved<Progress> progressNotifier;

	/**
	 * Builds a new instance.
	 * 
	 * @param nameOrNull The name for the instance (null for none)
	 */
	protected AbstractSession(String nameOrNull) {
		super();
		this.name = nameOrNull;
		this.steps = new InteractiveSequence<>();
		this.sessionChangeNotifier = new BasicObserved<>();
		this.sequenceChangeNotifier = new BasicObserved<>();
		this.progressNotifier = new BasicObserved<>();
	}

	@Override
	public final boolean hasName() {
		return this.name != null;
	}

	@Override
	public final String getName() throws IllegalStateException {
		if (this.name == null) {
			throw new IllegalStateException("Session has no name");
		}
		return this.name;
	}

	// Steps

	@Override
	public AbstractModel<D, P> getModel() {
		return this.steps.getCurrent().getModel();
	}

	@Override
	public int nbSteps() {
		return this.steps.size();
	}

	@Override
	public void to(int step) throws IndexOutOfBoundsException {
		this.steps.to(step);
	}

	@Override
	public void cancelCurrentStep() throws IllegalStateException {
		boolean wasDNA = this.getModel().isDNA();
		boolean wasProteins = this.getModel().isProteins();
		this.steps.removeCurrent();
		if (this.steps.isEmpty()) {
			this.name = null;
		}
		boolean isDNA = this.getModel().isDNA();
		boolean isProteins = this.getModel().isProteins();
		SessionChange change = (wasDNA && isDNA) || (wasProteins && isProteins) ? SessionChange.NEW_MODEL
				: !isDNA && !isProteins ? SessionChange.NOW_EMPTY
						: isDNA ? SessionChange.NOW_DNA : SessionChange.NOW_PROTEINS;
		this.sessionChangeNotifier.notify(change);
	}

	@Override
	public List<String> getStepDescriptions() {
		List<String> res = new ArrayList<>();
		for (int step = 0; step < this.steps.size(); step++) {
			res.add(this.steps.get(step).getDescription());
		}
		return res;
	}

	// Observers

	@Override
	public void addSessionChangeObserver(Observer<SessionChange> observer) {
		this.sessionChangeNotifier.addObserver(observer);
	}

	@Override
	public void removeSessionChangeObserver(Observer<SessionChange> observer) {
		this.sessionChangeNotifier.removeObserver(observer);
	}

	@Override
	public void addSequenceChangeObserver(Observer<SequenceChange> observer) {
		this.sequenceChangeNotifier.addObserver(observer);
	}

	@Override
	public void removeSequenceChangeObserver(Observer<SequenceChange> observer) {
		this.sequenceChangeNotifier.removeObserver(observer);
	}

	@Override
	public void addOperationObserver(Observer<Progress> observer) {
		this.progressNotifier.addObserver(observer);
	}

	@Override
	public void removeOperationObserver(Observer<Progress> observer) {
		this.progressNotifier.removeObserver(observer);
	}

	// Operations which change the model

	@Override
	public void addDNALoading(SequenceInputStream stream, long streamSize, String streamName, List<String> unreadable,
			List<String> errors) throws IllegalStateException, IllegalArgumentException, IOException {
		this.checkSessionStarted(false);
		D dna = this.getDNASequences(SequenceReader.forDNA(), stream, streamSize, unreadable, errors);
		if (dna != null) {
			this.name = streamName;
			String stepName = String.format(proteinLoadingDescription, streamName);
			AbstractModel<D, P> newModel = this.makeDnaModel(dna, this.sequenceChangeNotifier);
			RichModel<D, P> newRichModel = new RichModel<>(newModel, stepName, null);
			this.steps.extendFromCurrent(newRichModel);
			this.sessionChangeNotifier.notify(SessionChange.NOW_DNA);
		}
	}

	@Override
	public void addProteinLoading(SequenceInputStream stream, long streamSize, String streamName,
			List<String> unreadable, List<String> errors)
			throws IllegalStateException, IllegalArgumentException, IOException {
		this.checkSessionStarted(false);
		P proteins = this.getProteinSequences(SequenceReader.forProteins(), stream, streamSize, unreadable, errors);
		if (proteins != null) {
			this.name = streamName;
			String stepName = String.format(proteinLoadingDescription, streamName);
			AbstractModel<D, P> newModel = this.makeProteinModel(proteins, this.sequenceChangeNotifier);
			RichModel<D, P> newRichModel = new RichModel<>(newModel, stepName, null);
			this.steps.extendFromCurrent(newRichModel);
			this.sessionChangeNotifier.notify(SessionChange.NOW_PROTEINS);
		}
	}

	@Override
	public void addDNAOperation(Operation<NucleotideCompound> operation) throws IllegalStateException {
		Map<Sequence<?>, OperationInformation> information = new HashMap<>();
		D dna = this.getDNASequences(operation, this.selectedDNA(), information);
		if (dna != null) {
			String stepName = operation.toString();
			AbstractModel<D, P> newModel = this.makeDnaModel(dna, this.sequenceChangeNotifier);
			RichModel<D, P> newRichModel = new RichModel<>(newModel, stepName, information);
			this.steps.extendFromCurrent(newRichModel);
			this.sessionChangeNotifier.notify(SessionChange.NEW_MODEL);
		}
	}

	@Override
	public void addDNAToProteinConversion(Conversion<NucleotideCompound, AminoAcidCompound> conversion)
			throws IllegalStateException {
		Map<Sequence<?>, OperationInformation> information = new HashMap<>();
		P proteins = this.getProteinSequences(conversion, this.selectedDNA(), information);
		if (proteins != null) {
			String stepName = conversion.toString();
			AbstractModel<D, P> newModel = this.makeProteinModel(proteins, this.sequenceChangeNotifier);
			RichModel<D, P> newRichModel = new RichModel<>(newModel, stepName, information);
			this.steps.extendFromCurrent(newRichModel);
			this.sessionChangeNotifier.notify(SessionChange.NOW_PROTEINS);
		}
	}

	@Override
	public void addProteinOperation(Operation<AminoAcidCompound> operation) throws IllegalStateException {
		Map<Sequence<?>, OperationInformation> information = new HashMap<>();
		P proteins = this.getProteinSequences(operation, this.selectedProteins(), information);
		if (proteins != null) {
			String stepName = operation.toString();
			AbstractModel<D, P> newModel = this.makeProteinModel(proteins, this.sequenceChangeNotifier);
			RichModel<D, P> newRichModel = new RichModel<>(newModel, stepName, information);
			this.steps.extendFromCurrent(newRichModel);
			this.sessionChangeNotifier.notify(SessionChange.NEW_MODEL);
		}
	}

	@Override
	public void addMacro(Macro macro) throws IllegalStateException {
		for (Operation<NucleotideCompound> operation : macro.getDNAOperations()) {
			this.addDNAOperation(operation);
		}
		if (macro.hasDNAToProteinConversion()) {
			this.addDNAToProteinConversion(macro.getDNAToProteinConversion());
		}
		for (Operation<AminoAcidCompound> operation : macro.getProteinOperations()) {
			this.addProteinOperation(operation);
		}
	}

	@Override
	public void close() {
		this.steps.clear();
		this.name = null;
		this.sessionChangeNotifier.notify(SessionChange.NOW_EMPTY);
	}

	// Other operations

	@Override
	public void rename(AnnotatedSequence<?> sequence, String name) {
		sequence.setAnnotation(name);
		this.sequenceChangeNotifier.notify(new SequenceChange(SequenceChangeType.RENAMED, sequence));
	}

	// Implementation of HistoryProvider

	@Override
	public OperationInformation getLastOperationInformation(Sequence<?> sequence) throws IllegalArgumentException {
		return this.getOperationInformation(sequence, this.steps.getCurrentIndex());
	}

	@Override
	public OperationInformation getOperationInformation(Sequence<?> sequence, int step)
			throws IllegalArgumentException {
		Map<Sequence<?>, OperationInformation> map = this.steps.get(step).getOperationInformation();
		if (map == null) {
			return OperationInformation.EMPTY_OPERATION_INFORMATION;
		} else if (!map.containsKey(sequence)) {
			throw new IllegalArgumentException("Unknown sequence: " + sequence);
		} else {
			return map.get(sequence);
		}
	}

	@Override
	public List<OperationInformation> getHistory(Sequence<?> sequence) throws IllegalArgumentException {
		List<OperationInformation> res = new ArrayList<>();
		Sequence<?> current = sequence;
		for (int step = this.steps.getCurrentIndex(); step >= 0; step--) {
			Map<Sequence<?>, OperationInformation> map = this.steps.get(step).getOperationInformation();
			if (map == null) {
				res.add(OperationInformation.EMPTY_OPERATION_INFORMATION);
			} else if (!map.containsKey(current)) {
				throw new IllegalArgumentException("Unknown sequence: " + current);
			} else {
				OperationInformation info = map.get(current);
				res.add(info);
				current = info.getParent();
			}
		}
		return res;
	}

	// Helper methods

	/**
	 * Helper method: make so that this session observes a given process.
	 * 
	 * @param observed A process
	 */
	protected void observe(BasicObserved<Progress> observed) {
		for (Observer<Progress> observer : this.progressNotifier.getObservers()) {
			observed.addObserver(observer);
		}
	}

	/**
	 * Helper method: checks the status of this session (started or not started),
	 * and throws an exception if the status is not the expected one (otherwise does
	 * nothing).
	 * 
	 * @param started Whether it is expected that this session has started (true) or
	 *                that is has not started (false)
	 * @throws IllegalStateException If the status of this session is not as
	 *                               expected
	 */
	protected void checkSessionStarted(boolean started) throws IllegalStateException {
		final boolean nonemptySession = this.steps.size() != 0;
		if (nonemptySession != started) {
			throw new IllegalStateException("Session has " + (started ? "not" : "") + "started");
		}
	}

	// Abstract operations

	/**
	 * Builds a model of the correct class for given DNA sequences.
	 * 
	 * @param sequences              The DNA sequences
	 * @param sequenceChangeNotifier A notifier to be used by the model for all
	 *                               sequence changes
	 * @return A model containing the given sequences
	 */
	protected abstract AbstractModel<D, P> makeDnaModel(D sequences, Observed<SequenceChange> sequenceChangeNotifier);

	/**
	 * Builds a model of the correct class for given protein sequences.
	 * 
	 * @param sequences              The protein sequences
	 * @param sequenceChangeNotifier A notifier to be used by the model for all
	 *                               sequence changes
	 * @return A model containing the given sequences
	 */
	protected abstract AbstractModel<D, P> makeProteinModel(P sequences,
			Observed<SequenceChange> sequenceChangeNotifier);

	/**
	 * Returns the DNA sequences selected at the current step.
	 * 
	 * @return The DNA sequences selected at the current step
	 * @throws IllegalStateException if the session does not consist of DNA
	 *                               sequences
	 */
	protected abstract D selectedDNA() throws IllegalStateException;

	/**
	 * Returns the protein sequences selected at the current step.
	 * 
	 * @return The protein sequences selected at the current step
	 * @throws IllegalStateException if the session does not consist of protein
	 *                               sequences
	 */
	protected abstract P selectedProteins() throws IllegalStateException;

	/**
	 * Returns the DNA sequences loaded from a given stream. This method is
	 * responsible for closing the stream when loading has been completed and for
	 * notifying progress.
	 * 
	 * @param reader     A reader for DNA sequences
	 * @param stream     The stream to load from
	 * @param streamSize The size of the stream (number of characters)
	 * @param unreadable A list of strings which will be augmented with those
	 *                   strings from which a sequence cannot be read from the
	 *                   stream
	 * @param errors     A list of strings which will be augmented with errors
	 *                   describing those strings from which a sequence cannot be
	 *                   read from the stream; errors will be added to the list in
	 *                   the same order as the corresponding sequences, so that an
	 *                   error is at the same position as the corresponding
	 *                   unreadable string
	 * @return The list of sequences read, or null if an error or cancellation
	 *         occurred
	 * @throws IllegalArgumentException if the lists of unreadable strings and of
	 *                                  errors do not have the same size
	 * @throws IOException              if an I/O error occurs (other than a
	 *                                  sequence which cannot be read, in which case
	 *                                  the operation is cancelled and the sequence
	 *                                  is stored with an error message in
	 *                                  {@code unreadable} and {@code errors})
	 */
	protected abstract D getDNASequences(SequenceReader<DNASequence> reader, SequenceInputStream stream,
			long streamSize, List<String> unreadable, List<String> errors) throws IllegalArgumentException, IOException;

	/**
	 * Returns the protein sequences loaded from a given stream. This method is
	 * responsible for closing the stream when loading has been completed and for
	 * notifying progress.
	 * 
	 * @param reader     A reader for protein sequences
	 * @param stream     The stream to load from
	 * @param streamSize The size of the stream (number of characters)
	 * @param unreadable A list of strings which will be augmented with those
	 *                   strings from which a sequence cannot be read from the
	 *                   stream
	 * @param errors     A list of strings which will be augmented with errors
	 *                   describing those strings from which a sequence cannot be
	 *                   read from the stream; errors will be added to the list in
	 *                   the same order as the corresponding sequences, so that an
	 *                   error is at the same position as the corresponding
	 *                   unreadable string
	 * @return The list of sequences read, or null if an error or cancellation
	 *         occurred
	 * @throws IllegalArgumentException if the lists of unreadable strings and of
	 *                                  errors do not have the same size
	 * @throws IOException              if an I/O error occurs (other than a
	 *                                  sequence which cannot be read, in which case
	 *                                  the operation is cancelled and the sequence
	 *                                  is stored with an error message in
	 *                                  {@code unreadable} and {@code errors})
	 */
	protected abstract P getProteinSequences(SequenceReader<ProteinSequence> reader, SequenceInputStream stream,
			long streamSize, List<String> unreadable, List<String> errors) throws IllegalArgumentException, IOException;

	/**
	 * Returns the DNA sequences resulting from an operation applied to given DNA
	 * sequences.
	 * 
	 * @param operation   An operation on DNA sequences
	 * @param sequences   The DNA sequences on which to operate
	 * @param information A map from sequences to pieces of information, to be
	 *                    completed by the method for each sequence produced
	 * @return The DNA sequences resulting from applying the given operation to the
	 *         given DNA sequences
	 */
	protected abstract D getDNASequences(Operation<NucleotideCompound> operation, D sequences,
			Map<Sequence<?>, OperationInformation> information);

	/**
	 * Returns the protein sequences resulting from a conversion applied to given
	 * DNA sequences.
	 * 
	 * @param conversion  A conversion from DNA to protein sequences
	 * @param sequences   The DNA sequences on which to operate
	 * @param information A map from sequences to pieces of information, to be
	 *                    completed by the method for each sequence produced
	 * @return The protein sequences resulting from applying the given operation to
	 *         the given DNA sequences
	 */
	protected abstract P getProteinSequences(Conversion<NucleotideCompound, AminoAcidCompound> conversion, D sequences,
			Map<Sequence<?>, OperationInformation> information);

	/**
	 * Returns the protein sequences resulting from an operation applied to given
	 * protein sequences.
	 * 
	 * @param operation   An operation on protein sequences
	 * @param sequences   The protein sequences on which to operate
	 * @param information A map from sequences to pieces of information, to be
	 *                    completed by the method for each sequence produced
	 * @return The protein sequences resulting from applying the given operation to
	 *         the given protein sequences
	 */
	protected abstract P getProteinSequences(Operation<AminoAcidCompound> operation, P sequences,
			Map<Sequence<?>, OperationInformation> information) throws IllegalStateException;

}
