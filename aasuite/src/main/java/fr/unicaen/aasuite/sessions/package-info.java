/**
 * Interfaces and classes for managing sessions of operations on sequences. The
 * package provides simple implementations of sessions, whereby lists of
 * sequences are computed explicitly each time an operation is pushed.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
package fr.unicaen.aasuite.sessions;