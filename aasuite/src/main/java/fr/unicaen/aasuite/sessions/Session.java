package fr.unicaen.aasuite.sessions;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

import fr.greyc.mad.observable.Observer;
import fr.greyc.mad.observable.Progress;
import fr.unicaen.aasuite.fasta.SequenceInputStream;
import fr.unicaen.aasuite.operations.Conversion;
import fr.unicaen.aasuite.operations.Macro;
import fr.unicaen.aasuite.operations.Operation;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * An interface for sessions, consisting of operations (loading, filtering,
 * converting, transforming...) on databases of DNA and/or protein sequences. A
 * session is always at a current step but also knows its history, and can be
 * placed back to a given step. The session goes from one step to the other
 * through operations which are added to it (loading a database of DNA,
 * filtering the sequences, etc.).
 * <p>
 * At each step, a session can hold either no sequences (when no database is
 * loaded), or DNA sequences (possibly an empty collection thereof), or protein
 * sequences (idem). To distinguish these situations, we say that the current
 * step "consists of DNA sequences", "consists of protein sequences", or
 * "consists of no sequence". In particular, "consisting of no sequence" is
 * different from containing no sequence (which is the case, e.g., when an empty
 * database of DNA sequences is loaded).
 * <p>
 * A session handles observers: of its own state, of the sequences (if any) at
 * its current step, and of operation progress.
 * <p>
 * In general, implementations of this interface may or not be thread-safe: if
 * an operation is added while another one is ongoing, the behavior is undefined
 * except specified otherwise by a subclass.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public interface Session extends HistoryProvider {

	/**
	 * Decides whether this session has a name.
	 * 
	 * @return true if this session has a name, false otherwise
	 */
	boolean hasName();

	/**
	 * Returns the name of this session.
	 * 
	 * @return The name of this session
	 * @throws IllegalStateException if this session has no name
	 */
	String getName() throws IllegalStateException;

	// Steps

	/**
	 * Returns the current model of this session, that is, the model at the current
	 * step.
	 * 
	 * @return The current model of this session
	 */
	Model getModel();

	/**
	 * Returns the number of steps in this session.
	 * 
	 * @return The number of steps in this session
	 */
	int nbSteps();

	/**
	 * Moves the state of the session to a given step.
	 * 
	 * @param step A step number (0-indexed)
	 * @throws IndexOutOfBoundsException if the given step is geq the number of
	 *                                   steps of this session
	 */
	void to(int step) throws IndexOutOfBoundsException;

	/**
	 * Cancels the current step in this session. As a consequence, after this call,
	 * this session has one step less.
	 * 
	 * @throws IllegalStateException if there is no step in this session
	 */
	void cancelCurrentStep() throws IllegalStateException;

	/**
	 * Returns a human-readable description of all steps in this session, in order
	 * (description of the first step as the first element of the returned list).
	 * 
	 * @return A human-readable description of all steps in this session
	 */
	List<String> getStepDescriptions();

	// Observers

	/**
	 * Adds an observer of session changes. The observer will be notified each time
	 * the step/the model is modified. Nothing occurs if the observer was already
	 * registered, otherwise it is added at the end of the list of observers (hence
	 * will be notified last).
	 * 
	 * @param observer An observer of session changes
	 */
	void addSessionChangeObserver(Observer<SessionChange> observer);

	/**
	 * Removes an observer of session changes. Nothing occurs if the observer was
	 * not registered.
	 * 
	 * @param observer An observer of session changes
	 */
	void removeSessionChangeObserver(Observer<SessionChange> observer);

	/**
	 * Adds an observer of sequence changes. The observer will be notified each time
	 * a sequence is modified (e.g., selected, renamed...). Nothing occurs if the
	 * observer was already registered, otherwise it is added at the end of the list
	 * of observers (hence will be notified last).
	 * 
	 * @param observer An observer of sequence changes
	 */
	void addSequenceChangeObserver(Observer<SequenceChange> observer);

	/**
	 * Removes an observer of sequence changes. Nothing occurs if the observer was
	 * not registered.
	 * 
	 * @param observer An observer of sequence changes
	 */
	void removeSequenceChangeObserver(Observer<SequenceChange> observer);

	/**
	 * Adds an observer of operation progress. The observer will be notified each
	 * time an ongoing operation progresses by one step (depending on the
	 * operation). Nothing occurs if the observer was already registered, otherwise
	 * it is added at the end of the list of observers (hence will be notified
	 * last).
	 * 
	 * @param observer An observer of operation progress
	 */
	void addOperationObserver(Observer<Progress> observer);

	/**
	 * Removes an observer of operation progress. Nothing occurs if the observer was
	 * not registered.
	 * 
	 * @param observer An observer of operation progress
	 */
	void removeOperationObserver(Observer<Progress> observer);

	// Operations which change the model

	/**
	 * Adds a loading operation for DNA sequences. This method is responsible for
	 * closing the stream when loading has been completed.
	 * 
	 * @param stream     The stream to load from
	 * @param streamSize The size of the stream (number of characters)
	 * @param streamName A name for the stream
	 * @param unreadable A list of strings which will be augmented with those
	 *                   strings from which a sequence cannot be read from the
	 *                   stream
	 * @param errors     A list of strings which will be augmented with errors
	 *                   describing those strings from which a sequence cannot be
	 *                   read from the stream; errors will be added to the list in
	 *                   the same order as the corresponding sequences, so that an
	 *                   error is at the same position as the corresponding
	 *                   unreadable string
	 * @throws IllegalStateException    if the session is already started, that is,
	 *                                  already consists of sequences
	 * @throws IllegalArgumentException if the lists of unreadable strings and of
	 *                                  errors do not have the same size
	 * @throws IOException              if an I/O error occurs (other than a
	 *                                  sequence which cannot be read, in which case
	 *                                  the operation is cancelled and the sequence
	 *                                  is stored with an error message in
	 *                                  {@code unreadable} and {@code errors})
	 */
	void addDNALoading(SequenceInputStream stream, long streamSize, String streamName, List<String> unreadable,
			List<String> errors) throws IllegalStateException, IllegalArgumentException, IOException;

	/**
	 * Adds a loading operation for protein sequences. This method is responsible
	 * for closing the stream when loading has been completed.
	 * 
	 * @param stream     The stream to load from
	 * @param streamSize The size of the stream (number of characters)
	 * @param streamName A name for the stream
	 * @param unreadable A list of strings which will be augmented with those
	 *                   strings from which a sequence cannot be read from the
	 *                   stream
	 * @param errors     A list of strings which will be augmented with errors
	 *                   describing those strings from which a sequence cannot be
	 *                   read from the stream; errors will be added to the list in
	 *                   the same order as the corresponding sequences, so that an
	 *                   error is at the same position as the corresponding
	 *                   unreadable string
	 * @throws IllegalStateException    if the session is already started, that is,
	 *                                  already consists of sequences
	 * @throws IllegalArgumentException if the lists of unreadable strings and of
	 *                                  errors do not have the same size
	 * @throws IOException              if an I/O error occurs (other than a
	 *                                  sequence which cannot be read, in which case
	 *                                  the operation is cancelled and the sequence
	 *                                  is stored with an error message in
	 *                                  {@code unreadable} and {@code errors})
	 */
	void addProteinLoading(SequenceInputStream stream, long streamSize, String streamName, List<String> unreadable,
			List<String> errors) throws IllegalStateException, IllegalArgumentException, IOException;

	/**
	 * Adds an operation on DNA sequences to this session. The operation is
	 * performed on selected sequences only.
	 * 
	 * @param operation An operation on DNA sequences
	 * @throws IllegalStateException if the current step does not consist of DNA
	 *                               sequences
	 */
	void addDNAOperation(Operation<NucleotideCompound> operation) throws IllegalStateException;

	/**
	 * Adds a conversion from DNA to protein sequences to this session. The
	 * operation is performed on selected sequences only.
	 * 
	 * @param conversion A conversion from DNA to protein sequences
	 * @throws IllegalStateException if the current step does not consist of DNA
	 *                               sequences
	 */
	void addDNAToProteinConversion(Conversion<NucleotideCompound, AminoAcidCompound> conversion)
			throws IllegalStateException;

	/**
	 * Adds an operation on protein sequences to this session. The operation is
	 * performed on selected sequences only.
	 * 
	 * @param operation An operation on protein sequences
	 * @throws IllegalStateException if the current step does not consist of protein
	 *                               sequences
	 */
	void addProteinOperation(Operation<AminoAcidCompound> operation) throws IllegalStateException;

	/**
	 * Adds a macro operation to this session. The operation is performed on
	 * selected sequences only.
	 * 
	 * @param macro A macro operation
	 * @throws IllegalStateException if the current step does not consist of
	 *                               sequences to which the macro applies
	 */
	void addMacro(Macro macro) throws IllegalStateException;

	/**
	 * Closes this session, that is, unloads the current database and removes all
	 * steps. Nothing occurs if the session consists of no sequence.
	 */
	void close() throws IllegalStateException;

	// Other operations

	/**
	 * Changes the annotation of a sequence. The change is for all occurrences of
	 * the sequence in the session, at whatever step (in particular, if this
	 * sequence has been retained after filtering by a constraint, then its previous
	 * occurrence (before filtering) also has its annotation changed.
	 * <p>
	 * Note that it is NOT necessarily checked that the sequence indeed occurs in
	 * this session.
	 * 
	 * @param sequence   A sequence
	 * @param annotation The new annotation for the sequence
	 */
	void rename(AnnotatedSequence<?> sequence, String annotation);

	/**
	 * Writes the sequences of the current model. This method is responsible for
	 * closing the stream when loading has been completed.
	 * 
	 * @param writer          A writer to which to write the sequences
	 * @param onlySelected    Whether to write only the sequences currently selected
	 *                        (true) or all sequences (false)
	 * @param cropAnnotations Whether to write the sequences with the annotations
	 *                        cropped (true) or with the current annotations (false)
	 * @throws IOException if an I/O error occurs when the stream is closed
	 */
	void write(Writer writer, boolean onlySelected, boolean cropAnnotations) throws IOException;

}
