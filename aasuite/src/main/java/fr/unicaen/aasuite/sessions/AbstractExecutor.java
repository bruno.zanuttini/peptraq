package fr.unicaen.aasuite.sessions;

import java.util.ArrayList;
import java.util.List;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.observable.MonitorableExecutor;
import fr.greyc.mad.observable.Progress;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A base class for monitorable executions of operations on lists of sequences.
 * In this context, an operation can be anything which produces sequences.
 * <p>
 * This base class takes care of executing the operation step by step (where one
 * step is performed by the abstract method {@link #getNextSequences()}), and
 * notifying its observers. In addition to the execution of one step, concrete
 * subclasses must be able to answer how many atomic operations have been
 * performed, and (an estimate of) how many remain, at any moment. The notion of
 * an atomic operation here has no specific semantics, but the interpretation
 * must be consistent between the number so far and the estimate of how many
 * remain, being it intended that such atomic operations should all last
 * approximately the same time. The value of a notification is always the number
 * of atomic operations executed so far, except for notifications of type
 * {@code START} for which this is the estimated total number of atomic
 * operations to perform.
 * <p>
 * Like for monitorable executors in general, an instance of this class is
 * supposed to represent only one execution, so that {@link #execute()} can be
 * called only once.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences produced by the operation
 */
public abstract class AbstractExecutor<C extends Compound> extends MonitorableExecutor {

	/**
	 * Whether the operation has already been executed, or an execution has already
	 * started (true), or not (false).
	 */
	private Boolean alreadyExecuted;

	/** Whether cancellation has been requested (true) or not (false). */
	private boolean cancelled;

	/**
	 * Builds a new instance.
	 * 
	 * @param description A human-readable description of this instance
	 */
	public AbstractExecutor(String description) {
		super(description);
		this.alreadyExecuted = false;
		this.cancelled = false;
	}

	@Override
	public void cancel() {
		this.cancelled = true;
	}

	/**
	 * Executes this operation, notifying the observers about progress. This
	 * implementation defaults to calling {@link #execute}, but subclasses may
	 * override this behavior and also add exceptions.
	 * 
	 * @return The list of sequences produced by this execution, or null if an error
	 *         or cancellation occurred
	 * @throws IllegalStateException if this operation has already been executed, or
	 *                               execution has already started
	 * @throws Exception             if an error occurs (actually, the default
	 *                               implementation does not throw an exception, but
	 *                               overriding definitions may)
	 */
	public List<AnnotatedSequence<C>> execute() throws IllegalStateException, Exception {
		return this.doExecute();
	}

	/**
	 * Executes this operation, notifying the observers about progress.
	 * 
	 * @return The list of sequences produced by this execution, or null if an error
	 *         or cancellation occurred
	 * @throws IllegalStateException if this operation has already been executed, or
	 *                               execution has already started
	 */
	protected final List<AnnotatedSequence<C>> doExecute() throws IllegalStateException {
		synchronized (this.alreadyExecuted) {
			if (this.alreadyExecuted) {
				throw new IllegalStateException("Operation is already being, or has already been executed");
			}
			this.alreadyExecuted = true;
		}
		List<AnnotatedSequence<C>> res = new ArrayList<>();
		super.notify(new Progress(this, Progress.ProgressType.STARTED, this.getEstimatedNbAtomicOperationsRemaining()));
		while (!this.isOver() && !this.cancelled) {
			try {
				res.addAll(this.getNextSequences());
			} catch (Exception e) {
				super.notify(new Progress(this, this.getNbAtomicOperationsSoFar(), e.getMessage()));
				return null;
			}
			super.notify(new Progress(this, Progress.ProgressType.PROGRESSED, this.getNbAtomicOperationsSoFar()));
		}
		if (this.cancelled) {
			super.notify(new Progress(this, Progress.ProgressType.CANCELLED, this.getNbAtomicOperationsSoFar()));
			return null;
		} else {
			super.notify(new Progress(this, Progress.ProgressType.FINISHED, this.getNbAtomicOperationsSoFar()));
			return res;
		}
	}

	/**
	 * Returns the next produced sequences in the execution, and updates the numbers
	 * of operations performed and remaining.
	 * 
	 * @return The next produced sequences in the execution
	 * @throws Exception if an error occurs
	 */
	protected abstract List<? extends AnnotatedSequence<C>> getNextSequences() throws Exception;

	/**
	 * Decides whether this execution has ended normally (without error nor
	 * cancellation).
	 * 
	 * @return true if this execution has ended normally, false otherwise
	 */
	protected abstract boolean isOver();

	/**
	 * Returns the number of atomic operations performed so far during this
	 * execution.
	 * 
	 * @return The number of atomic operations performed so far during this
	 *         execution
	 */
	protected abstract long getNbAtomicOperationsSoFar();

	/**
	 * Returns an estimate of how many atomic operations remain to be performed.
	 * 
	 * @return An estimate of how many atomic operations remain to be performed
	 */
	protected abstract long getEstimatedNbAtomicOperationsRemaining();

}
