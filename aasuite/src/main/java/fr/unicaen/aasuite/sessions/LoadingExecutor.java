package fr.unicaen.aasuite.sessions;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

import org.biojava.nbio.core.sequence.template.AbstractSequence;
import org.biojava.nbio.core.sequence.template.Compound;

import fr.unicaen.aasuite.fasta.SequenceInputStream;
import fr.unicaen.aasuite.fasta.SequenceReader;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sequences.AnnotatedSequenceAdapter;

/**
 * A monitorable loading operation (of sequences, from an input stream). An
 * atomic operation here is the handling of one character from the input stream.
 * <p>
 * In addition to loading, an instance of this class, when executed, fills given
 * lists of unreadable strings and of the corresponding error messages, so that
 * correct sequences can be loaded without the execution failing and at the same
 * time, incorrect sequences are made accessible.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the loaded sequences
 */
public class LoadingExecutor<C extends Compound> extends AbstractExecutor<C> {

	/** The description of instances of this class. */
	public static String description = "Loading";

	/** A reader for sequences. */
	protected SequenceReader<? extends AbstractSequence<C>> reader;

	/** The input stream to read from. */
	private SequenceInputStream stream;

	/** The size (number of characters) of the whole input stream. */
	private long streamSize;

	/** How many characters from the input stream have been handled so far. */
	private long streamSizeHandled;

	/** The list of unreadable strings. */
	protected final List<String> unreadable;

	/** The list of errors corresponding to the unreadable strings. */
	protected final List<String> errors;

	/**
	 * Builds a new instance.
	 * 
	 * @param reader     A reader for sequences
	 * @param stream     The input stream to read from
	 * @param streamSize The size (number of characters) of the whole input stream
	 * @param unreadable A list of unreadable strings, to be completed (at the end)
	 *                   by the unreadable strings found by this loading operation
	 * @param errors     A list of error messages, to be completed by those
	 *                   corresponding to the unreadable strings found by this
	 *                   loading operation; an error message in this list will
	 *                   correspond to the unreadble string at the same position in
	 *                   the list of unreadable strings
	 * @throws IllegalArgumentException if the unreadable and error lists do not
	 *                                  have the same size
	 */
	public LoadingExecutor(SequenceReader<? extends AbstractSequence<C>> reader, SequenceInputStream stream,
			long streamSize, List<String> unreadable, List<String> errors) throws IllegalArgumentException {
		super(description);
		this.reader = reader;
		this.stream = stream;
		this.streamSize = streamSize;
		this.streamSizeHandled = 0;
		if (unreadable.size() != errors.size()) {
			throw new IllegalArgumentException(
					"Lists of unreadable strings and errors should have the same length initially: " + unreadable.size()
							+ ", " + errors.size());
		}
		this.unreadable = unreadable;
		this.errors = errors;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Closes the stream after execution, returning null and notifying the observers
	 * of an error in case closing itself throws an exception.
	 * 
	 * @throws IOException if an I/O error occurs when closing the stream
	 */
	@Override
	public List<AnnotatedSequence<C>> execute() throws IllegalStateException, IOException {
		try {
			List<AnnotatedSequence<C>> res = super.doExecute();
			return res;
		} finally {
			this.stream.reallyClose();
		}
	}

	@Override
	protected List<? extends AnnotatedSequence<C>> getNextSequences() {
		AbstractSequence<C> res = null;
		try {
			res = this.reader.getSequence(stream);
			this.streamSizeHandled += stream.getLastStringRead().length();
			return Collections.singletonList(new AnnotatedSequenceAdapter<C>(res));
		} catch (IOException | NoSuchElementException exception) {
			this.unreadable.add(stream.getLastStringRead());
			this.errors.add(exception.getMessage() == null ? "" : exception.getMessage());
			return Collections.emptyList();
		} finally {
			if (!stream.isOver()) {
				stream.nextSequence();
			}
		}
	}

	@Override
	protected boolean isOver() {
		return this.stream.isOver();
	}

	@Override
	protected long getNbAtomicOperationsSoFar() {
		return this.streamSizeHandled;
	}

	@Override
	protected long getEstimatedNbAtomicOperationsRemaining() {
		return this.streamSize - this.streamSizeHandled;
	}

}
