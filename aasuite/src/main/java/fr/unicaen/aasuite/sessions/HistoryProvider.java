package fr.unicaen.aasuite.sessions;

import java.util.List;

import org.biojava.nbio.core.sequence.template.Sequence;

/**
 * An interface for classes which can provide information about the (operation)
 * history of sequences. The history of a sequence is simply a sequence of
 * operation information, where at each step the sequence is the parent of the
 * sequence in the subsequent step.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public interface HistoryProvider {

	/**
	 * Returns the information about the last operation which produced the given
	 * sequence.
	 * 
	 * @param sequence A sequence
	 * @return The information about the last operation which produced the given
	 *         sequence
	 * @throws IllegalArgumentException If there is no information about the given
	 *                                  sequence
	 */
	OperationInformation getLastOperationInformation(Sequence<?> sequence) throws IllegalArgumentException;

	/**
	 * Returns the information about the operation at a given step in the history of
	 * a given sequence.
	 * 
	 * @param sequence A sequence
	 * @param step     A step in the history of the given sequence (0-indexed)
	 * @return The information about the operation at the given step in the history
	 *         of the given sequence
	 * @throws IllegalArgumentException If there is no information about the given
	 *                                  sequence
	 */
	OperationInformation getOperationInformation(Sequence<?> sequence, int step) throws IllegalArgumentException;

	/**
	 * Returns the complete history of a sequence, as a list with the last operation
	 * at the first position, the last but one at the second position, etc.
	 * 
	 * @param sequence A sequence
	 * @return The complete history of the given sequence
	 * @throws IllegalArgumentException If there is no information about the given
	 *                                  sequence
	 */
	List<OperationInformation> getHistory(Sequence<?> sequence) throws IllegalArgumentException;

}
