package fr.unicaen.aasuite.sessions;

import java.util.Map;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A helper class for storing together a model, a description, and operation
 * information, that is, everything corresponding to a single step in a session.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <D> The type for collections of DNA sequences
 * @param <P> The type for collections of protein sequences
 */
public class RichModel<D extends Iterable<AnnotatedSequence<NucleotideCompound>>, P extends Iterable<AnnotatedSequence<AminoAcidCompound>>> {

	/** The model. */
	protected final AbstractModel<D, P> model;

	/** The description. */
	protected final String description;

	/** Operation information (intended to be about the sequences in the model). */
	protected final Map<Sequence<?>, OperationInformation> operationInformation;

	/**
	 * Builds a new instance.
	 * 
	 * @param model                The model for the instance
	 * @param description          The description for the instance
	 * @param operationInformation The operation information for the instance
	 */
	public RichModel(AbstractModel<D, P> model, String description,
			Map<Sequence<?>, OperationInformation> operationInformation) {
		this.model = model;
		this.description = description;
		this.operationInformation = operationInformation;
	}

	/**
	 * Returns the model stored in this instance.
	 * 
	 * @return The model stored in this instance
	 */
	public AbstractModel<D, P> getModel() {
		return model;
	}

	/**
	 * Returns the description of this instance.
	 * 
	 * @return The description of this instance
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Returns the operation information stored in this instance.
	 * 
	 * @return The operation information stored in this instance
	 */
	public Map<Sequence<?>, OperationInformation> getOperationInformation() {
		return operationInformation;
	}

}
