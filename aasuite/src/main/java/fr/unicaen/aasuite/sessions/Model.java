package fr.unicaen.aasuite.sessions;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * An interface for a model in a session, that is, the collection of sequences
 * at a given step, together with their selection status.
 * <p>
 * A model can hold either no sequences (when no database is loaded), or DNA
 * sequences (possibly an empty collection thereof), or protein sequences
 * (idem). To distinguish these situations, we say that the model "consists of
 * DNA sequences", "consists of protein sequences", or "consists of no
 * sequence". In particular, "consisting of no sequence" is different from
 * containing no sequence (which is the case, e.g., when an empty database of
 * DNA sequences is loaded).
 * <p>
 * A model handles observers of sequence changes.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public interface Model {

	// Access to sequences

	/**
	 * Decides whether this model consists of DNA sequences (possibly none).
	 * 
	 * @return true if this model consists of DNA sequences, false otherwise
	 */
	boolean isDNA();

	/**
	 * Decides whether this model consists of protein sequences (possibly none).
	 * 
	 * @return true if this model consists of protein sequences, false otherwise
	 */
	boolean isProteins();

	/**
	 * Returns the DNA sequences of this model.
	 * 
	 * @return the DNA sequences of this model
	 * @throws IllegalStateException if this model does not consist of DNA sequences
	 */
	Iterable<AnnotatedSequence<NucleotideCompound>> dna() throws IllegalStateException;

	/**
	 * Returns the protein sequences of this model.
	 * 
	 * @return the protein sequences of this model
	 * @throws IllegalStateException if this model does not consist of protein
	 *                               sequences
	 */
	Iterable<AnnotatedSequence<AminoAcidCompound>> proteins() throws IllegalStateException;

	/**
	 * Returns the number of sequences in this model (optional operation).
	 * 
	 * @return The number of sequences in this model
	 * @throws UnsupportedOperationException if this operation is not supported
	 */
	int nbSequences() throws UnsupportedOperationException;

	// Selection

	/**
	 * Selects or unselects a given sequence at the current step.
	 * 
	 * @param sequence A sequence
	 * @param selected true for selecting the sequence, false for unselecting it
	 */
	void select(AnnotatedSequence<?> sequence, boolean selected);

	/**
	 * Selects or unselects all sequences at the current step.
	 * 
	 * @param selected true for selecting all sequences, false for unselecting all
	 *                 sequences
	 * @throws IllegalStateException if the session consists of no sequence
	 */
	void selectAllSequences(boolean selected) throws IllegalStateException;

	/**
	 * Decides whether a given sequence is selected at the current step.
	 * 
	 * @param sequence A sequence
	 * @return true if the given sequence is selected at the current step, false
	 *         otherwise
	 */
	boolean isSelected(Sequence<?> sequence);

	/**
	 * Returns the number of sequences selected at the current step (optional
	 * operation).
	 * 
	 * @return The number of sequences selected at the current step
	 * @throws UnsupportedOperationException if the operation is not supported
	 */
	int nbSelected() throws UnsupportedOperationException;

}
