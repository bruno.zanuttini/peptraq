package fr.unicaen.aasuite.sessions;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;
import org.biojava.nbio.core.sequence.template.Sequence;

import fr.greyc.mad.datastructures.Selection;
import fr.greyc.mad.observable.BasicObserved;
import fr.greyc.mad.observable.Observed;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;
import fr.unicaen.aasuite.sessions.SequenceChange.SequenceChangeType;

/**
 * An adapter for interface {@link Model}, which provides most functionalities
 * required by the interface.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <D> The type of sequence collection for DNA sequences
 * @param <P> The type of sequence collection for protein sequences
 */
public abstract class AbstractModel<D extends Iterable<AnnotatedSequence<NucleotideCompound>>, P extends Iterable<AnnotatedSequence<AminoAcidCompound>>>
		extends BasicObserved<SessionChange> implements Model {

	/**
	 * The DNA sequences in this model, or null if it does not consist of DNA
	 * sequences.
	 */
	private final D dna;

	/**
	 * The protein sequences in this model, or null if it does not consist of
	 * protein sequences.
	 */
	private final P proteins;

	/** The current selection of sequences. */
	private final Selection<Sequence<?>> selection;

	/** A notifier for all sequence changes. */
	protected final Observed<SequenceChange> sequenceChangeNotifier;

	/**
	 * Builds a new instance.
	 * 
	 * @param dna                    A collection of DNA sequences
	 * @param proteins               A collection of protein sequences
	 * @param sequenceChangeNotifier A notifier for all sequence changes
	 * @throws IllegalArgumentException if bot {@code dna} and {@code proteins} are
	 *                                  nonnull
	 */
	protected AbstractModel(D dna, P proteins, Observed<SequenceChange> sequenceChangeNotifier)
			throws IllegalArgumentException {
		if (dna != null && proteins != null) {
			throw new IllegalArgumentException("Both DNA and proteins are nonnull");
		}
		this.dna = dna;
		this.proteins = proteins;
		this.selection = new Selection<>(true);
		this.sequenceChangeNotifier = sequenceChangeNotifier;
	}

	/**
	 * Builds a new instance, with no sequence at all.
	 * 
	 * @param sequenceChangeNotifier A notifier for all sequence changes
	 */
	public AbstractModel(Observed<SequenceChange> sequenceChangeNotifier) {
		this(null, null, sequenceChangeNotifier);
	}

	// Access to sequences

	@Override
	public boolean isDNA() {
		return this.dna != null;
	}

	@Override
	public boolean isProteins() {
		return this.proteins != null;
	}

	@Override
	public D dna() throws IllegalStateException {
		if (this.dna == null) {
			throw new IllegalStateException("Model does not consist of DNA sequences");
		} else {
			return this.dna;
		}
	}

	@Override
	public P proteins() throws IllegalStateException {
		if (this.proteins == null) {
			throw new IllegalStateException("Model does not consist of protein sequences");
		} else {
			return this.proteins;
		}
	}

	// Selection

	@Override
	public void select(AnnotatedSequence<?> sequence, boolean selected) {
		this.selection.select(sequence, selected);
		SequenceChangeType type = selected ? SequenceChangeType.SELECTED : SequenceChangeType.DESELECTED;
		this.sequenceChangeNotifier.notify(new SequenceChange(type, sequence));
	}

	@Override
	public void selectAllSequences(boolean select) throws IllegalStateException {
		this.selection.selectAll(select);
		SequenceChangeType type = select ? SequenceChangeType.ALL_SELECTED : SequenceChangeType.ALL_DESELECTED;
		this.sequenceChangeNotifier.notify(new SequenceChange(type));
	}

	@Override
	public boolean isSelected(Sequence<?> sequence) {
		return this.selection.isSelected(sequence);
	}

	@Override
	public int nbSelected() throws UnsupportedOperationException {
		// Implementation note: exception may be thrown by this.nbSequences()
		return this.selection.nbSelected(this.nbSequences());
	}

}
