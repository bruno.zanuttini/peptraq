package fr.unicaen.aasuite.sessions;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import org.biojava.nbio.core.sequence.template.Compound;

import fr.greyc.mad.observable.MonitorableExecutor;
import fr.greyc.mad.observable.Progress;
import fr.unicaen.aasuite.fasta.SequenceWriter;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A monitorable writing operation (of sequences, to a writer). An atomic
 * operation here is the handling of one sequence to write.
 * 
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 *
 * @param <C> The type of compounds in the sequences to write
 */
public class WritingExecutor<C extends Compound> extends MonitorableExecutor {

	/** The description of instances of this class. */
	public static String description = "Saving";

	/** The sequences to write. */
	protected final Collection<? extends AnnotatedSequence<?>> sequences;

	/** The writer to which the sequences should be written. */
	protected final Writer writer;

	/**
	 * Whether to crop the annotations (true) or not (false) (see
	 * {@link SequenceWriter}).
	 */
	protected final boolean cropAnnotations;

	/**
	 * Whether the operation has already been executed, or an execution has already
	 * started (true), or not (false).
	 */
	private Boolean alreadyExecuted;

	/** Whether cancellation has been requested (true) or not (false). */
	private boolean cancelled;

	/**
	 * Builds a new instance.
	 * 
	 * @param sequences       The sequences to write
	 * @param writer          The writer to which the sequences should be written
	 * @param cropAnnotations Whether to crop the annotations (true) or not (false)
	 *                        (see {@link SequenceWriter})
	 */
	public WritingExecutor(Collection<? extends AnnotatedSequence<?>> sequences, Writer writer,
			boolean cropAnnotations) {
		super(description);
		this.sequences = sequences;
		this.writer = writer;
		this.cropAnnotations = cropAnnotations;
		this.cancelled = false;
		this.alreadyExecuted = false;
	}

	/**
	 * Writes the sequences and notifies the observers as needed. The writer is NOT
	 * closed by this method.
	 * 
	 * @throws IllegalArgumentException if this writing has already been executed,
	 *                                  or an execution has already started
	 */
	public void save() {
		synchronized (this.alreadyExecuted) {
			if (this.alreadyExecuted) {
				throw new IllegalStateException("Saving is already being, or has already been executed");
			}
			this.alreadyExecuted = true;
		}
		SequenceWriter sequenceWriter = new SequenceWriter(this.cropAnnotations);
		super.notify(new Progress(this, Progress.ProgressType.STARTED, this.sequences.size()));
		long nbSequencesHandled = 0;
		for (AnnotatedSequence<?> sequence : this.sequences) {
			if (this.cancelled) {
				break;
			} else {
				try {
					this.writer.write(sequenceWriter.sequenceToLines(sequence));
				} catch (IOException e) {
					super.notify(new Progress(this, nbSequencesHandled, e.getMessage()));
					return;
				}
				nbSequencesHandled++;
				super.notify(new Progress(this, Progress.ProgressType.PROGRESSED, nbSequencesHandled));
			}
		}
		if (this.cancelled) {
			super.notify(new Progress(this, Progress.ProgressType.CANCELLED, nbSequencesHandled));
		} else {
			super.notify(new Progress(this, Progress.ProgressType.FINISHED, nbSequencesHandled));
		}
	}

	@Override
	public void cancel() {
		this.cancelled = true;

	}

}
