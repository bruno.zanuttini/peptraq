package fr.unicaen.aasuite.sessions;

import java.util.List;

import org.biojava.nbio.core.sequence.compound.AminoAcidCompound;
import org.biojava.nbio.core.sequence.compound.NucleotideCompound;

import fr.greyc.mad.observable.Observed;
import fr.unicaen.aasuite.sequences.AnnotatedSequence;

/**
 * A basic implementation of interface {@link Model}, in which sequences are
 * stored in explicit lists.
 *
 * @author Bruno Zanuttini, Université de Caen Normandie, France
 */
public class ConcreteModel
		extends AbstractModel<List<AnnotatedSequence<NucleotideCompound>>, List<AnnotatedSequence<AminoAcidCompound>>> {

	/**
	 * Builds a new instance, with no sequence at all.
	 * 
	 * @param sequenceChangeNotifier A notifier for all sequence changes
	 */
	public ConcreteModel(Observed<SequenceChange> sequenceChangeNotifier) {
		super(sequenceChangeNotifier);
	}

	/**
	 * Builds a new instance.
	 * 
	 * @param dna                    A collection of DNA sequences
	 * @param proteins               A collection of protein sequences
	 * @param sequenceChangeNotifier A notifier for all sequence changes
	 * @throws IllegalArgumentException if both {@code dna} and {@code proteins} are
	 *                                  nonnull
	 */
	protected ConcreteModel(List<AnnotatedSequence<NucleotideCompound>> dna,
			List<AnnotatedSequence<AminoAcidCompound>> proteins, Observed<SequenceChange> sequenceChangeNotifier)
			throws IllegalArgumentException {
		super(dna, proteins, sequenceChangeNotifier);
	}

	/**
	 * Builds and returns a new instance, with DNA sequences.
	 * 
	 * @param dna                    The DNA sequences for the instance
	 * @param sequenceChangeNotifier A notifier for all sequence changes in the
	 *                               instance
	 * @return A new instance with the given DNA sequences
	 */
	public static ConcreteModel fromDNA(List<AnnotatedSequence<NucleotideCompound>> dna,
			Observed<SequenceChange> sequenceChangeNotifier) {
		return new ConcreteModel(dna, null, sequenceChangeNotifier);
	}

	/**
	 * Builds and returns a new instance, with protein sequences.
	 * 
	 * @param proteins               The protein sequences for the instance
	 * @param sequenceChangeNotifier A notifier for all sequence changes in the
	 *                               instance
	 * @return A new instance with the given protein sequences
	 */
	public static ConcreteModel fromProteins(List<AnnotatedSequence<AminoAcidCompound>> proteins,
			Observed<SequenceChange> sequenceChangeNotifier) {
		return new ConcreteModel(null, proteins, sequenceChangeNotifier);
	}

	@Override
	public int nbSequences() throws UnsupportedOperationException {
		return super.isDNA() ? super.dna().size() : super.isProteins() ? super.proteins().size() : 0;
	}

}
